import React, { useMemo } from "react";
import objectPath from "object-path";

import { useHtmlClassService } from "../../_core/MetronicLayout";

import { QuickUserToggler } from "../extras/QuiclUserToggler";
// import { QuickActionsDropdown } from "../extras/dropdowns/QuickActionsDropdown";

export function TopbarDJP() {
  const uiService = useHtmlClassService();

  const layoutProps = useMemo(() => {
    return {
    
      viewUserDisplay: objectPath.get(uiService.config, "extras.user.display"),
      viewQuickActionsDisplay: objectPath.get(
          uiService.config,
          "extras.quick-actions.display"
      ),
    };
  }, [uiService]);

  return (
    <div className="topbar">

      {/*{layoutProps.viewQuickActionsDisplay && <QuickActionsDropdown />}*/}

      {layoutProps.viewUserDisplay && <QuickUserToggler />}

    </div>
  );
}
