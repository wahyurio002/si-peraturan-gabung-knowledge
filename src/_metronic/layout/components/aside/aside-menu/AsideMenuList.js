/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from "react";
import { useLocation } from "react-router";
import { NavLink } from "react-router-dom";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl, checkIsActive } from "../../../../_helpers";
import axios from "axios";
import { MenuOpenOutlined } from "@material-ui/icons";

export function AsideMenuList({ layoutProps }) {
  const location = useLocation();
  const getMenuItemActive = (url, hasSubmenu = false) => {
    return checkIsActive(location, url)
      ? ` ${!hasSubmenu &&
          "menu-item-active"} menu-item-open menu-item-not-hightlighted`
      : "";
  };

  const { REACT_APP_MENU_URL } = process.env;
  const [menu, setMenu] = useState([]);

  const getMenu = async () => {
    try {
      const data = {
        role: ["99", "8"],
        route: "dashboard",
      };
      const response = await axios.post(`${REACT_APP_MENU_URL}`, data);
      return response;
    } catch (err) {
      if (err.response) {
        console.log(err.response);
      } else {
        console.log(err);
      }
    }
  };

  useEffect(() => {
    // getMenu()
    // .then(({data: {data}})=> {
    //   setMenu(data.menu);
    //   // console.log(data.menu);
    //   Object.values(data.menu).map((data)=>{
    //     console.log(data[1]);
    //   })
    // })
  }, []);

  // const showMenu = () => {
  //    menu.map((data, index)=>{
  //       console.log(index);
  //   })
  // }

  return (
    <>
      <ul className={`menu-nav ${layoutProps.ulClasses}`}>
        {/* {showMenu()} */}
        {/* Dashboard Pages */}
        <li
          className={`menu-item ${getMenuItemActive("/dashboard", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/dashboard">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
            </span>
            <span className="menu-text">Dashboard</span>
          </NavLink>
        </li>
        {/* End of Dashboard Pages */}

        {/* Evaluation Pages */}
        <li
          className={`menu-item menu-item-submenu ${getMenuItemActive(
            "/evaluation",
            true
          )}`}
          aria-haspopup="true"
          data-menu-toggle="hover"
        >
          <NavLink className="menu-link menu-toggle" to="/evaluation">
            <span className="svg-icon menu-icon">
              <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/Communication/Clipboard-list.svg"
                )}
              />
            </span>
            <span className="menu-text">Evaluasi</span>
            <i className="menu-arrow" />
          </NavLink>
          <div className="menu-submenu ">
            <i className="menu-arrow" />
            <ul className="menu-subnav">
              <li className="menu-item  menu-item-parent" aria-haspopup="true">
                <span className="menu-link">
                  <span className="menu-text">Evaluasi</span>
                </span>
              </li>

              <li
                className={`menu-item ${getMenuItemActive(
                  "/evaluation/proposal"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/evaluation/proposal"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Usulan Asosiasi / Unit</span>
                </NavLink>
              </li>
              {/* <li
                className={`menu-item ${getMenuItemActive("/evaluation/unit")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link menu-toggle" to="/evaluation/unit">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Usulan Evaluasi</span>
                </NavLink>
              </li> */}
              <li
                className={`menu-item ${getMenuItemActive(
                  "/evaluation/research"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/evaluation/research"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Penelitian Usulan</span>
                </NavLink>
              </li>
              <li
                className={`menu-item ${getMenuItemActive(
                  "/evaluation/validate"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/evaluation/validate"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Validasi Evaluasi</span>
                </NavLink>
              </li>
              <li
                className={`menu-item ${getMenuItemActive(
                  "/evaluation/revalidate"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/evaluation/revalidate"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Penelitian Validasi</span>
                </NavLink>
              </li>
              <li
                className={`menu-item ${getMenuItemActive(
                  "/evaluation/monitoring"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/evaluation/monitoring"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Monitoring Evaluasi</span>
                </NavLink>
              </li>
            </ul>
          </div>
        </li>
        {/* End of Evaluasi Pages */}
        {/* Planning Pages */}
        <li
          className={`menu-item menu-item-submenu ${getMenuItemActive(
            "/plan",
            true
          )}`}
          aria-haspopup="true"
          data-menu-toggle="hover"
        >
          <NavLink className="menu-link menu-toggle" to="/plan">
            <span className="svg-icon menu-icon">
              <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/Communication/Clipboard-check.svg"
                )}
              />
            </span>
            <span className="menu-text">Perencanaan</span>
            <i className="menu-arrow" />
          </NavLink>
          <div className="menu-submenu ">
            <i className="menu-arrow" />
            <ul className="menu-subnav">
              <li className="menu-item  menu-item-parent" aria-haspopup="true">
                <span className="menu-link">
                  <span className="menu-text">Perencanaan</span>
                </span>
              </li>

              <li
                className={`menu-item ${getMenuItemActive("/plan/proposal")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link menu-toggle" to="/plan/proposal">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Usulan Perencanaan</span>
                </NavLink>
              </li>
              <li
                className={`menu-item ${getMenuItemActive("/plan/research")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link menu-toggle" to="/plan/research">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Penelitian Perencanaan</span>
                </NavLink>
              </li>
              <li
                className={`menu-item ${getMenuItemActive("/plan/monitoring")}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/plan/monitoring"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Monitoring Perencanaan</span>
                </NavLink>
              </li>
            </ul>
          </div>
        </li>
        {/* End of Planning Pages */}
        {/* Composing Pages */}
        <li
          className={`menu-item menu-item-submenu ${getMenuItemActive(
            "/compose",
            true
          )}`}
          aria-haspopup="true"
          data-menu-toggle="hover"
        >
          <NavLink className="menu-link menu-toggle" to="/compose">
            <span className="svg-icon menu-icon">
              <SVG
                src={toAbsoluteUrl("/media/svg/icons/Files/Selected-file.svg")}
              />
            </span>
            <span className="menu-text">Penyusunan</span>
            <i className="menu-arrow" />
          </NavLink>
          <div className="menu-submenu ">
            <i className="menu-arrow" />
            <ul className="menu-subnav">
              <li className="menu-item  menu-item-parent" aria-haspopup="true">
                <span className="menu-link">
                  <span className="menu-text">Penyusunan</span>
                </span>
              </li>
              <li
                className={`menu-item ${getMenuItemActive(
                  "/compose/proposal"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/compose/proposal"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Usulan Penyusunan</span>
                </NavLink>
              </li>
              <li
                className={`menu-item ${getMenuItemActive(
                  "/compose/research"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/compose/research"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Penelitian Draft Usulan</span>
                </NavLink>
              </li>
              <li
                className={`menu-item ${getMenuItemActive("/compose/process")}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/compose/process"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Proses Penyusunan</span>
                </NavLink>
              </li>
              <li
                className={`menu-item ${getMenuItemActive(
                  "/compose/reprocess"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/compose/reprocess"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Penelitian Penyusunan</span>
                </NavLink>
              </li>
              <li
                className={`menu-item ${getMenuItemActive("/compose/bkf")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link menu-toggle" to="/compose/bkf">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">
                    Inisiasi Unit Eselon 1 Kemenkeu
                  </span>
                </NavLink>
              </li>
            </ul>
          </div>
        </li>
        {/* End of Composing Pages */}

        {/* KM Pages */}
        <li
          className={`menu-item menu-item-submenu ${getMenuItemActive(
            "/knowledge",
            true
          )}`}
          aria-haspopup="true"
          data-menu-toggle="hover"
        >
          <NavLink className="menu-link menu-toggle" to="/knowledge">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Home/Book-open.svg")} />
            </span>
            <span className="menu-text">Knowledge Management</span>
            <i className="menu-arrow" />
          </NavLink>
          <div className="menu-submenu ">
            <i className="menu-arrow" />
            <ul className="menu-subnav">
              <li className="menu-item  menu-item-parent" aria-haspopup="true">
                <span className="menu-link">
                  <span className="menu-text">Knowledge Management</span>
                </span>
              </li>

               {/* Regulasi Pages */}

               <li
                className={`menu-item menu-item-submenu ${getMenuItemActive(
                  "/regulasi",
                  true
                )}`}
                aria-haspopup="true"
                data-menu-toggle="hover"
              >
                <NavLink className="menu-link menu-toggle" to="/regulasi">
                  <span className="svg-icon menu-icon">
                    <SVG
                      src={toAbsoluteUrl("/media/svg/icons/Home/Book.svg")}
                    />
                  </span>
                  <span className="menu-text">Regulasi Perpajakan</span>
                  <i className="menu-arrow" />
                </NavLink>
                <div className="menu-submenu ">
                  <i className="menu-arrow" />
                  <ul className="menu-subnav">
                    <li
                      className="menu-item  menu-item-parent"
                      aria-haspopup="true"
                    >
                      <span className="menu-link">
                        <span className="menu-text">Regulasi Perpajakan</span>
                      </span>
                    </li>

                    <li
                      className={`menu-item ${getMenuItemActive(
                        "/knowledge/regulasi/usulan"
                      )}`}
                      aria-haspopup="true"
                    >
                      <NavLink
                        className="menu-link menu-toggle"
                        to="/knowledge/regulasi/usulan"
                      >
                        <i className="menu-bullet menu-bullet-dot">
                          <span />
                        </i>
                        <span className="menu-text">
                          Usulan Regulasi Perpajakan
                        </span>
                      </NavLink>
                    </li>

                    <li
                      className={`menu-item ${getMenuItemActive(
                        "/knowledge/regulasi/penelitian"
                      )}`}
                      aria-haspopup="true"
                    >
                      <NavLink
                        className="menu-link menu-toggle"
                        to="/knowledge/regulasi/penelitian"
                      >
                        <i className="menu-bullet menu-bullet-dot">
                          <span />
                        </i>
                        <span className="menu-text">
                          Penelitian Regulasi Perpajakan
                        </span>
                      </NavLink>
                    </li>

                    <li
                      className={`menu-item ${getMenuItemActive(
                        "/knowledge/regulasi/monitoring"
                      )}`}
                      aria-haspopup="true"
                    >
                      <NavLink
                        className="menu-link menu-toggle"
                        to="/knowledge/regulasi/monitoring"
                      >
                        <i className="menu-bullet menu-bullet-dot">
                          <span />
                        </i>
                        <span className="menu-text">
                          Monitoring Regulasi Perpajakan
                        </span>
                      </NavLink>
                    </li>
                  </ul>
                </div>
              </li>

        {/* Penegasan */}
        <li
          className={`menu-item menu-item-submenu ${getMenuItemActive(
            "/penegasan",
            true
          )}`}
          aria-haspopup="true"
          data-menu-toggle="hover"
        >
          <NavLink className="menu-link menu-toggle" to="/penegasan">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Home/Book.svg")} />
            </span>
            <span className="menu-text">Surat Penegasan</span>
            <i className="menu-arrow" />
          </NavLink>
          <div className="menu-submenu ">
            <i className="menu-arrow" />
            <ul className="menu-subnav">
              <li className="menu-item  menu-item-parent" aria-haspopup="true">
                <span className="menu-link">
                  <span className="menu-text">Surat Penegasan</span>
                </span>
              </li>

              <li
                className={`menu-item ${getMenuItemActive(
                  "/knowledge/penegasan/usulan"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/knowledge/penegasan/usulan"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Usulan Surat Penegasan</span>
                </NavLink>
              </li>

              <li
                className={`menu-item ${getMenuItemActive(
                  "/knowledge/penegasan/penelitian"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/knowledge/penegasan/penelitian"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Penelitian Surat Penegasan</span>
                </NavLink>
              </li>

              <li
                className={`menu-item ${getMenuItemActive(
                  "/knowledge/penegasan/monitoring"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/knowledge/penegasan/monitoring"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Monitoring Surat Penegasan</span>
                </NavLink>
              </li>
              </ul>
              </div>
              </li>

              {/* Kajian Pages */}

              <li
                className={`menu-item menu-item-submenu ${getMenuItemActive(
                  "/kajian",
                  true
                )}`}
                aria-haspopup="true"
                data-menu-toggle="hover"
              >
                <NavLink className="menu-link menu-toggle" to="/kajian">
                  <span className="svg-icon menu-icon">
                    <SVG
                      src={toAbsoluteUrl("/media/svg/icons/Home/Book.svg")}
                    />
                  </span>
                  <span className="menu-text">Hasil Kajian RIA dan CBA</span>
                  <i className="menu-arrow" />
                </NavLink>
                <div className="menu-submenu ">
                  <i className="menu-arrow" />
                  <ul className="menu-subnav">
                    <li
                      className="menu-item  menu-item-parent"
                      aria-haspopup="true"
                    >
                      <span className="menu-link">
                        <span className="menu-text">
                          Hasil Kajian RIA dan CBA
                        </span>
                      </span>
                    </li>

                    <li
                      className={`menu-item ${getMenuItemActive(
                        "/knowledge/kajian/usulan"
                      )}`}
                      aria-haspopup="true"
                    >
                      <NavLink
                        className="menu-link menu-toggle"
                        to="/knowledge/kajian/usulan"
                      >
                        <i className="menu-bullet menu-bullet-dot">
                          <span />
                        </i>
                        <span className="menu-text">
                          Usulan Hasil Kajian RIA dan CBA
                        </span>
                      </NavLink>
                    </li>

                    <li
                      className={`menu-item ${getMenuItemActive(
                        "/knowledge/kajian/penelitian"
                      )}`}
                      aria-haspopup="true"
                    >
                      <NavLink
                        className="menu-link menu-toggle"
                        to="/knowledge/kajian/penelitian"
                      >
                        <i className="menu-bullet menu-bullet-dot">
                          <span />
                        </i>
                        <span className="menu-text">
                          Penelitian Hasil Kajian RIA dan CBA
                        </span>
                      </NavLink>
                    </li>

                    <li
                      className={`menu-item ${getMenuItemActive(
                        "/knowledge/kajian/monitoring"
                      )}`}
                      aria-haspopup="true"
                    >
                      <NavLink
                        className="menu-link menu-toggle"
                        to="/knowledge/kajian/monitoring"
                      >
                        <i className="menu-bullet menu-bullet-dot">
                          <span />
                        </i>
                        <span className="menu-text">
                          Monitoring Hasil Kajian RIA dan CBA
                        </span>
                      </NavLink>
                    </li>
                  </ul>
                </div>
              </li>

              {/* Putusan */}
            <li
              className={`menu-item menu-item-submenu ${getMenuItemActive(
                "/putusan",
                true
              )}`}
              aria-haspopup="true"
              data-menu-toggle="hover"
            >
            <NavLink className="menu-link menu-toggle" to="/putusan">
              <span className="svg-icon menu-icon">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Home/Book.svg")} />
              </span>
              <span className="menu-text">Putusan Pengadilan</span>
              <i className="menu-arrow" />
            </NavLink>
            <div className="menu-submenu ">
              <i className="menu-arrow" />
              <ul className="menu-subnav">
                <li className="menu-item  menu-item-parent" aria-haspopup="true">
                  <span className="menu-link">
                    <span className="menu-text">Putusan Pengadilan</span>
                  </span>
                </li>

              <li
                className={`menu-item ${getMenuItemActive(
                  "/knowledge/putusan/usulan"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/knowledge/putusan/usulan"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Usulan Putusan Pengadilan</span>
                </NavLink>
              </li>

              <li
                className={`menu-item ${getMenuItemActive(
                  "/knowledge/putusan/penelitian"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/knowledge/putusan/penelitian"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Penelitian Putusan Pengadilan</span>
                </NavLink>
              </li>

              <li
                className={`menu-item ${getMenuItemActive(
                  "/knowledge/putusan/monitoring"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/knowledge/putusan/monitoring"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Monitoring Putusan Pengadilan</span>
                </NavLink>
              </li>
              </ul>
              </div>
              </li>

              {/* Kodefikasi Pages */}

              <li
                className={`menu-item menu-item-submenu ${getMenuItemActive(
                  "/kodefikasi",
                  true
                )}`}
                aria-haspopup="true"
                data-menu-toggle="hover"
              >
                <NavLink className="menu-link menu-toggle" to="/kodefikasi">
                  <span className="svg-icon menu-icon">
                    <SVG
                      src={toAbsoluteUrl("/media/svg/icons/Home/Book.svg")}
                    />
                  </span>
                  <span className="menu-text">Kodefikasi</span>
                  <i className="menu-arrow" />
                </NavLink>
                <div className="menu-submenu ">
                  <i className="menu-arrow" />
                  <ul className="menu-subnav">
                    <li
                      className="menu-item  menu-item-parent"
                      aria-haspopup="true"
                    >
                      <span className="menu-link">
                        <span className="menu-text">
                          Kodefikasi
                        </span>
                      </span>
                    </li>

                    <li
                      className={`menu-item ${getMenuItemActive(
                        "/knowledge/kodefikasi/usulan"
                      )}`}
                      aria-haspopup="true"
                    >
                      <NavLink
                        className="menu-link menu-toggle"
                        to="/knowledge/kodefikasi/usulan"
                      >
                        <i className="menu-bullet menu-bullet-dot">
                          <span />
                        </i>
                        <span className="menu-text">
                          Usulan Kodefikasi
                        </span>
                      </NavLink>
                    </li>

                    <li
                      className={`menu-item ${getMenuItemActive(
                        "/knowledge/kodefikasi/penelitian"
                      )}`}
                      aria-haspopup="true"
                    >
                      <NavLink
                        className="menu-link menu-toggle"
                        to="/knowledge/kodefikasi/penelitian"
                      >
                        <i className="menu-bullet menu-bullet-dot">
                          <span />
                        </i>
                        <span className="menu-text">
                          Penelitian Kodefikasi
                        </span>
                      </NavLink>
                    </li>

                    <li
                      className={`menu-item ${getMenuItemActive(
                        "/knowledge/kodefikasi/monitoring"
                      )}`}
                      aria-haspopup="true"
                    >
                      <NavLink
                        className="menu-link menu-toggle"
                        to="/knowledge/kodefikasi/monitoring"
                      >
                        <i className="menu-bullet menu-bullet-dot">
                          <span />
                        </i>
                        <span className="menu-text">
                          Monitoring Kodefikasi
                        </span>
                      </NavLink>
                    </li>
                  </ul>
                </div>
              </li>
              
          {/* Rekomendasi */}
          <li
            className={`menu-item menu-item-submenu ${getMenuItemActive(
              "/rekomendasi",
              true
            )}`}
            aria-haspopup="true"
            data-menu-toggle="hover"
          >
          <NavLink className="menu-link menu-toggle" to="/rekomendasi">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Home/Book.svg")} />
            </span>
            <span className="menu-text">Rekomendasi</span>
            <i className="menu-arrow" />
          </NavLink>
          <div className="menu-submenu ">
            <i className="menu-arrow" />
            <ul className="menu-subnav">
              <li className="menu-item  menu-item-parent" aria-haspopup="true">
                <span className="menu-link">
                  <span className="menu-text">Rekomendasi</span>
                </span>
              </li>

              <li
                className={`menu-item ${getMenuItemActive(
                  "/knowledge/rekomendasi/usulan"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/knowledge/rekomendasi/usulan"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Usulan Surat Rekomendasi</span>
                </NavLink>
              </li>

              <li
                className={`menu-item ${getMenuItemActive(
                  "/knowledge/rekomendasi/penelitian"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/knowledge/rekomendasi/penelitian"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Penelitian Surat Rekomendasi</span>
                </NavLink>
              </li>

              <li
                className={`menu-item ${getMenuItemActive(
                  "/knowledge/rekomendasi/monitoring"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/knowledge/rekomendasi/monitoring"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Monitoring Surat Rekomendasi</span>
                </NavLink>
              </li>
              </ul>
              </div>
              </li>

              <li
                className={`menu-item ${getMenuItemActive("/knowledge/fgd")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link menu-toggle" to="/knowledge/fgd">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">FGD</span>
                </NavLink>
              </li>

              <li
                className={`menu-item ${getMenuItemActive(
                  "/knowledge/audiensi"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/knowledge/audiensi"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Audiensi</span>
                </NavLink>
              </li>

              <li
                className={`menu-item ${getMenuItemActive(
                  "/knowledge/audone"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/knowledge/audone"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Daftar Audiensi</span>
                </NavLink>
              </li>
              
              <li
                className={`menu-item ${getMenuItemActive(
                  "/knowledge/survei"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/knowledge/survei"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Survei</span>
                </NavLink>
              </li>

              <li
                className={`menu-item ${getMenuItemActive("/knowledge/surdone")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link menu-toggle" to="/knowledge/surdone">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Daftar Survei</span>
                </NavLink>
              </li>

              <li
                className={`menu-item ${getMenuItemActive("/knowledge/doc")}`}
                aria-haspopup="true"
              >
                <NavLink className="menu-link menu-toggle" to="/knowledge/doc">
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">
                    Dokumentasi Perenanaan dan Penyusunan
                  </span>
                </NavLink>
              </li>
            </ul>
          </div>
        </li>
        {/* End of KM Pages */}
        {/* Administrator Pages */}
        <li
          className={`menu-item menu-item-submenu ${getMenuItemActive(
            "/admin",
            true
          )}`}
          aria-haspopup="true"
          data-menu-toggle="hover"
        >
          <NavLink className="menu-link menu-toggle" to="/admin">
            <span className="svg-icon menu-icon">
              <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/Communication/Group.svg"
                )}
              />
            </span>
            <span className="menu-text">Administrator</span>
            <i className="menu-arrow" />
          </NavLink>
          <div className="menu-submenu ">
            <i className="menu-arrow" />
            <ul className="menu-subnav">
              <li className="menu-item  menu-item-parent" aria-haspopup="true">
                <span className="menu-link">
                  <span className="menu-text">Administrator</span>
                </span>
              </li>

              <li
                className={`menu-item ${getMenuItemActive(
                  "/admin/jenis-pajak"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/admin/jenis-pajak"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Jenis Pajak</span>
                </NavLink>
              </li>
              <li
                className={`menu-item ${getMenuItemActive(
                  "/admin/master-jenis"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/admin/master-jenis"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Master Jenis</span>
                </NavLink>
              </li>
              <li
                className={`menu-item ${getMenuItemActive(
                  "/admin/detil-jenis"
                )}`}
                aria-haspopup="true"
              >
                <NavLink
                  className="menu-link menu-toggle"
                  to="/admin/detil-jenis"
                >
                  <i className="menu-bullet menu-bullet-dot">
                    <span />
                  </i>
                  <span className="menu-text">Detil Jenis</span>
                </NavLink>
              </li>
            </ul>
          </div>
        </li>
        {/* End of Administrator Pages */}
      </ul>
    </>
  );
}
