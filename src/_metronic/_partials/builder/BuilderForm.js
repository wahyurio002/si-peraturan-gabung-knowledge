/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/img-redundant-alt */
import React, { useEffect, useState } from "react";
import DatePicker from "react-multi-date-picker"
import {Field, Form, Formik} from "formik";
import {Input, Select as Sel} from "../controls";
import {getJenisPeraturan, getJenisPajak} from "../../../app/modules/Knowledge/Api";
import { useSelector } from "react-redux";

export function BuilderForm({ initialValues }) {

    const { user } = useSelector((state) => state.auth);
    const [jenisPajak, setJenisPajak] = useState([]);
    const [jenisPeraturan, setJenisPeraturan] = useState([]);

    useEffect(() => {
        getJenisPeraturan().then(({ data }) => {
        setJenisPeraturan(data);
        });
    }, []);

    useEffect(() => {
        getJenisPajak().then(({ data }) => {
        setJenisPajak(data);
        });
    }, []);

    return (
    <Formik
                enableReinitialize={true}
                initialValues={initialValues}
                // validationSchema={AudiensiEditSchema}
                onSubmit={(values) => {
                console.log(values);
                //   saveProposal(values);
                }}
            >
                {({ handleSubmit, setFieldValue, values }) => {
                    return (
                <>
                <Form className="form form-label-right">

                    {/* FIELD NO SURAT */}
                    <div className="form-group row">
                        <label className="col-lg-3 col-form-label text-lg-right">
                            Kata Kunci:
                        </label>
                        <div className="col-lg-9 col-xl-4">
                            <Field
                                name="kata_kunci"
                                component={Input}
                                placeholder="Masukkan Kata Kunci"
                                label="Kata Kunci"
                                // onClick={()=>handleChangeNip()}
                            />
                        </div>
                    
                    <div className="col-form-label text-lg-right"></div>
                    <div className="radio">
                            <label>
                            <Field type="radio" name="metode" value="match"/>
                            MATCH&nbsp;&nbsp;
                            </label>
                    </div>
                    <div className="radio">
                            <label>
                            <Field type="radio" name="metode" value="and"/>
                            AND&nbsp;&nbsp;
                            </label>
                    </div>
                    <div className="radio">
                            <label>
                            <Field type="radio" name="metode" value="or"/>
                            OR&nbsp;&nbsp;
                            </label>
                    </div>
                    </div>
                    {/* FIELD Cari Di */}
                    <div className="form-group row">
                        <label className="col-lg-3 col-form-label text-lg-right">
                            Cari di
                        </label>
                        <div className="col-lg-9 col-xl-4">
                        <div className="radio">
                        <label>
                            <Field type="radio" name="text" value="perihal"/>
                            Perihal
                        </label>
                        </div>
                        <div className="radio">
                        <label>
                        <Field type="radio" name="text" value="isi"/>
                            Isi
                        </label>
                        </div>
                        </div>
                    </div>
                    
                    {/* FIELD Jenis Topik */}
                    <div className="form-group row">
                        <label className="col-lg-3 col-form-label text-lg-right">
                            Jenis Topik
                        </label>
                    <Sel name="id_jnspajak">
                        <option value="">Pilih Jenis Topik</option>
                        {jenisPajak.map((data, index) => (
                        <option key={index} value={data.id_jnspajak}>{data.nm_jnspajak}</option>
                        ))}
                    </Sel>
                    </div>

                    {/* FIELD Jenis Peraturan */}
                    <div className="form-group row">
                    <label className="col-lg-3 col-form-label text-lg-right">
                            Jenis Peraturan
                        </label>
                    <Sel name="id_jnsperaturan">
                        <option value="">Pilih Jenis Peraturan</option>
                        {jenisPeraturan.map((data, index) => (
                        <option key={index} value={data.id_jnsperaturan}>{data.nm_jnsperaturan}</option>
                        ))}
                    </Sel>
                    </div>

                    {/* FIELD Tanggal Peraturan */}
                    <div className="form-group row">
                    <label className="col-lg-3 col-form-label text-lg-right">
                        Tanggal Peraturan (diisi rentang)
                    </label>
                    <DatePicker
                    name="tanggal_peraturan"
                    value="a"
                    range
                    />
                    </div>

                    {/* FIELD NO SURAT */}
                    <div className="form-group row">
                        <label className="col-lg-3 col-form-label text-lg-right">
                            Nomor
                        </label>
                        <div className="col-lg-9 col-xl-4">
                            <Field
                                style={{ fontSize: 12, padding: 3 }}
                                name="nomor_surat"
                                component={Input}
                                placeholder="Nomor Peraturan"
                                label="Nomor"
                                // onClick={()=>handleChangeNip()}
                            />
                        </div>
                    </div>

                    <div className="col-lg-12" style={{ textAlign: "center" }}>
                        <button
                        type="submit"
                        className="btn btn-success ml-2"
                        onClick={handleSubmit}
                        style={{
                            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                        }}
                        // disabled={disabled}
                        >
                        <i className="fas fa-search"></i>
                        Cari
                        </button>
                    </div>
                </Form>
                </>
            );
            }}
    </Formik>
    )
}