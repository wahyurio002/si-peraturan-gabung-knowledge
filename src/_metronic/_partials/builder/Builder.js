/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/img-redundant-alt */
import React, {useEffect, useRef, useState} from "react";
import { DateObject } from "react-multi-date-picker"
import {get, merge} from "lodash";
import {FormHelperText, Switch} from "@material-ui/core";
import clsx from "clsx";
// https://github.com/conorhastings/react-syntax-highlighter#prism
import {Prism as SyntaxHighlighter} from "react-syntax-highlighter";
// See https://github.com/PrismJS/prism-themes
import {coy as highlightStyle} from "react-syntax-highlighter/dist/esm/styles/prism";
import {useHtmlClassService, setLayoutConfig, getInitLayoutConfig} from "../../layout";
import {Card, CardBody, CardHeader, Notice} from "../controls";
import {BuilderForm} from "./BuilderForm";

const localStorageActiveTabKey = "builderActiveTab";

export function Builder() {
    const activeTab = localStorage.getItem(localStorageActiveTabKey);
    const [key, setKey] = useState(activeTab ? +activeTab : 0);
    const [isLoading, setIsLoading] = useState(false);
    const htmlClassService = useHtmlClassService();
    
    const initialValues = {
      kata_kunci: "" ,
      metode: "" ,
      text: "" ,
      id_jnspajak: "" ,
      id_jnsperaturan: "",
      tanggal_peraturan:"",
      nomor_surat:""
    };

    const saveCurrentTab = (_tab) => {
        localStorage.setItem(localStorageActiveTabKey, _tab);
    } 

    const btnRef = useRef();

    const [title, setTitle] = useState("");

    useEffect(() => {
      let _title = "Pencarian Seluruh Peraturan";
      setTitle(_title);
    }, []);

    const [values, setValues] = useState([
      new DateObject().subtract(4, "days"),
      new DateObject().add(4, "days")
    ])
      return (
        <>
        <Card>
        {/* {actionsLoading && <ModalProgressBar />} */}
        <CardHeader
            title={title}
            style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
        <div className="mt-5">
          <BuilderForm
              initialValues={initialValues}
              btnRef={btnRef}
          />
        </div>
    </CardBody>
    </Card>
    </>
  );
}
