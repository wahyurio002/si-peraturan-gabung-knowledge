import React, { forwardRef } from "react";
import { useField, useFormikContext } from "formik";
import DatePicker from "react-datepicker";

const getFieldCSSClasses = (touched, errors) => {
  const classes = ["form-control"];
  if (touched && errors) {
    classes.push("is-invalid");
  }

  if (touched && !errors) {
    classes.push("is-valid");
  }

  return classes.join(" ");
};

export function DatePickerField({ ...props }) {
  const { setFieldValue, touched, errors } = useFormikContext();
  const [field] = useField(props);

  const CustomDate = forwardRef(({ value, onClick, onChange }, ref) => {
    return (
      <div>
        <div className="input-group input-group-lg ">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <i className="far fa-calendar-alt"></i>
            </span>
          </div>
          <input
            type="text"
            placeholder={`Pilih ${props.label}`}
            className={getFieldCSSClasses(
              touched[field.name],
              errors[field.name]
            )}
            {...field}
            {...props}
            name={field.name}
            onClick={onClick}
            onChange={onChange}
            ref={ref}
            value={value}
          />
        </div>
        {errors[field.name] && touched[field.name] ? (
          <div className="invalid-feedback" style={{ display: "block" }}>
            {errors[field.name].toString()}
          </div>
        ) : null}
      </div>
    );
  });
  return (
    <>
      <div className="col-lg-9 col-xl-6">
        <DatePicker
          style={{ width: "100%" }}
          {...field}
          {...props}
          selected={(field.value && new Date(field.value)) || null}
          dateFormat="dd-MM-yyyy"
          customInput={<CustomDate />}
          onChange={(val) => {
            setFieldValue(field.name, val);
          }}
        />
      </div>
    </>
  );
}
