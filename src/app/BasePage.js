import React, { Suspense, useEffect, useState, lazy } from "react";
import { Redirect, Switch, Route } from "react-router-dom";
import { LayoutSplashScreen, ContentRoute } from "../_metronic/layout";
// import { BuilderPage } from "./pages/BuilderPage";
import { MyPage } from "./pages/MyPage";
import { DashboardPage } from "./pages/DashboardPage";
import axios from 'axios';

const EvaluationPage = lazy(() =>
  import("./modules/Evaluation/Evaluation")
);

const PlanningPage = lazy(() =>
  import("./modules/Planning/Planning")
);

const ComposingPage =  lazy(() =>
  import("./modules/Composing/Composing")
)

const KnowledgePage = lazy(() => 
import("./modules/Knowledge/Knowledge")
)

const AdministratorPage = lazy(() => 
import("./modules/Administrator/Administrator")
)


export default function BasePage() {
  // useEffect(() => {
  //   console.log('Base page');
  // }, []) // [] - is required if you need only one call
  // https://reactjs.org/docs/hooks-reference.html#useeffect
  const {REACT_APP_MENU_URL} = process.env;
  const [menu, setMenu] = useState([])

  const getMenu = async () => {
    try {
      const data = {
            role:['99','8'],
            route: 'dashboard'
      }
    const response = await axios.post(`${REACT_APP_MENU_URL}`, data);
    return response;
  } catch (err) {
    if (err.response) {
       console.log(err.response);
    } else {
      console.log(err);
    }
  }
  };

  useEffect(() => {
    // getMenu()
    // .then(({data: {data}})=> {
    //   setMenu(data.menu);
    // })
  }, [])

  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        {
          /* Redirect from root URL to /dashboard. */
          <Redirect exact from="/" to="/dashboard" />
        }
        <ContentRoute path="/dashboard" component={DashboardPage} />
        <Route path="/evaluation" component={EvaluationPage} />
        <Route path="/plan" component={PlanningPage} />
        <Route path="/compose" component={ComposingPage} />
        <Route path="/knowledge" component={KnowledgePage} />
        <Route path="/admin" component={AdministratorPage} />
        {/* <ContentRoute path="/builder" component={BuilderPage} /> */}
        <ContentRoute path="/my-page" component={MyPage} />
        <Redirect to="error/error-v1" />
      </Switch>
    </Suspense>
  );
}
