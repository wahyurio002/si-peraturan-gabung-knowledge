// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
import React from "react";

export function StatusColumnFormatterAdminJenisPajak(cellContent, row) {
  const CustomerStatusCssClasses = ["success", "warning", ""];
  const CustomerStatusTitles = ["AKTIF", "PASIF", ""];
  let status = "";

  switch (row.status) {
    case "AKTIF":
      status = 0;
      break;

    case "PASIF":
      status = 1;
      break;
      
    default:
      status = 2;
  }
  const getLabelCssClasses = () => {
    if(row.status === "Terima"){
      return '';
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
    
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}
