// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function ActionsColumnFormatterAdminJenisPajak(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
    <a
      title="Edit Dialog"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openEditDialog(row.id_jnspajak)}
    >
      <span className="svg-icon svg-icon-md svg-icon-primary">
        <SVG
          src={toAbsoluteUrl(
            "/media/svg/icons/Communication/Write.svg"
          )}
        />
      </span>
    </a>
    <> </>

    <a
      title="Delete Dialog"
      className="btn btn-icon btn-light btn-hover-danger btn-sm"
      onClick={() => openDeleteDialog(row.id_jnspajak)}
    >
      <span className="svg-icon svg-icon-md svg-icon-danger">
        <SVG
          src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
        />
      </span>
    </a>
  </>
  );
  // const checkStatus = (status) => {
  //   switch (status) {
  //     case "Aktif":
  //       return (
  //         <>
  //         <a
  //           title="Edit Proposal"
  //           className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
  //           onClick={() => openEditDialog(row.id_usulan)}
  //         >
  //           <span className="svg-icon svg-icon-md svg-icon-primary">
  //             <SVG
  //               src={toAbsoluteUrl(
  //                 "/media/svg/icons/Communication/Write.svg"
  //               )}
  //             />
  //           </span>
  //         </a>
  //         <> </>

  //         <a
  //           title="Hapus Proposal"
  //           className="btn btn-icon btn-light btn-hover-danger btn-sm"
  //           onClick={() => openDeleteDialog(row.id)}
  //         >
  //           <span className="svg-icon svg-icon-md svg-icon-danger">
  //             <SVG
  //               src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
  //             />
  //           </span>
  //         </a>
  //       </>
  //       );

  //     case "Pasif":
  //       return (
  //         <>
  //           <a
  //             title="Edit Proposal"
  //             className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
  //             onClick={() => openEditDialog(row.id_usulan)}
  //           >
  //             <span className="svg-icon svg-icon-md svg-icon-primary">
  //               <SVG
  //                 src={toAbsoluteUrl(
  //                   "/media/svg/icons/Communication/Write.svg"
  //                 )}
  //               />
  //             </span>
  //           </a>
  //           <> </>

  //           <a
  //             title="Hapus Proposal"
  //             className="btn btn-icon btn-light btn-hover-danger btn-sm"
  //             onClick={() => openDeleteDialog(row.id)}
  //           >
  //             <span className="svg-icon svg-icon-md svg-icon-danger">
  //               <SVG
  //                 src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
  //               />
  //             </span>
  //           </a>
  //         </>
  //       );
  //     default:
  //       break;
  //   }
  // };

  // return <>{checkStatus(row.status)}</>;
}

export function ActionsColumnFormatterAdminMasterJenis(
  cellContent,
  row,
  rowIndex,
  { openDeleteDialog }
) {
  return (
    <>
    <a
      title="Delete Dialog"
      className="btn btn-icon btn-light btn-hover-danger btn-sm"
      onClick={() => openDeleteDialog(row.id)}
    >
      <span className="svg-icon svg-icon-md svg-icon-danger">
        <SVG
          src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
        />
      </span>
    </a>
  </>
  );
}

export function ActionsColumnFormatterAdminDetilJenis(
  cellContent,
  row,
  rowIndex,
  { openEditDialog, openDeleteDialog }
) {
  return (
    <>
    <a
      title="Edit Dialog"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openEditDialog(row.id)}
    >
      <span className="svg-icon svg-icon-md svg-icon-primary">
        <SVG
          src={toAbsoluteUrl(
            "/media/svg/icons/Communication/Write.svg"
          )}
        />
      </span>
    </a>
    <> </>

    <a
      title="Delete Dialog"
      className="btn btn-icon btn-light btn-hover-danger btn-sm"
      onClick={() => openDeleteDialog(row.id_jnspajak)}
    >
      <span className="svg-icon svg-icon-md svg-icon-danger">
        <SVG
          src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
        />
      </span>
    </a>
  </>
  );
}