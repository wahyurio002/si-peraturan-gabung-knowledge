// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";

export function DateColumnFormatter(
  cellContent,
  row,
  rowIndex,
) {


  const date = new Date(row.usulan.wkt_create);
  const tanggal = String(date.getDate()).padStart(2, '0');
  var bulan = String(date.getMonth() + 1).padStart(2, '0');
  const tahun = date.getFullYear();
  // const tanggal = date.getDate();
  // var bulan = date.getMonth();
  // const tahun = date.getFullYear();
  // switch(bulan) {
  //   case 0: bulan = "Januari"; break;
  //   case 1: bulan = "Februari"; break;
  //   case 2: bulan = "Maret"; break;
  //   case 3: bulan = "April"; break;
  //   case 4: bulan = "Mei"; break;
  //   case 5: bulan = "Juni"; break;
  //   case 6: bulan = "Juli"; break;
  //   case 7: bulan = "Agustus"; break;
  //   case 8: bulan = "September"; break;
  //   case 9: bulan = "Oktober"; break;
  //   case 10: bulan = "November"; break;
  //   case 11: bulan = "Desember"; break;
  //   default:
  //  }
  const tampilTanggal = tanggal + "-" + bulan + "-" + tahun ;


  return (
    <>
    <div>{tampilTanggal}</div>
    </>
  );
}


export function DateFormatter(
cellContent,
  row,
  rowIndex,
) {


  const date = new Date(row.tgl_surat);
  const tanggal = String(date.getDate()).padStart(2, '0');
  var bulan = String(date.getMonth() + 1).padStart(2, '0');
  const tahun = date.getFullYear();

 const tampilTanggal = tanggal + "-" + bulan + "-" + tahun ;

  return (
    <>
    <div>{tampilTanggal}</div>
    </>
  );
}

export function DateFormatterTwo(
  cellContent,
    row,
    rowIndex,
  ) {
  
  
    const date = new Date(row.usulan.tgl_surat);
    const tanggal = String(date.getDate()).padStart(2, '0');
    var bulan = String(date.getMonth() + 1).padStart(2, '0');
    const tahun = date.getFullYear();
  
   const tampilTanggal = tanggal + "-" + bulan + "-" + tahun ;
  
    return (
      <>
      <div>{tampilTanggal}</div>
      </>
    );
  }

  export function ProposalDateColumnFormatter(
    cellContent,
    row,
    rowIndex,
  ) {
  
  
    const date = new Date(row.tgl_perencanaan);
    const tanggal = String(date.getDate()).padStart(2, '0');
    var bulan = String(date.getMonth() + 1).padStart(2, '0');
    const tahun = date.getFullYear();
    const tampilTanggal = tanggal + "-" + bulan + "-" + tahun ;
  
    return (
      <>
      <div>{tampilTanggal}</div>
      </>
    );
  }
  