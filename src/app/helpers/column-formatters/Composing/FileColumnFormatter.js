// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */

import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
const { SLICE_URL } = window.ENV;

export function FileColumnFormatterComposeDrafting(cellContent, row) {
  const showFile = (file) => {
    window.open(file);
  };
  return (
    <a
      title="Show Status"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => showFile(row.file_kajian)}
    >
      <span className="svg-icon svg-icon-md svg-icon-dark">
        <SVG src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")} />
      </span>
    </a>
  );
}
export function FileColumnFormatterComposeDetilLhr(cellContent, row) {
  return (
    <a href={row.file_lhr} target="_blank" rel="noopener noreferrer">
      {row.file_lhr.slice(SLICE_URL)}
    </a>
  );
}

export function FileColumnFormatterComposeDetilPerter(cellContent, row) {
  return (
    <a href={row.file_peraturan} target="_blank" rel="noopener noreferrer">
      {row.file_peraturan.slice(SLICE_URL)}
    </a>
  );
}

export function FileColumnFormatterComposeResearch(
  cellContent,
  row,
  rowIndex,
  { openDraft }
) {
  return (
    <a title="Open Draft" href="#" onClick={() => openDraft(row.id_penyusunan)}>
      Lihat
    </a>
  );
}

export function FileColumnFormatterComposeProcess(cellContent, row) {
  return (
    <a href="#" target="_blank" rel="noopener noreferrer">
      Lihat
    </a>
  );
}

export function FileColumnFormatterComposeProcessTindakLanjutDirektoratHasilAnalisis(
  cellContent,
  row,
  rowIndex,
  { openAnalisis }
) {
  return (
    <a
      title="Open Analisis Harmon"
      className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
      onClick={() => openAnalisis(row.id_harmon)}
    >
      <span className="svg-icon svg-icon-md svg-icon-success">
        <SVG src={toAbsoluteUrl("/media/svg/icons/Files/Selected-file.svg")} />
      </span>
    </a>
    // <a href='#' target="_blank" rel="noopener noreferrer">
    //   <span className="svg-icon svg-icon-lg svg-icon-primary">
    //     <SVG src={toAbsoluteUrl("/media/svg/icons/Files/Selected-file.svg")} />
    //   </span>
    //   </a>
  );
}

export function FileColumnFormatterComposeProcessTindakLanjutDirektoratDraftPeraturan(
  cellContent,
  row,
  rowIndex,
  {openDraft}
) {
  return (
    <a 
    title="Apply Tindak Lanjut"
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openDraft(row.id_draftperaturan)}
    >
      <span className="svg-icon svg-icon-lg svg-icon-success">
        <SVG src={toAbsoluteUrl("/media/svg/icons/Files/Selected-file.svg")} />
      </span>
    </a>
  );
}

export function FileColumnFormatterComposeProcessNdPermintaan(
  cellContent,
  row
) {
  return (
    <a href={row.file_permintaan} target="_blank" rel="noopener noreferrer">
    {row.file_permintaan.slice(SLICE_URL)}
  </a>
  );
}

export function FileColumnFormatterComposeProcessTindakLanjutSahliHasilAnalisis(
  cellContent,
  row
) {
  return (
    <a href="#" target="_blank" rel="noopener noreferrer">
      <span className="svg-icon svg-icon-lg svg-icon-primary">
        <SVG src={toAbsoluteUrl("/media/svg/icons/Files/Selected-file.svg")} />
      </span>
    </a>
  );
}

export function FileColumnFormatterComposeProcessTindakLanjutSahliDraftPeraturan(
  cellContent,
  row
) {
  return (
    <a href="#" target="_blank" rel="noopener noreferrer">
      <span className="svg-icon svg-icon-lg svg-icon-success">
        <SVG src={toAbsoluteUrl("/media/svg/icons/Files/Selected-file.svg")} />
      </span>
    </a>
  );
}

export function FileColumnFormatterComposeProcessPengajuanRancanganDraftPeraturan(
  cellContent,
  row
) {
  return (
    <a href="#" target="_blank" rel="noopener noreferrer">
      <span className="svg-icon svg-icon-lg svg-icon-primary">
        <SVG src={toAbsoluteUrl("/media/svg/icons/Files/Selected-file.svg")} />
      </span>
    </a>
  );
}

export function FileColumnFormatterComposeProcessPengajuanRancanganHasilAnalisis(
  cellContent,
  row
) {
  return (
    <a href="#" target="_blank" rel="noopener noreferrer">
      <span className="svg-icon svg-icon-lg svg-icon-success">
        <SVG src={toAbsoluteUrl("/media/svg/icons/Files/Selected-file.svg")} />
      </span>
    </a>
  );
}

export function FileColumnFormatterComposeReProcess(cellContent, row) {
  return (
    <a href="#" target="_blank" rel="noopener noreferrer">
      Lihat
    </a>
  );
}
export function FileColumnFormatterComposeBkf(cellContent, row) {
  return (
    <a href="#" target="_blank" rel="noopener noreferrer">
      Lihat
    </a>
  );
}
