// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";

export function DateFormatterComposeProposal(
  cellContent,
  row,
  rowIndex,
) {


  const date = new Date(row.tgl_penyusunan);
  const tanggal = String(date.getDate()).padStart(2, '0');
  var bulan = String(date.getMonth() + 1).padStart(2, '0');
  const tahun = date.getFullYear();
  const tampilTanggal = tanggal + "-" + bulan + "-" + tahun ;


  return (
    <>
    <div>{tampilTanggal}</div>
    </>
  );
}
export function DateFormatterComposeProcessNdPermintaan(
  cellContent,
  row,
  rowIndex,
) {


  const date = new Date(row.tgl_surat);
  const tanggal = String(date.getDate()).padStart(2, '0');
  var bulan = String(date.getMonth() + 1).padStart(2, '0');
  const tahun = date.getFullYear();
  const tampilTanggal = tanggal + "-" + bulan + "-" + tahun ;


  return (
    <>
    <div>{tampilTanggal}</div>
    </>
  );
}
export function DateFormatterComposeProcessCosignDjp(
  cellContent,
  row,
  rowIndex,
) {


  const date = new Date(row.tgl_surat);
  const tanggal = String(date.getDate()).padStart(2, '0');
  var bulan = String(date.getMonth() + 1).padStart(2, '0');
  const tahun = date.getFullYear();
  const tampilTanggal = tanggal + "-" + bulan + "-" + tahun ;


  return (
    <>
    <div>{tampilTanggal}</div>
    </>
  );
}
export function DateFormatterComposeProcessTindakLanjutDirektoratJawaban(
  cellContent,
  row,
  rowIndex,
) {


  const date = new Date(row.tgl_surat);
  const tanggal = String(date.getDate()).padStart(2, '0');
  var bulan = String(date.getMonth() + 1).padStart(2, '0');
  const tahun = date.getFullYear();
  const tampilTanggal = tanggal + "-" + bulan + "-" + tahun ;


  return (
    <>
    <div>{tampilTanggal}</div>
    </>
  );
}


export function SuratDateFormatter(
cellContent,
  row,
  rowIndex,
) {


  const date = new Date(row.tgl_surat);
  const tanggal = String(date.getDate()).padStart(2, '0');
  var bulan = String(date.getMonth() + 1).padStart(2, '0');
  const tahun = date.getFullYear();

 const tampilTanggal = tanggal + "-" + bulan + "-" + tahun ;

  return (
    <>
    <div>{tampilTanggal}</div>
    </>
  );
}

export function SuratDateFormatterNested(
  cellContent,
    row,
    rowIndex,
  ) {
  
  
    const date = new Date(row.usulan.tgl_surat);
    const tanggal = String(date.getDate()).padStart(2, '0');
    var bulan = String(date.getMonth() + 1).padStart(2, '0');
    const tahun = date.getFullYear();
  
   const tampilTanggal = tanggal + "-" + bulan + "-" + tahun ;
  
    return (
      <>
      <div>{tampilTanggal}</div>
      </>
    );
  }

  export function DateFormatterComposeReProcess(
    cellContent,
    row,
    rowIndex,
  ) {
  
  
    const date = new Date(row.tgl_penyusunan);
    const tanggal = String(date.getDate()).padStart(2, '0');
    var bulan = String(date.getMonth() + 1).padStart(2, '0');
    const tahun = date.getFullYear();
    const tampilTanggal = tanggal + "-" + bulan + "-" + tahun ;
  
  
    return (
      <>
      <div>{tampilTanggal}</div>
      </>
    );
  }

  export function DateFormatterComposeBkf(
    cellContent,
    row,
    rowIndex,
  ) {
  
  
    const date = new Date(row.tgl_penyusunan);
    const tanggal = String(date.getDate()).padStart(2, '0');
    var bulan = String(date.getMonth() + 1).padStart(2, '0');
    const tahun = date.getFullYear();
    const tampilTanggal = tanggal + "-" + bulan + "-" + tahun ;
  
  
    return (
      <>
      <div>{tampilTanggal}</div>
      </>
    );
  }

  export function DateFormatterComposeProposalLhr(
    cellContent,
    row,
    rowIndex,
  ) {
  
  
    const date = new Date(row.tgl_rapat);
    const tanggal = String(date.getDate()).padStart(2, '0');
    var bulan = String(date.getMonth() + 1).padStart(2, '0');
    const tahun = date.getFullYear();
    const tampilTanggal = tanggal + "-" + bulan + "-" + tahun ;
  
  
    return (
      <>
      <div>{tampilTanggal}</div>
      </>
    );
  }

  export function DateFormatterComposeProposalPeraturanTerkaitDitetapkan(
    cellContent,
    row,
    rowIndex,
  ) {
  
  
    const date = new Date(row.tgl_ditetapkan);
    const tanggal = String(date.getDate()).padStart(2, '0');
    var bulan = String(date.getMonth() + 1).padStart(2, '0');
    const tahun = date.getFullYear();
    const tampilTanggal = tanggal + "-" + bulan + "-" + tahun ;
  
  
    return (
      <>
      <div>{tampilTanggal}</div>
      </>
    );
  }

  export function DateFormatterComposeProposalPeraturanTerkaitDiundangkan(
    cellContent,
    row,
    rowIndex,
  ) {
  
  
    const date = new Date(row.tgl_diundangkan);
    const tanggal = String(date.getDate()).padStart(2, '0');
    var bulan = String(date.getMonth() + 1).padStart(2, '0');
    const tahun = date.getFullYear();
    const tampilTanggal = tanggal + "-" + bulan + "-" + tahun ;
  
  
    return (
      <>
      <div>{tampilTanggal}</div>
      </>
    );
  }