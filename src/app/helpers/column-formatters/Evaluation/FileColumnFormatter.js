// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
import React from "react";

export function FileColumnFormatterEvaResearch(cellContent, row) {
 
  return (
    // <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
    <a href={row.file_upload} target="_blank" rel="noopener noreferrer">Lihat</a>
  );
}


export function FileColumnFormatterEvaValidate(cellContent, row) {
  return (
    <a href={row.usulan.file_upload} target="_blank" rel="noopener noreferrer">Lihat</a>
  );
}

export function FileColumnFormatterEvaRevalidateSurat(cellContent, row) {
  return (
    <a href={row.usulan.file_upload} target="_blank" rel="noopener noreferrer">Lihat</a>
  );
}

export function FileColumnFormatterEvaRevalidateValidasi(cellContent, row) {
  return (
    <a href={row.file_upload} target="_blank" rel="noopener noreferrer">Lihat</a>
  );
}
