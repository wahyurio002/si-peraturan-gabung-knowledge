import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import BkfTable from "./BkfTable";

function Bkf() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Penyusunan BKF"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
            <BkfTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Bkf;
