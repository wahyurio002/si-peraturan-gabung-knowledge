import React from "react";
import { useHistory } from "react-router-dom";

import BootstrapTable from "react-bootstrap-table-next";
import * as columnFormatters from "../../../../helpers/column-formatters";

function BkfProcessDetils({ id }) {
  const history = useHistory();

  const detils = [
    {
      id_detil: "surat-undangan",
      nama_proses: "Pembuatan Surat Undangan"
    },
    {
      id_detil: "lhr",
      nama_proses: "Input LHR Pembahasan"
    },
    {
      id_detil: "peraturan-terkait",
      nama_proses: "Peraturan Terkait"
    }
  ];
  const process = id_detil => {
    switch (id_detil) {
      case "surat-undangan":
        history.push(`/compose/bkf/process/detils/${id}/surat-undangan`);
        break;
      case "lhr":
        history.push(`/compose/bkf/process/detils/${id}/lhr`);
        break;
      case "peraturan-terkait":
        history.push(`/compose/bkf/process/detils/${id}/peraturan-terkait`);
        break;
      default:
        break;
    }
  };
  const columns = [
    {
      dataField: "nama_proses",
      text: "Detil Proses",
      sort: true
    },

    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterComposeProcess,
      formatExtraData: {
        openProcess: process
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

 

  return (
    <>
      <div className="row"></div>
      <BootstrapTable
        wrapperClasses="table-responsive"
        bordered={false}
        headerWrapperClasses="thead-light"
        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
        keyField="id_detil"
        data={detils}
        columns={columns}
        bootstrap4
      ></BootstrapTable>
    </>
  );
}

export default BkfProcessDetils;
