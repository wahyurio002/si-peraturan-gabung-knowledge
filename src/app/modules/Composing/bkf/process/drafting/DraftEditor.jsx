import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../../../_metronic/_partials/controls";
import {
  DocumentEditorContainerComponent,
  Toolbar
} from "@syncfusion/ej2-react-documenteditor";
DocumentEditorContainerComponent.Inject(Toolbar);
export function DraftEditor() {
  return (
    <Card>
      <CardHeader
        title="Buat Draft Peraturan"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <DocumentEditorContainerComponent
          id="container"
          height={"590px"}
        //   serviceUrl="https://ej2services.syncfusion.com/production/web-services/api/documenteditor/"
          enableToolbar={true}
        />
      </CardBody>
    </Card>
  );
}
