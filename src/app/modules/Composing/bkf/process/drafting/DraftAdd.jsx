/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { useHistory } from "react-router-dom";
import { toAbsoluteUrl } from "../../../../../../_metronic/_helpers";
import "./Styles.css";

import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
export function DraftAdd({
  match: {
    params: { id }
  }
}) {
  const history = useHistory();

  const handleClickDraft = (val) => {
    history.push(`/compose/bkf/draft/${id}/add/${val}`)
  }

  return (
    <Card>
      <CardHeader
        title="Buat Draft Peraturan"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="row">
            <div
              className="col-lg-12 col-xl-12 mb-3"
              style={{ textAlign: "right" }}
            >
              <button
                type="button"
                className="btn btn-light-success"
                style={{
                  float: "right"
                }}
                onClick={() => history.push(`/compose/bkf/${id}/process`)}
              >
                <i className="fa fa-arrow-left"></i>
                Kembali
              </button>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3 mb-3 text-center">
              <img
                src={toAbsoluteUrl("/media/images/image-grey.png")}
                alt="Avatar"
                className="image"
              />
              <div className="middle">
                <button
                  type="button"
                  className="btn btn-primary"
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                    float: "right",
                    width: '100px'
                  }}
                  onClick={() =>handleClickDraft('PMK')}
                >
                  Pilih
                </button>
              </div>
              <div className="text-center mt-5">
                <h5> PMK </h5>
              </div>
            </div>
            <div className="col-lg-3 mb-3 text-center">
              <img
                src={toAbsoluteUrl("/media/images/image-grey.png")}
                alt="Avatar"
                className="image"
              />
              <div className="middle">
                <button
                  type="button"
                  className="btn btn-primary"
                  style={{
                    boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                    float: "right",
                    width: '100px'
                  }}
                  onClick={()=>handleClickDraft('PER')}
                >
                  Pilih
                </button>
              </div>
              <div className="text-center mt-5">
                <h5> PER </h5>
              </div>
            </div>
          </div>
        </>
      </CardBody>

      <CardFooter style={{ borderTop: "none" }} />
    </Card>
  );
}


