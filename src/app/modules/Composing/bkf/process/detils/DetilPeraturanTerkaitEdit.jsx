import React, { useEffect, useState, useRef } from "react";

import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../../_metronic/layout";
import DetilPeraturanTerkaitForm from "./DetilPeraturanTerkaitForm";
import DetilPeraturanTerkaitFooter from "./DetilPeraturanTerkaitFooter";


export function DetilPeraturanTerkaitEdit({history,
    match: {
      params: { id_perter,id }
    }
  }) {
    const initValues = {
        no_bkf: "",
        no_peraturan: "",
        tgl_ditetapkan: "",
        tgl_diundangkan: "",
        perihal: "",
        file: ""
      };

  const [title, setTitle] = useState("");
  const suhbeader = useSubheader();

  useEffect(() => {
    let _title = id_perter ? "Edit Peraturan" : "Tambah Peraturan";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [id_perter, suhbeader]);
  const btnRef = useRef();

  const handleBack = () => {
    history.push(`/compose/bkf/process/detils/${id}/peraturan-terkait`)
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
        <DetilPeraturanTerkaitForm 
        initValues={initValues}
        btnRef={btnRef}
        />
        </>
      </CardBody>

      <CardFooter style={{ borderTop: "none" }}>
          <DetilPeraturanTerkaitFooter
          btnRef={btnRef}
          backAction={handleBack}
          />
      </CardFooter>
    </Card>
  );
}
