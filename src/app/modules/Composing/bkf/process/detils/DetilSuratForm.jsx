import React from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import {
    DatePickerField,
    Input,
    Textarea,
  } from "../../../../../helpers/";

function DetilSuratForm({ initValues, btnRef }) {
  const SuratEditSchema = Yup.object().shape({
    no_bkf: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("No BKF is required"),
    no_surat: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("No Surat is required"),
    tgl_surat: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat is required"),
    perihal: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Perihal is required"),
    instansi_penerbit: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Instansi Penerbit is required"),
    instansi_penerima: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Instansi Penerima is required")
  });
  return (
    <>
    <Formik
      enableReinitialize={true}
      initialValues={initValues}
      validationSchema={SuratEditSchema}
      onSubmit={values => {
        console.log(values);
        // saveProposal(values);
      }}
    >
      {({
        handleSubmit,
        setFieldValue,
        handleBlur,
        handleChange,
        errors,
        touched,
        values,
        isValid,
      }) => {
        return (
          <>
            <Form className="form form-label-right">
            {/* FIELD NO BKF */}
              <div className="form-group row">
                <Field
                  name="no_bkf"
                  component={Input}
                  placeholder="No BKF"
                  label="No BKF"
                  disabled
                />
              </div>
              {/* FIELD NO SURAT */}
              <div className="form-group row">
                <Field
                  name="no_surat"
                  component={Input}
                  placeholder="No Surat"
                  label="No Surat"
                />
              </div>
              {/* FIELD TANGGAL SURAT */}
              <div className="form-group row">
                <DatePickerField name="tgl_surat" label="Tanggal Surat" />
              </div>
              {/* FIELD PERIHAL */}
              <div className="form-group row">
                <Field
                  name="perihal"
                  component={Textarea}
                  placeholder="Perihal"
                  label="Perihal"
                />
              </div>
               {/* FIELD Instansi Penerbit */}
               <div className="form-group row">
                <Field
                  name="instansi_penerbit"
                  component={Input}
                  placeholder="Instansi Penerbit"
                  label="Instansi Penerbit"
                />
              </div>
               {/* FIELD Instansi Penerima */}
               <div className="form-group row">
                <Field
                  name="instansi_penerima"
                  component={Input}
                  placeholder="Instansi Penerima"
                  label="Instansi Penerima"
                />
              </div>
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
              {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
            </Form>
          </>
        );
      }}
    </Formik>
  </>
  );
}

export default DetilSuratForm;
