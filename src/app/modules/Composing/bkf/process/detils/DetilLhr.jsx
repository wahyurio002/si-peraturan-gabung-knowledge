import React from "react";
import BootstrapTable from "react-bootstrap-table-next";

import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import * as columnFormatters from "../../../../../helpers/column-formatters";

export function DetilLhr({
  history,
  match: {
    params: { id }
  }
}) {
  const surat = [
    {
      id_surat: "1",
      no_undangan: "Sahli PPHP",
      tgl_undangan: "15-12-2021",
      tgl_rapat: "15-12-2021",
      tempat: 'Kanwil A',
      status: "Draft",
    //   classes: "text-center pr-0",
    //   headerClasses: "text-center pr-3",
    }
  ];
  const columns = [
    {
        dataField: "any",
        text: "No",
        sort: true,
        formatter: columnFormatters.IdColumnFormatter
      },
    {
      dataField: "no_undangan",
      text: "No Undangan",
      sort: true,
    },
    {
      dataField: "tgl_undangan",
      text: "Tgl Undangan",
      sort: true,
    },
    {
        dataField: "tgl_rapat",
        text: "Tgl Rapat",
        sort: true,
      },
      {
        dataField: "tempat",
        text: "Tempat",
        sort: true,
      },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      formatter: columnFormatters.StatusColumnFormatterComposeDetilSurat,
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3"
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterComposeDetilSurat,
      formatExtraData: {
        openProcess: process
        // openEditDialog: edit,
        // openDeleteDialog: deleteAction,
        // showProposal: open,
        // showReject: reject,
        // applyProposal: applyProposal
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

const emptyDataMessage = () => {
    return <div className="text-center">No Data to Display</div>;
  };
  return (
    <Card>
      <CardHeader
        title="Daftar Surat"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="row">
            <div
              className="col-lg-12 col-xl-12 mb-3"
              style={{ textAlign: "right" }}
            >
                <button
                type="button"
                className="btn btn-primary ml-3"
                style={{
                  float: "right"
                }}
                onClick={() => history.push(`/compose/bkf/process/detils/${id}/lhr/add`)}
              >
                <i className="fa fa-plus"></i>
                Tambah
              </button>
              <button
                type="button"
                className="btn btn-light-success"
                style={{
                  float: "right"
                }}
                onClick={() => history.push(`/compose/bkf/${id}/process`)}

              >
                <i className="fa fa-arrow-left"></i>
                Kembali
              </button>
              
            </div>
          </div>
          <BootstrapTable
            wrapperClasses="table-responsive"
            bordered={false}
            headerWrapperClasses="thead-light"
            classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
            keyField="id_jenis"
            data={surat}
            columns={columns}
            bootstrap4
            noDataIndication={emptyDataMessage}
          ></BootstrapTable>
        </>
      </CardBody>

      <CardFooter style={{ borderTop: "none" }} />
    </Card>
  );
}
