import React from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { DatePickerField, Input, Textarea } from "../../../../../helpers";
import CustomFileInput from "../../../../../helpers/form/CustomFileInput";

function DetilPeraturanTerkaitForm({ initValues, btnRef }) {
  const PerterEditSchema = Yup.object().shape({
    no_bkf: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("No BKF is required"),
    no_peraturan: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("No Peraturan is required"),
    tgl_ditetapkan: Yup.mixed()
      .nullable(false)
      .required("Tanggal Ditetapkan is required"),
    tgl_diundangkan: Yup.mixed()
      .nullable(false)
      .required("Tanggal Diundangkan is required"),
    perihal: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Perihal is required"),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      )
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initValues}
        validationSchema={PerterEditSchema}
        onSubmit={values => {
          console.log(values);
          // saveProposal(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD NO BKF */}
                <div className="form-group row">
                  <Field
                    name="no_bkf"
                    component={Input}
                    placeholder="No BKF"
                    label="No BKF"
                    disabled
                  />
                </div>
                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    name="no_peraturan"
                    component={Input}
                    placeholder="No Peraturan"
                    label="No Peraturan"
                  />
                </div>
                {/* FIELD TANGGAL DITETAPKAN */}
                <div className="form-group row">
                  <DatePickerField
                    name="tgl_ditetapkan"
                    label="Tanggal Ditetapkan"
                  />
                </div>
                {/* FIELD TANGGAL DIUNDANGKAN */}
                <div className="form-group row">
                  <DatePickerField
                    name="tgl_diundangkan"
                    label="Tanggal Diundangkan"
                  />
                </div>
                {/* FIELD PERIHAL */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                  />
                </div>
                {/* FIELD UPLOAD FILE */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Upload File
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Field
                      name="file"
                      component={CustomFileInput}
                      title="Select a file"
                      label="File"
                      style={{ display: "flex" }}
                    />
                  </div>
                </div>
                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default DetilPeraturanTerkaitForm;
