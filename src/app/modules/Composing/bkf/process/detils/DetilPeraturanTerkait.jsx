import React from "react";
import BootstrapTable from "react-bootstrap-table-next";

import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import * as columnFormatters from "../../../../../helpers/column-formatters";

export function DetilPeraturanTerkait({
  history,
  match: {
    params: { id }
  }
}) {
  const perter = [
    {
      id_perter: "1",
      no_peraturan: "Sahli PPHP",
      tgl_ditetapkan: "15-12-2021",
      tgl_diundangkan: "15-12-2021",
      tentang: 'Tentang',
      file: "Peraturan.pdf",
    //   classes: "text-center pr-0",file
    //   headerClasses: "text-center pr-3",
    }
  ];
  const columns = [
    {
        dataField: "any",
        text: "No",
        sort: true,
        formatter: columnFormatters.IdColumnFormatter
      },
    {
      dataField: "no_peraturan",
      text: "No Peraturan",
      sort: true,
    },
    {
      dataField: "tgl_ditetapkan",
      text: "Tgl Ditetapkan",
      sort: true,
    },
    {
        dataField: "tgl_diundangkan",
        text: "Tgl Diundangkan",
        sort: true,
      },
      {
        dataField: "tentang",
        text: "Tentang",
        sort: true,
      },
    {
      dataField: "file",
      text: "File",
      sort: true,
      formatter: columnFormatters.FileColumnFormatterComposeDetilPerter
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterComposeDetilPerter,
      formatExtraData: {
        openProcess: process
        // openEditDialog: edit,
        // openDeleteDialog: deleteAction,
        // showProposal: open,
        // showReject: reject,
        // applyProposal: applyProposal
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

const emptyDataMessage = () => {
    return <div className="text-center">No Data to Display</div>;
  };
  return (
    <Card>
      <CardHeader
        title="Daftar Surat"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="row">
            <div
              className="col-lg-12 col-xl-12 mb-3"
              style={{ textAlign: "right" }}
            >
                <button
                type="button"
                className="btn btn-primary ml-3"
                style={{
                  float: "right"
                }}
                onClick={() => history.push(`/compose/bkf/process/detils/${id}/peraturan-terkait/add`)}
              >
                <i className="fa fa-plus"></i>
                Tambah
              </button>
              <button
                type="button"
                className="btn btn-light-success"
                style={{
                  float: "right"
                }}
                onClick={() => history.push(`/compose/bkf/${id}/process`)}

              >
                <i className="fa fa-arrow-left"></i>
                Kembali
              </button>
              
            </div>
          </div>
          <BootstrapTable
            wrapperClasses="table-responsive"
            bordered={false}
            headerWrapperClasses="thead-light"
            classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
            keyField="id_perter"
            data={perter}
            columns={columns}
            bootstrap4
            noDataIndication={emptyDataMessage}
          ></BootstrapTable>
        </>
      </CardBody>

      <CardFooter style={{ borderTop: "none" }} />
    </Card>
  );
}

