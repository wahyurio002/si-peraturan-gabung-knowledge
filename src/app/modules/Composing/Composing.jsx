import React from "react";
import { useSelector } from "react-redux";
import { Switch, Redirect, Route } from "react-router-dom";
import { ContentRoute } from "../../../_metronic/layout";
import CosignDjp from "./process/cosign-djp/CosignDjp";
import CosignDjpEdit from "./process/cosign-djp/CosignDjpEdit";
import NdPermintaan from "./process/nd-permintaan/NdPermintaan";
import NdPermintaanEdit from "./process/nd-permintaan/NdPermintaanEdit";
import Process from "./process/Process";
import ProcessDetil from "./process/ProcessDetil";
import TindakLanjutDirektorat from "./process/tindak-lanjut-direktorat/TindakLanjutDirektorat";
import ProposalEdit from "./proposal/edit/ProposalEdit";
import DetilLhr from "./proposal/process/detils/DetilLhr";
import DetilLhrEdit from "./proposal/process/detils/DetilLhrEdit";
import DetilPeraturanTerkait from "./proposal/process/detils/DetilPeraturanTerkait";
import DetilPeraturanTerkaitEdit from "./proposal/process/detils/DetilPeraturanTerkaitEdit";
import DetilSurat from "./proposal/process/detils/DetilSurat";
import DetilSuratEdit from "./proposal/process/detils/DetilSuratEdit";
import DraftAdd from "./proposal/process/drafting/DraftAdd";
import DraftEditor from "./proposal/process/drafting/DraftEditor";
import ProposalProcess from "./proposal/process/ProposalProcess";
import Proposal from "./proposal/Proposal";
import Research from "./research/Research";
import CosignKementerian from "./process/cosign-kementerian/CosignKementerian";
import CosignKementerianEdit from "./process/cosign-kementerian/CosignKementerianEdit";
import CosignSahli from "./process/cosign-sahli/CosignSahli";
import CosignSahliEdit from "./process/cosign-sahli/CosginSahliEdit";
import TindakLanjutSahli from "./process/tindak-lanjut-sahli/TindakLanjutSahli";
import PengajuanRancangan from "./process/pengajuan-rancangan/PengajuanRancangan";
import InputLhr from "./process/input-lhr/InputLhr";
import InputLhrEdit from "./process/input-lhr/InputLhrEdit";
import SuratUndangan from "./process/surat-undangan/SuratUndangan";
import SuratUndanganEdit from "./process/surat-undangan/SuratUndanganEdit";
import PerekamanPeraturan from "./process/perekaman-peraturan/PerekamanPeraturan";
import PerekamanPeraturanEdit from "./process/perekaman-peraturan/PerekamanPeraturanEdit";
import ReProcess from "./process-research/ReProcess";
import Bkf from "./bkf/Bkf";
import BkfEdit from "./bkf/edit/BkfEdit";
import BkfProcess from "./bkf/process/BkfProcess";
import { DraftAdd as DraftAddBkf } from "./bkf/process/drafting/DraftAdd";
import { DraftEditor as DraftEditorBkf } from "./bkf/process/drafting/DraftEditor";
import { DetilSurat as DetilSuratBkf } from "./bkf/process/detils/DetilSurat";
import { DetilLhr as DetilLhrBkf } from "./bkf/process/detils/DetilLhr";
import { DetilPeraturanTerkait as DetilPeraturanTerkaitBkf } from "./bkf/process/detils/DetilPeraturanTerkait";
import { DetilSuratEdit as DetilSuratEditBkf } from "./bkf/process/detils/DetilSuratEdit";
import { DetilLhrEdit as DetilLhrEditBkf } from "./bkf/process/detils/DetilLhrEdit";
import { DetilPeraturanTerkaitEdit as DetilPeraturanTerkaitEditBkf } from "./bkf/process/detils/DetilPeraturanTerkaitEdit";
import ResearchProcess from "./research/process/ResearchProcess";
import ResearchDraftEditor from "./research/process/ResearchDraftEditor";
import ResearchSurat from "./research/process/ResearchSurat";
import ResearchLhr from "./research/process/ResearchLhr";
import ResearchLhrOpen from "./research/process/ResearchLhrOpen";
import ResearchPeraturanTerkait from "./research/process/ResearchPeraturanTerkait";
import InputLhrDraft from "./process/input-lhr/InputLhrDraft";
import TindakLanjutDirektoratDraftLihat from "./process/tindak-lanjut-direktorat/TindakLanjutDirektoratDraftLihat";
import TindakLanjutDirektoratDraftEdit from "./process/tindak-lanjut-direktorat/TindakLanjutDirektoratDraftEdit";
import TindakLanjutDirektoratHasilAnalisis from "./process/tindak-lanjut-direktorat/TindakLanjutDirektoratHasilAnalisis";
import ReProcessDetil from "./process-research/ReProcessDetil";
import InputLhrReprocess from "./process-research/input-lhr/InputLhr";
import ComposeErrors from "./ComposeErrors";
import InputLhrReprocessOpen from "./process-research/input-lhr/InputLhrDraft";
import SuratUndanganReprocess from "./process-research/surat-undangan/SuratUndangan";
import PerekamanPeraturanReprocess from "./process-research/perekaman-peraturan/PerekamanPeraturan";
import NdPermintaanReprocess from "./process-research/nd-permintaan/NdPermintaan";
import CosignDjpReprocess from "./process-research/cosign-djp/CosignDjp";
import CosignKementerianReprocess from "./process-research/cosign-kementerian/CosignKementerian";
import TindakLanjutDirektoratReprocess from "./process-research/tindak-lanjut-direktorat/TindakLanjutDirektorat";
import TindakLanjutDirektoratHasilAnalisisReprocess from "./process-research/tindak-lanjut-direktorat/TindakLanjutDirektoratHasilAnalisis";
import TindakLanjutDirektoratDraftLihatReprocess from "./process-research/tindak-lanjut-direktorat/TindakLanjutDirektoratDraftLihat";
import TindakLanjutSahliDraftLihat from "./process/tindak-lanjut-sahli/TindakLanjutSahliDraftLihat"
import TindakLanjutSahliDraftEdit from "./process/tindak-lanjut-sahli/TindakLanjutSahliDraftEdit";
import TindakLanjutSahliHasilAnalisis from "./process/tindak-lanjut-sahli/TindakLanjutSahliHasilAnalisis";
import CosignSahliReprocess from "./process-research/cosign-sahli/CosignSahli";
import TindakLanjutSahliReprocess from "./process-research/tindak-lanjut-sahli/TindakLanjutSahli";
import TindakLanjutSahliDraftLihatReprocess from "./process-research/tindak-lanjut-sahli/TindakLanjutSahliDraftLihat";
import TindakLanjutSahliHasilAnalisisReprocess from "./process-research/tindak-lanjut-sahli/TindakLanjutSahliHasilAnalisis"
import PengajuanRancanganDraftLihat from "./process/pengajuan-rancangan/PengajuanRancanganDraftLihat";
import PengajuanRancanganDraftEdit from "./process/pengajuan-rancangan/PengajuanRancanganDraftEdit";
import PengajuanRancanganHasilAnalisis from "./process/pengajuan-rancangan/PengajuanRancanganHasilAnalisis";
import PengajuanRancanganReprocess from "./process-research/pengajuan-rancangan/PengajuanRancangan";
import PengajuanRancanganDraftLihatReprocess from "./process-research/pengajuan-rancangan/PengajuanRancanganDraftLihat";
import PengajuanRancanganHasilAnalisisReprocess from "./process-research/pengajuan-rancangan/PengajuanRancanganHasilAnalisis";
import PenetapanPeraturan from "./process/penetapan-peraturan/PenetapanPeraturan";
import DraftTolakEditor from "./proposal/process/drafting/DraftTolakEditor";

export default function Composing() {

  const {status} = useSelector(state => state.auth);
  // console.log(status)
  return (
    <Switch>
      {/* Proposal */}
      <ContentRoute path="/compose/proposal/add" component={ProposalEdit} />
      <ContentRoute
        path="/compose/proposal/:id/edit"
        component={ProposalEdit}
      />
      <ContentRoute
        path="/compose/proposal/process/detils/:id/surat/add"
        component={DetilSuratEdit}
      />
      <ContentRoute
        path="/compose/proposal/process/detils/:id/surat/:id_surat/edit"
        component={DetilSuratEdit}
      />
      <ContentRoute
        path="/compose/proposal/process/detils/:id/surat"
        component={DetilSurat}
      />
      <ContentRoute
        path="/compose/proposal/process/detils/:id/lhr/add"
        component={DetilLhrEdit}
      />
      <ContentRoute
        path="/compose/proposal/process/detils/:id/lhr/:id_lhr/edit"
        component={DetilLhrEdit}
      />
      <ContentRoute
        path="/compose/proposal/process/detils/:id/lhr"
        component={DetilLhr}
      />
      <ContentRoute
        path="/compose/proposal/process/detils/:id/perter/add"
        component={DetilPeraturanTerkaitEdit}
      />
      <ContentRoute
        path="/compose/proposal/process/detils/:id/perter/:id_perter/edit"
        component={DetilPeraturanTerkaitEdit}
      />
      <ContentRoute
        path="/compose/proposal/process/detils/:id/perter"
        component={DetilPeraturanTerkait}
      />
      <ContentRoute
        path="/compose/proposal/:id/process"
        component={ProposalProcess}
      />
      <ContentRoute
        path="/compose/proposal/draft/:id/add/:name"
        component={DraftEditor}
      />
       <ContentRoute
        path="/compose/proposal/draft/:id/edit-tolak/:id_draft/:name"
        component={DraftTolakEditor}
      />
      <ContentRoute
        path="/compose/proposal/draft/:id/edit/:id_draft/:name"
        component={DraftEditor}
      />
      <ContentRoute
        path="/compose/proposal/draft/:id/add"
        component={DraftAdd}
      />
      <ContentRoute path="/compose/proposal" component={Proposal} />

      {/* Research */}
      <ContentRoute
        path="/compose/research/:id/view/perter"
        component={ResearchPeraturanTerkait}
      />
      <ContentRoute
        path="/compose/research/:id/view/lhr/:id_lhr/open"
        component={ResearchLhrOpen}
      />
      <ContentRoute
        path="/compose/research/:id/view/lhr"
        component={ResearchLhr}
      />
      <ContentRoute
        path="/compose/research/:id/view/surat"
        component={ResearchSurat}
      />
      <ContentRoute
        path="/compose/research/:id/view/draft/:id_draft/:name"
        component={ResearchDraftEditor}
      />
      <ContentRoute
        path="/compose/research/:id/view"
        component={ResearchProcess}
      />
      <ContentRoute path="/compose/research" component={Research} />
      {/*End Research*/}


      {/* Process */}
      {/* Process - ND Permintaaan Pendapat */}
      <ContentRoute
        path="/compose/process/detils/:id/nd-permintaan/add"
        component={NdPermintaanEdit}
      />
      <ContentRoute
        path="/compose/process/detils/:id/nd-permintaan/:id_mintajawab/edit"
        component={NdPermintaanEdit}
      />
      <ContentRoute
        path="/compose/process/detils/:id/nd-permintaan"
        component={NdPermintaan}
      />
      {/*End Process - ND Minta Jawab */}

      {/*Process - Penetapan Peraturan */}
      <ContentRoute
        path="/compose/process/detils/:id/penetapan-peraturan"
       component={status === 'Penetapan' ? PenetapanPeraturan : ComposeErrors}
      /> 
      {/*End Process - Penetapan Peraturan */}

      {/*Process - Pengajuan Rancangan */}
      <ContentRoute
        path="/compose/process/detils/:id/pengajuan-rancangan/:id_harmon/hasil-analisis-harmonisasi"
       component={status === 'Cosign Sahli' || status === 'Cosign Dirjen' || status === 'Penelitian Cosign Sahli' || status === 'Penelitian Cosign Dirjen' || status === 'Penetapan' ? PengajuanRancanganHasilAnalisis : ComposeErrors}
      /> 
      <ContentRoute
        path="/compose/process/detils/:id/pengajuan-rancangan/:id_draft/draft/edit"
       component={status === 'Cosign Sahli' || status === 'Cosign Dirjen' || status === 'Penelitian Cosign Sahli' || status === 'Penelitian Cosign Dirjen' || status === 'Penetapan' ? PengajuanRancanganDraftEdit : ComposeErrors}
      /> 
      <ContentRoute
        path="/compose/process/detils/:id/pengajuan-rancangan/:id_draft/draft/:name/open"
       component={status === 'Cosign Sahli' || status === 'Cosign Dirjen' || status === 'Penelitian Cosign Sahli' || status === 'Penelitian Cosign Dirjen' || status === 'Penetapan' ? PengajuanRancanganDraftLihat : ComposeErrors}
      /> 
      <ContentRoute
        path="/compose/process/detils/:id/pengajuan-rancangan"
       component={status === 'Cosign Sahli' || status === 'Cosign Dirjen' || status === 'Penelitian Cosign Sahli' || status === 'Penelitian Cosign Dirjen' || status === 'Penetapan' ? PengajuanRancangan : ComposeErrors}
      /> 
      {/*End Process - Pengajuan Rancangan */}

      {/*Process - Tindak Lanjut Sahli */}
      <ContentRoute
        path="/compose/process/detils/:id/tindak-lanjut-sahli/:id_harmon/hasil-analisis-harmonisasi"
       component={status === 'Cosign Sahli' || status === 'Cosign Dirjen' || status === 'Penelitian Cosign Sahli' || status === 'Penelitian Cosign Dirjen' || status === 'Penetapan' ? TindakLanjutSahliHasilAnalisis : ComposeErrors}
      /> 
      <ContentRoute
        path="/compose/process/detils/:id/tindak-lanjut-sahli/:id_draft/draft/edit"
       component={status === 'Cosign Sahli' || status === 'Cosign Dirjen' || status === 'Penelitian Cosign Sahli' || status === 'Penelitian Cosign Dirjen' || status === 'Penetapan' ? TindakLanjutSahliDraftEdit : ComposeErrors}
      /> 
      <ContentRoute
        path="/compose/process/detils/:id/tindak-lanjut-sahli/:id_draft/draft/:name/open"
       component={status === 'Cosign Sahli' || status === 'Cosign Dirjen' || status === 'Penelitian Cosign Sahli' || status === 'Penelitian Cosign Dirjen' || status === 'Penetapan' ? TindakLanjutSahliDraftLihat : ComposeErrors}
      /> 
      <ContentRoute
        path="/compose/process/detils/:id/tindak-lanjut-sahli"
       component={status === 'Cosign Sahli' || status === 'Cosign Dirjen' || status === 'Penelitian Cosign Sahli' || status === 'Penelitian Cosign Dirjen' || status === 'Penetapan' ? TindakLanjutSahli : ComposeErrors}
      /> 
      {/*End Process - Tindak Lanjut Sahli */}

      {/*Process - Cosign Sahli */}
      <ContentRoute
        path="/compose/process/detils/:id/cosign-sahli/:id_cosign_sahli/edit"
       component={status === 'Cosign Sahli' || status === 'Cosign Dirjen' || status === 'Penelitian Cosign Sahli' || status === 'Penelitian Cosign Dirjen' || status === 'Penetapan' ? CosignSahliEdit : ComposeErrors}
      /> 
      <ContentRoute
        path="/compose/process/detils/:id/cosign-sahli/add"
       component={status === 'Cosign Sahli' || status === 'Cosign Dirjen' || status === 'Penelitian Cosign Sahli' || status === 'Penelitian Cosign Dirjen' || status === 'Penetapan' ? CosignSahliEdit : ComposeErrors}
      /> 
      <ContentRoute
        path="/compose/process/detils/:id/cosign-sahli"
       component={status === 'Cosign Sahli' || status === 'Cosign Dirjen' || status === 'Penelitian Cosign Sahli' || status === 'Penelitian Cosign Dirjen' || status === 'Penetapan' ? CosignSahli : ComposeErrors}
      /> 
      {/*End Process - Cosign Sahli */}
      


      {/* Process - Cosign DJP */}
      <ContentRoute
        path="/compose/process/detils/:id/cosign-djp/add"
        component={CosignDjpEdit}
      />
      <ContentRoute
        path="/compose/process/detils/:id/cosign-djp/:id_cosign/edit"
        component={CosignDjpEdit}
      />
      <ContentRoute
        path="/compose/process/detils/:id/cosign-djp"
        component={CosignDjp}
      />
      {/*End Process - Cosign DJP */}

      {/* Process - Cosign Kementerian */}
      <ContentRoute
        path="/compose/process/detils/:id/cosign-kementerian/add"
        component={CosignKementerianEdit}
      />
      <ContentRoute
        path="/compose/process/detils/:id/cosign-kementerian/:id_cosign/edit"
        component={CosignKementerianEdit}
      />
      <ContentRoute
        path="/compose/process/detils/:id/cosign-kementerian"
        component={CosignKementerian}
      />
      {/*End Process - Cosign Kementerian */}

      {/* Process - Tindak Lanjut Direktorat */}
      <ContentRoute
        path="/compose/process/detils/:id/tindak-lanjut-direktorat/:id_harmon/hasil-analisis-harmonisasi"
        component={TindakLanjutDirektoratHasilAnalisis}
      />
      <ContentRoute
        path="/compose/process/detils/:id/tindak-lanjut-direktorat/hasil-analisis-harmonisasi"
        component={TindakLanjutDirektoratHasilAnalisis}
      />
      <ContentRoute
        path="/compose/process/detils/:id/tindak-lanjut-direktorat/:id_draft/draft/:name/open"
        component={TindakLanjutDirektoratDraftLihat}
      />
      <ContentRoute
        path="/compose/process/detils/:id/tindak-lanjut-direktorat/:id_draft/draft/edit"
        component={TindakLanjutDirektoratDraftEdit}
      />
      <ContentRoute
        path="/compose/process/detils/:id/tindak-lanjut-direktorat"
        component={TindakLanjutDirektorat}
      />
      {/*End Process - Tindak Lanjut Direktorat */}

     
      {/* Process - Input LHR */}
      <ContentRoute
        path="/compose/process/detils/:id/input-lhr/add"
        component={InputLhrEdit}
      />
      <ContentRoute
        path="/compose/process/detils/:id/input-lhr/:id_lhr/edit"
        component={InputLhrEdit}
      />
      <ContentRoute
        path="/compose/process/detils/:id/input-lhr/:id_lhr/open"
        component={InputLhrDraft}
      />
      
      <ContentRoute
        path="/compose/process/detils/:id/input-lhr"
        component={InputLhr}
      />
      {/*End Process - Input Lhr */}

      {/* Process - Surat Undangan */}
      <ContentRoute
        path="/compose/process/detils/:id/surat-undangan/add"
        component={SuratUndanganEdit}
      />
       <ContentRoute
        path="/compose/process/detils/:id/surat-undangan/:id_surat/edit"
        component={SuratUndanganEdit}
      />
      <ContentRoute
        path="/compose/process/detils/:id/surat-undangan"
        component={SuratUndangan}
      />
      {/*End Process - Surat Undangan */}

      {/* Process - Peraturan Terkait */}
      <ContentRoute
        path="/compose/process/detils/:id/perekaman-peraturan/add"
        component={PerekamanPeraturanEdit}
      />
      <ContentRoute
        path="/compose/process/detils/:id/perekaman-peraturan/:id_perter/edit"
        component={PerekamanPeraturanEdit}
      />
      <ContentRoute
        path="/compose/process/detils/:id/perekaman-peraturan"
        component={PerekamanPeraturan}
      />
      {/*End Process - Peraturan Terkait */}


      <ContentRoute
        path="/compose/process/:id/detil"
        component={ProcessDetil}
      />
      <ContentRoute path="/compose/process" component={Process} />
      {/*End Process*/}

      {/*ReProcess*/}
      {/*Start ReProcess - Pengajuan Rancangan */}
      <ContentRoute path="/compose/reprocess/detils/:id/pengajuan-rancangan/:id_harmon/hasil-analisis-harmonisasi" component={PengajuanRancanganHasilAnalisisReprocess} />
      <ContentRoute path="/compose/reprocess/detils/:id/pengajuan-rancangan/:id_draft/draft/:name/open" component={PengajuanRancanganDraftLihatReprocess} />
      <ContentRoute path="/compose/reprocess/detils/:id/pengajuan-rancangan" component={PengajuanRancanganReprocess} />
      {/*End of ReProcess - Pengajuan Rancangan */}

      {/*Start ReProcess - Tindak Lanjut Sahli */}
      <ContentRoute path="/compose/reprocess/detils/:id/tindak-lanjut-sahli/:id_harmon/hasil-analisis-harmonisasi" component={TindakLanjutSahliHasilAnalisisReprocess} />
      <ContentRoute path="/compose/reprocess/detils/:id/tindak-lanjut-sahli/:id_draft/draft/:name/open" component={TindakLanjutSahliDraftLihatReprocess} />
      <ContentRoute path="/compose/reprocess/detils/:id/tindak-lanjut-sahli" component={TindakLanjutSahliReprocess} />
      {/*End of ReProcess - Tindak lanjut Sahli */}

      {/*Start ReProcess - Cosign Sahli */}
      {/* <ContentRoute path="/compose/reprocess/detils/:id/cosign-sahli" component={CosignSahliReprocess} /> */}
      {/*End of ReProcess - Cosign Sahli */}

      {/*Start ReProcess - Tindak Lanjut Direktorat */}
      <ContentRoute path="/compose/reprocess/detils/:id/tindak-lanjut-direktorat/:id_draft/draft/:name/open" component={TindakLanjutDirektoratDraftLihatReprocess}/>
      <ContentRoute  path="/compose/reprocess/detils/:id/tindak-lanjut-direktorat/:id_harmon/hasil-analisis-harmonisasi" component={TindakLanjutDirektoratHasilAnalisisReprocess} />
      <ContentRoute path="/compose/reprocess/detils/:id/tindak-lanjut-direktorat" component={TindakLanjutDirektoratReprocess} />
      {/*End of ReProcess - Tindak Lanjut Direktorat */}

      {/*Start ReProcess - Cosign Kementerian */}
      {/* <ContentRoute path="/compose/reprocess/detils/:id/cosign-kementerian" component={CosignKementerianReprocess} /> */}
      {/*End of ReProcess - Cosign Kementerian */}
      
      {/*Start ReProcess - Cosign Unit */}
      {/* <ContentRoute path="/compose/reprocess/detils/:id/cosign-djp" component={CosignDjpReprocess} /> */}
      {/*End of ReProcess - Cosign Unit */}

      {/*Start ReProcess - ND Permintaan Jawaban */}
      {/* <ContentRoute path="/compose/reprocess/detils/:id/nd-permintaan" component={NdPermintaanReprocess} /> */}
      {/*End of ReProcess - ND Permintaan Jawaban */}

      {/*Start ReProcess - Perekaman Peraturan */}
      {/* <ContentRoute path="/compose/reprocess/detils/:id/perekaman-peraturan" component={PerekamanPeraturanReprocess} /> */}
      {/*End of ReProcess - Perekaman Peraturan */}

      {/*Start ReProcess - Surat Undangan */}
      {/* <ContentRoute path="/compose/reprocess/detils/:id/surat-undangan" component={SuratUndanganReprocess} /> */}
      {/*End of ReProcess - Surat Undangan */}

      {/*Start ReProcess - Input LHR Pembahasan */}
      {/* <ContentRoute path="/compose/reprocess/detils/:id/input-lhr/:id_lhr/open" component={InputLhrReprocessOpen} />
      <ContentRoute path="/compose/reprocess/detils/:id/input-lhr" component={InputLhrReprocess} /> */}
      {/*End of ReProcess - Input LHR Pembahasan */}


      {/* Research Process */}
      <ContentRoute path="/compose/reprocess/:id/detil" component={ReProcessDetil} />
      <ContentRoute path="/compose/reprocess" component={ReProcess} />
      {/*End of Research Process */}


    
      {/* BKF */}
      {/* BKF - Drafting */}
      <ContentRoute
        path="/compose/bkf/draft/:id/add/:name"
        component={DraftEditorBkf}
      />
      <ContentRoute path="/compose/bkf/draft/:id/add" component={DraftAddBkf} />
      {/* BKF - Detil */}
      <ContentRoute
        path="/compose/bkf/process/detils/:id/surat-undangan/add"
        component={DetilSuratEditBkf}
      />
      <ContentRoute
        path="/compose/bkf/process/detils/:id/surat-undangan"
        component={DetilSuratBkf}
      />
      <ContentRoute
        path="/compose/bkf/process/detils/:id/lhr/add"
        component={DetilLhrEditBkf}
      />
      <ContentRoute
        path="/compose/bkf/process/detils/:id/lhr"
        component={DetilLhrBkf}
      />
      <ContentRoute
        path="/compose/bkf/process/detils/:id/peraturan-terkait/add"
        component={DetilPeraturanTerkaitEditBkf}
      />
      <ContentRoute
        path="/compose/bkf/process/detils/:id/peraturan-terkait"
        component={DetilPeraturanTerkaitBkf}
      />
      <ContentRoute path="/compose/bkf/:id/process" component={BkfProcess} />

      <ContentRoute path="/compose/bkf/add" component={BkfEdit} />
      <ContentRoute path="/compose/bkf" component={Bkf} />

      {/* Monitoring */}
      {/* <ContentRoute path="/plan/monitoring" component={Monitoring} /> */}

     






    </Switch>
  );
}
