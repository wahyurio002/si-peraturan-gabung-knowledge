import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import {
  getMintaJawabByIdMintaJawab,
  getCosignKementerianByIdCosign
} from "../../../Evaluation/Api";
import { DateFormat } from "../../../../helpers/DateFormat";

function CosignKementerianOpen({ id, show, onHide, id_cosign_kl }) {
  const [data, setData] = useState([]);
  const [surat, setSurat] = useState([]);

  useEffect(() => {
    if (id_cosign_kl) {
      getCosignKementerianByIdCosign(id_cosign_kl).then(({ data }) => {
        setData(data);
        getMintaJawabByIdMintaJawab(data.id_minta_jawab).then(({ data }) => {
          setSurat(data);
        });
      });
    }
  }, [id_cosign_kl]);

  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Detil Cosign Kementerian
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Nomor Surat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.no_surat}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Tanggal Surat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${DateFormat(data.tgl_surat)}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">Nama Unit</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${data.nama_unit}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">No ND Permintaan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="text-muted text-hover-primary">
              {`: ${surat.nomor_surat}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3">
            <span className="font-weight-bold mr-2">File Cosign</span>
          </div>
          <div className="col-lg-9 col-xl-6">
            <a
              href={data.file_cosign}
              target="_blank"
              rel="noopener noreferrer"
            >
              {data.file_cosign
                ? `: ${data.file_cosign.slice(28)}`
                : null}
            </a>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-danger"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tutup
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}

export default CosignKementerianOpen;
