import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../_metronic/_partials/controls";
import ProcessDetilTable from "./ProcessDetilTable";

import { getDraftComposingById, getPenyusunanById } from "../../Evaluation/Api";
import { DateFormat } from "../../../helpers/DateFormat";
import * as auth from "../../../modules/Auth/_redux/authRedux"
import { connect } from "react-redux";



function ProcessDetil({
  history,
  setStatus,
  setDraftUsulan,
  setDraftCosign,
  match: {
    params: { id }
  }
}) {
  const [content, setContent] = useState([]);


  useEffect(() => {
    // console.log(props)
    getPenyusunanById(id).then(({ data }) => {
      setContent(data);
      setStatus(data.status)
    });
    getDraftComposingById(id).then(({data})=> {
      // console.log(data)
      data[0] ? setDraftUsulan(data[0].body_draft) : setDraftUsulan(undefined);
      data[0] ? setDraftCosign(data[0].body_cosign) : setDraftCosign(undefined);
    })
  }, [id, setDraftCosign, setDraftUsulan, setStatus]);

  const handleBack = () => {
    history.push(`/compose/process`)
  }

  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Proses Penyusunan Peraturan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <>
            <div className="row">
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    No Penyusunan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${content.no_penyusunan}`}
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Jenis Peraturan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${content.jns_peraturan}`}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Tanggal Penyusunan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${DateFormat(content.tgl_penyusunan)}`}
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Judul Peraturan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${content.judul_peraturan}`}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Unit In Charge 
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${content.unit_incharge}`}
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Usulan Simfoni
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">{`: ${content.simfoni}`}</p>
                  </div>
                </div>
              </div>
            </div>
            {content.alasan_tolak ? <div className="row">
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Alasan Tolak
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${content.alasan_tolak}`}
                    </p>
                  </div>
                </div>
              </div>
            </div> : null}
          </>
          <ProcessDetilTable id={id} status={content.status}/>
        </CardBody>
        <CardFooter style={{ borderTop: "none" }}>
          <>
          <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={handleBack}
            className="btn btn-light-success"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          
        </div>
          </>
      </CardFooter>
      </Card>
    </>
  );
}

export default connect(null, auth.actions)(ProcessDetil);
