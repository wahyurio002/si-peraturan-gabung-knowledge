import React, { useState, useEffect } from "react";
import Select from "react-select";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel
} from "../../../../helpers";
import CustomFileInput from "../../../../helpers/form/CustomFileInput";
import { unitKerja } from "../../../../references/UnitKerja";

function NdPermintaanForm({ initValues, btnRef, saveProposal }) {
  const [kantor, setKantor] = useState([]);
  useEffect(() => {
    unitKerja.map(data => {
      setKantor(kantor => [
        ...kantor,
        {
          label: data.nm_UNIT_KERJA,
          value: data.kd_unit_kerja,
          alamat: data.alamat
        }
      ]);
    });
  }, []);

  const CosignEditSchema = Yup.object().shape({
    perihal: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Perihal is required"),
    nomor_surat: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("No Surat Permintaan is required"),
    tgl_surat: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat Permintaan is required"),
    tujuan: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Tujuan Surat is required"),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      )
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  const asosiasi = () => {
    return (
      <>
        <div className="form-group row">
          <Field
            name="tujuan"
            component={Input}
            placeholder="Tujuan Surat"
            label="Tujuan Surat"
          />
        </div>
      </>
    );
  };

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initValues}
        validationSchema={CosignEditSchema}
        onSubmit={values => {
          // console.log(values);
          saveProposal(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          const handleChangeUnit = val => {
            setFieldValue("tujuan", val.label);
          };
          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD PERIHAL */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                  />
                </div>
                {/* FIELD NO SURAT PERMINTAAN */}
                <div className="form-group row">
                  <Field
                    name="nomor_surat"
                    component={Input}
                    placeholder="No Surat Permintaan"
                    label="No Surat Permintaan"
                  />
                </div>
                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField
                    name="tgl_surat"
                    label="Tanggal Surat Permintaan"
                  />
                </div>
                {/* FIELD TUJUAN */}
                <div className="form-group row">
                  <Sel name="jenis_instansi" label="Jenis Instansi">
                    <option>Pilih Jenis Instansi</option>
                    <option value="1">Asosiasi / KL</option>
                    <option value="2">Unit DJP</option>
                  </Sel>
                </div>
                {values.jenis_instansi ? (
                  <>
                    {values.jenis_instansi === "1" ? (
                      asosiasi()
                    ) : (
                      <div className="form-group row">
                        <label className="col-xl-3 col-lg-3 col-form-label">
                          Tujuan Surat
                        </label>
                        <div className="col-lg-9 col-xl-6">
                          <Select
                            options={kantor}
                            onChange={value => handleChangeUnit(value)}
                            value={kantor.filter(
                              data => data.label === values.tujuan
                            )}
                          />
                        </div>
                      </div>
                    )}
                  </>
                ) : null}
                
                {/* FIELD UPLOAD FILE */}
                {values.nomor_surat ? (
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Upload ND Permintaan
                    </label>
                    <div className="col-lg-9 col-xl-6">
                      <Field
                        name="file"
                        component={CustomFileInput}
                        title="Select a file"
                        label="File"
                        style={{ display: "flex" }}
                      />
                    </div>
                  </div>
                ) : null}

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default NdPermintaanForm;
