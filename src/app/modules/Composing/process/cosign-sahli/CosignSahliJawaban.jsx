import React from "react";
import { useHistory } from "react-router-dom";

import BootstrapTable from "react-bootstrap-table-next";
import * as columnFormatters from "../../../../helpers/column-formatters";

function CosignSahliJawaban({ id }) {
  const history = useHistory();

  const date = new Date();

  const openAction = () => {
    return;
  };

  const jawaban = [
    {
      id_jawaban: "1",
      nama_unit: "KPP 1",
      tgl_jawaban: date,
      
    },
    {
      id_jawaban: "2",
      nama_unit: "Direktorat 1",
      tgl_jawaban: date,
      
    },
    {
      id_jawaban: "3",
      nama_unit: "Kanwil 1",
      tgl_jawaban: date,
      
    },
    {
      id_jawaban: "4",
      nama_unit: "Kanwil 2",
      tgl_jawaban: date,
      
    }
  ];
  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter
    },
    {
      dataField: "nama_unit",
      text: "Nama Unit",
      sort: true
    },
    {
      dataField: "tgl_jawaban",
      text: "Tanggal Jawaban",
      sort: true,
      formatter:
        columnFormatters.DateFormatterComposeProcessTindakLanjutDirektoratJawaban
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        columnFormatters.ActionsColumnFormatterComposeProcessTindakLanjutDirektoratJawaban,
      formatExtraData: {
        open: openAction
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const columnsSecond = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter
    },
    {
      dataField: "nama_unit",
      text: "Nama Unit",
      sort: true
    },
    {
      dataField: "tgl_jawaban",
      text: "Tanggal Jawaban",
      sort: true,
      formatter:
        columnFormatters.DateFormatterComposeProcessTindakLanjutDirektoratJawaban
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        columnFormatters.ActionsColumnFormatterComposeProcessTindakLanjutDirektoratJawaban,
      formatExtraData: {
        open: openAction
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  const emptyDataMessage = () => {
    return "No Data";
  };

  return (
    <>
      <div className="mt-5">
        <BootstrapTable
          wrapperClasses="table-responsive"
          bordered={false}
          headerWrapperClasses="thead-light"
          classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
          keyField="id_jawaban"
          data={jawaban}
          columns={columns}
          bootstrap4
          noDataIndication={emptyDataMessage}
        ></BootstrapTable>
      </div>
      <div className="mt-5">
        <BootstrapTable
          wrapperClasses="table-responsive"
          bordered={false}
          headerWrapperClasses="thead-light"
          classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
          keyField="id_jawaban"
          data={jawaban}
          columns={columnsSecond}
          bootstrap4
          noDataIndication={emptyDataMessage}
        ></BootstrapTable>
      </div>
      <div className="row">
        <div className="col-lg-12 col-xl-12 mb-3" style={{ textAlign: "right" }}>
          <button
            type="button"
            className="btn btn-success mx3"
            style={{
              float: "right"
            }}
            onClick={() => history.push(`/compose/process/${id}/detil`)}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
        </div>
      </div>
    </>
  );
}

export default CosignSahliJawaban;
