import React, { useState, useEffect } from "react";
import Select from "react-select";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { DatePickerField, Input, Select as Sel } from "../../../../helpers";
import CustomFileInput from "../../../../helpers/form/CustomFileInput";
import { getMintaJawab } from "../../../Evaluation/Api";

function CosignSahliForm({ initValues, btnRef, saveCosign, id }) {
  const [valSurat, setValSurat] = useState();
  const [surat, setSurat] = useState([]);

  const CosignEditSchema = Yup.object().shape({
    no_surat: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("No Surat Permintaan is required"),
    tgl_surat: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat Permintaan is required"),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      )
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    getMintaJawab(id).then(({ data }) => {
      data.map(data => {
        setSurat(surat => [
          ...surat,
          { label: data.nomor_surat, value: data.id_minta_jawab }
        ]);
      });
    });
  }, [id]);
  useEffect(() => {
    if (initValues.id_minta_jawab) {
      surat
        .filter(data => data.value === initValues.id_minta_jawab)
        .map(data => {
          setValSurat(data.label);
        });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [initValues.id_minta_jawab]);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initValues}
        validationSchema={CosignEditSchema}
        onSubmit={values => {
          saveCosign(values);
          // console.log(values)
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          const handleChangeSurat = val => {
            setFieldValue("id_minta_jawab", val.value);
            setValSurat(val.label);
          };
          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD NO ND PERMINTAAN */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    No ND Permintaan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={surat}
                      onChange={value => handleChangeSurat(value)}
                      value={surat.filter(data => data.label === valSurat)}
                    />
                  </div>
                </div>
                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    name="no_surat"
                    component={Input}
                    placeholder="No Surat"
                    label="No Surat"
                  />
                </div>
                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField name="tgl_surat" label="Tanggal Surat" />
                </div>

                {/* FIELD UPLOAD FILE */}
                {values.no_surat ? 
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Upload ND Jawaban
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Field
                      name="file"
                      component={CustomFileInput}
                      title="Select a file"
                      label="File"
                      style={{ display: "flex" }}
                    />
                  </div>
                </div>
                : null}

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default CosignSahliForm;
