import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { Textarea, Select as Sel } from "../../../../helpers";
import {
  getLhrDetilByIdDetil,
  saveLhrDetil,
  updateLhrDetil
} from "../../../Evaluation/Api";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";

function InputLhrTablePokokOpen({ id, show, onHide, after, id_detil, id_lhr }) {
  const history = useHistory();
  const [content, setContent] = useState();

  useEffect(() => {
    if (id_detil) {
      getLhrDetilByIdDetil(id_detil).then(({ data }) => {
        // console.log(data)
        setContent({
          pokok_pengaturan: data.pokok_pengaturan,
          jenis: data.jenis
        });
      });
    } else {
      setContent({
        pokok_pengaturan: "",
        jenis: ""
      });
    }
  }, [id, id_detil]);

  // const initialValues = {
  //   pokok_pengaturan: "",
  //   jenis: ""
  // };

  const validationSchema = Yup.object().shape({
    pokok_pengaturan: Yup.string()
      .min(2, "Minimum 2 Characters")
      .required("Alasan is required"),
    jenis: Yup.string().required("Jenis is required")
  });

  const savePengaturan = values => {
    if (!id_detil) {
      saveLhrDetil(id_lhr, values.jenis, values.pokok_pengaturan).then(
        ({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push(`/dashboard`);
              history.replace(
                `/compose/process/detils/${id}/input-lhr/${id_lhr}/edit`
              );
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(`/compose/proposal/process/detils/${id}/surat/add`);
              history.replace(
                `/compose/process/detils/${id}/input-lhr/add`
              );
            });
          }
        }
      );
    } else {
      updateLhrDetil(
        id_detil,
        id_lhr,
        values.jenis,
        values.pokok_pengaturan
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push(`/dashboard`);
            history.replace(
              `/compose/process/detils/${id}/input-lhr/${id_lhr}/edit`
            );
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push(`/compose/proposal/process/detils/${id}/surat/add`);
            history.replace(`/compose/process/detils/${id}/input-lhr/add`);
          });
        }
      });
    }
  };

  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Tambah Pokok Pengaturan Pembahasan
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <>
          <Formik
            enableReinitialize={true}
            initialValues={content}
            validationSchema={validationSchema}
            onSubmit={values => {
              // console.log(values);
              savePengaturan(values);
            }}
          >
            {({ handleSubmit }) => {
              return (
                <Form className="form form-label-right">
                  <div
                    className="form-group row"
                    style={{ marginBottom: "0px" }}
                  >
                    <div className="col-lg-9 col-xl-6">
                      <h5 className="mt-6" style={{ fontWeight: "600" }}>
                        Pokok Pengaturan
                      </h5>
                    </div>
                  </div>
                  <div className="form-group row">
                    <Field
                      name="pokok_pengaturan"
                      component={Textarea}
                      placeholder="Pokok Pengaturan"
                      custom="custom"
                    />
                  </div>
                  <div
                    className="form-group row"
                    style={{ marginBottom: "0px" }}
                  >
                    <div className="col-lg-9 col-xl-6">
                      <h5 className="mt-6" style={{ fontWeight: "600" }}>
                        Jenis Pokok Pengaturan
                      </h5>
                    </div>
                  </div>
                  <div className="form-group row">
                    <Sel
                      name="jenis"
                      // label="Jenis Pokok Pengaturan"
                      custom="custom"
                    >
                      <option value="">Pilih Jenis Pengaturan</option>
                      <option value="Sebelum">Sebelum</option>
                      <option value="Sesudah">Sesudah</option>
                      {/* {jenisPeraturan.map(data => (
                  <option
                    key={data.id_jnsperaturan}
                    value={data.nm_jnsperaturan}
                  >
                    {data.nm_jnsperaturan}
                  </option>
                ))} */}
                    </Sel>
                  </div>
                  <div className="col-lg-12" style={{ textAlign: "center" }}>
                    <button
                      type="button"
                      onClick={onHide}
                      className="btn btn-light"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="flaticon2-cancel icon-nm"></i>
                      Batal
                    </button>
                    {`  `}
                    <button
                      type="submit"
                      onSubmit={() => handleSubmit()}
                      className="btn btn-success ml-2"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="fas fa-check"></i>
                      Kirim
                    </button>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </>
      </Modal.Body>
    </Modal>
  );
}

export default InputLhrTablePokokOpen;
