import React, { useState, useEffect, useRef } from "react";
import { useSelector } from "react-redux";
import { Field, Formik, Form } from "formik";
import swal from "sweetalert";
import * as Yup from "yup";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import { Radio } from "../../../../helpers";
import {
  saveAnalisisHarmon,
  getAnalisisHarmonByIdHarmon,
  updateAnalisisHarmon
} from "../../../Evaluation/Api";

function PengajuanRancanganHasilAnalisis({
  history,
  match: {
    params: { id, id_harmon }
  }
}) {

  const { status } = useSelector(state => state.auth);
  const [content, setContent] = useState();
  const initValues = {
    draft: "",
    jawab1: "",
    jawab2: "",
    jawab3: "",
    jawab4: "",
    jawab5: ""
  };
  const btnRef = useRef();

  const ProposalEditSchema = Yup.object().shape({
    draft: Yup.string().required("Simfoni is required"),
    jawab1: Yup.string().required("Pertanyaan 1 is required"),
    jawab2: Yup.string().required("Pertanyaan 2 is required"),
    jawab3: Yup.string().required("Pertanyaan 3 is required"),
    jawab4: Yup.string().required("Pertanyaan 4 is required"),
    jawab5: Yup.string().required("Pertanyaan 5 is required")
  });

  useEffect(() => {
    if (id_harmon) {
      getAnalisisHarmonByIdHarmon(id_harmon).then(({ data }) => {
        setContent(data);
      });
    }
  }, [id_harmon]);
  const handleBack = () => {
    history.push(`/compose/process/detils/${id}/pengajuan-rancangan`);
  };
  const saveProposal = values => {
    if (!id_harmon) {
      saveAnalisisHarmon(
        id,
        values.draft,
        values.jawab1,
        values.jawab2,
        values.jawab3,
        values.jawab4,
        values.jawab5
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push(
              `/compose/process/detils/${id}/pengajuan-rancangan`
            );
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push(
              `/compose/process/detils/${id}/pengajuan-rancangan`
            );
          });
        }
      });
    } else {
      updateAnalisisHarmon(
        id_harmon,
        id,
        values.draft,
        values.jawab1,
        values.jawab2,
        values.jawab3,
        values.jawab4,
        values.jawab5
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push(
              `/compose/process/detils/${id}/pengajuan-rancangan`
            );
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push(
              `/compose/process/detils/${id}/pengajuan-rancangan`
            );
          });
        }
      });
    }
  };
  const saveForm = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };
  const choice = [
    { name: "Sudah", value: "Sudah" },
    { name: "Belum", value: "Belum" }
  ];
  const draftChoice = [
    { name: "Ya", value: "Ya" },
    { name: "Tidak", value: "Tidak" }
  ];
  return (
    <>
      <Card>
        <CardHeader
          title="Hasil Analisis Harmonisasi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <>
            <Formik
              enableReinitialize={true}
              initialValues={content || initValues}
              validationSchema={ProposalEditSchema}
              onSubmit={values => {
                // console.log(values);
                saveProposal(values);
              }}
            >
              {({
                handleSubmit,
                setFieldValue,
                handleBlur,
                handleChange,
                errors,
                touched,
                values,
                isValid
              }) => {

                return (
                  <>
                  {status !== 'Proses Penyusunan' ? (
                    <Form className="form form-label-right">
                    {/* Field Draft */}
                    <div className="form-group row">
                      <h5 className="col-xl-12 col-lg-12 ">
                        Apakah Draft terkait penyusunan RPMK ?
                      </h5>
                    </div>
                    <div className="form-group row">
                      <div className="col-lg-9 col-xl-6">
                        <div className="checkbox-inline">
                          {draftChoice.map((data, index) => (
                            <Field
                              name="draft"
                              component={Radio}
                              type="radio"
                              value={data.value}
                              key={index}
                              content={data.name}
                              disabled
                            />
                          ))}
                        </div>
                      </div>
                    </div>
                    {/* Field Jawab 1 */}
                    <div className="form-group row">
                      <h5 className="col-xl-12 col-lg-12 ">
                        1. Apakah atas Rancangan Regulasi Perpajakan Sudah
                        Dilakukan Harmonisasi Vertikal ?
                      </h5>
                    </div>
                    <div className="form-group row">
                      <div className="col-lg-9 col-xl-6">
                        <div className="checkbox-inline">
                          {choice.map((data, index) => (
                            <Field
                              name="jawab1"
                              component={Radio}
                              type="radio"
                              value={data.value}
                              key={index}
                              content={data.name}
                              disabled

                            />
                          ))}
                        </div>
                      </div>
                    </div>
                    {/* Field Jawab 2 */}
                    <div className="form-group row">
                      <h5 className="col-xl-12 col-lg-12 ">
                        2. Apakah atas Rancangan Regulasi Perpajakan Sudah
                        Dilakukan Harmonisasi Horisontal ?
                      </h5>
                    </div>
                    <div className="form-group row">
                      <div className="col-lg-9 col-xl-6">
                        <div className="checkbox-inline">
                          {choice.map((data, index) => (
                            <Field
                              name="jawab2"
                              component={Radio}
                              type="radio"
                              value={data.value}
                              key={index}
                              content={data.name}
                              disabled

                            />
                          ))}
                        </div>
                      </div>
                    </div>
                    {/* Field Jawab 3 */}
                    <div className="form-group row">
                      <h5 className="col-xl-12 col-lg-12 ">
                        3. Apakah atas Rancangan Regulasi Perpajakan Sudah
                        Dilakukan Kajian Keterkaitan dengan Hasil Putusan
                        Pengadilan (PP/MA/MK) ?
                      </h5>
                    </div>
                    <div className="form-group row">
                      <div className="col-lg-9 col-xl-6">
                        <div className="checkbox-inline">
                          {choice.map((data, index) => (
                            <Field
                              name="jawab3"
                              component={Radio}
                              type="radio"
                              value={data.value}
                              key={index}
                              content={data.name}
                              disabled

                            />
                          ))}
                        </div>
                      </div>
                    </div>
                    {/* Field Jawab 4 */}
                    <div className="form-group row">
                      <h5 className="col-xl-12 col-lg-12 ">
                        4. Apakah atas Rancangan Regulasi Perpajakan Sudah
                        Dilakukan Kajian Risk Impack Analisyst dan Cost &
                        Benefit Analysis ?
                      </h5>
                    </div>
                    <div className="form-group row">
                      <div className="col-lg-9 col-xl-6">
                        <div className="checkbox-inline">
                          {choice.map((data, index) => (
                            <Field
                              name="jawab4"
                              component={Radio}
                              type="radio"
                              value={data.value}
                              key={index}
                              content={data.name}
                              disabled

                            />
                          ))}
                        </div>
                      </div>
                    </div>
                    {/* Field Jawab 5 */}
                    <div className="form-group row">
                      <h5 className="col-xl-12 col-lg-12 ">
                        5. Apakah Rancangan Regulasi Perpajakan secara
                        substansi sudah terdapat kesepakatan antara konseptor
                        dengan para pihak terkait ?
                      </h5>
                    </div>
                    <div className="form-group row">
                      <div className="col-lg-9 col-xl-6">
                        <div className="checkbox-inline">
                          {choice.map((data, index) => (
                            <Field
                              name="jawab5"
                              component={Radio}
                              type="radio"
                              value={data.value}
                              key={index}
                              content={data.name}
                              disabled

                            />
                          ))}
                        </div>
                      </div>
                    </div>

                    <button
                      type="submit"
                      style={{ display: "none" }}
                      ref={btnRef}
                      onSubmit={() => handleSubmit()}
                    ></button>
                    {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
                  </Form>
                  ) : (
                    <Form className="form form-label-right">
                    {/* Field Draft */}
                    <div className="form-group row">
                      <h5 className="col-xl-12 col-lg-12 ">
                        Apakah Draft terkait penyusunan RPMK ?
                      </h5>
                    </div>
                    <div className="form-group row">
                      <div className="col-lg-9 col-xl-6">
                        <div className="checkbox-inline">
                          {draftChoice.map((data, index) => (
                            <Field
                              name="draft"
                              component={Radio}
                              type="radio"
                              value={data.value}
                              key={index}
                              content={data.name}
                            />
                          ))}
                        </div>
                      </div>
                    </div>
                    {/* Field Jawab 1 */}
                    <div className="form-group row">
                      <h5 className="col-xl-12 col-lg-12 ">
                        1. Apakah atas Rancangan Regulasi Perpajakan Sudah
                        Dilakukan Harmonisasi Vertikal ?
                      </h5>
                    </div>
                    <div className="form-group row">
                      <div className="col-lg-9 col-xl-6">
                        <div className="checkbox-inline">
                          {choice.map((data, index) => (
                            <Field
                              name="jawab1"
                              component={Radio}
                              type="radio"
                              value={data.value}
                              key={index}
                              content={data.name}
                            />
                          ))}
                        </div>
                      </div>
                    </div>
                    {/* Field Jawab 2 */}
                    <div className="form-group row">
                      <h5 className="col-xl-12 col-lg-12 ">
                        2. Apakah atas Rancangan Regulasi Perpajakan Sudah
                        Dilakukan Harmonisasi Horisontal ?
                      </h5>
                    </div>
                    <div className="form-group row">
                      <div className="col-lg-9 col-xl-6">
                        <div className="checkbox-inline">
                          {choice.map((data, index) => (
                            <Field
                              name="jawab2"
                              component={Radio}
                              type="radio"
                              value={data.value}
                              key={index}
                              content={data.name}
                            />
                          ))}
                        </div>
                      </div>
                    </div>
                    {/* Field Jawab 3 */}
                    <div className="form-group row">
                      <h5 className="col-xl-12 col-lg-12 ">
                        3. Apakah atas Rancangan Regulasi Perpajakan Sudah
                        Dilakukan Kajian Keterkaitan dengan Hasil Putusan
                        Pengadilan (PP/MA/MK) ?
                      </h5>
                    </div>
                    <div className="form-group row">
                      <div className="col-lg-9 col-xl-6">
                        <div className="checkbox-inline">
                          {choice.map((data, index) => (
                            <Field
                              name="jawab3"
                              component={Radio}
                              type="radio"
                              value={data.value}
                              key={index}
                              content={data.name}
                            />
                          ))}
                        </div>
                      </div>
                    </div>
                    {/* Field Jawab 4 */}
                    <div className="form-group row">
                      <h5 className="col-xl-12 col-lg-12 ">
                        4. Apakah atas Rancangan Regulasi Perpajakan Sudah
                        Dilakukan Kajian Risk Impack Analisyst dan Cost &
                        Benefit Analysis ?
                      </h5>
                    </div>
                    <div className="form-group row">
                      <div className="col-lg-9 col-xl-6">
                        <div className="checkbox-inline">
                          {choice.map((data, index) => (
                            <Field
                              name="jawab4"
                              component={Radio}
                              type="radio"
                              value={data.value}
                              key={index}
                              content={data.name}
                            />
                          ))}
                        </div>
                      </div>
                    </div>
                    {/* Field Jawab 5 */}
                    <div className="form-group row">
                      <h5 className="col-xl-12 col-lg-12 ">
                        5. Apakah Rancangan Regulasi Perpajakan secara
                        substansi sudah terdapat kesepakatan antara konseptor
                        dengan para pihak terkait ?
                      </h5>
                    </div>
                    <div className="form-group row">
                      <div className="col-lg-9 col-xl-6">
                        <div className="checkbox-inline">
                          {choice.map((data, index) => (
                            <Field
                              name="jawab5"
                              component={Radio}
                              type="radio"
                              value={data.value}
                              key={index}
                              content={data.name}
                            />
                          ))}
                        </div>
                      </div>
                    </div>

                    <button
                      type="submit"
                      style={{ display: "none" }}
                      ref={btnRef}
                      onSubmit={() => handleSubmit()}
                    ></button>
                    {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
                  </Form>
                  )}
                    
                  </>
                );
              }}
            </Formik>
          </>
        </CardBody>
        <CardFooter style={{ borderTop: "none" }}>
          <>
            <div className="col-lg-12" style={{ textAlign: "right" }}>
              <button
                type="button"
                onClick={handleBack}
                className="btn btn-light"
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                }}
              >
                <i className="fa fa-arrow-left"></i>
                Kembali
              </button>
              {`  `}

              {status !== 'Proses Penyusunan' ? null : (  <button
                type="submit"
                className="btn btn-success ml-2"
                onClick={saveForm}
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                }}
                // disabled={disabled}
              >
                <i className="fas fa-save"></i>
                Simpan
              </button>)}
            
            </div>
          </>
        </CardFooter>
      </Card>
    </>
  );
}

export default PengajuanRancanganHasilAnalisis;
