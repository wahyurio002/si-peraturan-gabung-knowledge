import React, { useState, useEffect } from "react";
import { useHistory, Route } from "react-router-dom";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import * as columnFormatters from "../../../../helpers/column-formatters";
import {
  getAnalisisHarmon,
  getDraftComposingById,
  getPenyusunanById,
  updateStatusPenyusunan
} from "../../../Evaluation/Api";
import PengajuanRancanganDraftOpen from "./PengajuanRancanganDraftOpen";
import PengajuanRancanganKajianOpen from "./PengajuanRancanganKajianOpen";
import ProposalOpen from "../../proposal/ProposalOpen";

function PengajuanRancanganDraft({ id }) {
  const history = useHistory();
  const [draft, setDraft] = useState([]);

  const openAnalisis = id_harmon => {
    history.push(
      `/compose/process/detils/${id}/pengajuan-rancangan/${id_harmon}/hasil-analisis-harmonisasi`
    );
  };

  const openDraft = id_draft => {
    history.push(
      `/compose/process/detils/${id}/pengajuan-rancangan/${id_draft}/draft`
    );
  };
  const openProcess = () => {
    history.push(`/compose/process/detils/${id}/pengajuan-rancangan/kajian`);
  };
  const showStatus = () => {
    history.push(`/compose/process/detils/${id}/pengajuan-rancangan/status`);
  };
  const applyDraft = () => {
    getPenyusunanById(id).then(({ data }) => {
      updateStatusPenyusunan(id, 14, data.kd_kantor).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/dashboard");
            history.replace("/compose/proposal");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace(`/compose/proposal/${id}/process`);
          });
        }
      });
    });
  };
  useEffect(() => {
    getDraftComposingById(id).then(({ data }) => {
      data.map(dt => {
        getPenyusunanById(dt.id_penyusunan).then(({ data }) => {
          getAnalisisHarmon(data.id_penyusunan).then(data_1 => {
            setDraft(draft => [
              ...draft,
              {
                id_draftperaturan: dt.id_draftperaturan,
                jns_draft: dt.jns_draft,
                file_kajian: data.update_file_kajian,
                status: data.status,
                id_harmon:
                  data_1.data.length > 0
                    ? data_1.data[0].id_analisis_harmon
                    : null
              }
            ]);
          });
        });
      });
    });
  }, [id]);

  const columns = [
    {
      dataField: "draft_peraturan",
      text: "Draft Peraturan",
      sort: true,
      formatter:
        columnFormatters.FileColumnFormatterComposeProcessTindakLanjutDirektoratDraftPeraturan,
      formatExtraData: {
        openDraft: openDraft
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3"
    },
    {
      dataField: "hasil_analisis",
      text: "Hasil Analisis Harmonisasi",
      sort: true,
      formatter:
        columnFormatters.FileColumnFormatterComposeProcessTindakLanjutDirektoratHasilAnalisis,
      formatExtraData: {
        openAnalisis: openAnalisis
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        textAlign: "center"
      }
    },
    {
      dataField: "kajian",
      text: "Kajian",
      formatter:
        columnFormatters.ActionsColumnFormatterComposeProcessTindakLanjutDirektoratKajian,
      formatExtraData: {
        openProcess: openProcess
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      formatter:
        columnFormatters.StatusColumnFormatterComposeProcessTindakLanjutDirektoratKajian,
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3"
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        columnFormatters.ActionsColumnFormatterComposeProcessPengajuanRancanganKajianAksi,
      formatExtraData: {
        openProcess: applyDraft,
        showStatus: showStatus
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  const emptyDataMessage = () => {
    return "No Data";
  };

  return (
    <>
      <>
        <BootstrapTable
          wrapperClasses="table-responsive"
          bordered={false}
          headerWrapperClasses="thead-light"
          classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
          keyField="id_draftperaturan"
          data={draft}
          columns={columns}
          bootstrap4
          noDataIndication={emptyDataMessage}
        ></BootstrapTable>
      </>
      {/*Route to Open Draft*/}
      <Route path="/compose/process/detils/:id/pengajuan-rancangan/:id_draft/draft">
        {({ history, match }) => (
          <PengajuanRancanganDraftOpen
            show={match != null}
            id={match && match.params.id}
            id_draft={match && match.params.id_draft}
            onHide={() => {
              history.push(`/compose/process/detils/${id}/pengajuan-rancangan`);
            }}
            onRef={() => {
              history.push(`/compose/process/detils/${id}/pengajuan-rancangan`);
            }}
          />
        )}
      </Route>
      {/*Route to Open Kajian*/}
      <Route path="/compose/process/detils/:id/pengajuan-rancangan/kajian">
        {({ history, match }) => {
          if (match) {
            return (
              <PengajuanRancanganKajianOpen
                show={match != null}
                id={match && match.params.id}
                onHide={() => {
                  history.push(
                    `/compose/process/detils/${id}/pengajuan-rancangan`
                  );
                }}
                onRef={() => {
                  history.push(
                    `/compose/process/detils/${id}/pengajuan-rancangan`
                  );
                }}
              />
            );
          }
          return <></>;
        }}
      </Route>
      {/*Route to Open Status*/}
      <Route path="/compose/process/detils/:id/pengajuan-rancangan/status">
        {({ history, match }) => (
          <ProposalOpen
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push(`/compose/process/detils/${id}/pengajuan-rancangan`);
            }}
            onRef={() => {
              history.push(`/compose/process/detils/${id}/pengajuan-rancangan`);
            }}
          />
        )}
      </Route>
    </>
  );
}

export default PengajuanRancanganDraft;
