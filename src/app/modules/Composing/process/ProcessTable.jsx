/* Library */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
/* Utility */
import {getPenyusunan} from "../../Evaluation/Api"


function ProcessTable() {
  const history = useHistory();
  const [content, setContent] = useState([]);
  const detil = id => history.push(`/compose/process/${id}/detil`);
  useEffect(() => {
    getPenyusunan().then(({data})=> {
      data.map(dt => {
        return dt.status !== 'Draft' &&  dt.status !== 'Eselon 4' && dt.status !== 'Terima' ? setContent(content => [...content, dt]) : null
      })
    })
  }, [])



  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "no_penyusunan",
      text: "No Penyusunan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_penyusunan",
      text: "Tgl Penyusunan",
      sort: true,
      formatter: columnFormatters.DateFormatterComposeProposal,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "jns_peraturan",
      text: "Jenis Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "judul_peraturan",
      text: "Judul Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    // {
    //   dataField: "draft_peraturan",
    //   text: "Draft Peraturan",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   formatter: columnFormatters.FileColumnFormatterComposeProcess,
    //   headerSortingClasses
    // },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterComposeProcessModule,
      formatExtraData: {
        openDialog: detil
        // openEditDialog: edit,
        // openDeleteDialog: deleteAction,
        // showDialog: open
        // showReject: reject,
        // applyProposal: applyProposal
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_penyusunan",
    pageNumber: 1,
    pageSize: 5
  };
  const defaultSorted = [{ dataField: "no_penyusunan", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  //   useEffect(() => {
  //     getPerencanaan().then(({ data }) => {
  //       data.map(data => {
  //         return data.status !== 'Terima' ? setPlan(plan => [...plan, data]) : null;
  //       })
  //     });
  //   }, []);

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_penyusunan"
                  data={content}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      {/* <Route path="/plan/proposal/:id/reject">
        {({ history, match }) => (
          <ProposalReject
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/plan/proposal");
            }}
            onRef={() => {
              history.push("/plan/proposal");
            }}
          />
        )}
      </Route> */}
      {/* <Route path="/compose/research/:id/open">
        {({ history, match }) => (
          <ResearchOpen
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/compose/research");
            }}
            onRef={() => {
              history.push("/compose/research");
            }}
          />
        )}
      </Route> */}
    </>
  );
}

export default ProcessTable;
