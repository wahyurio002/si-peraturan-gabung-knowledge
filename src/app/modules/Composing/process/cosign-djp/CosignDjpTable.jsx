/* Library */
import React, { useEffect, useState } from "react";
import swal from "sweetalert";
import { useSelector } from "react-redux";
import { useHistory, Route } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import * as columnFormatters from "../../../../helpers/column-formatters";
import { deleteCosignUnit, getCosignUnit } from "../../../Evaluation/Api";
import CosignDjpOpen from "./CosignDjpOpen";
// import ResearchOpen from "./ResearchOpen";

/* Component */
// import ProposalOpen from "./ProposalOpen";
// import ProposalReject from "./ProposalReject";

/* Utility */
function CosignDjpTable({ id }) {
  const history = useHistory();
  const [content, setContent] = useState([]);
  const { status } = useSelector(state => state.auth);

  const add = () =>
    history.push(`/compose/process/detils/${id}/cosign-djp/add`);
  const editAction = id_cosign =>
    history.push(`/compose/process/detils/${id}/cosign-djp/${id_cosign}/edit`);
  const showDetil = id_cosign_unit =>
    history.push(
      `/compose/process/detils/${id}/cosign-djp/${id_cosign_unit}/open`
    );

  const deleteAction = id_cosign_unit => {
    swal({
      title: "Apakah Anda Yakin?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
        deleteCosignUnit(id_cosign_unit).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace(`/compose/process/detils/${id}/cosign-djp`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/dashboard");
              history.replace(`/compose/process/detils/${id}/cosign-djp`);
            });
          }
        });
      }
      // else {
      //   swal("Your imaginary file is safe!");
      // }
    });
  };

  useEffect(() => {
    getCosignUnit(id).then(({ data }) => {
      setContent(data);
    });
  }, [id]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "nama_unit",
      text: "Nama Unit",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_surat",
      text: "Tgl Jawaban",
      sort: true,
      formatter: columnFormatters.DateFormatterComposeProcessCosignDjp,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        status === "Proses Penyusunan"
          ? columnFormatters.ActionsColumnFormatterComposeProcessCosignDjp
          : columnFormatters.ActionsColumnFormatterComposeProcessCosignDjpJustView,
      formatExtraData: {
        editAction: editAction,
        deleteAction: deleteAction,
        showDetil: showDetil
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "nama_unit",
    pageNumber: 1,
    pageSize: 5
  };
  const defaultSorted = [{ dataField: "nama_unit", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_cosign_unit"
                  data={content}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div
                          className="col-lg-6 col-xl-6 mb-3"
                          style={{ textAlign: "right" }}
                        >
                          {status === "Proses Penyusunan" ? (
                            <button
                              type="button"
                              className="btn btn-primary mx-3"
                              style={{
                                float: "right"
                              }}
                              onClick={add}
                            >
                              <i className="fa fa-plus"></i>
                              Tambah Cosign
                            </button>
                          ) : null}
                          <button
                            type="button"
                            className="btn btn-light mx3"
                            style={{
                              float: "right"
                            }}
                            onClick={() =>
                              history.push(`/compose/process/${id}/detil`)
                            }
                          >
                            <i className="fa fa-arrow-left"></i>
                            Kembali
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/compose/process/detils/:id/cosign-djp/:id_cosign_unit/open">
        {({ history, match }) => (
          <CosignDjpOpen
            show={match != null}
            id={match && match.params.id}
            id_cosign_unit={match && match.params.id_cosign_unit}
            after={false}
            onHide={() => {
              history.push(`/compose/process/detils/${id}/cosign-djp`);
            }}
            onRef={() => {
              history.push(`/compose/process/detils/${id}/cosign-djp`);
            }}
          />
        )}
      </Route>
    </>
  );
}

export default CosignDjpTable;
