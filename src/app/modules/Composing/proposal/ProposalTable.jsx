/* Library */
import React, { useEffect, useState } from "react";
import { useHistory, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import { deletePenyusunan, getPenyusunan } from "../../Evaluation/Api";
import ProposalOpen from "./ProposalOpen";

/* Component */
// import ProposalOpen from "./ProposalOpen";
// import ProposalReject from "./ProposalReject";

/* Utility */



function ProposalTable() {
  const history = useHistory();
  const { user } = useSelector(state => state.auth);
  const [proposal, setProposal] = useState([]);


  const add = () => history.push("/compose/proposal/add");
  const open = id => history.push(`/compose/proposal/${id}/open`);
  const reject = id => history.push(`/compose/proposal/${id}/reject`);
  const edit = id => history.push(`/compose/proposal/${id}/edit`);
  const process = id => history.push(`/compose/proposal/${id}/process`);

  const deleteAction = id_penyusunan => {
    swal({
      title: "Apakah Anda Yakin?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
        deletePenyusunan(id_penyusunan).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace(`/compose/proposal`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/dashboard");
              history.replace(`/compose/proposal`);
            });
          }
        });
      }
      // else {
      //   swal("Your imaginary file is safe!");
      // }
    });
  };
  // const deleteAction = id => {
  //   swal({
  //     title: "Apakah Anda Yakin?",
  //     text: "Klik OK untuk melanjutkan",
  //     icon: "warning",
  //     buttons: true,
  //     dangerMode: true
  //   }).then(willDelete => {
  //     if (willDelete) {
  //       deletePerencanaan(id).then(({ status }) => {
  //         if (status === 200) {
  //           swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
  //             history.push("/dashboard");
  //             history.replace("/plan/proposal");
  //           });
  //         } else {
  //           swal("Gagal", "Data gagal disimpan", "error").then(() => {
  //             history.push("/dashboard");
  //             history.replace("/plan/proposal");
  //           });
  //         }
  //       });
  //     }
  //     // else {
  //     //   swal("Your imaginary file is safe!");
  //     // }
  //   });
  // };

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "no_perencanan",
      text: "No Perencanaan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_penyusunan",
      text: "Tgl Penyusunan",
      sort: true,
      formatter: columnFormatters.DateFormatterComposeProposal,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "jns_peraturan",
      text: "Jenis Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "judul_peraturan",
      text: "Judul Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusColumnFormatterComposeProposal,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterComposeProposal,
      formatExtraData: {
        openProcess: process,
        openEditDialog: edit,
        showDetil: open,
        openDeleteDialog: deleteAction,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_perencanaan",
    pageNumber: 1,
    pageSize: 5
  };
  const defaultSorted = [{ dataField: "no_perencanaan", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: proposal.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  useEffect(() => {
    getPenyusunan().then(({ data }) => {
      // setProposal(data)
      data.map(data => {
        return data.delete !== 'yes' ? setProposal(proposal => [...proposal, data]) : null;
      })
    });
  }, []);

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_penyusunan"
                  data={proposal}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right"
                            }}
                            onClick={add}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      {/* <Route path="/plan/proposal/:id/reject">
        {({ history, match }) => (
          <ProposalReject
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/plan/proposal");
            }}
            onRef={() => {
              history.push("/plan/proposal");
            }}
          />
        )}
      </Route>
       */}


      <Route path="/compose/proposal/:id/open">
        {({ history, match }) => (
          <ProposalOpen
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/compose/proposal");
            }}
            onRef={() => {
              history.push("/compose/proposal");
            }}
          />
        )}
      </Route>

     
    </>
  );
}

export default ProposalTable;
