import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import "../../../../helpers/DatePickerStyles.css";
import {
  DatePickerField,
  Input,
  Radio,
  Textarea,
  Select as Sel
} from "../../../../helpers";
import CustomFileInput from "../../../../helpers/form/CustomFileInput";
import { getJenisPeraturan } from "../../../Evaluation/Api";

function ProposalEditForm({ proposal, btnRef, saveProposal }) {
  const { user } = useSelector(state => state.auth);
  const [jenisPeraturan, setJenisPeraturan] = useState([]);

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    no_perencanaan: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("No Perencanaan is required"),
    tgl_penyusunan: Yup.mixed()
      .nullable(false)
      .required("Tanggal Penyusunan is required"),
    jns_peraturan: Yup.string().required("Jenis Peraturan is required"),
    judul_peraturan: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Judul Peraturan is required"),
    no_peraturan: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("No Peraturan is required"),
    tentang: Yup.string()
      .min(2, "Minimum 2 symbols")
      .required("Tentang is required"),
    simfoni: Yup.string().required("Simfoni is required"),
    // unit_incharge: Yup.string()
    //   .min(2, "Minimum 2 symbols")
    //   .max(50, "Maximum 50 symbols")
    //   .required("Unit Incharge is required"),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      )
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    getJenisPeraturan().then(({ data }) => {
      setJenisPeraturan(data);
    });
  }, []);

  const simf = [
    { name: "Ya", value: "Ya" },
    { name: "Tidak", value: "Tidak" }
  ];

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        validationSchema={ProposalEditSchema}
        onSubmit={values => {
          // console.log(values);
          saveProposal(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          return (
            <>
              <Form className="form form-label-right">
                {/* Field No Perencanaan */}
                <div className="form-group row">
                  <Field
                    name="no_perencanaan"
                    component={Input}
                    placeholder="No Perencanaan"
                    label="No Perencanaan"
                    disabled
                    solid={"true"}
                  />
                </div>
                {/* FIELD Tanggal Penyusunan */}
                <div className="form-group row">
                  <DatePickerField
                    name="tgl_penyusunan"
                    label="Tanggal Usulan Penyusunan"
                  />
                </div>
                {/* Field Jenis Peraturan */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Jenis Peraturan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <select
                      className='form-control form-control-lg'
                      value={values.jns_peraturan}
                      disabled
                    >
                   <option value="">Pilih Jenis Peraturan</option>
                {jenisPeraturan.map(data => (
                  <option
                    key={data.id_jnsperaturan}
                    value={data.nm_jnsperaturan}
                  >
                    {data.nm_jnsperaturan}
                  </option>
                ))}

                    </select>
                  </div>
                  {/* <Sel name="jns_peraturan" label="Jenis Peraturan" disabled >
                    <option value=''>Pilih Jenis Peraturan</option>
                    {jenisPeraturan.map(data => (
                      <option
                        key={data.id_jnsperaturan}
                        value={data.nm_jnsperaturan}
                      >
                        {data.nm_jnsperaturan}
                      </option>
                    ))}
                  </Sel> */}
                </div>
                {/* Field Judul Peraturan */}
                <div className="form-group row">
                  <Field
                    name="judul_peraturan"
                    component={Input}
                    placeholder="Judul Peraturan"
                    label="Judul Peraturan"
                    disabled
                  />
                </div>
                {/* Field No Peraturan */}
                <div className="form-group row">
                  <Field
                    name="no_peraturan"
                    component={Input}
                    placeholder="Nomor Peraturan"
                    label="No Peraturan"
                  />
                </div>
                {/* Field Tentang */}
                <div className="form-group row">
                  <Field
                    name="tentang"
                    component={Textarea}
                    placeholder="Tentang"
                    label="Tentang"
                    disabled
                  />
                </div>
                {/* Field Simfoni */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Simfoni
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    {simf.map((data, index) => (
                      <Field
                        name="simfoni"
                        component={Radio}
                        type="radio"
                        value={data.value}
                        key={index}
                        content={data.name}
                      />
                    ))}
                  </div>
                </div>
                {/* Field Unit Incharge */}
                <div className="form-group row">
                  <Field
                    name="unit_incharge"
                    component={Input}
                    placeholder="Unit Incharge"
                    label="Unit Incharge"
                    disabled
                    solid={"true"}
                  />
                </div>
                {/* Field Kajian Perencanaan */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    File Kajian Perencanaan
                  </label>
                  <div
                    className="col-lg-9 col-xl-6"
                    style={{ marginTop: "10px" }}
                  >
                    <a href={values.file_upload} target="_blank" rel="noopener noreferrer">
                      {/* {values.file_kajian} */}
                      {values.file_upload.slice(28)}
                    </a>
                  </div>
                </div>
                {/* FIELD UPLOAD FILE */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Upload Kajian Penyusunan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Field
                      name="file"
                      component={CustomFileInput}
                      title="Select a file"
                      label="File"
                      // setFieldValue={setFieldValue}
                      // errorMessage={errors["file"] ? errors["file"] : undefined}
                      // touched={touched["file"]}
                      style={{ display: "flex" }}
                      // onBlur={handleBlur}
                    />
                  </div>
                </div>

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default ProposalEditForm;
