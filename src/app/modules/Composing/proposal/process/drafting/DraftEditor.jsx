import React, { useEffect, useState, useRef, useCallback } from "react";
import axios from "axios";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../../../_metronic/_partials/controls";
import {
  getDraft,
  saveDraftComposing,
  updateDraftComposing,
  uploadFile
} from "../../../../Evaluation/Api";
import EmailEditor from "react-email-editor";
import { RPerDirjen } from "../../../../../references/template_draft/RPerDirjen";
import { RUU } from "../../../../../references/template_draft/RUU";
import { RPP } from "../../../../../references/template_draft/RPP";
import { RPMK } from "../../../../../references/template_draft/RPMK";
import CustomFileInput from "../../../../../helpers/form/CustomFileInput";
import CustomLampiranInput from "../../../../../helpers/form/CustomLampiranInput";

function DraftEditor({
  history,
  match: {
    params: { id, id_draft, name }
  }
}) {
  const [draft, setDraft] = useState("");
  const [show, setShow] = useState(false);
  const [isEditorLoaded, setIsEditorLoaded] = useState(false);
  const [isComponentMounted, setIsComponentMounted] = useState(false);
  const [content, setContent] = useState({
    file: "",
    lampiran: ""
  });
  // const [draftRuu, setDraftRuu] = useState([]);
  // const [draftRpp, setDraftRpp] = useState([]);
  // const [draftRpmk, setDraftRpmk] = useState([]);
  // const [draftRper, setDraftRper] = useState([]);

  const validationSchema = Yup.object().shape({
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      )
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    // setIsComponentMounted(true);
    // loadTemplate();
    if (id_draft) {
      axios.get(`/draftperaturan/byidpenyusunan/${id}`, {
        transformResponse: res => {
          setDraft(JSON.parse(res[0].body_draft));
          return res;
        },
        responseType: "json"
      });
      // .then(({ data }) => {
      //   console.log(JSON.parse(data[0].body_draft));
      // });
      getDraft(id_draft).then(({ data }) => {
        setContent({
          jns_draft: data.jns_draft,
          file_upload: data.fileupload,
          lampiran: data.filelampiranr
        });
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id, id_draft]);

  const emailEditorRef = useRef(null);

  const onLoad = () => {
    setTimeout(() => {
      if (!id_draft) {
        try {
          switch (name) {
            case "RPP":
              emailEditorRef.current.editor.loadDesign(RPP);
              break;
            case "RPMK":
              emailEditorRef.current.editor.loadDesign(RPMK);
              break;
            case "RUU":
              emailEditorRef.current.editor.loadDesign(RUU);
              break;
            case "RperDirjen":
              emailEditorRef.current.editor.loadDesign(RPerDirjen);
              break;

            default:
              break;
          }
        } catch (error) {
          swal(error);
        }
      } else {
        emailEditorRef.current.editor.loadDesign(draft);
      }
    }, 2000);
  };

  const onReady = () => {
    // editor is ready
    setShow(true);
  };

  const saveDraft = values => {
    if (!id_draft) {
      const formData = new FormData();
      formData.append("file", values.file);
      uploadFile(formData).then(data_1 => {
        const formLampiran = new FormData();
        formLampiran.append("file", values.lampiran);
        uploadFile(formLampiran).then(data_2 => {
          emailEditorRef.current.editor.saveDesign(data => {
            const design = JSON.stringify(data);
            saveDraftComposing(
              design,
              name,
              id,
              data_1.data.message,
              data_2.data.message
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Data berhasil disimpan", "success").then(
                  () => {
                    history.push(`/compose/proposal/${id}/process`);
                  }
                );
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.push(`/compose/proposal/draft/${id}/add/${name}`);
                });
              }
            });
          });
        });
      });
    } else {
      if (values.file.name) {
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFile(formData).then(data_1 => {
          if (!values.lampiran.name) {
            emailEditorRef.current.editor.saveDesign(data => {
              const design = JSON.stringify(data);
              updateDraftComposing(design, id_draft, data_1.data.message).then(
                ({ status }) => {
                  if (status === 201 || status === 200) {
                    swal("Berhasil", "Data berhasil disimpan", "success").then(
                      () => {
                        history.push(`/compose/proposal/${id}/process`);
                      }
                    );
                  } else {
                    swal("Gagal", "Data gagal disimpan", "error").then(() => {
                      history.push(`/compose/proposal/draft/${id}/add/${name}`);
                    });
                  }
                }
              );
            });
          } else {
            const formLampiran = new FormData();
            formLampiran.append("file", values.lampiran);
            uploadFile(formLampiran).then(data_2 => {
              emailEditorRef.current.editor.saveDesign(data => {
                const design = JSON.stringify(data);
                updateDraftComposing(
                  design,
                  id_draft,
                  data_1.data.message,
                  data_2.data.message
                ).then(({ status }) => {
                  if (status === 201 || status === 200) {
                    swal("Berhasil", "Data berhasil disimpan", "success").then(
                      () => {
                        history.push(`/compose/proposal/${id}/process`);
                      }
                    );
                  } else {
                    swal("Gagal", "Data gagal disimpan", "error").then(() => {
                      history.push(`/compose/proposal/draft/${id}/add/${name}`);
                    });
                  }
                });
              });
            });
          }
        });
      } else if (values.lampiran.name) {
        const formLampiran = new FormData();
        formLampiran.append("file", values.lampiran);
        uploadFile(formLampiran).then(data_1 => {
          if (!values.file.name) {
            emailEditorRef.current.editor.saveDesign(data => {
              const design = JSON.stringify(data);
              updateDraftComposing(
                design,
                id_draft,
                values.file_upload,
                data_1.data.message
              ).then(({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Data berhasil disimpan", "success").then(
                    () => {
                      history.push(`/compose/proposal/${id}/process`);
                    }
                  );
                } else {
                  swal("Gagal", "Data gagal disimpan", "error").then(() => {
                    history.push(`/compose/proposal/draft/${id}/add/${name}`);
                  });
                }
              });
            });
          } else {
            const formData = new FormData();
            formData.append("file", values.file);
            uploadFile(formData).then(data_2 => {
              emailEditorRef.current.editor.saveDesign(data => {
                const design = JSON.stringify(data);
                updateDraftComposing(
                  design,
                  id_draft,
                  data_2.data.message,
                  data_1.data.message
                ).then(({ status }) => {
                  if (status === 201 || status === 200) {
                    swal("Berhasil", "Data berhasil disimpan", "success").then(
                      () => {
                        history.push(`/compose/proposal/${id}/process`);
                      }
                    );
                  } else {
                    swal("Gagal", "Data gagal disimpan", "error").then(() => {
                      history.push(`/compose/proposal/draft/${id}/add/${name}`);
                    });
                  }
                });
              });
            });
          }
        });
      } else {
        emailEditorRef.current.editor.saveDesign(data => {
          const design = JSON.stringify(data);
          updateDraftComposing(
            design,
            id_draft,
            values.file_upload,
            values.lampiran
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push(`/compose/proposal/${id}/process`);
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push(`/compose/proposal/draft/${id}/add/${name}`);
              });
            }
          });
        });
      }
    }
  };
  const handleBack = () => {
    history.push(`/compose/proposal/${id}/process`);
  };

  return (
    <Card>
      <CardHeader
        title="Buat Draft Peraturan"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="col-lg-12 mb-4" style={{ textAlign: "right" }}>
            <button
              type="button"
              className="btn btn-light-primary ml-2"
              onClick={onLoad}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              <i className="fa fa-redo"></i>
              Reload
            </button>
          </div>
          <div className="row">
            <EmailEditor
              ref={emailEditorRef}
              options={{
                features: {
                  textEditor: {
                    fontSizes: ["70px", "60px", "50px"]
                  }
                },
                fonts: {
                  showDefaultFonts: true,
                  customFonts: [
                    {
                      label: "Comic Sans",
                      value: "'Comic Sans MS', cursive, sans-serif"
                    },
                    // {
                    //   label: "DotGothic16",
                    //   value: "'DotGothic16',cursive",
                    //   url: "https://fonts.googleapis.com/css?family=DotGothic16"
                    // },
                    {
                      label: "Book Antiqua",
                      value: "book antiqua,palatino"
                    }
                  ]
                }
              }}
              style={{ height: "1000px" }}
              onReady={onReady}
              onLoad={onLoad}
            />
          </div>
          <div className="mt-4">
            <Formik
              enableReinitialize={true}
              initialValues={content}
              validationSchema={validationSchema}
              onSubmit={values => {
                saveDraft(values);
                // console.log(values);
              }}
            >
              {({ handleSubmit }) => {
                return (
                  <Form className="form form-label-right">
                    {show ? (
                      <>
                        <div className="form-group row">
                          <label className="col-xl-3 col-lg-3 col-form-label">
                            Upload File
                          </label>
                          <div className="col-lg-9 col-xl-6">
                            <Field
                              name="file"
                              component={CustomFileInput}
                              title="Select a file"
                              label="File"
                              style={{ display: "flex" }}
                            />
                          </div>
                        </div>
                      </>
                    ) : null}
                    <div className="form-group row">
                      <label className="col-xl-3 col-lg-3 col-form-label">
                        Upload Lampiran
                      </label>
                      <div className="col-lg-9 col-xl-6">
                        <Field
                          name="lampiran"
                          component={CustomLampiranInput}
                          title="Select a file"
                          label="Lampiran"
                          style={{ display: "flex" }}
                        />
                      </div>
                    </div>
                    <div className="col-lg-12" style={{ textAlign: "right" }}>
                      <button
                        type="button"
                        onClick={handleBack}
                        className="btn btn-light"
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                        }}
                      >
                        <i className="fa fa-arrow-left"></i>
                        Kembali
                      </button>
                      {`  `}
                      <button
                        type="submit"
                        className="btn btn-success ml-2"
                        onSubmit={() => handleSubmit()}
                        style={{
                          boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                        }}
                      >
                        <i className="fas fa-save"></i>
                        Simpan
                      </button>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          </div>
        </>
      </CardBody>
    </Card>
  );
}

export default DraftEditor;
