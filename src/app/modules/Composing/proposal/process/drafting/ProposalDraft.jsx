import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

import BootstrapTable from "react-bootstrap-table-next";
import * as columnFormatters from "../../../../../helpers/column-formatters";
import {
  getDraftComposingById,
  getPenyusunanById,
  updateStatusPenyusunan
} from "../../../../Evaluation/Api";
import swal from "sweetalert";

function ProposalDraft({ id }) {
  const history = useHistory();
  const [draft, setDraft] = useState([]);

  const addDraft = () => history.push(`/compose/proposal/draft/${id}/add`);
  const edit = (id_draft, jns_draft) =>
    history.push(`/compose/proposal/draft/${id}/edit/${id_draft}/${jns_draft}`);

  const editTolak = (id_draft, jns_draft) =>
    history.push(`/compose/proposal/draft/${id}/edit-tolak/${id_draft}/${jns_draft}`);

  const applyProposal = () => {
    getPenyusunanById(id).then(({ data }) => {
      updateStatusPenyusunan(id, 2, data.kd_kantor).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/dashboard");
            history.replace("/compose/proposal");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace(`/compose/proposal/${id}/process`);
          });
        }
      });
    });
  };

  const columns = [
    {
      dataField: "jns_draft",
      text: "Jenis",
      sort: true,
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3"
    },
    {
      dataField: "file_kajian",
      text: "Kajian",
      sort: true,
      formatter: columnFormatters.FileColumnFormatterComposeDrafting,
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        textAlign: "center"
      }
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      formatter: columnFormatters.StatusColumnFormatterComposeDrafting,
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3"
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterComposeDrafting,
      formatExtraData: {
        openEditDialog: edit,
        openEditTolakDialog: editTolak,
        // openDeleteDialog: deleteAction,
        applyProposal: applyProposal
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  useEffect(() => {
    getDraftComposingById(id).then(({ data }) => {
      data.map(dt => {
        getPenyusunanById(dt.id_penyusunan).then(({ data }) => {
          setDraft(draft => [
            ...draft,
            {
              id_draftperaturan: dt.id_draftperaturan,
              jns_draft: dt.jns_draft,
              file_kajian: data.update_file_kajian,
              status: data.status
            }
          ]);
        });
      });
    });
  }, [id]);

  const emptyDataMessage = () => {
    return (
      <div className="col-lg-6 col-xl-6 mb-3">
        <button
          type="button"
          className="btn btn-light-primary"
          style={{ float: "right" }}
          onClick={addDraft}
        >
          Tambah Draft
        </button>
      </div>
    );
  };

  return (
    <>
      <div className="row">
        <div
          className="col-lg-12 col-xl-12 mb-3"
          style={{ textAlign: "right" }}
        >
          <button
            type="button"
            className="btn btn-light-success"
            style={{
              float: "right"
            }}
            onClick={() => history.push(`/compose/proposal`)}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
        </div>
      </div>
      <BootstrapTable
        wrapperClasses="table-responsive"
        bordered={false}
        headerWrapperClasses="thead-light"
        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
        keyField="id_draftperaturan"
        data={draft}
        columns={columns}
        bootstrap4
        noDataIndication={emptyDataMessage}
      ></BootstrapTable>
    </>
  );
}

export default ProposalDraft;
