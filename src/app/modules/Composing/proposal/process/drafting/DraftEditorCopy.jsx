import React, { useEffect, useState } from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import swal from "sweetalert";
import { Textarea, Input } from "../../../../../helpers";
import CustomFileInput from "../../../../../helpers/form/CustomFileInput";
import CustomLampiranInput from "../../../../../helpers/form/CustomLampiranInput";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../../../_metronic/_partials/controls";
import {
  getDraft,
  saveDraftComposing,
  updateDraftComposing,
  uploadFile,
  getPenyusunanById,
  updateStatusPenyusunan
} from "../../../../Evaluation/Api";

function DraftEditor({
  history,
  match: {
    params: { id, id_draft, name }
  }
}) {
  const [content, setContent] = useState({
    body_content: "",
    jns_draft: name,
    file: "",
    lampiran: ""
  });

  const validationSchema = Yup.object().shape({
    body_content: Yup.string()
      .min(2, "Minimum 2 Characters")
      .required("Content is required"),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      )
    // lampiran: Yup.mixed()
    //   .required("A file is required")
    //   .test(
    //     "fileSize",
    //     "File too large",
    //     value => value && value.size <= FILE_SIZE
    //   )
    //   .test(
    //     "fileFormat",
    //     "Unsupported Format",
    //     value => value && SUPPORTED_FORMATS.includes(value.type)
    //   )
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    if (id_draft) {
      getDraft(id_draft).then(({ data }) => {
        setContent({
          body_content: data.body_draft,
          jns_draft: data.jns_draft,
          file_upload: data.fileupload,
          lampiran: data.filelampiranr
        });
      });
    }
  }, [id_draft]);

  const saveDraft = values => {
    if (!id_draft) {
      const formData = new FormData();
      formData.append("file", values.file);
      uploadFile(formData).then(({ data }) => {
        const formLampiran = new FormData();
        formLampiran.append("file", values.lampiran);
        uploadFile(formLampiran).then(data_2 => {
          // console.log(`ini file ${data.message}`)
          // console.log(`ini lampiran ${data_2.data.message}`)
          saveDraftComposing(
            values.body_content,
            values.jns_draft,
            id,
            data.message,
            data_2.data.message
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push(`/compose/proposal/${id}/process`);
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push(`/compose/proposal/draft/${id}/add/${name}`);
              });
            }
          });
        });
      });
      // saveDraftComposing(values.body_content, values.jns_draft, id, ).then(
      //   ({ status }) => {
      //     if (status === 201 || status === 200) {
      //       swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
      //         history.push(`/compose/proposal/${id}/process`);
      //       });
      //     } else {
      //       swal("Gagal", "Data gagal disimpan", "error").then(() => {
      //         history.push(`/compose/proposal/draft/${id}/add/${name}`);
      //       });
      //     }
      //   }
      // );
    } else {
      if (values.file.name) {
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFile(formData).then(({ data }) => {
          if (!values.lampiran.name) {
            updateDraftComposing(
              values.body_content,
              id_draft,
              data.message
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                // updateStatus();

                swal("Berhasil", "Data berhasil disimpan", "success").then(
                  () => {
                    history.push(`/compose/proposal/${id}/process`);
                  }
                );
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.push(`/compose/proposal/draft/${id}/add/${name}`);
                });
              }
            });
          } else {
            const formLampiran = new FormData();
            formLampiran.append("file", values.lampiran);
            uploadFile(formLampiran).then(data_2 => {
              updateDraftComposing(
                values.body_content,
                id_draft,
                data.message,
                data_2.data.message
              ).then(({ status }) => {
                if (status === 201 || status === 200) {
                  // updateStatus();

                  swal("Berhasil", "Data berhasil disimpan", "success").then(
                    () => {
                      history.push(`/compose/proposal/${id}/process`);
                    }
                  );
                } else {
                  swal("Gagal", "Data gagal disimpan", "error").then(() => {
                    history.push(`/compose/proposal/draft/${id}/add/${name}`);
                  });
                }
              });
            });
          }
        });
      } else if (values.lampiran.name) {
        const formLampiran = new FormData();
        formLampiran.append("file", values.lampiran);
        uploadFile(formLampiran).then(({ data }) => {
          if (!values.file.name) {
            updateDraftComposing(
              values.body_content,
              id_draft,
              values.file_upload,
              data.message
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                // updateStatus();

                swal("Berhasil", "Data berhasil disimpan", "success").then(
                  () => {
                    history.push(`/compose/proposal/${id}/process`);
                  }
                );
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.push(`/compose/proposal/draft/${id}/add/${name}`);
                });
              }
            });
          } else {
            const formData = new FormData();
            formData.append("file", values.file);
            uploadFile(formData).then(data_2 => {
              updateDraftComposing(
                values.body_content,
                id_draft,
                data_2.data.message,
                data.message
              ).then(({ status }) => {
                if (status === 201 || status === 200) {
                  // updateStatus();
                  swal("Berhasil", "Data berhasil disimpan", "success").then(
                    () => {
                      history.push(`/compose/proposal/${id}/process`);
                    }
                  );
                } else {
                  swal("Gagal", "Data gagal disimpan", "error").then(() => {
                    history.push(`/compose/proposal/draft/${id}/add/${name}`);
                  });
                }
              });
            });
          }
        });
      } else {
        updateDraftComposing(
          values.body_content,
          id_draft,
          values.file_upload,
          values.lampiran
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            // updateStatus();
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push(`/compose/proposal/${id}/process`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(`/compose/proposal/draft/${id}/add/${name}`);
            });
          }
        });
      }
    }
  };

  // const updateStatus = () => {
  //   getPenyusunanById(id).then(({ data }) => {
  //     updateStatusPenyusunan(id, 1, data.kd_kantor).then(({ status }) => {
  //       if (status === 201 || status === 200) {
  //         swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
  //           history.push("/dashboard");
  //           history.replace(`/compose/proposal/${id}/process`);
  //         });
  //       } else {
  //         swal("Gagal", "Data gagal disimpan", "error").then(() => {
  //           history.push("/dashboard");
  //           history.replace(`/compose/proposal/draft/${id}/add/${name}`);
  //         });
  //       }
  //     });
  //   });
  // };

  return (
    <Card>
      <CardHeader
        title="Buat Draft Peraturan"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <Formik
            enableReinitialize={true}
            initialValues={content}
            validationSchema={validationSchema}
            onSubmit={values => {
              saveDraft(values);
              // console.log(values);
            }}
          >
            {({ handleSubmit, values }) => {
              return (
                <Form className="form form-label-right">
                  {/* FIELD PERIHAL */}
                  <div className="form-group row">
                    <Field
                      name="body_content"
                      component={Textarea}
                      placeholder="Content . . . ."
                      full="full"
                    />
                  </div>

                  {values.body_content ? (
                    <>
                      <div className="form-group row">
                        <label className="col-xl-3 col-lg-3 col-form-label">
                          Upload File
                        </label>
                        <div className="col-lg-9 col-xl-6">
                          <Field
                            name="file"
                            component={CustomFileInput}
                            title="Select a file"
                            label="File"
                            style={{ display: "flex" }}
                          />
                        </div>
                      </div>
                    </>
                  ) : null}
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Upload Lampiran
                    </label>
                    <div className="col-lg-9 col-xl-6">
                      <Field
                        name="lampiran"
                        component={CustomLampiranInput}
                        title="Select a file"
                        label="Lampiran"
                        style={{ display: "flex" }}
                      />
                    </div>
                  </div>
                  <div className="col-lg-12" style={{ textAlign: "right" }}>
                    <button
                      type="submit"
                      className="btn btn-success ml-2"
                      onSubmit={() => handleSubmit()}
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="fas fa-save"></i>
                      Simpan
                    </button>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </>
      </CardBody>
    </Card>
  );
}

export default DraftEditor;
