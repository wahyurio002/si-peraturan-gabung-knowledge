import React, {  useMemo } from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../../../_metronic/_partials/controls";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../../../_metronic/_helpers";
import { get, merge } from "lodash";
import {
  setLayoutConfig,
  getInitLayoutConfig,
  useHtmlClassService
} from "../../../../../../_metronic/layout";

import {
  DocumentEditorContainerComponent,
  Toolbar
} from "@syncfusion/ej2-react-documenteditor";
DocumentEditorContainerComponent.Inject(Toolbar);
function DraftEditor() {
  const htmlClassService = useHtmlClassService();

  const Layout = {
    demo: "demo1",
    js: {
      breakpoints: {
        sm: "576",
        md: "768",
        lg: "992",
        xl: "1200",
        xxl: "1200"
      },
      colors: {
        theme: {
          base: {
            white: "#ffffff",
            primary: "#6993FF",
            secondary: "#E5EAEE",
            success: "#1BC5BD",
            info: "#8950FC",
            warning: "#FFA800",
            danger: "#F64E60",
            light: "#F3F6F9",
            dark: "#212121"
          },
          light: {
            white: "#ffffff",
            primary: "#E1E9FF",
            secondary: "#ECF0F3",
            success: "#C9F7F5",
            info: "#EEE5FF",
            warning: "#FFF4DE",
            danger: "#FFE2E5",
            light: "#F3F6F9",
            dark: "#D6D6E0"
          },
          inverse: {
            white: "#ffffff",
            primary: "#ffffff",
            secondary: "#212121",
            success: "#ffffff",
            info: "#ffffff",
            warning: "#ffffff",
            danger: "#ffffff",
            light: "#464E5F",
            dark: "#ffffff"
          }
        },
        gray: {
          gray100: "#F3F6F9",
          gray200: "#ECF0F3",
          gray300: "#E5EAEE",
          gray400: "#D6D6E0",
          gray500: "#B5B5C3",
          gray600: "#80808F",
          gray700: "#464E5F",
          gray800: "#1B283F",
          gray900: "#212121"
        }
      },
      fontFamily: "Poppins"
    },
    loader: {
      enabled: true,
      type: "",
      logo: "/media/logos/logo-dark-sm.png",
      message: "Please wait..."
    },
    toolbar: {
      display: true
    },
    header: {
      self: {
        width: "fluid",
        theme: "dark",
        fixed: {
          desktop: true,
          mobile: true
        }
      },
      menu: {
        self: {
          display: false,
          layout: "default",
          "root-arrow": false,
          "icon-style": "duotone"
        },
        desktop: {
          arrow: true,
          toggle: "click",
          submenu: {
            theme: "light",
            arrow: true
          }
        },
        mobile: {
          submenu: {
            theme: "dark",
            accordion: true
          }
        }
      }
    },
    subheader: {
      display: false,
      displayDesc: false,
      displayDaterangepicker: true,
      layout: "subheader-v1",
      fixed: true,
      width: "fluid",
      clear: false,
      style: "solid"
    },
    content: {
      width: "fluid"
    },
    brand: {
      self: {
        theme: "light"
      }
    },
    aside: {
      self: {
        theme: "light",
        display: false,
        fixed: true,
        minimize: {
          toggle: true,
          default: false,
          hoverable: true
        }
      },
      footer: {
        self: {
          display: true
        }
      },
      menu: {
        dropdown: false,
        scroll: true,
        "icon-style": "duotone",
        submenu: {
          accordion: true,
          dropdown: {
            arrow: true,
            "hover-timeout": 500
          }
        }
      }
    },
    footer: {
      self: {
        fixed: true,
        width: "fluid"
      }
    },
    extras: {
      search: {
        display: true,
        layout: "dropdown",
        offcanvas: {
          direction: "right"
        }
      },
      notifications: {
        display: true,
        layout: "dropdown",
        dropdown: {
          style: "dark"
        },
        offcanvas: {
          directions: "right"
        }
      },
      "quick-actions": {
        display: true,
        layout: "dropdown",
        dropdown: {
          style: "dark"
        },
        offcanvas: {
          directions: "right"
        }
      },
      user: {
        display: true,
        layout: "dropdown",
        dropdown: {
          style: "dark"
        },
        offcanvas: {
          directions: "right"
        }
      },
      languages: {
        display: true
      },
      cart: {
        display: true,
        dropdown: {
          style: "dark"
        }
      },
      "quick-panel": {
        display: true,
        offcanvas: {
          directions: "right"
        }
      },
      chat: {
        display: true
      },
      toolbar: {
        display: true
      },
      scrolltop: {
        display: true
      }
    }
  };

  const Reset = {
    demo: "demo1",
    js: {
      breakpoints: {
        sm: "576",
        md: "768",
        lg: "992",
        xl: "1200",
        xxl: "1200"
      },
      colors: {
        theme: {
          base: {
            white: "#ffffff",
            primary: "#6993FF",
            secondary: "#E5EAEE",
            success: "#1BC5BD",
            info: "#8950FC",
            warning: "#FFA800",
            danger: "#F64E60",
            light: "#F3F6F9",
            dark: "#212121"
          },
          light: {
            white: "#ffffff",
            primary: "#E1E9FF",
            secondary: "#ECF0F3",
            success: "#C9F7F5",
            info: "#EEE5FF",
            warning: "#FFF4DE",
            danger: "#FFE2E5",
            light: "#F3F6F9",
            dark: "#D6D6E0"
          },
          inverse: {
            white: "#ffffff",
            primary: "#ffffff",
            secondary: "#212121",
            success: "#ffffff",
            info: "#ffffff",
            warning: "#ffffff",
            danger: "#ffffff",
            light: "#464E5F",
            dark: "#ffffff"
          }
        },
        gray: {
          gray100: "#F3F6F9",
          gray200: "#ECF0F3",
          gray300: "#E5EAEE",
          gray400: "#D6D6E0",
          gray500: "#B5B5C3",
          gray600: "#80808F",
          gray700: "#464E5F",
          gray800: "#1B283F",
          gray900: "#212121"
        }
      },
      fontFamily: "Poppins"
    },
    loader: {
      enabled: true,
      type: "",
      logo: "/media/logos/logo-dark-sm.png",
      message: "Please wait..."
    },
    toolbar: {
      display: true
    },
    header: {
      self: {
        width: "fluid",
        theme: "dark",
        fixed: {
          desktop: true,
          mobile: true
        }
      },
      menu: {
        self: {
          display: true,
          layout: "default",
          "root-arrow": false,
          "icon-style": "duotone"
        },
        desktop: {
          arrow: true,
          toggle: "click",
          submenu: {
            theme: "light",
            arrow: true
          }
        },
        mobile: {
          submenu: {
            theme: "dark",
            accordion: true
          }
        }
      }
    },
    subheader: {
      display: true,
      displayDesc: false,
      displayDaterangepicker: true,
      layout: "subheader-v1",
      fixed: true,
      width: "fluid",
      clear: false,
      style: "solid"
    },
    content: {
      width: "fluid"
    },
    brand: {
      self: {
        theme: "light"
      }
    },
    aside: {
      self: {
        theme: "light",
        display: true,
        fixed: true,
        minimize: {
          toggle: true,
          default: false,
          hoverable: true
        }
      },
      footer: {
        self: {
          display: true
        }
      },
      menu: {
        dropdown: false,
        scroll: true,
        "icon-style": "duotone",
        submenu: {
          accordion: true,
          dropdown: {
            arrow: true,
            "hover-timeout": 500
          }
        }
      }
    },
    footer: {
      self: {
        fixed: true,
        width: "fluid"
      }
    },
    extras: {
      search: {
        display: true,
        layout: "dropdown",
        offcanvas: {
          direction: "right"
        }
      },
      notifications: {
        display: true,
        layout: "dropdown",
        dropdown: {
          style: "dark"
        },
        offcanvas: {
          directions: "right"
        }
      },
      "quick-actions": {
        display: true,
        layout: "dropdown",
        dropdown: {
          style: "dark"
        },
        offcanvas: {
          directions: "right"
        }
      },
      user: {
        display: true,
        layout: "dropdown",
        dropdown: {
          style: "dark"
        },
        offcanvas: {
          directions: "right"
        }
      },
      languages: {
        display: true
      },
      cart: {
        display: true,
        dropdown: {
          style: "dark"
        }
      },
      "quick-panel": {
        display: true,
        offcanvas: {
          directions: "right"
        }
      },
      chat: {
        display: true
      },
      toolbar: {
        display: true
      },
      scrolltop: {
        display: true
      }
    }
  };

  const changeLayout = () => {
    setLayoutConfig(Layout);
  };

  const reset = () => {
    setLayoutConfig(Reset);
  };
  const initialValues = useMemo(
    () =>
      merge(
        // Fulfill changeable fields.
        getInitLayoutConfig(),
        htmlClassService.config
      ),
    [htmlClassService.config]
  );
  return (
    <Card>
      <CardHeader
        title="Buat Draft Peraturan"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="row">
          {/* <div className="col-lg-6 col-xl-6 mb-3 ">
           
             
          </div> */}
          <div
            className="col-lg-12 col-xl-12 mb-3"
            style={{ textAlign: "right" }}
          >
            {initialValues.aside.self.display ? (
              <button
                type="button"
                className="btn btn-light-primary ml-2"
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  float: "right"
                }}
                onClick={changeLayout}
              >
                <span className="svg-icon menu-icon">
                  <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Size.svg")}
                  />
                </span>
                Maximize
              </button>
            ) : (
              <button
                type="button"
                className="btn btn-light-primary ml-2"
                style={{
                  boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                  float: "right"
                }}
                onClick={reset}
              >
                <span className="svg-icon menu-icon">
                  <SVG
                    src={toAbsoluteUrl("/media/svg/icons/General/Scale.svg")}
                  />
                </span>
                Minimize
              </button>
            )}
             <button
              type="button"
              className="btn btn-success ml-2"
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                float: "right"
              }}
              // onClick={changeLayout}
            >
              <span className="svg-icon menu-icon">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Files/Upload.svg")} />
              </span>
              Upload Lampiran
            </button>
            <button
              type="button"
              className="btn btn-success "
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                float: "right"
              }}
              // onClick={changeLayout}
            >
              <span className="svg-icon menu-icon">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Files/Upload.svg")} />
              </span>
              Upload File
            </button>
           
          </div>
        </div>
        <DocumentEditorContainerComponent
          id="container"
          height={"1000px"}
          //   serviceUrl="https://ej2services.syncfusion.com/production/web-services/api/documenteditor/"
          enableToolbar={true}
        />
      </CardBody>
    </Card>
  );
}

export default DraftEditor;
