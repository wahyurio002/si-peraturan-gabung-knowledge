import React, { useState, useEffect } from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { DatePickerField, Input, Select as Sel } from "../../../../../helpers";
import Select from "react-select";
import { unitKerja } from "../../../../../references/UnitKerja";
import CustomFileInput from "../../../../../helpers/form/CustomFileInput";
import { getSurat } from "../../../../Evaluation/Api";

function DetilLhrFormAdd({ initValues, btnRef, saveLhr, id_penyusunan }) {
  const [kantor, setKantor] = useState([]);
  const [surat, setSurat] = useState([]);
  const [valSurat, setValSurat] = useState();

  useEffect(() => {
    unitKerja.map(data => {
      setKantor(kantor => [
        ...kantor,
        {
          label: data.nm_UNIT_KERJA,
          value: data.kd_unit_kerja,
          alamat: data.alamat
        }
      ]);
    });
  }, []);

  useEffect(() => {
    getSurat(id_penyusunan).then(({ data }) => {
      data.map(data => {
        setSurat(surat => [
          ...surat,
          { label: data.nomor_surat, value: data.id_surat }
        ]);
      });
    });
  }, [id_penyusunan]);

  useEffect(() => {
    if (initValues.no_surat) {
      surat
        .filter(data => data.value === initValues.no_surat)
        .map(data => {
          setValSurat(data.label);
        });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const LhrEditSchema = Yup.object().shape({
    no_surat_lhr: Yup.string()
      .min(1, "Minimum 1 symbols")
      .max(50, "Maximum 50 symbols")
      .required("No LHR is required"),
    no_surat: Yup.number().required("No Surat Undangan is required"),
    tgl_rapat: Yup.mixed()
      .nullable(false)
      .required("Tanggal Rapat is required"),
    tmpt_bahas: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Tempat Pembahasan is required"),
    unit_penyelenggara: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Unit Penyelenggara Penerbit is required"),
    jenis_instansi: Yup.number().required("Jenis Instansi is required"),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      )
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  const asosiasi = () => {
    return (
      <>
        <div className="form-group row">
          <Field
            name="unit_penyelenggara"
            component={Input}
            placeholder="Unit Penyelenggara Rapat"
            label="Unit Penyelenggara Rapat"
          />
        </div>
      </>
    );
  };
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initValues}
        validationSchema={LhrEditSchema}
        onSubmit={values => {
        //   console.log(values);
            saveLhr(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          const handleChangeUnit = val => {
            setFieldValue("unit_penyelenggara", val.label);
          };

          const handleChangeSurat = val => {
            setFieldValue("no_surat", val.value);
            setValSurat(val.label);
          };
          return (
            <>
              <Form className="form form-label-right">
                <div className="form-group row">
                  {/* FIELD TANGGAL SURAT */}
                  <DatePickerField name="tgl_rapat" label="Tanggal Rapat" />
                </div>
                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    No Surat Undangan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <Select
                      options={surat}
                      onChange={value => handleChangeSurat(value)}
                      value={surat.filter(data => data.label === valSurat)}
                    />
                  </div>
                </div>
                {/* FIELD PERIHAL */}
                <div className="form-group row">
                  <Field
                    name="tmpt_bahas"
                    component={Input}
                    placeholder="Tempat Pembahasan"
                    label="Tempat Pembahasan"
                  />
                </div>
                <div className="form-group row">
                  {/* FIELD NO LHR */}
                  <Field
                    name="no_surat_lhr"
                    component={Input}
                    placeholder="No Surat LHR"
                    label="No Surat LHR"
                  />
                </div>
                {/* FIELD UNIT PENYELENGGARA */}
                <div className="form-group row">
                  <Sel name="jenis_instansi" label="Jenis Instansi">
                    <option>Pilih Jenis Instansi</option>
                    <option value="1">Asosiasi / KL</option>
                    <option value="2">Unit DJP</option>
                  </Sel>
                </div>
                {values.jenis_instansi ? (
                  <>
                    {values.jenis_instansi === "1" ? (
                      asosiasi()
                    ) : (
                      <div className="form-group row">
                        <label className="col-xl-3 col-lg-3 col-form-label">
                          Unit Penyelenggara Rapat
                        </label>
                        <div className="col-lg-9 col-xl-6">
                          <Select
                            options={kantor}
                            onChange={value => handleChangeUnit(value)}
                            value={kantor.filter(
                              data => data.label === values.unit_penyelenggara
                            )}
                          />
                        </div>
                      </div>
                    )}
                  </>
                ) : null}

                {values.no_surat_lhr ? (
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Upload LHR
                    </label>
                    <div className="col-lg-9 col-xl-6">
                      <Field
                        name="file"
                        component={CustomFileInput}
                        title="Select a file"
                        label="File"
                        style={{ display: "flex" }}
                      />
                    </div>
                  </div>
                ) : null}
                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default DetilLhrFormAdd;
