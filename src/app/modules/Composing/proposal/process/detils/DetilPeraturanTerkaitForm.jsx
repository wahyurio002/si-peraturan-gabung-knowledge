import React from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { DatePickerField, Input, Textarea } from "../../../../../helpers";

function DetilPeraturanTerkaitForm({ initValues, btnRef, savePeraturan }) {
  const PerterEditSchema = Yup.object().shape({
    nomor_peraturan: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("No Peraturan is required"),
    tgl_ditetapkan: Yup.mixed()
      .nullable(false)
      .required("Tanggal Ditetapkan is required"),
    tgl_diundangkan: Yup.mixed()
      .nullable(false)
      .required("Tanggal Diundangkan is required"),
    perihal: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Perihal is required")
  });

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initValues}
        validationSchema={PerterEditSchema}
        onSubmit={values => {
          savePeraturan(values);
          // console.log(values)
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          handleChange,
          errors,
          touched,
          values,
          isValid
        }) => {
          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    name="nomor_peraturan"
                    component={Input}
                    placeholder="No Peraturan"
                    label="No Peraturan"
                    disabled
                  />
                </div>
                {/* FIELD TANGGAL DITETAPKAN */}
                <div className="form-group row">
                  <DatePickerField
                    name="tgl_ditetapkan"
                    label="Tanggal Ditetapkan"
                  />
                </div>
                {/* FIELD TANGGAL DIUNDANGKAN */}
                <div className="form-group row">
                  <DatePickerField
                    name="tgl_diundangkan"
                    label="Tanggal Diundangkan"
                  />
                </div>
                {/* FIELD PERIHAL */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                  />
                </div>
                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default DetilPeraturanTerkaitForm;
