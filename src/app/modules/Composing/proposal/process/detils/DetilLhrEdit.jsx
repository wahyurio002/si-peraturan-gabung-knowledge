import React, { useEffect, useState, useRef } from "react";

import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../../_metronic/layout";
import DetilLhrForm from "./DetilLhrForm";
import DetilLhrFooter from "./DetilLhrFooter";
import {
  uploadFile,
  saveLhr,
  updateLhr,
  getLhrById
} from "../../../../Evaluation/Api";

import swal from "sweetalert";
import DetilLhrTablePokok from "./DetilLhrTablePokok";
import DetilLhrFormAdd from "./DetilLhrFormAdd";

function DetilLhrEdit({
  history,
  match: {
    params: { id_lhr, id }
  }
}) {
  const initValues = {
    no_surat_lhr: "",
    no_surat: "",
    tgl_rapat: "",
    tmpt_bahas: "",
    unit_penyelenggara: "",
    file: "",
    jenis_instansi: "",
    status: "draft"
  };

  const [title, setTitle] = useState("");
  const [content, setContent] = useState();
  const suhbeader = useSubheader();

  useEffect(() => {
    let _title = id_lhr ? "Edit LHR" : "Tambah LHR";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    if (id_lhr) {
      getLhrById(id_lhr).then(({ data }) => {
        setContent({
          no_surat_lhr: data.no_surat_lhr,
          tmpt_bahas: data.tmpt_bahas,
          no_surat: data.id_surat,
          unit_penyelenggara: data.unit_penyelenggara,
          file_upload: data.file_lhr,
          tgl_rapat: data.tgl_rapat,
          jenis_instansi: data.jenis_instansi,
          status: data.status
        });
      });
    }
  }, [id_lhr, suhbeader]);
  const btnRef = useRef();

  const handleBack = () => {
    history.push(`/compose/proposal/process/detils/${id}/lhr`);
  };

  const saveFormLhr = values => {
    if (!id_lhr) {
      const formData = new FormData();
      formData.append("file", values.file);
      uploadFile(formData).then(({ data }) => {
        saveLhr(
          id,
          values.no_surat,
          values.no_surat_lhr,
          values.tgl_rapat,
          values.tmpt_bahas,
          values.unit_penyelenggara,
          data.message,
          values.jenis_instansi,
          values.status
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push(`/compose/proposal/process/detils/${id}/lhr`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(`/compose/proposal/process/detils/${id}/lhr/add`);
            });
          }
        });
      });
    } else {
      if (values.file.name) {
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFile(formData).then(({ data }) => {
          updateLhr(
            id_lhr,
            id,
            values.no_surat,
            values.no_surat_lhr,
            values.tgl_rapat,
            values.tmpt_bahas,
            values.unit_penyelenggara,
            data.message,
            values.jenis_instansi,
            values.status
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push(`/compose/proposal/process/detils/${id}/lhr`);
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push(`/compose/proposal/process/detils/${id}/lhr/add`);
              });
            }
          });
        });
      } else {
        updateLhr(
          id_lhr,
          id,
          values.no_surat,
          values.no_surat_lhr,
          values.tgl_rapat,
          values.tmpt_bahas,
          values.unit_penyelenggara,
          values.file_upload,
          values.jenis_instansi,
          values.status
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push(`/compose/proposal/process/detils/${id}/lhr`);
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push(`/compose/proposal/process/detils/${id}/lhr/add`);
            });
          }
        });
      }
    }
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          {id_lhr ? (
            <DetilLhrForm
              initValues={content || initValues}
              btnRef={btnRef}
              saveLhr={saveFormLhr}
              id_penyusunan={id}
            />
          ) : (
            <DetilLhrFormAdd
              initValues={initValues}
              btnRef={btnRef}
              saveLhr={saveFormLhr}
              id_penyusunan={id}
            />
          )}
          {/* <DetilLhrForm
            initValues={content || initValues}
            btnRef={btnRef}
            saveLhr={saveFormLhr}
            id_penyusunan={id}
          /> */}
          {id_lhr ? <DetilLhrTablePokok id={id} id_lhr={id_lhr} /> : null}
        </>
      </CardBody>

      <CardFooter style={{ borderTop: "none" }}>
        <DetilLhrFooter btnRef={btnRef} backAction={handleBack} />
      </CardFooter>
    </Card>
  );
}

export default DetilLhrEdit;
