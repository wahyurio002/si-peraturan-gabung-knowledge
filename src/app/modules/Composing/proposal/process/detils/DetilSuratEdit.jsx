import React, { useEffect, useState, useRef } from "react";
import { useSelector } from "react-redux";


import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../../_metronic/layout";
import DetilSuratForm from "./DetilSuratForm";
import DetilSuratFooter from "./DetilSuratFooter";
import {
  getSuratByIdSurat,
  saveSuratUndangan,
  updateSuratUndangan
} from "../../../../Evaluation/Api";
import swal from "sweetalert";

function DetilSuratEdit({
  history,
  match: {
    params: { id_surat, id }
  }
}) {
  const [content, setContent] = useState();
  const { user } = useSelector(state => state.auth);

  const initValues = {
    nomor_surat: "",
    tgl_surat: "",
    perihal: "",
    unit_penerbit: user.unit,
    unit_penerima: "",
    id_jenis: "",
    jenis_instansi:"",
    status: "draft"
  };
  const [title, setTitle] = useState("");
  const suhbeader = useSubheader();

  useEffect(() => {
    let _title = id_surat ? "Edit Surat Menyurat" : "Tambah Surat Menyurat";

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id_surat, suhbeader]);

  useEffect(() => {
    if (id_surat) {
      getSuratByIdSurat(id_surat).then(({ data }) => {
        setContent(data);
      });
    }
  }, [id_surat]);
  const btnRef = useRef();

  const handleBack = () => {
    history.push(`/compose/proposal/process/detils/${id}/surat`);
  };

  const saveSurat = values => {
    if (!id_surat) {
      saveSuratUndangan(
        id,
        values.nomor_surat,
        values.perihal,
        values.tgl_surat,
        values.unit_penerbit,
        values.unit_penerima,
        values.id_jenis,
        values.jenis_instansi,
        values.status
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push(`/compose/proposal/process/detils/${id}/surat`);
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push(`/compose/proposal/process/detils/${id}/surat/add`);
          });
        }
      });
    } else {
      updateSuratUndangan(
        id_surat,
        id,
        values.nomor_surat,
        values.perihal,
        values.tgl_surat,
        values.unit_penerbit,
        values.unit_penerima,
        values.id_jenis,
        values.jenis_instansi,
        values.status
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push(`/compose/proposal/process/detils/${id}/surat`);
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push(`/compose/proposal/process/detils/${id}/surat/add`);
          });
        }
      });
    }
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <DetilSuratForm
            initValues={content || initValues}
            btnRef={btnRef}
            saveSurat={saveSurat}
          />
        </>
      </CardBody>

      <CardFooter style={{ borderTop: "none" }}>
        <DetilSuratFooter btnRef={btnRef} backAction={handleBack} />
      </CardFooter>
    </Card>
  );
}

export default DetilSuratEdit;
