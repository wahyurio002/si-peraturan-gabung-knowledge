import React, { useEffect, useState, useRef } from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import EmailEditor from "react-email-editor";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../../_metronic/_partials/controls";
import {
  getDraft,
  saveDraftComposing,
  updateDraftComposing,
  uploadFile
} from "../../../Evaluation/Api";
import axios from "axios";

function ResearchDraftEditor({
  history,
  match: {
    params: { id, id_draft, name }
  }
}) {
  const [draft, setDraft] = useState("");
  const [content, setContent] = useState({
    body_content: "",
    jns_draft: name,
    file: "",
    lampiran: ""
  });

  const validationSchema = Yup.object().shape({
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      )
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    if (id_draft) {
      axios.get(`/draftperaturan/byidpenyusunan/${id}`, {
        transformResponse: res => {
          setDraft(JSON.parse(res[0].body_draft));
          return res;
        },
        responseType: "json"
      });
      getDraft(id_draft).then(({ data }) => {
        setContent({
          body_content: data.body_draft,
          jns_draft: data.jns_draft,
          file_upload: data.fileupload,
          lampiran: data.filelampiranr
        });
      });
    }
  }, [id_draft, id]);
  const emailEditorRef = useRef(null);
  const onLoad = () => {
    setTimeout(() => {
      emailEditorRef.current.editor.showPreview(
        emailEditorRef.current.editor.loadDesign(draft)
      );
    }, 2000);
  };

  return (
    <Card>
      <CardHeader
        title="Lihat Draft Peraturan"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="col-lg-12 mb-4" style={{ textAlign: "right" }}>
            <button
              type="button"
              className="btn btn-light-primary ml-2"
              // onSubmit={() => handleSubmit()}
              onClick={onLoad}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              <i className="fa fa-redo"></i>
              Reload
            </button>
          </div>
          <div className="row">
            <EmailEditor
              ref={emailEditorRef}
              options={{
                features: {
                  textEditor: {
                    fontSizes: ["70px", "60px", "50px"]
                  }
                },
                fonts: {
                  showDefaultFonts: true,
                  customFonts: [
                    {
                      label: "Comic Sans",
                      value: "'Comic Sans MS', cursive, sans-serif"
                    },
                    {
                      label: "Book Antiqua",
                      value: "book antiqua,palatino"
                    }
                  ]
                }
              }}
              style={{ height: "1000px" }}
              onLoad={onLoad}
            />
          </div>
          <Formik
            enableReinitialize={true}
            initialValues={content}
            validationSchema={validationSchema}
            onSubmit={values => {}}
          >
            {({ values }) => {
              return (
                <Form className="form form-label-right">
                  {/* Field File */}
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      File Tersimpan
                    </label>
                    <div
                      className="col-lg-9 col-xl-6"
                      style={{ marginTop: "10px" }}
                    >
                      <a
                        href={values.file_upload}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {values.file_upload
                          ? values.file_upload.slice(28)
                          : null}
                      </a>
                    </div>
                  </div>
                  {/* Field Lampiran */}
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Lampiran Tersimpan
                    </label>
                    <div
                      className="col-lg-9 col-xl-6"
                      style={{ marginTop: "10px" }}
                    >
                      <a
                        href={values.lampiran}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {values.lampiran ? values.lampiran.slice(28) : null}
                      </a>
                    </div>
                  </div>

                  <div className="col-lg-12" style={{ textAlign: "right" }}>
                    <button
                      type="button"
                      className="btn btn-light-success ml-2"
                      // onSubmit={() => handleSubmit()}
                      onClick={() =>
                        history.push(`/compose/research/${id}/view`)
                      }
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="fa fa-arrow-left"></i>
                      Kembali
                    </button>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </>
      </CardBody>
    </Card>
  );
}

export default ResearchDraftEditor;
