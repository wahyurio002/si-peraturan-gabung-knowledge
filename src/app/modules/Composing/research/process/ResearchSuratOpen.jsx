import React,{useEffect, useState} from "react";
import { Modal } from "react-bootstrap";
import { DateFormat } from "../../../../helpers/DateFormat";
import {getSuratByIdSurat} from "../../../Evaluation/Api"


function ResearchSuratOpen({ id, id_surat, show, onHide }) {

  const [data, setData] = useState([]);


useEffect(()=> {
  if(id_surat){
    getSuratByIdSurat(id_surat).then(({data})=> {
      setData(data)
    })

  }
},[id_surat])

  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Detil Surat
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Nomor Surat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
            {`: ${data.nomor_surat}`}
            {/* : No.1/PJ.12/2020 */}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Perihal</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
            {`: ${data.perihal}`}
            {/* : 15/12/2020 */}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Tgl Surat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
            {`: ${DateFormat(data.tgl_surat)}`}
            {/* : 15/12/2020 */}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Unit Penerbit</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
                {`: ${data.unit_penerbit}`}
            {/* : PP */}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Unit Penerima</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
                {`: ${data.unit_penerima}`}
            {/* : Peraturan 1 */}
            </span>
          </div>
        </div>
       
      </Modal.Body>
      <Modal.Footer style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tutup
          </button>
        </div>
      </Modal.Footer> 
      
    </Modal>
  );
}

export default ResearchSuratOpen;
