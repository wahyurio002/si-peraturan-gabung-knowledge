import React, { useState, useEffect } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import { Route, useHistory } from "react-router-dom";

//Helpers
import {
  sortCaret,
  headerSortingClasses
} from "../../../../../_metronic/_helpers";
import { Pagination } from "../../../../helpers/pagination/Pagination";
import * as columnFormatters from "../../../../helpers/column-formatters";
import { getLhrDetil } from "../../../Evaluation/Api";
// import DetilLhrTablePokokOpen from "./DetilLhrTablePokokOpen";

function ResearchLhrOpenTable({ id, id_lhr }) {
  const history = useHistory();
  const [content, setContent] = useState([]);

  useEffect(() => {
    getLhrDetil(id_lhr).then(({ data }) => {
      setContent(data);
    });
  }, [id_lhr]);

  const add = () => {
    history.push(
      `/compose/proposal/process/detils/${id}/lhr/${id_lhr}/edit/pokok`
    );
  };
  const edit = id_lhr_detil => {
    history.push(`/compose/proposal/process/detils/${id}/lhr/${id_lhr}/edit/${id_lhr_detil}/pokok`);
  };

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "pokok_pengaturan",
      text: "Pokok Pengaturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "jenis",
      text: "Jenis",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    // {
    //   dataField: "action",
    //   text: "Aksi",
    //   formatter: columnFormatters.ActionsColumnFormatterComposeDetilLhrPokok,
    //   formatExtraData: {
    //     openEditDialog: edit
    //     // openDeleteDialog: deleteAction,
    //   },
    //   classes: "text-center pr-0",
    //   headerClasses: "text-center pr-3",
    //   style: {
    //     minWidth: "100px"
    //   }
    // }
  ];

  const emptyDataMessage = () => {
    return <div className="text-center">No Data to Display</div>;
  };
  const { SearchBar } = Search;
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id_lhr_detil",
    pageNumber: 1,
    pageSize: 50
  };
  const defaultSorted = [{ dataField: "id_lhr_detil", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_lhr_detil"
                  data={content}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-12 col-xl-12 mb-3">
                          <h4> Pokok Pengaturan Pembahasan</h4>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default ResearchLhrOpenTable;
