import React, { useState, useEffect } from "react";
import { useHistory, Route } from "react-router-dom";

import BootstrapTable from "react-bootstrap-table-next";
import * as columnFormatters from "../../../../helpers/column-formatters";
import {
  getDraftComposingById,
  getPenyusunanById,
  updateStatusPenyusunan
} from "../../../Evaluation/Api";
import swal from "sweetalert";
import ResearchReject from "../ResearchReject";

function ResearchDraft({ id }) {
  const history = useHistory();
  const [draft, setDraft] = useState([]);

  const showDraft = (id_draft, jns_draft) =>
    history.push(`/compose/research/${id}/view/draft/${id_draft}/${jns_draft}`);

  const applyProposal = () => {
    getPenyusunanById(id).then(({ data }) => {
      updateStatusPenyusunan(id, 8, data.kd_kantor).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/dashboard");
            history.replace("/compose/proposal");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace(`/compose/proposal/${id}/process`);
          });
        }
      });
    });
  };

  const rejectProposal = () => {
    history.push(`/compose/research/${id}/view/reject`);
  };

  const handleReject = (val) => {
    getPenyusunanById(id).then(({ data }) => {
      updateStatusPenyusunan(id, 5, data.kd_kantor, val.alasan_penolakan).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/dashboard");
            history.replace("/compose/proposal");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace(`/compose/proposal/${id}/process`);
          });
        }
      });
    });
  }

  const columns = [
    {
      dataField: "jns_draft",
      text: "Jenis",
      sort: true,
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3"
    },
    {
      dataField: "file_kajian",
      text: "Kajian",
      sort: true,
      formatter: columnFormatters.FileColumnFormatterComposeDrafting,
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        textAlign: "center"
      }
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      formatter: columnFormatters.StatusColumnFormatterComposeDrafting,
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3"
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterComposeDrafting,
      formatExtraData: {
        showDraft: showDraft,
        rejectProposal: rejectProposal,
        applyProposal: applyProposal
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  useEffect(() => {
    getDraftComposingById(id).then(({ data }) => {
      data.map(dt => {
        getPenyusunanById(dt.id_penyusunan).then(({ data }) => {
          setDraft(draft => [
            ...draft,
            {
              id_draftperaturan: dt.id_draftperaturan,
              jns_draft: dt.jns_draft,
              file_kajian: data.update_file_kajian,
              status: data.status
            }
          ]);
        });
      });
    });
  }, [id]);

  return (
    <>
      <>
        <div className="row">
          <div
            className="col-lg-12 col-xl-12 mb-3"
            style={{ textAlign: "right" }}
          >
            <button
              type="button"
              className="btn btn-light-success"
              style={{
                float: "right"
              }}
              onClick={() => history.push(`/compose/research`)}
            >
              <i className="fa fa-arrow-left"></i>
              Kembali
            </button>
          </div>
        </div>
        <BootstrapTable
          wrapperClasses="table-responsive"
          bordered={false}
          headerWrapperClasses="thead-light"
          classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
          keyField="id_draftperaturan"
          data={draft}
          columns={columns}
          bootstrap4
        ></BootstrapTable>
      </>
      <Route path="/compose/research/:id/view/reject">
        {({ history, match }) => (
          <ResearchReject
            show={match != null}
            id={match && match.params.id}
            reject={handleReject}
            onHide={() => {
              history.push(`/compose/research/${id}/view`);
            }}
            onRef={() => {
              history.push(`/compose/research/${id}/view`);
            }}
          />
        )}
      </Route>
    </>
  );
}

export default ResearchDraft;
