import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import { getLhrById, getSuratByIdSurat } from "../../../Evaluation/Api";
import { DateFormat } from "../../../../helpers/DateFormat";
import ResearchLhrOpenTable from "./ResearchLhrOpenTable";

function ResearchLhrOpen({
  history,
  match: {
    params: { id, id_lhr }
  }
}) {
  const [detil, setDetil] = useState([]);
  const [surat, setSurat] = useState([]);

  useEffect(() => {
    if (id_lhr) {
      getLhrById(id_lhr).then(({ data }) => {
        setDetil(data);
        getSuratByIdSurat(data.id_surat).then(data => {
          setSurat(data.data);
        });
      });
    }
  }, [id_lhr]);

  const backAction = () => {
    history.push(`/compose/research/${id}/view/lhr`);
  }
  return (
    <Card>
      <CardHeader
        title="Detil LHR"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="row">
            <div className="col-xl-6 col-lg-6 mb-3">
              <div className="row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  Tanggal Rapat
                </label>
                <div className="col-lg-9 col-xl-6">
                  <p className="text-muted pt-2">
                    {`: ${DateFormat(detil.tgl_rapat)}`}
                  </p>
                </div>
              </div>
            </div>
            <div className="col-xl-6 col-lg-6 mb-3">
              <div className="row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  No Surat Undangan
                </label>
                <div className="col-lg-9 col-xl-6">
                  <p className="text-muted pt-2">{`: ${surat.nomor_surat}`}</p>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-xl-6 col-lg-6 mb-3">
              <div className="row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  Tempat Pembahasan
                </label>
                <div className="col-lg-9 col-xl-6">
                  <p className="text-muted pt-2">{`: ${detil.tmpt_bahas}`}</p>
                </div>
              </div>
            </div>
            <div className="col-xl-6 col-lg-6 mb-3">
              <div className="row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  No Surat LHR
                </label>
                <div className="col-lg-9 col-xl-6">
                  <p className="text-muted pt-2">{`: ${detil.no_surat_lhr}`}</p>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-xl-6 col-lg-6 mb-3">
              <div className="row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  Unit Penyelenggara Rapat
                </label>
                <div className="col-lg-9 col-xl-6">
                  <p className="text-muted pt-2">
                    {`: ${detil.unit_penyelenggara}`}
                  </p>
                </div>
              </div>
            </div>
            <div className="col-xl-6 col-lg-6 mb-3">
              <div className="row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  File LHR
                </label>
                <div
                  className="col-lg-9 col-xl-6"
                  style={{ marginTop: "10px" }}
                >
                  <a
                    href={detil.file_lhr}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {detil.file_lhr ? `: ${detil.file_lhr.slice(28)}` : null}
                  </a>
                </div>
              </div>
            </div>
          </div>
        </>
        <ResearchLhrOpenTable id={id}  id_lhr={id_lhr}/>
      </CardBody>

      <CardFooter style={{ borderTop: "none" }} >
      <div className="col-lg-12" style={{ textAlign: "right" }}>
          <button
            type="button"
            onClick={backAction}
            className="btn btn-light-success"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fa fa-arrow-left"></i>
            Kembali
          </button>
          
        </div>
      </CardFooter>
    </Card>
  );
}

export default ResearchLhrOpen;
