/* Library */
import React, { useEffect, useState } from "react";
import { useHistory, Route } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import { getPenyusunan } from "../../Evaluation/Api";
import * as auth from "../../Auth/_redux/authRedux";
import { connect } from "react-redux";

/* Component */

/* Utility */

function ReProcessTable({setStatus}) {
  const history = useHistory();
  const [content, setContent] = useState([]);
  const open = (id, status) => {
    setStatus(status)
    switch (status) {
      case "Penelitian Cosign Unit/KL Es4":
        // history.push(`/compose/reprocess/${id}/detil`);
        history.push(
          `/compose/reprocess/detils/${id}/tindak-lanjut-direktorat`
        );
        break;
      case "Penelitian Cosign Sahli":
        history.push(`/compose/reprocess/detils/${id}/tindak-lanjut-sahli`);
        break;
      case "Penelitian Cosign Dirjen":
        history.push(`/compose/reprocess/detils/${id}/pengajuan-rancangan`);
        break;

      default:
        break;
    }
  };

  useEffect(() => {
    getPenyusunan().then(({ data }) => {
      data.map(dt => {
        return dt.status === "Penelitian Cosign Unit/KL Es4" ||
          dt.status === "Penelitian Cosign Sahli" ||
          dt.status === "Penelitian Cosign Dirjen"
          ? setContent(content => [...content, dt])
          : null;
      });
    });
  }, []);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "no_penyusunan",
      text: "No Penyusunan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_penyusunan",
      text: "Tgl Penyusunan",
      sort: true,
      formatter: columnFormatters.DateFormatterComposeReProcess,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "jns_peraturan",
      text: "Jenis Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "judul_peraturan",
      text: "Judul Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    // {
    //   dataField: "draft_peraturan",
    //   text: "Draft Peraturan",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   formatter: columnFormatters.FileColumnFormatterComposeReProcess,
    //   headerSortingClasses
    // },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterComposeReProcess,
      formatExtraData: {
        openDialog: open
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_penyusunan",
    pageNumber: 1,
    pageSize: 5
  };
  const defaultSorted = [{ dataField: "no_penyusunan", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: content.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_penyusunan"
                  data={content}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
    </>
  );
}

export default connect(null, auth.actions)(ReProcessTable);
