import React, { useState, useEffect } from "react";
import { useHistory, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import BootstrapTable from "react-bootstrap-table-next";
import * as columnFormatters from "../../../../helpers/column-formatters";
import { getCosignSahli } from "../../../Evaluation/Api";
import CosignSahliDraftOpen from "./TindakLanjutSahliCosignOpen";
import TindakLanjutSahliCosignOpen from "./TindakLanjutSahliCosignOpen";
import TindakLanjutSahliCosignOpenReprocess from "./TindakLanjutSahliCosignOpen";

function TindakLanjutSahliCosignReprocess({ id }) {
  const history = useHistory();
  const [content, setContent] = useState([]);
  const { status } = useSelector(state => state.auth);

  const editAction = id_cosign_sahli => {
    history.push(
      `/compose/reprocess/detils/${id}/tindak-lanjut-sahli/${id_cosign_sahli}/edit`
    );
  };

  const addCosign = () => {
    history.push(`/compose/reprocess/detils/${id}/tindak-lanjut-sahli/add`);
  };

  const showDetil = id_cosign => {
    history.push(
      `/compose/reprocess/detils/${id}/tindak-lanjut-sahli/${id_cosign}/cosign/open`
    );
  };

  useEffect(() => {
    getCosignSahli(id).then(({ data }) => {
      setContent(data);
    });
  }, [id]);

  const columns = [
    {
      dataField: "nama_unit",
      text: "Nama Unit",
      sort: true
    },
    {
      dataField: "tgl_surat",
      text: "Tanggal Surat",
      formatter: columnFormatters.DateFormatterComposeProcessCosignDjp,
      sort: true
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        status === "Cosign Sahli"
          ? columnFormatters.ActionsColumnFormatterComposeProcessCosignSahli
          : columnFormatters.ActionsColumnFormatterComposeProcessCosignSahliJustView,
      formatExtraData: {
        editAction: editAction,
        showDetil: showDetil
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  const emptyDataMessage = () => {
    return (
      <div className="col-lg-6 col-xl-6 mb-3">
        <button
          type="button"
          className="btn btn-light-primary"
          style={{ float: "right" }}
          onClick={addCosign}
        >
          Tambah Cosign
        </button>
      </div>
    );
  };

  return (
    <>
      <>
        <div className="row">
          <div className="col-lg-12 col-xl-12 mb-3 mt-3">
            <h4> Cosign Sahli</h4>
          </div>
        </div>
        <BootstrapTable
          wrapperClasses="table-responsive"
          bordered={false}
          headerWrapperClasses="thead-light"
          classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
          keyField="id_cosign_sahli"
          data={content}
          columns={columns}
          bootstrap4
          noDataIndication={emptyDataMessage}
        ></BootstrapTable>
      </>
      <Route path="/compose/reprocess/detils/:id/tindak-lanjut-sahli/:id_cosign/cosign/open">
        {({ history, match }) => (
          <TindakLanjutSahliCosignOpenReprocess
            show={match != null}
            id={match && match.params.id}
            id_cosign={match && match.params.id_cosign}
            after={false}
            onHide={() => {
              history.push(
                `/compose/reprocess/detils/${id}/tindak-lanjut-sahli`
              );
            }}
            onRef={() => {
              history.push(
                `/compose/reprocess/detils/${id}/tindak-lanjut-sahli`
              );
            }}
          />
        )}
      </Route>
    </>
  );
}

export default TindakLanjutSahliCosignReprocess;
