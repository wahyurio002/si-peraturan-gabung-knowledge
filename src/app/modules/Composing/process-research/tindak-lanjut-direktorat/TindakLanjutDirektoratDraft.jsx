import React, { useState, useEffect } from "react";
import { useHistory, Route } from "react-router-dom";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import * as columnFormatters from "../../../../helpers/column-formatters";
import {
  getAnalisisHarmon,
  getDraftComposingById,
  getPenyusunanById,
  updateStatusPenyusunan
} from "../../../Evaluation/Api";
import TindakLanjutDirektoratKajianOpen from "../../process/tindak-lanjut-direktorat/TindakLanjutDirektoratKajianOpen";
import ProposalOpen from "../../proposal/ProposalOpen"
import TindakLanjutDirektoratDraftOpenReprocess from "./TindakLanjutDirektoratDraftOpen";
import TindakLanjutDirektoratReject from "./TindakLanjutDirektoratReject"

function TindakLanjutDirektoratDraftReprocess({ id }) {
  const history = useHistory();
  const [draft, setDraft] = useState([]);

  const openAnalisis = (id_harmon) => {
    if(id_harmon){
      history.push(`/compose/reprocess/detils/${id}/tindak-lanjut-direktorat/${id_harmon}/hasil-analisis-harmonisasi`);
    }else{
      history.push(`/compose/reprocess/detils/${id}/tindak-lanjut-direktorat/hasil-analisis-harmonisasi`);
    }
  };

  const openDraft = id_draft => {
    history.push(
      `/compose/reprocess/detils/${id}/tindak-lanjut-direktorat/${id_draft}/draft`
    );
  };
  const openProcess = () => {
    history.push(
      `/compose/reprocess/detils/${id}/tindak-lanjut-direktorat/kajian`
    );
  };
  const showStatus = () => {
    history.push(
      `/compose/reprocess/detils/${id}/tindak-lanjut-direktorat/status`
    );

  }

  useEffect(() => {
    getDraftComposingById(id).then(({ data }) => {
      data.map(dt => {
        getPenyusunanById(dt.id_penyusunan).then(({ data }) => {
          getAnalisisHarmon(data.id_penyusunan).then((data_1)=> {
            // console.log(data_1.data.length > 1 ? 'ya' : 'tidak')
            setDraft(draft => [
              ...draft,
              {
                id_draftperaturan: dt.id_draftperaturan,
                jns_draft: dt.jns_draft,
                file_kajian: data.update_file_kajian,
                status: data.status,
                id_harmon: data_1.data.length > 0 ? data_1.data[0].id_analisis_harmon : null 
              }
            ]);
            
          })
        });
      });
    });
  }, [id]);

  const applyDraft = () => {
    getPenyusunanById(id).then(({ data }) => {
      updateStatusPenyusunan(id, 11, data.kd_kantor).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/dashboard");
            history.replace("/compose/proposal");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace(`/compose/proposal/${id}/process`);
          });
        }
      });
    });
  }
  const rejectProposal = () => {
    history.push(`/compose/reprocess/detils/${id}/tindak-lanjut-direktorat/reject`);
  };
  const handleReject = (val) => {
    getPenyusunanById(id).then(({ data }) => {
      updateStatusPenyusunan(id, 8, data.kd_kantor, val.alasan_penolakan).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/dashboard");
            history.replace("/compose/process");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace(`/compose/process`);
          });
        }
      });
    });
  }

  const columns = [
    {
      dataField: "draft_peraturan",
      text: "Draft Peraturan",
      sort: true,
      formatter:
        columnFormatters.FileColumnFormatterComposeProcessTindakLanjutDirektoratDraftPeraturan,
      formatExtraData: {
        openDraft: openDraft
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3"
    },
    {
      dataField: "hasil_analisis",
      text: "Hasil Analisis Harmonisasi",
      sort: true,
      formatter:
        columnFormatters.FileColumnFormatterComposeProcessTindakLanjutDirektoratHasilAnalisis,
      formatExtraData: {
        openAnalisis: openAnalisis
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        textAlign: "center"
      }
    },
    {
      dataField: "kajian",
      text: "Kajian",
      formatter:
        columnFormatters.ActionsColumnFormatterComposeProcessTindakLanjutDirektoratKajian,
      formatExtraData: {
        openProcess: openProcess
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      formatter:
        columnFormatters.StatusColumnFormatterComposeProcessTindakLanjutDirektoratKajian,
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3"
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        columnFormatters.ActionsColumnFormatterComposeReProcessTindakLanjutDirektoratKajianAksi,
      formatExtraData: {
        openProcess: applyDraft,
        showStatus: showStatus,
        rejectProposal: rejectProposal
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  const emptyDataMessage = () => {
    return "No Data";
  };

  return (
    <>
      <>
        <BootstrapTable
          wrapperClasses="table-responsive"
          bordered={false}
          headerWrapperClasses="thead-light"
          classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
          keyField="id_draftperaturan"
          data={draft}
          columns={columns}
          bootstrap4
          noDataIndication={emptyDataMessage}
        ></BootstrapTable>
      </>
      {/*Route to Open Draft*/}
      <Route path="/compose/reprocess/detils/:id/tindak-lanjut-direktorat/:id_draft/draft">
        {({ history, match }) => (
          <TindakLanjutDirektoratDraftOpenReprocess
            show={match != null}
            id={match && match.params.id}
            id_draft={match && match.params.id_draft}
            onHide={() => {
              history.push(
                `/compose/reprocess/detils/${id}/tindak-lanjut-direktorat`
              );
            }}
            onRef={() => {
              history.push(
                `/compose/reprocess/detils/${id}/tindak-lanjut-direktorat`
              );
            }}
          />
        )}
      </Route>
       {/*Route to Open Kajian*/}
       <Route path="/compose/reprocess/detils/:id/tindak-lanjut-direktorat/kajian">
        {({ history, match }) => {
          if(match){
            return(
              (
                <TindakLanjutDirektoratKajianOpen
                  show={match != null}
                  id={match && match.params.id}
                  onHide={() => {
                    history.push(
                      `/compose/reprocess/detils/${id}/tindak-lanjut-direktorat`
                    );
                  }}
                  onRef={() => {
                    history.push(
                      `/compose/reprocess/detils/${id}/tindak-lanjut-direktorat`
                    );
                  }}
                />
              )
            )
          }
          return(<></>)}}
      </Route>
       {/*Route to Open Kajian*/}
      <Route path="/compose/reprocess/detils/:id/tindak-lanjut-direktorat/status">
        {({ history, match }) => (
          <ProposalOpen
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push(`/compose/reprocess/detils/${id}/tindak-lanjut-direktorat`);
            }}
            onRef={() => {
              history.push(`/compose/reprocess/detils/${id}/tindak-lanjut-direktorat`);
            }}
          />
        )}
      </Route>
       {/*Route to Reject Proposal*/}
      <Route path="/compose/reprocess/detils/:id/tindak-lanjut-direktorat/reject">
        {({ history, match }) => (
          <TindakLanjutDirektoratReject
            show={match != null}
            id={match && match.params.id}
            reject={handleReject}
            onHide={() => {
              history.push(`/compose/reprocess/detils/${id}/tindak-lanjut-direktorat`);
             
            }}
            onRef={() => {
              history.push(`/compose/reprocess/detils/${id}/tindak-lanjut-direktorat`);
             
            }}
          />
        )}
      </Route>
    </>
  );
}

export default TindakLanjutDirektoratDraftReprocess;
