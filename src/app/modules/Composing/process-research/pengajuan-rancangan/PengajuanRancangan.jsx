import React, { useState, useEffect } from "react";

import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import PengajuanRancanganDraft from "./PengajuanRancanganDraft";
import { getPenyusunanById } from "../../../Evaluation/Api";
import { DateFormat } from "../../../../helpers/DateFormat";


function PengajuanRancanganReprocess({
  history,
  match: {
    params: { id }
  }
}) {
  const [detil, setDetil] = useState([]);

  useEffect(() => {
    getPenyusunanById(id).then(({ data }) => {
      setDetil(data);
    });
  }, [id]);

  const handleBack = () => {
    history.push(`/compose/process/${id}/detil`)
  }
  return (
    <>
      <Card>
        <CardHeader
          title="Pengajuan Rancangan Regulasi Perpajakan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
        <>
            <div className="row">
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    No Penyusunan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${detil.no_penyusunan}`}
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Jenis Peraturan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${detil.jns_peraturan}`}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Tanggal Penyusunan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${DateFormat(detil.tgl_penyusunan)}`}
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Judul Peraturan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${detil.judul_peraturan}`}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Unit In Charge
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {`: ${detil.unit_incharge}`}
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Usulan Simfoni
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">{`: ${detil.simfoni}`}</p>
                  </div>
                </div>
              </div>
            </div>
          </>
            <PengajuanRancanganDraft id={id}/>
        </CardBody>
        <CardFooter style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "right" }}>
        <button
          type="button"
          onClick={handleBack}
          className="btn btn-light-success"
          style={{
            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
          }}
        >
          <i className="fa fa-arrow-left"></i>
          Kembali
        </button>
      </div>
      </CardFooter>
      </Card>
    </>
  );
}

export default PengajuanRancanganReprocess;
