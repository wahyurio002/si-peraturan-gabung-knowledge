import React, { useEffect, useState, useRef } from "react";
import EmailEditor from "react-email-editor";
import axios from "axios";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../../_metronic/_partials/controls";
import { getDraft } from "../../../Evaluation/Api";

function PengajuanRancanganDraftLihatReprocess({
  history,
  match: {
    params: { id, id_draft, name }
  }
}) {
  const [draft, setDraft] = useState("");
  const [content, setContent] = useState({
    file: "",
    lampiran: ""
  });

  const validationSchema = Yup.object().shape({
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      )
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    if (id_draft) {
      axios.get(`/draftperaturan/byidpenyusunan/${id}`, {
        transformResponse: res => {
          switch (name) {
            case "usulan":
              setDraft(JSON.parse(res[0].body_draft));
              break;
            case "cosign unit":
              setDraft(JSON.parse(res[0].body_cosign));
              break;
            case "cosign sahli":
              setDraft(JSON.parse(res[0].body_sahli));
              break;
            case "cosign dirjen":
              setDraft(JSON.parse(res[0].body_direktur));
              break;

            default:
              break;
          }
          return res;
        },
        responseType: "json"
      });
      getDraft(id_draft).then(({ data }) => {
        switch (name) {
          case "usulan":
            setContent({
              file_upload: data.fileupload,
              lampiran: data.filelampiranr
            });
            break;
          case "cosign unit":
            setContent({
              file_upload: data.fileupload_cosign,
              lampiran: data.filelampiran_cosign
            });
            break;
          case "cosign sahli":
            setContent({
              file_upload: data.fieupload_sahli,
              lampiran: data.filelampiran_sahli
            });
            break;
          case "cosign dirjen":
            setContent({
              file_upload: data.fileupload_direktur,
              lampiran: data.filelampiran_direktur
            });
            break;

          default:
            break;
        }
      });
    }
  }, [id_draft, name, id]);
  const emailEditorRef = useRef(null);
  const onLoad = () => {
    setTimeout(() => {
      emailEditorRef.current.editor.showPreview(
        emailEditorRef.current.editor.loadDesign(draft)
      );
    }, 2000);
  };
  return (
    <Card>
      <CardHeader
        title="Lihat Draft Peraturan"
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <>
          <div className="col-lg-12 mb-4" style={{ textAlign: "right" }}>
            <button
              type="button"
              className="btn btn-light-primary ml-2"
              // onSubmit={() => handleSubmit()}
              onClick={onLoad}
              style={{
                boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
              }}
            >
              <i className="fa fa-redo"></i>
              Reload
            </button>
          </div>
          <div className="row">
            <EmailEditor
              ref={emailEditorRef}
              options={{
                features: {
                  textEditor: {
                    fontSizes: ["70px", "60px", "50px"]
                  }
                },
                fonts: {
                  showDefaultFonts: true,
                  customFonts: [
                    {
                      label: "Comic Sans",
                      value: "'Comic Sans MS', cursive, sans-serif"
                    },
                    {
                      label: "Book Antiqua",
                      value: "book antiqua,palatino"
                    }
                  ]
                }
              }}
              style={{ height: "1000px" }}
              onLoad={onLoad}
            />
          </div>
          <Formik
            enableReinitialize={true}
            initialValues={content}
            validationSchema={validationSchema}
            onSubmit={values => {
              // saveDraft(values);
              // console.log(values);
            }}
          >
            {({ values }) => {
              return (
                <Form className="form form-label-right">
                  {/* Field File */}
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      File Tersimpan
                    </label>
                    <div
                      className="col-lg-9 col-xl-6"
                      style={{ marginTop: "10px" }}
                    >
                      <a
                        href={values.file_upload}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {values.file_upload
                          ? values.file_upload.slice(28)
                          : null}
                      </a>
                    </div>
                  </div>
                  {/* Field Lampiran */}
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Lampiran Tersimpan
                    </label>
                    <div
                      className="col-lg-9 col-xl-6"
                      style={{ marginTop: "10px" }}
                    >
                      <a
                        href={values.lampiran}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {values.lampiran ? values.lampiran.slice(28) : null}
                      </a>
                    </div>
                  </div>

                  <div className="col-lg-12" style={{ textAlign: "right" }}>
                    <button
                      type="button"
                      className="btn btn-light-success ml-2"
                      onClick={() =>
                        history.push(
                          `/compose/reprocess/detils/${id}/pengajuan-rancangan`
                        )
                      }
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
                      }}
                    >
                      <i className="fa fa-arrow-left"></i>
                      Kembali
                    </button>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </>
      </CardBody>
    </Card>
  );
}

export default PengajuanRancanganDraftLihatReprocess;
