import React from "react";
import { Switch } from "react-router-dom";
import { ContentRoute } from "../../../_metronic/layout";
import Proposal from "./Proposal/Proposal";
import ProposalEdit from "./Proposal/Edit/ProposalEdit";
import Research from "./Research/Research";
import Monitoring from "./Monitoring/Monitoring";

export default function Planning() {
  return (
    <Switch>
      
      {/* Proposal */}
      <ContentRoute path="/plan/proposal/add" component={ProposalEdit} />
      <ContentRoute
          path="/plan/proposal/:id/edit"
          component={ProposalEdit}
        />
      <ContentRoute path="/plan/proposal" component={Proposal} />

      {/* Research */}
      <ContentRoute path="/plan/research" component={Research} />

      {/* Monitoring */}
      <ContentRoute path="/plan/monitoring" component={Monitoring} />
    </Switch>
  );
}
