import React from "react";
import { Modal, Table } from "react-bootstrap";
import swal from "sweetalert";

function MonitoringOpen({ id, show, onHide }) {

  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Detil Usulan Perencanaan
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Nomor Surat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
              : No 1/PJ.12/2020
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Tgl Surat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
              : 15/12/2020
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Perihal</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">: Prakerja</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Instansi Penerbit/ Unit Kerja</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">: Asosiasi ABC</span>
          </div>
        </div>
        <div className="row">
            <div className="col-lg-9 col-xl-6">
              <h5 className="mb-6 mt-6" style={{fontWeight: '600'}}>ISU PERMASALAHAN</h5>
            </div>
          </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">No Evaluasi</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">: Eva 01/10/2020</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Jenis Peraturan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">: PPH 21</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Judul Peraturan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">: Peraturan Tentang</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Isu Permasalahan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">: Isu Permasalahan</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Nomor Peraturan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">: Nomor Peraturan</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Tentang</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">: Tentang</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Konten Peraturan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">: Konten Peraturan</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Alasan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">: Alasan</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Analisa Dampak Kebijakan</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">: Analisa Dampak Kebijakan</span>
          </div>
        </div>
        {/* Begin Status Pengajuan */}
        <div className="row">
          <Table responsive hover>
            <thead style={{border: '1px solid #3699FF', textAlign: 'center', backgroundColor:'#3699FF'}}>
              <tr>
                <th>NO</th>
                <th style={{textAlign:'left'}}>STATUS</th>
                <th style={{textAlign:'left'}}>TANGGAL</th>
              </tr>
            </thead>
            <tbody style={{border:'1px solid lightgrey', textAlign:'center'}}>
              <tr style={{borderBottom: '1px solid lightgrey'}}>
                <td>1</td>
                <td style={{textAlign:'left'}}>Pengajuan</td>
                <td style={{textAlign:'left'}}>01/01/2021 - 14:00</td>
              </tr>
              <tr style={{borderBottom: '1px solid lightgrey'}}>
                <td>2</td>
                <td style={{textAlign:'left'}}>Proses Pengajuan</td>
                <td style={{textAlign:'left'}}>01/01/2021 - 18:00</td>
              </tr>
            </tbody>
            <tfoot style={{border: '1px solid #3699FF', textAlign: 'center', backgroundColor:'#3699FF'}}>
              <tr style={{height: '40px'}}>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tfoot>
          </Table>
        </div>
        {/* End of Status Pengajuan */}
      </Modal.Body>
      <Modal.Footer style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            // onClick={backAction}
            onClick={onHide}
            className="btn btn-light"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tutup
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}

export default MonitoringOpen;
