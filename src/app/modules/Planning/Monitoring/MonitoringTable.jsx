import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import {Pagination} from "../../../helpers/pagination/Pagination"
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import { useHistory } from "react-router-dom";
import { getPerencanaan } from "../../Evaluation/Api";

function MonitoringTable() {
  const history = useHistory();

  const openProposal = (id) => history.push(`/plan/monitoring/${id}/open`);
  const [monitoring, setMonitoring] = useState([]);

  useEffect(() => {
    getPerencanaan().then(({ data }) => {
      data.map(data => {
        return data.no_perencanaan ? setMonitoring(monitoring=> [...monitoring,data]) : null
      })
    })
  }, [])


  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.IdColumnFormatter,
      headerSortingClasses,
    },
    {
      dataField: "no_perencanaan",
      text: "No Perencanaan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "no_evaluasi",
      text: "No Evaluasi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "tgl_perencanaan",
      text: "Tgl Perencanaan",
      sort: true,
      formatter: columnFormatters.ProposalDateColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "jns_peraturan",
      text: "Jenis Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "judul_peraturan",
      text: "Judul Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "file_kajian",
      text: "File Kajian",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.ProposalFileColumnFormatter,
      headerSortingClasses,
    }
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_perencanaan",
    pageNumber: 1,
    pageSize: 5
  };
  const defaultSorted = [{ dataField: "no_perencanaan", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: monitoring.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;


  return (
    <>
      <>
      <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_perencanaan"
                  data={monitoring}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

    </>
  );
}

export default MonitoringTable;
