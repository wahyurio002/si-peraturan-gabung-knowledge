import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import ResearchTable from "./ResearchTable";

function Research() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Surat Usulan Evaluasi Regulasi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
            <ResearchTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Research;
