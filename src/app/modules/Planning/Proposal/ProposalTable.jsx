/* Library */
import React, { useEffect, useState } from "react";
import { useHistory, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";

/* Component */
import ProposalOpen from "./ProposalOpen";
import ProposalReject from "./ProposalReject";

/* Utility */
import { applyPerencanaan, deletePerencanaan, getPerencanaan } from "../../Evaluation/Api";



function ProposalTable() {
  const history = useHistory();
  const { user } = useSelector(state => state.auth);
  const [plan, setPlan] = useState([]);


  const addProposal = () => history.push("/plan/proposal/add");
  const openProposal = id => history.push(`/plan/proposal/${id}/open`);
  const rejectProposal = id => history.push(`/plan/proposal/${id}/reject`);
  const editProposal = id => history.push(`/plan/proposal/${id}/edit`);
  const applyProposal = id => {

    applyPerencanaan(id, user.kantorLegacyKode)
      .then(({status})=> {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(
            () => {
              history.push("/dashboard");
              history.replace("/plan/proposal");
            }
          );
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace("/plan/proposal");
          });
        }
      })
  };

  const deleteAction = id => {
    swal({
      title: "Apakah Anda Yakin?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
        deletePerencanaan(id).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace("/plan/proposal");
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/dashboard");
              history.replace("/plan/proposal");
            });
          }
        });
      }
      // else {
      //   swal("Your imaginary file is safe!");
      // }
    });
  };


  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "no_evaluasi",
      text: "No Evaluasi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_perencanaan",
      text: "Tgl Perencanaan",
      sort: true,
      formatter: columnFormatters.ProposalDateColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "jns_peraturan",
      text: "Jenis Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "judul_peraturan",
      text: "Judul Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "file_kajian",
      text: "File Kajian",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.ProposalFileColumnFormatter,
      headerSortingClasses
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.ProposalStatusColumnFormatter,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterPlanProposal,
      formatExtraData: {
        openEditDialog: editProposal,
        openDeleteDialog: deleteAction,
        showProposal: openProposal,
        showReject: rejectProposal,
        applyProposal: applyProposal
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_evaluasi",
    pageNumber: 1,
    pageSize: 5
  };
  const defaultSorted = [{ dataField: "no_evaluasi", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: plan.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  useEffect(() => {
    getPerencanaan().then(({ data }) => {
      // setPlan(data);
      data.map(data => {
        return data.status !== 'Terima' ? setPlan(plan => [...plan, data]) : null;
      })
    });
  }, []);

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_perencanaan"
                  data={plan}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right"
                            }}
                            onClick={addProposal}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      <Route path="/plan/proposal/:id/reject">
        {({ history, match }) => (
          <ProposalReject
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/plan/proposal");
            }}
            onRef={() => {
              history.push("/plan/proposal");
            }}
          />
        )}
      </Route>
      <Route path="/plan/proposal/:id/open">
        {({ history, match }) => (
          <ProposalOpen
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/plan/proposal");
            }}
            onRef={() => {
              history.push("/plan/proposal");
            }}
          />
        )}
      </Route>

      {/*Route to Add Apps*/}
      {/* <Route path="/evaluation/proposal/add">
            {({ history, match }) => (
                <ProposalEdit
                    show={match != null}
                    onHide={() => {
                       history.push("/evaluation/proposal");
                    }}

                    onRef={() => {
                        history.push("/evaluation/proposal");
                    }}
                />
            )}
        </Route> */}

      {/*Route to Edit Apps*/}
      {/* <Route path="/ads/:id/edit">
            {({ history, match }) => (
                <AdsEdit
                    show={match != null}
                    id={match && match.params.id}
                    onHide={() => {
                        history.push("/ads");
                    }}
                    onRef={() => {
                        history.push("/ads");
                    }}
                />
            )}
        </Route> */}

      {/*Route to Delete Apps*/}
      {/* <Route path="/apps/:id/delete">
            {({ history, match }) => (
                <AppsDeleteDialog
                    show={match != null}
                    id={match && match.params.id}
                    onHide={() => {
                        history.push("/apps");
                    }}
                    onRef={() => {
                        history.push("/monitoring");
                        history.replace("/apps");
                    }}
                />
            )} 
        </Route> */}
    </>
  );
}

export default ProposalTable;
