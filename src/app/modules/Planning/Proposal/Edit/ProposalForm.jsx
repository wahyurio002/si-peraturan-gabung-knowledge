import React, {useEffect, useState, useRef} from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import "../../styles.css";
// import { DatePickerField, Input, Textarea, Select } from "../../helpers";
// import CustomFileInput from "../../helpers/CustomFileInput";
import FormStepper from "./FormStepper";

function ProposalForm({ validate, handleChangeEvaluation, setShow, savePlan, isEdit }) {
  const ValidateSchema = Yup.object().shape({
    // no_evaluasi: Yup.string().required("Jenis Peraturan is required"),
    tgl_perencanaan: Yup.mixed()
    .nullable(false)
    .required("Tanggal Perencanaan is required"),
    jns_peraturan: Yup.string().required("Jenis Peraturan is required"),
    judul_peraturan: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Judul Peraturan is required"),
    simfoni: Yup.string().required("Simfoni is required"),
    // unit_incharge: Yup.string()
    //   .min(2, "Minimum 2 symbols")
    //   .max(50, "Maximum 50 symbols")
    //   .required("Unit Incharge is required"),
    // file: Yup.mixed()
    // .required("A file is required")
    // .test(
    //   "fileSize",
    //   "File too large",
    //   value => value && value.size <= FILE_SIZE
    // )
    // .test(
    //   "fileFormat",
    //   "Unsupported Format",
    //   value => value && SUPPORTED_FORMATS.includes(value.type)
    // ),
    jns_pajak:  Yup.array().min(1, "Jenis Pajak is required").required("Jenis Pajak is required"),
    isu_masalah: Yup.string()
    .min(2, "Minimum 2 symbols")
    .required("Isu Masalah is required"),
    no_peraturan: Yup.string()
    .min(2, "Minimum 2 symbols")
    .max(50, "Maximum 50 symbols")
    .required("No Peraturan is required"),
    tentang: Yup.string()
      .min(2, "Minimum 2 symbols")
      .required("Tentang is required"),
    konten_peraturan: Yup.string()
      .min(2, "Minimum 2 symbols")
      .required("Konten Peraturan is required"),
    alasan: Yup.string()
      .min(2, "Minimum 2 symbols")
      .required("Alasan is required"),
    analisa_dampak: Yup.string()
      .min(2, "Minimum 2 symbols")
      .required("Analisis is required"),

  });

  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  const btnRef = useRef();

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={validate}
        validationSchema={ValidateSchema}
        onSubmit={values => {
          // console.log(values);
          savePlan(values);
        }}
      >
        {({ handleSubmit, isValid }) => {
          return (
          <>
            <Form className="form form-label-right">
                <FormStepper
                btnRef={btnRef}
                check={!(isValid)}
                handleChangeEvaluation={handleChangeEvaluation}
                setShow={setShow}
                isEdit={isEdit}
                 />
                  <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
            </Form>
          </>
          )}}
      </Formik>
    </>
  );
}

export default ProposalForm;
