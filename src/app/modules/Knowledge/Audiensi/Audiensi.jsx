import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import AudiensiTable from "./AudiensiTable";

function Audiensi() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Audiensi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <AudiensiTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default Audiensi;
