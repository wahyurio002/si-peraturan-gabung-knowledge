import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import DaftarAudiensiTable from "./DaftarAudiensiTable";

function DaftarAudiensi() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Audiensi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <DaftarAudiensiTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default DaftarAudiensi;
