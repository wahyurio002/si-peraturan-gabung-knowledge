import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";

import { Pagination } from "../pagination/Pagination";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import swal from "sweetalert";
import SVG from "react-inlinesvg";
import {
  updateStatusAudiensi,
  getAudiensiById,
  deleteAudiensiById,
  getAudiensiNonTerima,
  getAudiensiNonTerimaByNip,
  getAudiensiSearchEselon4,
  getAudiensiSearchEselon3
} from "../Api";
import AudiensiHistory from "./AudiensiHistory";
import { useSelector } from "react-redux";

function AudiensiTable() {
  const { role, user } = useSelector(state => state.auth);
  const konseptor = role.includes("ROLE_PERATURAN_KONSEPTOR");
  const es4 = role.includes("ROLE_PERATURAN_PENELITI_LVL1");
  const es3 = role.includes("ROLE_PERATURAN_PENELITI_LVL2");
  const admin = role.includes("ROLE_ADMIN_PERATURAN");
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_nd",
    pageNumber: 1,
    pageSize: 5,
  };
  const history = useHistory();
  const addAudiensi = () => history.push("/knowledge/audiensi/new");
  const editAudiensi = (id) =>
    history.push(`/knowledge/audiensi/${id}/edit`);

  const applyAudiensi = (id) => {
      if (id) {
      getAudiensiById(id).then(({ data }) => {
        if (konseptor){
          updateStatusAudiensi(data.id_audiensi, 1, 2, user.nip9).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push("/dashboard");
                history.replace("/knowledge/audiensi");
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push("/dashboard");
                history.replace("/knowledge/audiensi");
              });
              }
            });
          } else {
            if (es4){
              updateStatusAudiensi(data.id_audiensi, 1, 2, user.nip9).then(({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                    history.push("/dashboard");
                    history.replace("/knowledge/audiensi");
                  });
                } else {
                  swal("Gagal", "Data gagal disimpan", "error").then(() => {
                    history.push("/dashboard");
                    history.replace("/knowledge/audiensi");
                  });
                }
              });
            } else {
              if(es3){
                updateStatusAudiensi(data.id_audiensi, 1, 3, user.nip9).then(({ status }) => {
                  if (status === 201 || status === 200) {
                    swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                      history.push("/dashboard");
                      history.replace("/knowledge/audiensi");
                    });
                  } else {
                    swal("Gagal", "Data gagal disimpan", "error").then(() => {
                      history.push("/dashboard");
                      history.replace("/knowledge/audiensi");
                    });
                  }
                });
              }
            }
          }
        });
      }
    };

  const deleteDialog = (id) => {
    swal({
      title: "Hapus",
      text: "Apakah Anda yakin menghapus data ini ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((ret) => {
      if (ret == true) {
        deleteAudiensiById(id).then(({ data }) => {
          if (data.deleted == true) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/audiensi");
            });
          } else {
            swal("Gagal", "Data gagal dihapus", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/audiensi");
            });
          }
        });
      }
    });
  };

  const showHistoryPengajuan = (id) =>
    history.push(`/knowledge/audiensi/${id}/showhistory`);

  const prosesAudiensi = (id) =>
    history.push(`/knowledge/audiensi/${id}/proses`);

  const detailAudiensi = (id) =>
    history.push(`/knowledge/audiensi/${id}/detail`);

  const [audiensi, setAudiensi] = useState([]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "kmAudiensi.no_nd",
      text: "No ND Hasil Audiensi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "kmAudiensi.tgl_nd",
      text: "Tgl ND Audiensi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "kmAudiensi.perihal",
      text: "Perihal Audiensi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "jenisAudiensi.nama",
      text: "Jenis Audiensi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "kmAudiensi.status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.AudiensiStatusColumnFormatter,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        user.jabatan === "Pelaksana"
          ? columnFormatters.AudiensiActionColumnFormatterPelaksana
          : user.jabatan === "Kepala Seksi"
          ? columnFormatters.AudiensiActionColumnFormatterEselon4
          : columnFormatters.AudiensiActionColumnFormatterEselon3,
      formatExtraData: {
        deleteDialog: deleteDialog,
        applyAudiensi: applyAudiensi,
        editAudiensi: editAudiensi,
        showHistory: showHistoryPengajuan,
        detailAudiensi: detailAudiensi,
        prosesAudiensi: prosesAudiensi,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  const { SearchBar } = Search;

  useEffect (() => {
    if(konseptor){
      getAudiensiNonTerimaByNip(user.nip9).then(({ data }) => {
        data.map(dt => {
          setAudiensi(audiensi => [...audiensi, dt]);
        });
      });
    }else{
      getAudiensiNonTerimaByNip(user.nip9).then(({ data }) => {
        data.map(dt => {
          setAudiensi(audiensi => [...audiensi, dt]);
        });
      });
      if(es4){
        getAudiensiSearchEselon4(user.kantorLegacyKode, user.unitLegacyKode, "Eselon%204").then(({ data }) => {
          data.map(dt => {
            setAudiensi(audiensi => [...audiensi, dt]);
          });
        });
      }else{
        getAudiensiSearchEselon3(user.kantorLegacyKode, user.unitLegacyKode, "Eselon%203").then(({ data }) => {
          data.map(dt => {
            setAudiensi(audiensi => [...audiensi, dt]);
          });
        });
      }
    }
  }, [user, konseptor]);

  const defaultSorted = [{ dataField: "no_nd", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: audiensi.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="kmAudiensi.id_audiensi"
                  data={audiensi}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right",
                            }}
                            onClick={addAudiensi}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/audiensi/:id/showhistory">
        {({ history, match }) => (
          <AudiensiHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/audiensi");
            }}
            onRef={() => {
              history.push("/knowledge/audiensi");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default AudiensiTable;