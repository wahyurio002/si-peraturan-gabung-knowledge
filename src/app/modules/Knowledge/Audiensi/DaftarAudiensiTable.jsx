import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";
import { Pagination } from "../pagination/Pagination";
import {
    getAudiensiTerima,
} from "../Api";
import AudiensiHistory from "../Audiensi/AudiensiHistory";

function DaftarAudiensiTable() {
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_nd",
    pageNumber: 1,
    pageSize: 5,
  };
  const history = useHistory();

  const showHistoryPengajuan = (id) =>
    history.push(`/knowledge/audone/${id}/showhistory`);

  const detailDaftarAudiensi = (id) =>
    history.push(`/knowledge/audone/${id}/detail`);

  const [audiensiterima, setAudiensiTerima] = useState([]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "kmAudiensi.no_nd",
      text: "No ND Hasil Audiensi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "kmAudiensi.tgl_nd",
      text: "Tgl ND Audiensi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "kmAudiensi.perihal",
      text: "Perihal Audiensi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "jenisAudiensi.nama",
      text: "Jenis Audiensi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.DaftarAudiensiActionColumnFormatter,
      formatExtraData: {
        showHistory: showHistoryPengajuan,
        detailDaftarAudiensi: detailDaftarAudiensi,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  const { SearchBar } = Search;

  useEffect(() => {
    getAudiensiTerima().then(({ data }) => {
      data.map((dt) => {
        // if(dt.id_tahapan !== 2 ){
        setAudiensiTerima((audiensiterima) => [...audiensiterima, dt]);
        // }
      });
    });
  }, []);

  const defaultSorted = [{ dataField: "no_nd", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: audiensiterima.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="kmAudiensi.id_audiensi"
                  data={audiensiterima}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                            <SearchBar
                                {...props.searchProps}
                                style={{ width: "500px" }}
                            />
                            <br />
                        </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/audone/:id/showhistory">
        {({ history, match }) => (
          <AudiensiHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/audone");
            }}
            onRef={() => {
              history.push("/knowledge/audone");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default DaftarAudiensiTable;
