import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";
// import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../_metronic/layout";
import AudiensiEditForm from "./AudiensiEditForm";
import AudiensiEditFooter from "./AudiensiEditFooter";
import {
  uploadFile,
  getAudiensiById,
  saveAudiensi,
  updateAudiensi,
  updateStatusAudiensi,
} from "../../Api";
import swal from "sweetalert";

//12 data yang dikirim, yang belum dimasukkan 'nip_perekam', 'jns_usul' dan yang tidak dimasukkan disini adalah 'alasan_tolak'.
const initValues = {
    alasan_tolak: "" ,
    body: "" ,
    file_upload: "" ,
    id_detiljns: "" ,
    kd_kantor: "" ,
    kd_unit_org: "" ,
    nip_pengusul: "" ,
    nip_pjbt_es3: "" ,
    nip_pjbt_es4: "" ,
    no_nd: "" ,
    perihal: "" ,
    status: "" ,
    tgl_nd: "",
};

function AudiensiEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [audiensi, setAudiensi] = useState();

  useEffect(() => {
    let _title = id ? "Edit Audiensi" : "Tambah Audiensi";

    setTitle(_title);
    suhbeader.setTitle(_title);

    if (id) {
      getAudiensiById(id).then(({ data }) => {
        setAudiensi({
          id_audiensi: data.id_audiensi,
          no_nd: data.no_nd,
          tgl_nd: data.tgl_nd,
          id_detiljns: data.id_detiljns,
          perihal: data.perihal,
          file_upload: data.file_upload,
          alasan_tolak: data.alasan_tolak,
          body: data.body,
          nip_pengusul: data.nip_pengusul,
          nip_pjbt_es4: data.nip_pjbt_es4,
          nip_pjbt_es3: data.nip_pjbt_es3,
          wkt_usul: data.wkt_usul,
          wkt_teliti_es4: data.wkt_teliti_es4,
          wkt_teliti_es3: data.wkt_teliti_es3,
          status: data.status,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update,
          kd_kantor: data.kd_kantor,
          kd_unit_org: data.kd_unit_org
        });
      });
    }
  }, [id, suhbeader]);
  const btnRef = useRef();

  const saveSurat = (values) => {
    if (!id) {
      const formData = new FormData();

      formData.append("file", values.file);

      //console.log(values)

      uploadFile(formData)
        .then(({ data }) =>
          saveAudiensi(
            "" ,
            "" ,
            data.message ,
            values.id_detiljns ,
            values.kd_kantor ,
            values.kd_unit_org,
            values.nip_pengusul ,
            "" ,
            "" ,
            values.no_nd ,
            values.perihal ,
            "" ,
            values.tgl_nd,
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push("/knowledge/audiensi");
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push("/knowledge/audiensi/new");
              });
            }
          })
        )
        .catch(() => window.alert("Oops Something went wrong !"));
    } else {
      // console.log(values);
      if (values.file) {
        //console.log(values.file.name);
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFile(formData)
          .then(({ data }) =>
            updateAudiensi(
              values.id_audiensi,
              "" ,
              "" ,
              data.message ,
              values.id_detiljns ,
              values.kd_kantor ,
              values.kd_unit_org,
              values.nip_pengusul ,
              "" ,
              "" ,
              values.no_nd ,
              values.perihal ,
              "" ,
              values.tgl_nd,
            ).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Data berhasil disimpan", "success").then(
                  () => {
                    history.push("/knowledge/audiensi");
                  }
                );
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.push("/knowledge/audiensi/new");
                });
              }
            })
          )
          .catch(() => window.alert("Oops Something went wrong !"));
      } else {
        updateAudiensi(
          values.id_audiensi,
          "" ,
          "" ,
          values.file_upload ,
          values.id_detiljns ,
          values.kd_kantor ,
          values.kd_unit_org,
          values.nip_pengusul ,
          "" ,
          "" ,
          values.no_nd ,
          values.perihal ,
          "" ,
          values.tgl_nd,
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push("/knowledge/audiensi");
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/knowledge/audiensi/new");
            });
          }
        });
      }
      if (values.status == "Tolak") {
        updateStatusAudiensi(id, 1).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Peraturan Berubah Menjadi Draft", "success").then(
              () => {
                history.push("/dashboard");
                history.replace("/knowledge/audiensi");
              }
            );
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/audiensi");
            });
          }
        });
      }
    }
  };

  const backToAudiensiList = () => {
    history.push(`/knowledge/audiensi`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <AudiensiEditForm
            actionsLoading={actionsLoading}
            proposal={audiensi || initValues}
            btnRef={btnRef}
            saveProposal={saveSurat}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <AudiensiEditFooter
          backAction={backToAudiensiList}
          btnRef={btnRef}
        ></AudiensiEditFooter>
      </CardFooter>
    </Card>
  );
}

export default AudiensiEdit;