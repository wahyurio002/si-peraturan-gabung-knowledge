import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Field, Formik, Form } from "formik";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
} from "../proposal-edit/helpers";
import { getAudiensiTerima, getJenisAudiensi } from "../../Api";

function DaftarAudiensiDetailForm({
  proposal,
}) {
  const [audiensiterima, setAudiensiTerima] = useState([]);
  const [jenis, setJenis] = useState([]);
  const { user } = useSelector((state) => state.auth);

  useEffect(() => {
    getAudiensiTerima().then(({ data }) => {
        setAudiensiTerima(data);
    });

  }, []);

  useEffect(() => {
    getJenisAudiensi().then(({ data }) => {
      setJenis(data);
    });
  }, []);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
      >
        {() => {
          return (
            <>
              <Form className="form form-label-right">

                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    disabled
                    name="no_nd"
                    component={Input}
                    placeholder="Nomor ND Hasil Audiensi"
                    label="Nomor ND Hasil Audiensi"
                  />
                </div>

                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField
                    name="tgl_nd"
                    label="Tanggal ND Audiensi"
                    disabled
                  />
                </div>

                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                    disabled
                  />
                </div>

                {/* FIELD jenis */}
                <div className="form-group row">
                  <Sel name="id_detiljns" label="Jenis Audiensi" disabled>
                    <option value=""></option>
                    {jenis.map((data, index) => (

                      <option key={index} value={data.id}>{data.nama}</option>
                    ))}
                  </Sel>
                </div>

                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    File Rekomendasi
                  </label>
                  <div
                    className="col-lg-9 col-xl-6"
                    style={{ marginTop: "10px" }}
                  >
                    <a
                      href={proposal.file_upload}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {proposal.file_upload.slice(28)}
                    </a>
                  </div>
                </div>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default DaftarAudiensiDetailForm;
