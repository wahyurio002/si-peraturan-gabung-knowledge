import React, {useEffect, useState} from "react";
import { useSelector } from "react-redux";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import "./styles.css";
import { DatePickerField, Input, Textarea, Select as Sel } from "../proposal-edit/helpers";
import CustomFileInput from "./helpers/CustomFileInput";
import { getJenisAudiensi } from "../../Api";

function AudiensiEditForm({ proposal, btnRef, saveProposal }) {
  const [jenis, setJenis] = useState([]);
  const { user } = useSelector((state) => state.auth);

  //const validProductValues = products.map(({ _id }) => _id);

  // Validation schema
  const AudiensiEditSchema = Yup.object().shape({
    no_nd: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("No Regulasi is required"),
    tgl_nd: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat is required"),
    id_detiljns:  Yup.string()
      .min(1, "Jenis Audiensi is required")
      .required("Jenis Audiensi is required"),
    perihal: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(250, "Maximum 50 symbols")
      .required("Perihal is required"),
    // file: Yup.mixed()
    // .test(
    //   "fileSize",
    //   "File too large",
    //   value => value && value.size <= FILE_SIZE
    // )
    // .test(
    //   "fileFormat",
    //   "Unsupported Format",
    //   value => value && SUPPORTED_FORMATS.includes(value.type)
    // ),
  });

  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    getJenisAudiensi().then(({ data }) => {
      setJenis(data);
    });
  }, []);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        validationSchema={AudiensiEditSchema}
        onSubmit={(values) => {
          console.log(values);
          saveProposal(values);
        }}
      >
        {({ handleSubmit, setFieldValue }) => {
      
          const handleChangeNip = () => {
            setFieldValue("nip_pengusul", user.nip9);
          };
          const handleChangeKdKantor = () => {
            setFieldValue("kd_kantor", user.kantorLegacyKode);
          };
          const handleChangeKdUnit = () => {
            setFieldValue("kd_unit_org", user.unitLegacyKode);
          };

          return (
            <>
              <Form className="form form-label-right">

                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    name="no_nd"
                    component={Input}
                    placeholder="Nomor ND Hasil Audiensi"
                    label="Nomor ND Hasil Audiensi"
                    onClick={()=>handleChangeNip()}
                  />
                </div>

                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField
                    name="tgl_nd"
                    label="Tanggal Surat Hasil Audiensi"
                  />
                </div>
                
                {/* FIELD jenis */}
                <div className="form-group row">
                  <Sel name="id_detiljns" label="Jenis Audiensi">
                    <option value="">Pilih Jenis Audiensi</option>
                    {jenis.map((data, index) => (
                      <option key={index} value={data.id}>{data.nama}</option>
                    ))}
                  </Sel>
                </div>

                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                    onClick={()=>handleChangeKdKantor()}
                  />
                </div>

                {/* FIELD UPLOAD FILE */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Upload File
                  </label>
                  <div className="col-lg-8 col-xl-6">
                    <Field
                      name="file"
                      component={CustomFileInput}
                      title="Select a file"
                      label="File"
                      // setFieldValue={setFieldValue}
                      // // errorMessage={errors["file"] ? errors["file"] : undefined}
                      // // touched={touched["file"]}
                      style={{ display: "flex" }}
                      // onBlur={handleBlur}
                    />
                  </div>
                </div>

                {/* FIELD FILE */}

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                  onClick={()=>handleChangeKdUnit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default AudiensiEditForm;
