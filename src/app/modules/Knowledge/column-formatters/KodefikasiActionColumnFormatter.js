// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function KodefikasiActionColumnFormatter(
  cellContent,
  row,
  rowIndex,
  {
    prosesKodefikasi,
    detailKodefikasi,
    editKodefikasi,
    deleteDialog,
    showHistory,
    applyKodefikasi,
    openAddKodTerkait,
    // openPeraturan,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Show Status Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")} />
              </span>
            </a>

            {/* Detail  */}

            <a
              title="Detail Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailKodefikasi(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                />
              </span>
            </a>
          </>
        );
        break;

      case "Eselon 3":
        return (
          <>
            <a
              title="Show Status Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")} />
              </span>
            </a>
          </>
        );
        break;

      case "Draft":
        return (
          <>
            <a
              title="Edit Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editKodefikasi(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                />
              </span>
            </a>
            <> </>

            <a
              title="Hapus Kodefikasi"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => deleteDialog(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                />
              </span>
            </a>
            <> </>

            <a
              title="Ajukan Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyKodefikasi(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                />
              </span>
            </a>

            {/* <a
              title="Isi Body"
              className="btn btn-icon btn-light btn-hover-warning btn-sm mx-3"
              //onClick={() => openAddPerTerkait(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-warning">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Files/File.svg")} />
              </span>
            </a>

            <a
              title="Tambah Peraturan Terkait"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openAddKodTerkait(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Plus.svg")}
                />
              </span>
            </a> */}

            <> </>
          </>
        );
        break;

      case "Tolak":
        return (
          <>
            {/* <a
              title="Show Reject"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showReject(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                />
              </span>
            </a>
            <> </> */}
            <a
              title="Edit Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editKodefikasi(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                />
              </span>
            </a>
            <> </>
          </>
        );
        break;

      case "Terima":
        return (
          <>
            <a
              title="Show Status Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_Kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")} />
              </span>
            </a>
          </>
        );

        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}

export function KodefikasiActionColumnFormatterEs4(
  cellContent,
  row,
  rowIndex,
  {
    prosesKodefikasi,
    detailKodefikasi,
    editKodefikasi,
    deleteDialog,
    showHistory,
    showHistoryTolak,
    applyKodefikasi,
    openAddKodTerkait,
    // openPeraturan,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Show Status Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")} />
              </span>
            </a>

            {/* Detail  */}

            <a
              title="Detail Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailKodefikasi(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                />
              </span>
            </a>

            {/* Proses Terima Tolak */}
            <a
              title="Proses"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openPeraturan(row.id_peraturan)}
              onClick={() => prosesKodefikasi(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Settings-2.svg")}
                />
              </span>
            </a>
          </>
        );
        break;

      case "Eselon 3":
        return (
          <>
            <a
              title="Show Status Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")} />
              </span>
            </a>
          </>
        );
        break;

      case "Draft":
        return (
          <>
            <a
              title="Edit Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editKodefikasi(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                />
              </span>
            </a>
            <> </>

            <a
              title="Hapus Kodefikasi"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => deleteDialog(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                />
              </span>
            </a>
            <> </>

            <a
              title="Ajukan Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyKodefikasi(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                />
              </span>
            </a>

            {/* <a
              title="Isi Body"
              className="btn btn-icon btn-light btn-hover-warning btn-sm mx-3"
              //onClick={() => openAddPerTerkait(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-warning">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Files/File.svg")} />
              </span>
            </a>

            <a
              title="Tambah Peraturan Terkait"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openAddKodTerkait(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Plus.svg")}
                />
              </span>
            </a> */}

            <> </>
          </>
        );
        break;

      case "Tolak":
        return (
          <>
            {/* <a
              title="Show Reject"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showReject(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                />
              </span>
            </a>
            <> </> */}
            <a
              title="Show Alasan Tolak"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistoryTolak(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Code/info-circle.svg")}
                />
              </span>
            </a>
            <> </>
          </>
        );
        break;

      case "Terima":
        return (
          <>
            <a
              title="Show Status Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_Kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")} />
              </span>
            </a>
          </>
        );

        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}

export function KodefikasiActionColumnFormatterEs3(
  cellContent,
  row,
  rowIndex,
  {
    prosesKodefikasi,
    detailKodefikasi,
    editKodefikasi,
    deleteDialog,
    showHistory,
    applyKodefikasi,
    openAddKodTerkait,
    // openPeraturan,
  }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Show Status Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")} />
              </span>
            </a>

            {/* Detail  */}

            <a
              title="Detail Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailKodefikasi(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                />
              </span>
            </a>
          </>
        );
        break;

      case "Eselon 3":
        return (
          <>
            <a
              title="Show Status Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")} />
              </span>
            </a>

            {/* Proses Terima Tolak */}
            <a
              title="Proses"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              // onClick={() => openPeraturan(row.id_peraturan)}
              onClick={() => prosesKodefikasi(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Settings-2.svg")}
                />
              </span>
            </a>
          </>
        );
        break;

      case "Draft":
        return (
          <>
            <a
              title="Edit Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editKodefikasi(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                />
              </span>
            </a>
            <> </>

            <a
              title="Hapus Kodefikasi"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => deleteDialog(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                />
              </span>
            </a>
            <> </>

            <a
              title="Ajukan Peraturan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyKodefikasi(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                />
              </span>
            </a>

            {/* <a
              title="Isi Body"
              className="btn btn-icon btn-light btn-hover-warning btn-sm mx-3"
              //onClick={() => openAddPerTerkait(row.id_peraturan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-warning">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Files/File.svg")} />
              </span>
            </a>

            <a
              title="Tambah Peraturan Terkait"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openAddKodTerkait(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Plus.svg")}
                />
              </span>
            </a> */}

            <> </>
          </>
        );
        break;

      case "Tolak":
        return (
          <>
            <a
              title="Edit Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => editKodefikasi(row.id_kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                />
              </span>
            </a>
            <> </>
          </>
        );
        break;

      case "Terima":
        return (
          <>
            <a
              title="Show Status Kodefikasi"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.id_Kodefikasi)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")} />
              </span>
            </a>
          </>
        );

        break;
      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}
