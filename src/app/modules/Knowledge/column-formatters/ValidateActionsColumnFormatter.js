// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function ValidateActionsColumnFormatter(
  cellContent,
  row,
  rowIndex,
  { openAddDetil, openDeleteDialog, showProposal, showReject, applyValidation }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            <a
              title="Show Status"
              className="btn btn-icon btn-light btn-hover-dark btn-sm mx-3"
              onClick={() => showProposal(row.id_validator)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                />
              </span>
            </a>
          </>
        );
        break;

      case "Tolak":
        return (
          <>
            <a
              title="Show Reject"
              className="btn btn-icon btn-light btn-hover-dark btn-sm mx-3"
              onClick={() => showReject(row.id_validator)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                />
              </span>
            </a>
            <> </>
            <a
              title="Edit Detil"
              className="btn btn-icon btn-light btn-hover-primary btn-sm"
              onClick={() => openAddDetil(row.id_validator)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                />
              </span>
            </a>
            <> </>
            <a
              title="Ajukan Validasi"
              className="btn btn-icon btn-light btn-hover-success btn-sm mx-3"
              onClick={() => applyValidation(row.id_validator)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                />
              </span>
            </a>
          </>
        );
        break;

      case null:
        return (
          <>
            <a
              title="Tambah Detil"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openAddDetil(row.id_validator)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                />
              </span>
            </a>
          </>
        );
        break;

        case "Draft":
        return (
          <>
            <a
              title="Edit Detil"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openAddDetil(row.id_validator)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                />
              </span>
            </a>
            <> </>
            <a
              title="Ajukan Validasi"
              className="btn btn-icon btn-light btn-hover-success btn-sm"
              onClick={() => applyValidation(row.id_validator)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                />
              </span>
            </a>
          </>
        );
        break;

      default:
        break;
    }
  };

  return <>{checkStatus(row.status)}</>;
}
