
import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function RegulasiTerkaitActionColoumnFormatter(
  cellContent,
  row,
  rowIndex,
  {deleteDialog}
) {

    return (
        <>
          <a
              title="Hapus Peraturan"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => deleteDialog(row.id_per_terkait)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                />
              </span>
            </a>
        </>
      );
}
