// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function PutusanProposalActionColumnFormatter(
  cellContent,
  row,
  rowIndex,
  { rejectPutusan, openEditDialog, openDeleteDialog, showHistory, applyProposal, detailProposal, openAddPerTerkait, addBody }
) {
  const checkStatus = (status) => {
    switch (status) {
      case "Eselon 4":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Show Status Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")} />
              </span>
            </a>

            {/* Detail  */}

            <a
              title="Detail Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailProposal(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                />
              </span>
            </a>
          </>
        );
      case "Eselon 3":
        return (
          <>
            {/* Show history proses Pengajuan */}
            <a
              title="Show Status Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => showHistory(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG src={toAbsoluteUrl("/media/svg/icons/Home/Timer.svg")} />
              </span>
            </a>

            {/* Detail  */}

            <a
              title="Detail Surat Penegasan"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => detailProposal(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Visible.svg")}
                />
              </span>
            </a>
          </>
        );

      case "Draft":
        return (
          <>
            <a
              title="Edit Proposal"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                />
              </span>
            </a>
            <> </>

            <a
              title="Hapus Proposal"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => openDeleteDialog(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                />
              </span>
            </a>
            <> </>

            <a
              title="Ajukan Proposal"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => applyProposal(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-success">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Navigation/Check.svg")}
                />
              </span>
            </a>

            <a
              title="Isi Body"
              className="btn btn-icon btn-light btn-hover-warning btn-sm mx-3"
              onClick={() => addBody(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-warning">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Files/File.svg"
                  )}
                />
              </span>
            </a>


            <a
              title="Tambah Peraturan Terkait"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openAddPerTerkait(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Navigation/Plus.svg"
                  )}
                />
              </span>
            </a>
            <> </>
          </>
        );

      case "Tolak":
        return (
          <>
            <a
              title="Show Reject"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => rejectPutusan(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-dark">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/Code/Info-circle.svg")}
                />
              </span>
            </a>
            <a
              title="Edit Proposal"
              className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
              onClick={() => openEditDialog(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-primary">
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Communication/Write.svg"
                  )}
                />
              </span>
            </a>
            <a
              title="Hapus Proposal"
              className="btn btn-icon btn-light btn-hover-danger btn-sm"
              onClick={() => openDeleteDialog(row.putusanPengadilan.id_putpengadilan)}
            >
              <span className="svg-icon svg-icon-md svg-icon-danger">
                <SVG
                  src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")}
                />
              </span>
            </a>
            <> </>
          </>
        );

      default:
        break;
    }
  };

  return <>{checkStatus(row.putusanPengadilan.status)}</>;
}