
import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";

export function FileDokumenFormatter(
  cellContent,
  row,
  rowIndex
) {

  const str = row.file_upload
  const pieces = str.split("/")
  const last = pieces[pieces.length - 1]

    return (
        <>
          <a href={row.file_upload} target="_blank"
            >
                {last}
          </a>
        </>
      );
}
