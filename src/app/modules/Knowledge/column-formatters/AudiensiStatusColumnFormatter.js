// please be familiar with react-bootstrap-table-next column formaters
// https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html?selectedKind=Work%20on%20Columns&selectedStory=Column%20Formatter&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel
import React from "react";

export function AudiensiStatusColumnFormatter(cellContent, row) {
  const CustomerStatusCssClasses = ["warning", "primary", "danger","info","success",""];
  const CustomerStatusTitles = ["Eselon 4", "Draft", "Tolak", "Eselon 3" , "Terima",""];
  let status = "";

  switch (row.kmAudiensi.status) {
    case "Eselon 4":
      status = 0;
      break;

    case "Draft":
      status = 1;
      break;

    case "Tolak":
      status = 2;
      break;

    case "Eselon 3":
      status = 3;
      break;
    
    case "Terima":
      status = 4;
      break;

    default:
      status = 5;
  }
  const getLabelCssClasses = () => {
    if(row.kmAudiensi.status === ""){
      return '';
    } else {
      return `label label-lg label-light-${CustomerStatusCssClasses[status]} label-inline`;
    }
    
  };
  return (
    <span className={getLabelCssClasses()}>{CustomerStatusTitles[status]}</span>
  );
}
