import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Field, Formik, Form } from "formik";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
} from "../proposal-edit/helpers";
import {
  getJenisKajian,
  getDitPembuat,
  getDokumenKajian,
  getKajianById,
} from "../../Api";
import RegulasiTerkaitTableDetail from "./RegulasiTerkaitTableDetail";
import DokumenLainTableDetail from "./DokumenLainTableDetail";
// import BodyEditor from "./helpers/BodyEditor";

import "./decoupled.css";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "ckeditor5-custom-build/build/ckeditor";

function RegulasiDetailForm({
  proposal,
  btnRef,
  saveRegulasiTerkait,
  peraturanTerkait,
  idPeraturan,
}) {
  const [jenisKajian, setJenisKajian] = useState([]);
  const [ditPembuat, setDitPembuat] = useState([]);
  const [propbody, setPropbody] = useState("");
  const { user } = useSelector((state) => state.auth);
  const [dokLain, setDoklain] = useState([]);
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    getJenisKajian().then(({ data }) => {
      setJenisKajian(data);
    });

    getDitPembuat().then(({ data }) => {
      setDitPembuat(data);
    });
  }, []);

  useEffect(() => {
    getKajianById(idPeraturan).then(({ data }) => {
      if (data.body) {
        setPropbody(JSON.parse(data.body));
      }
    });
  }, []);

  useEffect(() => {
    getDokumenKajian(idPeraturan).then(({ data }) => {
      setDoklain(data);
    });
  }, []);

  //console.log(dokLain);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        onSubmit={(values) => {
          //console.log(values);
          saveRegulasiTerkait(values);
        }}
      >
        {({ handleSubmit, setFieldValue }) => {
          const handleChangeNip = () => {
            setFieldValue("nip_perekam", user.nip9);
          };

          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <DatePickerField
                    name="tgl_kajian"
                    label="Tanggal Kajian"
                    disabled
                  />
                </div>
                {/* FIELD jenis */}
                <div className="form-group row">
                  <Sel name="id_detiljns" label="Jenis Kajian" disabled>
                    <option value=""></option>
                    {jenisKajian.map((data, index) => (
                      <option key={index} value={data.id}>
                        {data.nama}
                      </option>
                    ))}
                  </Sel>
                </div>

                <div className="form-group row">
                  <Sel name="unit_kajian" label="Direktorat Pembuat" disabled>
                    <option value=""></option>
                    {ditPembuat.map((data, index) => (
                      <option key={index} value={data.id}>
                        {data.nama}
                      </option>
                    ))}
                  </Sel>
                </div>

                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                    disabled
                  />
                </div>

                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    File
                  </label>
                  <div
                    className="col-lg-9 col-xl-6"
                    style={{ marginTop: "10px" }}
                  >
                    <a
                      href={proposal.file_upload}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {proposal.file_upload.slice(28)}
                    </a>
                  </div>
                </div>

                <RegulasiTerkaitTableDetail
                  peraturanTerkait={peraturanTerkait}
                  idPeraturan={idPeraturan}
                />

                <DokumenLainTableDetail
                  dokLain={dokLain}
                  idPeraturan={idPeraturan}
                />
                <div>
                  <br />
                  <br />
                  <h3>Body</h3>
                </div>
                <div className="document-editor">
                  <div className="document-editor__toolbar"></div>
                  <div className="document-editor__editable-container">
                    <CKEditor
                      disabled
                      onReady={(editor) => {
                        console.log("Editor is ready to use!", editor);
                        window.editor = editor;

                        // Add these two lines to properly position the toolbar
                        const toolbarContainer = document.querySelector(
                          ".document-editor__toolbar"
                        );
                        toolbarContainer.appendChild(
                          editor.ui.view.toolbar.element
                        );
                      }}
                      config={{
                        removePlugins: ["Heading", "Link"],
                        toolbar: [],
                        isReadOnly: true,
                      }}
                      editor={Editor}
                      //ICAAAAAAAAAAAAAAALLLLLLLLLLLLLLLLLLLLLLLL
                      //INI BUAT NAMBAHIN TEMPLATE NYA DI BAWAH INI PAKAI HTML
                      data={propbody}
                    />
                  </div>
                </div>
                {/* <div>
                  <br />
                  <br />
                  <h3>Body</h3>
                </div> */}

                {/* <div className="form-group row m-1">
                  <Field
                    name="COBA"
                    component={BodyEditor}
                    label="Coba Text Editor"
                  />
                </div> */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default RegulasiDetailForm;
