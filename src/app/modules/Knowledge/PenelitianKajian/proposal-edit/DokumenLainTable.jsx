import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
  toAbsoluteUrl,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "../../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";

import { deleteDokLain, addPerTerkaitById } from "../../Api";

import { Pagination } from "../../pagination/Pagination";
import swal from "sweetalert";
import SVG from "react-inlinesvg";
import DokumenLainModal from "./DokumenLainModal";

function DokumenLainTable({ dokLain, idKajian }) {
  const history = useHistory();
  const addDokumenLainnya = () =>
    history.push(
      `/knowledge/kajian/penelitian/${idKajian}/terkait/doklainnya/show`
    );
  const deleteDialog = (idDokLainnya) => {
    swal({
      title: "Hapus",
      text: "Apakah Anda yakin menghapus data ini ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((ret) => {
      if (ret == true) {
        deleteDokLain(idDokLainnya).then(({ data }) => {
          if (data.deleted == true) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace(
                `/knowledge/kajian/penelitian/${idKajian}/terkait`
              );
            });
          } else {
            swal("Gagal", "Data gagal dihapus", "error").then(() => {
              history.push("/dashboard");
              history.replace(
                `/knowledge/kajian/penelitian/${idKajian}/terkait`
              );
            });
          }
        });
      }
    });
  };

  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id_kajian",
    pageNumber: 1,
    pageSize: 5,
  };
  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "download",
      text: "File Upload",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.FileDokumenFormatter,
      headerSortingClasses,
    },
    {
      dataField: "keterangan",
      text: "Keterangan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.DokLainyaActionColoumnFormatter,
      formatExtraData: {
        deleteDialog: deleteDialog,
        // applyProposal: applyProposal,
        // openAddPerTerkait: openAddPerTerkait
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  const { SearchBar } = Search;
  const defaultSorted = [{ dataField: "id_km_dok_lainnya", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: dokLain.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <div>
          <br />
          <br />
          <h3>Dokumen Lainnya</h3>
        </div>

        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_km_dok_lainnya"
                  data={dokLain}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right",
                            }}
                            onClick={addDokumenLainnya}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      <Route path="/knowledge/kajian/penelitian/:id/terkait/doklainnya/show">
        {({ history, match }) => (
          <DokumenLainModal
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push(`/knowledge/kajian/penelitian/${idKajian}/terkait`);
            }}
            onRef={() => {
              history.push(`/knowledge/kajian/penelitian/${idKajian}/terkait`);
            }}
          />
        )}
      </Route>
    </>
  );
}

export default DokumenLainTable;
