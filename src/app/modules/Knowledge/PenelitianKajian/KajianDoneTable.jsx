import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";

import { getKajianTerima , getKajianById, updateStatusKajian, deleteKajianById } from "../Api";

import { Pagination } from "../pagination/Pagination";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import swal from "sweetalert";
import SVG from "react-inlinesvg";
import KajianHistory from "./KajianHistory"



function KajianDoneTable() {
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id_kajian",
    pageNumber: 1,
    pageSize: 5
  };
  const history = useHistory();
  let thePath = document.URL;
  const lastPath = thePath.substring(thePath.lastIndexOf('/') + 1);
  const showHistoryPengajuan = id => history.push(`/knowledge/kajdone/${id}/showhistory`);

  const detailKajian = id => history.push(`/knowledge/kajdone/${id}/detail`);


  const [proposal, setProposal] = useState([]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "jenisKajian.nama",
      text: "Jenis  Kajian",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "hasilKajian.tgl_kajian",
      text: "Tanggal Kajian",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "hasilKajian.perihal",
      text: "Perihal Kajian",
      sort: true,
      //formatter: columnFormatters.DateFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "direktoratPembuat.nama",
      text: "Unit Pembuat  Kajian",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    
    {
      dataField: "hasilKajian.status",
      text: "status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusKajianColumnFormatter,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.KajianActionColoumnFormatter,
      formatExtraData: {
        showHistory : showHistoryPengajuan,
        detailKajian : detailKajian,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];


  const { SearchBar } = Search;




  

 
    useEffect(() => {
      //console.log(lastPath);
      getKajianTerima().then(({ data }) => {
        data.map((dt) => {
  
          setProposal(proposal => [...proposal, dt])
  
        })
      });
  }, []);



  const defaultSorted = [{ dataField: "id_kajian", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: proposal.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => { return 'No Data to Display'; }

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_kajian"
                  data={proposal}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      <Route path="/knowledge/kajdone/:id/showhistory">
        {({ history, match }) => (
          <KajianHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/kajdone");
            }}
            onRef={() => {
              history.push("/knowledge/kajdone");
            }}
          />
        )}
      </Route>





    </>
  );
}

export default KajianDoneTable;
