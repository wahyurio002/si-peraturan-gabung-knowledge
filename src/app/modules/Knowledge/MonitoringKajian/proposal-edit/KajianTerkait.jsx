import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../_metronic/layout";
import RegulasiTerkaitForm from "./RegulasiTerkaitForm";
import RegulasiTerkaitFooter from "./RegulasiTerkaitFooter";
import { getKajianById, getPeraturanTerkaitKajian } from "../../Api";
import swal from "sweetalert";

const initValues = {
    id_detiljns: "",
    perihal: "",
    alasan_tolak: "",
    tgl_kajian: "",
    unit_kajian : "",
    body: "",
    status: "",
    file_upload: "",
    nip_pengusul: "",
    nip_pjbt_es3: "",
    nip_pjbt_es4: "",
  };

function KajianTerkait({
  history,
  match: {
    params: { id }
  }
}) {
  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [proposal, setProposal] = useState();
  const [peraturanTerkait, setPeraturanTerkait] = useState([]);

  useEffect(() => {
    let _title = "Tambah Peraturan Terkait";

    setTitle(_title);
    suhbeader.setTitle(_title);

    getKajianById(id).then(({ data }) => {

        setProposal({
          id_kajian: data.id_kajian,
          tgl_kajian: data.tgl_kajian,
          id_detiljns: data.id_detiljns,
          unit_kajian: data.unit_kajian,
          perihal: data.perihal,
          file_upload: data.file_upload,
          alasan_tolak: data.alasan_tolak,
          body: data.body,
          status: data.status,
          nip_pengusul: data.nip_pengusul,
          nip_pjbt_es4: data.nip_pjbt_es4,
          nip_pjbt_es3: data.nip_pjbt_es3,
          wkt_usul: data.wkt_usul,
          wkt_teliti_es4: data.wkt_teliti_es4,
          wkt_teliti_es3: data.wkt_teliti_es3,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update
        });


      });


  }, [id, suhbeader]);


  useEffect(() => {

    getPeraturanTerkaitKajian(id).then(({ data }) => {
        //console.log(data)
      data.map((dt) => {
        //setPeraturanTerkait(peraturanTerkait => [...peraturanTerkait,dt.kmperaturanTerkait]);

        setPeraturanTerkait(peraturanTerkait => [...peraturanTerkait, {
          id_per_terkait: dt.kmperaturanTerkait.id_per_terkait,
          id_peraturan: dt.kmregulasiPerpajakan.id_peraturan,
          no_regulasi: dt.kmregulasiPerpajakan.no_regulasi,
          tgl_regulasi: dt.kmregulasiPerpajakan.tgl_regulasi,
          perihal: dt.kmregulasiPerpajakan.perihal,
          status: dt.kmregulasiPerpajakan.status,
          file_upload: dt.kmregulasiPerpajakan.file_upload,
          jns_regulasi: dt.kmregulasiPerpajakan.jns_regulasi,
          id_topik: dt.kmregulasiPerpajakan.id_topik,
          body: dt.kmregulasiPerpajakan.body,

        }]);

      })

    });



  }, []);


  //console.log(peraturanTerkait);



  const btnRef = useRef();

  const saveRegulasiTerkait = values => {
  };


  //console.log(peraturanTerkait)





  const backToProposalList = () => {
    history.push(`/knowledge/kajian`);
  };

  const setDisabled = val => {
    setIsDisabled(val);
  };
  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <RegulasiTerkaitForm
            actionsLoading={actionsLoading}
            proposal={proposal || initValues}
            peraturanTerkait={peraturanTerkait}
            idKajian = {id}
            btnRef={btnRef}
            saveRegulasiTerkait={saveRegulasiTerkait}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <RegulasiTerkaitFooter
          backAction={backToProposalList}
          btnRef={btnRef}
        ></RegulasiTerkaitFooter>
      </CardFooter>
    </Card>
  );
}

export default KajianTerkait;
