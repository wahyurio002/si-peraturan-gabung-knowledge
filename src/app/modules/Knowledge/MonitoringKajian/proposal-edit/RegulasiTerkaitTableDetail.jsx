import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
    PaginationProvider
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
    sortCaret,
    headerSortingClasses, toAbsoluteUrl
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "../../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";
import { Pagination } from "../../pagination/Pagination";
import swal from "sweetalert";
import SVG from "react-inlinesvg";


function RegulasiTerkaitTableDetail({ peraturanTerkait, idPeraturan }) {
    const history = useHistory();

    const initialFilter = {
        sortOrder: "asc", // asc||desc
        sortField: "no_regulasi",
        pageNumber: 1,
        pageSize: 5
    };
    const columns = [
        {
            dataField: "any",
            text: "No",
            sort: true,
            formatter: columnFormatters.IdColumnFormatter,
            sortCaret: sortCaret,
            headerSortingClasses
        },
        {
            dataField: "no_regulasi",
            text: "No Peraturan Terkait",
            sort: true,
            sortCaret: sortCaret,
            headerSortingClasses
        },
        {
            dataField: "perihal",
            text: "perihal",
            sort: true,
            sortCaret: sortCaret,
            headerSortingClasses
        }

    ];
    //const { SearchBar } = Search;
    const defaultSorted = [{ dataField: "no_regulasi", order: "asc" }];
    const sizePerPageList = [
        { text: "10", value: 10 },
        { text: "25", value: 25 },
        { text: "50", value: 50 }
    ];
    const pagiOptions = {
        custom: true,
        totalSize: peraturanTerkait.length,
        sizePerPageList: sizePerPageList,
        sizePerPage: initialFilter.pageSize, //default 10
        page: initialFilter.pageNumber //curent page (default 1),
    };
    const emptyDataMessage = () => { return 'No Data to Display'; }


    return (
        <>
            <>
                <div>
                    <br />
                    <br />
                    <h3>Peraturan Terkait</h3>

                </div>

                <PaginationProvider pagination={paginationFactory(pagiOptions)}>
                    {({ paginationProps, paginationTableProps }) => {
                        return (
                            <>
                                <ToolkitProvider
                                    keyField="id_peraturan"
                                    data={peraturanTerkait}
                                    columns={columns}
                                    search
                                >
                                    {props => (
                                        <div>
                                            <BootstrapTable
                                                {...props.baseProps}
                                                wrapperClasses="table-responsive"
                                                bordered={false}
                                                headerWrapperClasses="thead-light"
                                                classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                                                defaultSorted={defaultSorted}
                                                bootstrap4
                                                noDataIndication={emptyDataMessage}
                                                {...paginationTableProps}
                                            ></BootstrapTable>
                                            <Pagination paginationProps={paginationProps} />
                                        </div>
                                    )}
                                </ToolkitProvider>
                            </>
                        );
                    }}
                </PaginationProvider>


            </>
        </>
    );
}

export default RegulasiTerkaitTableDetail;
