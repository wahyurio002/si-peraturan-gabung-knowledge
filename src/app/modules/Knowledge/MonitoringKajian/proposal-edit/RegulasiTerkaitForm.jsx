import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Field, Formik, Form } from "formik";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
} from "../proposal-edit/helpers";
import { getJenisKajian, getDitPembuat, getDokumenKajian } from "../../Api";
import RegulasiTerkaitTable from "./RegulasiTerkaitTable";
import DokumenLainTable from "./DokumenLainTable";

function RegulasiTerkaitForm({
  proposal,
  btnRef,
  saveRegulasiTerkait,
  peraturanTerkait,
  idPeraturan,
}) {
  const [jenisKajian, setJenisKajian] = useState([]);
  const [ditPembuat, setDitPembuat] = useState([]);
  const { user } = useSelector((state) => state.auth);

  const [dokLain, setDoklain] = useState([]);

  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    getJenisKajian().then(({ data }) => {
      setJenisKajian(data);
    });

    getDitPembuat().then(({ data }) => {
      setDitPembuat(data);
    });
  }, []);

  useEffect(() => {
    getDokumenKajian(idPeraturan).then(({ data }) => {
      setDoklain(data);
    });
  }, []);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        onSubmit={(values) => {
          //console.log(values);
          saveRegulasiTerkait(values);
        }}
      >
        {({ handleSubmit, setFieldValue }) => {
          const handleChangeNip = () => {
            setFieldValue("nip_perekam", user.nip9);
          };

          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <DatePickerField
                    name="tgl_kajian"
                    label="Tanggal Kajian"
                    disabled
                  />
                </div>
                {/* FIELD jenis */}
                <div className="form-group row">
                  <Sel name="id_detiljns" label="Jenis Kajian" disabled>
                    <option value=""></option>
                    {jenisKajian.map((data, index) => (
                      <option key={index} value={data.id}>
                        {data.nama}
                      </option>
                    ))}
                  </Sel>
                </div>

                <div className="form-group row">
                  <Sel name="unit_kajian" label="Direktorat Pembuat" disabled>
                    <option value=""></option>
                    {ditPembuat.map((data, index) => (
                      <option key={index} value={data.id}>
                        {data.nama}
                      </option>
                    ))}
                  </Sel>
                </div>

                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                    disabled
                  />
                </div>

                <RegulasiTerkaitTable
                  peraturanTerkait={peraturanTerkait}
                  idPeraturan={idPeraturan}
                />

                <DokumenLainTable dokLain={dokLain} idPeraturan={idPeraturan} />

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default RegulasiTerkaitForm;
