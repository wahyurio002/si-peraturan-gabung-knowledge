import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import KajianDoneTable from "./KajianDoneTable";

function KajianDone() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Hasil Kajian"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <KajianDoneTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default KajianDone;
