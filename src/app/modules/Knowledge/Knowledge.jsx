import React from "react";
import { Switch } from "react-router-dom";
import { ContentRoute } from "../../../_metronic/layout";
import Regulasi from "./Regulasi/Regulasi";
import RegulasiEdit from "./Regulasi/proposal-edit/ProposalEdit";
import RegulasiTerkait from "./Regulasi/proposal-edit/RegulasiTerkait";
import RegulasiDetail from "./Regulasi/proposal-edit/RegulasiDetail";
import BodyRegulasi from "./Regulasi/proposal-edit/Body";

import PenelitianRegulasi from "./PenelitianRegulasi/Regulasi";
import PenelitianRegulasiEdit from "./PenelitianRegulasi/proposal-edit/ProposalEdit";
import PenelitianRegulasiTerkait from "./PenelitianRegulasi/proposal-edit/RegulasiTerkait";
import PenelitianRegulasiDetail from "./PenelitianRegulasi/proposal-edit/RegulasiDetail";
import PenelitianBodyRegulasi from "./PenelitianRegulasi/proposal-edit/Body";

import MonitoringRegulasi from "./MonitoringRegulasi/Regulasi";
import MonitoringRegulasiEdit from "./MonitoringRegulasi/proposal-edit/ProposalEdit";
import MonitoringRegulasiTerkait from "./MonitoringRegulasi/proposal-edit/RegulasiTerkait";
import MonitoringRegulasiDetail from "./MonitoringRegulasi/proposal-edit/RegulasiDetail";
import MonitoringBodyRegulasi from "./MonitoringRegulasi/proposal-edit/Body";

//WAHYU
import Penegasan from "./Penegasan/Penegasan";
import PenegasanProposal from "./Penegasan/Penegasan Proposal/PenegasanProposal";
import PenegasanEdit from "./Penegasan/Penegasan Proposal/proposal-edit/ProposalEdit";
import PenegasanDetail from "./Penegasan/proposal-edit/PenegasanDetail";
import DaftarPenegasan from "./Penegasan/DaftarPenegasan";
import DaftarRekomendasi from "./Rekomendasi/DaftarRekomendasi";
import DaftarPenegasanDetail from "./Penegasan/proposal-edit/DaftarPenegasanDetail";
import DaftarRekomendasiDetail from "./Rekomendasi/proposal-edit/DaftarRekomendasiDetail";
import Rekomendasi from "./Rekomendasi/Rekomendasi";
import RekomendasiProposal from "./Rekomendasi/Rekomendasi Proposal/RekomendasiProposal"
import RekomendasiEdit from "./Rekomendasi/Rekomendasi Proposal/proposal-edit/ProposalEdit";
import RekomendasiDetail from "./Rekomendasi/proposal-edit/RekomendasiDetail";

import Kajian from "./Kajian/Kajian";
import KajianEdit from "./Kajian/proposal-edit/ProposalEdit";
import KajianTerkait from "./Kajian/proposal-edit/KajianTerkait";
import KajianDetail from "./Kajian/proposal-edit/RegulasiDetail";
import BodyKajian from "./Kajian/proposal-edit/Body";

import PenelitianKajian from "./PenelitianKajian/Kajian";
import PenelitianKajianEdit from "./PenelitianKajian/proposal-edit/ProposalEdit";
import PenelitianKajianTerkait from "./PenelitianKajian/proposal-edit/KajianTerkait";
import PenelitianKajianDetail from "./PenelitianKajian/proposal-edit/RegulasiDetail";

import MonitoringKajian from "./MonitoringKajian/Kajian";
import MonitoringKajianEdit from "./MonitoringKajian/proposal-edit/ProposalEdit";
import MonitoringKajianTerkait from "./MonitoringKajian/proposal-edit/KajianTerkait";
import MonitoringKajianDetail from "./MonitoringKajian/proposal-edit/RegulasiDetail";

import Putusan from "./Putusan/Putusan";
import PutusanEdit from "./Putusan/Putusan Proposal/proposal-edit/ProposalEdit";
import PutusanTerkait from "./Putusan/Putusan Proposal/proposal-edit/PutusanTerkait";
import PutusanDetail from "./Putusan/proposal-edit/PutusanDetail";
import DetailPutusanDone from "./Putusan/proposal-edit/DetailPutusanDone";
import PutusanProposal from "./Putusan/Putusan Proposal/PutusanProposal";
import PutusanProposalDetail from "./Putusan/Putusan Proposal/PutusanProposalDetail";

import KodefikasiPenelitian from "./KodefikasiPenelitian/KodefikasiPenelitian";
import KodefikasiPenelitianEdit from "./KodefikasiPenelitian/proposal-edit/ProposalEdit";
import KodefikasiPenelitianDetail from "./KodefikasiPenelitian/proposal-edit/KodefikasiPenelitianDetail";

import KodefikasiProposal from "./KodefikasiProposal/KodefikasiProposal";
import KodefikasiProposalDetail from "./KodefikasiProposal/proposal-edit/KodefikasiProposalDetail";
import KodefikasiProposalEdit from "./KodefikasiProposal/proposal-edit/ProposalEdit";
import BodyKodefikasi from "./KodefikasiProposal/proposal-edit/Body";

import KodefikasiMonitoring from "./KodefikasiMonitoring/KodefikasiMonitoring";
import KodefikasiMonitoringEdit from "./KodefikasiMonitoring/proposal-edit/ProposalEdit";
import KodefikasiMonitoringDetail from "./KodefikasiMonitoring/proposal-edit/KodefikasiMonitoringDetail";
import KnowledgeErrors from "./KnowledgeErrors";
// import Rekomendasi from "./Rekomendasi/Rekomendasi";
// import RekomendasiEdit from "./Rekomendasi/proposal-edit/ProposalEdit";

import Fgd from "./Fgd/Fgd";
import FgdEdit from "./Fgd/proposal-edit/ProposalEdit";
import Audiensi from "./Audiensi/Audiensi";
import AudiensiEdit from "./Audiensi/proposal-edit/ProposalEdit";
import AudiensiDetail from "./Audiensi/proposal-edit/AudiensiDetail";
import DaftarAudiensi from "./Audiensi/DaftarAudiensi";
import DaftarAudiensiDetail from "./Audiensi/proposal-edit/DaftarAudiensiDetail";
import Survei from "./Survei/Survei";
import SurveiDetail from "./Survei/proposal-edit/SurveiDetail";
import SurveiEdit from "./Survei/proposal-edit/ProposalEdit";
import DaftarSurvei from "./Survei/DaftarSurvei";
import DaftarSurveiDetail from "./Survei/proposal-edit/DaftarSurveiDetail";
import Dokumentasi from "./Dokumentasi/Dokumentasi";
import DokumentasiEdit from "./Dokumentasi/proposal-edit/ProposalEdit";

import KajianDone from "./Kajian/KajianDone";
import RegDone from "./Regulasi/RegDone";
import PutusanDone from "./Putusan/PutusanDone";
import { useSelector } from "react-redux";
import PenegasanProposalDetail from "./Penegasan/Penegasan Proposal/proposal-edit/PenegasanProposalDetail";
import RekomendasiProposalDetail from "./Rekomendasi/Rekomendasi Proposal/proposal-edit/RekomendasiProposalDetail";
import RekomendasiBody from "./Rekomendasi/Rekomendasi Proposal/proposal-edit/RekomendasiBody";
import PenegasanBody from "./Penegasan/Penegasan Proposal/proposal-edit/PenegasanBody";
import PutusanBody from "./Putusan/Putusan Proposal/proposal-edit/PutusanBody";

export default function Knowledge() {

  const { role } = useSelector((state) => state.auth);
  const konseptor = role.includes("ROLE_PERATURAN_KONSEPTOR");
  const es4 = role.includes("ROLE_PERATURAN_PENELITI_LVL1");
  const es3 = role.includes("ROLE_PERATURAN_PENELITI_LVL2");
  const admin = role.includes("ROLE_ADMIN_PERATURAN");

  return (
    <Switch>
      {/* regulasi */}
      <ContentRoute
        path="/knowledge/regulasi/usulan/:id/proses"
        component={admin || konseptor ? RegulasiDetail : KnowledgeErrors}
      />
      <ContentRoute
        path="/knowledge/regulasi/usulan/:id/detail"
        component={admin || konseptor ? RegulasiDetail : KnowledgeErrors}
      />
      <ContentRoute
        path="/knowledge/regulasi/usulan/:id/terkait"
        component={admin || konseptor ? RegulasiTerkait : KnowledgeErrors}
      />
      <ContentRoute
        path="/knowledge/regulasi/usulan/new"
        component={admin || konseptor ? RegulasiEdit : KnowledgeErrors}
      />
      <ContentRoute
        path="/knowledge/regulasi/usulan/:id/body"
        component={admin || konseptor ? BodyRegulasi : KnowledgeErrors}
      />
      <ContentRoute
        path="/knowledge/regulasi/usulan/:id/edit"
        component={admin || konseptor ? RegulasiEdit : KnowledgeErrors}
      />
    
      <ContentRoute
        path="/knowledge/regulasi/usulan"
        component={admin || konseptor ? Regulasi : KnowledgeErrors}
      />
      <ContentRoute
        path="/knowledge/regulasi/monitoring/:id/proses"
        component={MonitoringRegulasiDetail}
      />
      <ContentRoute
        path="/knowledge/regulasi/monitoring/:id/detail"
        component={MonitoringRegulasiDetail}
      />
      <ContentRoute
        path="/knowledge/regulasi/monitoring/:id/terkait"
        component={MonitoringRegulasiTerkait}
      />
      <ContentRoute
        path="/knowledge/regulasi/monitoring/new"
        component={MonitoringRegulasiEdit}
      />
      <ContentRoute
        path="/knowledge/regulasi/monitoring/:id/body"
        component={MonitoringBodyRegulasi}
      />
      <ContentRoute
        path="/knowledge/regulasi/monitoring/:id/edit"
        component={MonitoringRegulasiEdit}
      />

      <ContentRoute
        path="/knowledge/regulasi/monitoring"
        component={MonitoringRegulasi}
      />
      <ContentRoute
        path="/knowledge/regulasi/penelitian/:id/proses"
        component={
          admin || es4 || es3 ? PenelitianRegulasiDetail : KnowledgeErrors
        }
      />
      <ContentRoute
        path="/knowledge/regulasi/penelitian/:id/detail"
        component={
          admin || es4 || es3 ? PenelitianRegulasiDetail : KnowledgeErrors
        }
      />
      <ContentRoute
        path="/knowledge/regulasi/penelitian/:id/terkait"
        component={
          admin || es4 || es3 ? PenelitianRegulasiTerkait : KnowledgeErrors
        }
      />
      <ContentRoute
        path="/knowledge/regulasi/penelitian/new"
        component={
          admin || es4 || es3 ? PenelitianRegulasiEdit : KnowledgeErrors
        }
      />
      <ContentRoute
        path="/knowledge/regulasi/penelitian/:id/body"
        component={
          admin || es4 || es3 ? PenelitianBodyRegulasi : KnowledgeErrors
        }
      />
      <ContentRoute
        path="/knowledge/regulasi/penelitian/:id/edit"
        component={
          admin || es4 || es3 ? PenelitianRegulasiEdit : KnowledgeErrors
        }
      />

      <ContentRoute
        path="/knowledge/regulasi/penelitian"
        component={admin || es4 || es3 ? PenelitianRegulasi : KnowledgeErrors}
      />

      {/* WAHYU PENEGASAN */}
      <ContentRoute
        path="/knowledge/penegasan/penelitian/:id/proses"
        component={
          admin || es4 || es3 ? PenegasanDetail : KnowledgeErrors}
      />
      <ContentRoute
        path="/knowledge/penegasan/penelitian/:id/detail"
        component={
          admin || es4 || es3 ? PenegasanDetail : KnowledgeErrors}
      />
      {/* kodefikasi proposal */}
      <ContentRoute
        path="/knowledge/penegasan/usulan/add"
        component={
          admin || konseptor ? PenegasanEdit : KnowledgeErrors
        }
      />

      <ContentRoute
        path="/knowledge/penegasan/usulan/:id/edit"
        component={
          admin || konseptor  ? PenegasanEdit : KnowledgeErrors
        }
      />

      <ContentRoute
        path="/knowledge/penegasan/usulan/:id/detail"
        component={
          admin || konseptor  ? PenegasanProposalDetail : KnowledgeErrors
        }
      />

      <ContentRoute
        path="/knowledge/penegasan/usulan/:id/body"
        component={
          admin || konseptor  ? PenegasanBody : KnowledgeErrors}
      />
      {/* <ContentRoute
        path="/knowledge/penegasan/usulan/:id/open"
        component={
          admin || konseptor || es4 || es3
            ? KodefikasiProposalOpen
            : KnowledgeErrors
        }
      /> */}

      <ContentRoute
        path="/knowledge/penegasan/usulan"
        component={
          admin || konseptor ? PenegasanProposal : KnowledgeErrors
        }
      />

      <ContentRoute path="/knowledge/penegasan/penelitian" component={
        admin || es4 || es3 ? Penegasan : KnowledgeErrors} />

      <ContentRoute
        path="/knowledge/penegasan/monitoring/:id/detail"
        component={DaftarPenegasanDetail}
      />
      <ContentRoute
        path="/knowledge/penegasan/monitoring"
        component={DaftarPenegasan}
      />

      {/* WAHYU REKOMENDASI */}
      <ContentRoute
        path="/knowledge/rekomendasi/penelitian/:id/proses"
        component={
          admin || es4 || es3 ? RekomendasiDetail : KnowledgeErrors}
      />
      <ContentRoute
        path="/knowledge/rekomendasi/penelitian/:id/detail"
        component={
          admin || es4 || es3 ? RekomendasiDetail : KnowledgeErrors}
      />
      {/* rekomendasi proposal */}
      <ContentRoute
        path="/knowledge/rekomendasi/usulan/add"
        component={
          admin || konseptor ? RekomendasiEdit : KnowledgeErrors
        }
      />

      <ContentRoute
        path="/knowledge/rekomendasi/usulan/:id/edit"
        component={
          admin || konseptor  ? RekomendasiEdit : KnowledgeErrors
        }
      />

      <ContentRoute
        path="/knowledge/rekomendasi/usulan/:id/detail"
        component={
          admin || konseptor  ? RekomendasiProposalDetail : KnowledgeErrors
        }
      />

      <ContentRoute
        path="/knowledge/rekomendasi/usulan/:id/body"
        component={
          admin || konseptor  ? RekomendasiBody : KnowledgeErrors}
      />

      {/* <ContentRoute
        path="/knowledge/rekomendasi/usulan/:id/open"
        component={
          admin || konseptor || es4 || es3
            ? KodefikasiProposalOpen
            : KnowledgeErrors
        }
      /> */}

      <ContentRoute
        path="/knowledge/rekomendasi/usulan"
        component={
          admin || konseptor ? RekomendasiProposal : KnowledgeErrors
        }
      />

      <ContentRoute path="/knowledge/rekomendasi/penelitian" component={
        admin || es4 || es3 ? Rekomendasi : KnowledgeErrors} />

      <ContentRoute
        path="/knowledge/rekomendasi/monitoring/:id/detail"
        component={DaftarRekomendasiDetail}
      />
      <ContentRoute
        path="/knowledge/rekomendasi/monitoring"
        component={DaftarRekomendasi}
      />

      {/* kajian */}

      <ContentRoute
        path="/knowledge/kajian/usulan/:id/proses"
        component={admin || konseptor ? KajianDetail : KnowledgeErrors}
      />
      <ContentRoute
        path="/knowledge/kajian/usulan/:id/detail"
        component={admin || konseptor ? KajianDetail : KnowledgeErrors}
      />
      <ContentRoute
        path="/knowledge/kajian/usulan/new"
        component={admin || konseptor ? KajianEdit : KnowledgeErrors}
      />

      <ContentRoute
        path="/knowledge/kajian/usulan/:id/body"
        component={BodyKajian}
      />
      <ContentRoute
        path="/knowledge/kajian/usulan/:id/edit"
        component={admin || konseptor ? KajianEdit : KnowledgeErrors}
      />
      <ContentRoute
        path="/knowledge/kajian/usulan/:id/terkait"
        component={admin || konseptor ? KajianTerkait : KnowledgeErrors}
      />
      <ContentRoute
        path="/knowledge/kajian/usulan"
        component={admin || konseptor ? Kajian : KnowledgeErrors}
      />
   
      <ContentRoute
        path="/knowledge/kajian/penelitian/:id/proses"
        component={
          admin || es4 || es3 ? PenelitianKajianDetail : KnowledgeErrors
        }
      />
      <ContentRoute
        path="/knowledge/kajian/penelitian/:id/detail"
        component={
          admin || es4 || es3 ? PenelitianKajianDetail : KnowledgeErrors
        }
      />
      <ContentRoute
        path="/knowledge/kajian/penelitian/new"
        component={admin || es4 || es3 ? PenelitianKajianEdit : KnowledgeErrors}
      />
      <ContentRoute
        path="/knowledge/kajian/penelitian/:id/edit"
        component={admin || es4 || es3 ? PenelitianKajianEdit : KnowledgeErrors}
      />
      <ContentRoute
        path="/knowledge/kajian/penelitian/:id/terkait"
        component={
          admin || es4 || es3 ? PenelitianKajianTerkait : KnowledgeErrors
        }
      />
      <ContentRoute
        path="/knowledge/kajian/penelitian"
        component={admin || es4 || es3 ? PenelitianKajian : KnowledgeErrors}
      />

      <ContentRoute
        path="/knowledge/kajian/monitoring/:id/proses"
        component={MonitoringKajianDetail}
      />
      <ContentRoute
        path="/knowledge/kajian/monitoring/:id/detail"
        component={MonitoringKajianDetail}
      />
      <ContentRoute
        path="/knowledge/kajian/monitoring/new"
        component={MonitoringKajianEdit}
      />
      <ContentRoute
        path="/knowledge/kajian/monitoring/:id/edit"
        component={MonitoringKajianEdit}
      />
      <ContentRoute
        path="/knowledge/kajian/monitoring/:id/terkait"
        component={MonitoringKajianTerkait}
      />
      <ContentRoute
        path="/knowledge/kajian/monitoring"
        component={MonitoringKajian}
      />

      {/* putusan */}

      <ContentRoute
        path="/knowledge/putusan/penelitian/:id/proses"
        component={
          admin || es4 || es3 ? PutusanDetail : KnowledgeErrors}
      />
      <ContentRoute
        path="/knowledge/putusan/penelitian/:id/detail"
        component={
          admin || es4 || es3 ? PutusanDetail : KnowledgeErrors}
      />
      {/* kodefikasi proposal */}
      <ContentRoute
        path="/knowledge/putusan/usulan/add"
        component={
          admin || konseptor ? PutusanEdit : KnowledgeErrors
        }
      />

      <ContentRoute
        path="/knowledge/putusan/usulan/:id/edit"
        component={
          admin || konseptor  ? PutusanEdit : KnowledgeErrors
        }
      />

      <ContentRoute
        path="/knowledge/putusan/usulan/:id/terkait"
        component={PutusanTerkait}
      />

      <ContentRoute
        path="/knowledge/putusan/usulan/:id/detail"
        component={
          admin || konseptor  ? PutusanProposalDetail : KnowledgeErrors
        }
      />

      <ContentRoute
        path="/knowledge/putusan/usulan/:id/body"
        component={
          admin || konseptor  ? PutusanBody : KnowledgeErrors}
      />
      {/* <ContentRoute
        path="/knowledge/putusan/usulan/:id/open"
        component={
          admin || konseptor || es4 || es3
            ? KodefikasiProposalOpen
            : KnowledgeErrors
        }
      /> */}

      <ContentRoute
        path="/knowledge/putusan/usulan"
        component={
          admin || konseptor ? PutusanProposal : KnowledgeErrors
        }
      />

      <ContentRoute path="/knowledge/putusan/penelitian" component={
        admin || es4 || es3 ? Putusan : KnowledgeErrors} />

      <ContentRoute
        path="/knowledge/putusan/monitoring/:id/detail"
        component={DetailPutusanDone}
      />

      <ContentRoute
        path="/knowledge/putusan/monitoring"
        component={PutusanDone}
      />

      {/* kodefikasi proposal */}
      <ContentRoute
        path="/knowledge/kodefikasi/usulan/:id/detail"
        component={
          admin || konseptor ? KodefikasiProposalDetail : KnowledgeErrors
        }
      />
      <ContentRoute
        path="/knowledge/kodefikasi/usulan/add"
        component={
          admin || konseptor ? KodefikasiProposalEdit : KnowledgeErrors
        }
      />
      <ContentRoute
        path="/knowledge/kodefikasi/usulan/:id/body"
        component={admin || konseptor ? BodyKodefikasi : KnowledgeErrors}
      />
      <ContentRoute
        path="/knowledge/kodefikasi/usulan/:id/edit"
        component={
          admin || konseptor ? KodefikasiProposalEdit : KnowledgeErrors
        }
      />
      <ContentRoute
        path="/knowledge/kodefikasi/usulan"
        component={admin || konseptor ? KodefikasiProposal : KnowledgeErrors}
      />
      <ContentRoute
        path="/knowledge/kodefikasi/penelitian/:id/proses"
        component={
          admin || es4 || es3 ? KodefikasiPenelitianDetail : KnowledgeErrors
        }
      />
      <ContentRoute
        path="/knowledge/kodefikasi/penelitian/:id/detail"
        component={
          admin || es4 || es3 ? KodefikasiPenelitianDetail : KnowledgeErrors
        }
      />
      <ContentRoute
        path="/knowledge/kodefikasi/penelitian/new"
        component={
          admin || es4 || es3 ? KodefikasiPenelitianEdit : KnowledgeErrors
        }
      />
      <ContentRoute
        path="/knowledge/kodefikasi/penelitian/:id/edit"
        component={
          admin || es4 || es3 ? KodefikasiPenelitianEdit : KnowledgeErrors
        }
      />
      <ContentRoute
        path="/knowledge/kodefikasi/penelitian"
        component={admin || es4 || es3 ? KodefikasiPenelitian : KnowledgeErrors}
      />
      <ContentRoute
        path="/knowledge/kodefikasi/monitoring/:id/proses"
        component={KodefikasiMonitoringDetail}
      />
      <ContentRoute
        path="/knowledge/kodefikasi/monitoring/:id/detail"
        component={KodefikasiMonitoringDetail}
      />
      <ContentRoute
        path="/knowledge/kodefikasi/monitoring/new"
        component={KodefikasiMonitoringEdit}
      />
      <ContentRoute
        path="/knowledge/kodefikasi/monitoring/:id/edit"
        component={KodefikasiMonitoringEdit}
      />
      <ContentRoute
        path="/knowledge/kodefikasi/monitoring"
        component={KodefikasiMonitoring}
      />

      <ContentRoute path="/knowledge/fgd/new" component={FgdEdit} />
      <ContentRoute path="/knowledge/fgd" component={Fgd} />
      <ContentRoute
        path="/knowledge/audiensi/:id/proses"
        component={AudiensiDetail}
      />
      <ContentRoute
        path="/knowledge/audiensi/:id/detail"
        component={AudiensiDetail}
      />
      <ContentRoute path="/knowledge/audiensi/new" component={AudiensiEdit} />
      <ContentRoute
        path="/knowledge/audiensi/:id/edit"
        component={AudiensiEdit}
      />

      <ContentRoute path="/knowledge/audiensi" component={Audiensi} />
      <ContentRoute
        path="/knowledge/audone/:id/detail"
        component={DaftarAudiensiDetail}
      />
      <ContentRoute
        path="/knowledge/audone"
        component={DaftarAudiensi}
      />
      <ContentRoute path="/knowledge/survei/new" component={SurveiEdit} />
      <ContentRoute
        path="/knowledge/survei/:id/proses"
        component={SurveiDetail}
      />
      <ContentRoute
        path="/knowledge/survei/:id/detail"
        component={SurveiDetail}
      />
      <ContentRoute path="/knowledge/survei/:id/edit" component={SurveiEdit} />
      <ContentRoute path="/knowledge/survei" component={Survei} />
      <ContentRoute
        path="/knowledge/surdone/:id/detail"
        component={DaftarSurveiDetail}
      />
      <ContentRoute
        path="/knowledge/surdone"
        component={DaftarSurvei}
      />
      <ContentRoute path="/knowledge/doc/new" component={DokumentasiEdit} />
      <ContentRoute path="/knowledge/doc" component={Dokumentasi} />
    </Switch>
  );
}
