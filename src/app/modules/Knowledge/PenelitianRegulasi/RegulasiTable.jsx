import BootstrapTable from "react-bootstrap-table-next";
import { useSelector } from "react-redux";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";

import {
  getRegulasiNonTerimaEs4,
  getRegulasiNonTerimaEs3,
  getRegulasiById,
  updateStatusRegulasi,
  deleteRegulasi,
} from "../Api";

import { Pagination } from "../pagination/Pagination";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import swal from "sweetalert";
import SVG from "react-inlinesvg";

import RegulasiHistory from "./RegulasiHistory";
import RegulasiHistoryReject from "./RegulasiHistoryReject";
import RegulasiOpen from "./RegulasiOpen";

function RegulasiTable() {
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id_peraturan",
    pageNumber: 1,
    pageSize: 50,
  };
  const history = useHistory();
  const { role, user } = useSelector((state) => state.auth);
  const konseptor = role.includes("ROLE_PERATURAN_KONSEPTOR");
  const es4 = role.includes("ROLE_PERATURAN_PENELITI_LVL1");
  const es3 = role.includes("ROLE_PERATURAN_PENELITI_LVL2");
  const admin = role.includes("ROLE_ADMIN_PERATURAN");
  const addProposal = () => history.push("/knowledge/regulasi/penelitian/new");

  const editProposal = (id) =>
    history.push(`/knowledge/regulasi/penelitian/${id}/edit`);

  let thePath = document.URL;
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);
  const applyProposal = (id) => {
    getRegulasiById(id).then(({ data }) => {
      updateStatusRegulasi(id, 1, 2, user.nip9).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/dashboard");
            history.replace("/knowledge/regulasi/penelitian");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace("/knowledge/regulasi/penelitian");
          });
        }
      });
    });
  };

  const deleteDialog = (id) => {
    swal({
      title: "Hapus",
      text: "Apakah Anda yakin menghapus data ini ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((ret) => {
      if (ret == true) {
        deleteRegulasi(id).then(({ data }) => {
          if (data.deleted == true) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/regulasi/penelitian");
            });
          } else {
            swal("Gagal", "Data gagal dihapus", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/regulasi/penelitian");
            });
          }
        });
      }
    });
  };

  const showHistoryPengajuan = (id) =>
    history.push(`/knowledge/regulasi/penelitian/${id}/showhistory`);

  const showHistoryPenolakan = (id) =>
    history.push(`/knowledge/regulasi/penelitian/${id}/showhistoryreject`);

  const openPeraturan = (id) =>
    history.push(`/knowledge/regulasi/penelitian/${id}/open`);

  const prosesPeraturan = (id) =>
    history.push(`/knowledge/regulasi/penelitian/${id}/proses`);

  const detailPeraturan = (id) =>
    history.push(`/knowledge/regulasi/penelitian/${id}/detail`);

  const openAddPerTerkait = (id) =>
    history.push(`/knowledge/regulasi/penelitian/${id}/terkait`);

  const addBody = (id) =>
    history.push(`/knowledge/regulasi/penelitian/${id}/body`);

  const [proposal, setProposal] = useState([]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "no_regulasi",
      text: "No Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "tgl_regulasi",
      text: "Tgl Peraturan",
      sort: true,
      //formatter: columnFormatters.DateFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "perihal",
      text: "perihal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusColumnFormatter,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        user.jabatan === "Pelaksana"
          ? columnFormatters.RegulasiActionColoumnFormatter
          : user.jabatan === "Kepala Seksi"
          ? columnFormatters.RegulasiActionColoumnFormatterEs4
          : columnFormatters.RegulasiActionColoumnFormatterEs3,
      formatExtraData: {
        deleteDialog: deleteDialog,
        applyProposal: applyProposal,
        openAddPerTerkait: openAddPerTerkait,
        editProposal: editProposal,
        showHistory: showHistoryPengajuan,
        showHistoryTolak: showHistoryPenolakan,
        openPeraturan: openPeraturan,
        detailPeraturan: detailPeraturan,
        prosesPeraturan: prosesPeraturan,
        addBody: addBody,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const { SearchBar } = Search;
  useEffect(() => {
    if (es4) {
      getRegulasiNonTerimaEs4(
        user.kantorLegacyKode,
        user.unitLegacyKode,
        "Eselon 4"
      ).then(({ data }) => {
        // setProposal(data);
        data.map((data) => {
          return data.status === "Eselon 4"
            ? setProposal((proposal) => [...proposal, data])
            : data.status === "Eselon 3"
            ? setProposal((proposal) => [...proposal, data])
            : data.status === "Tolak"
            ? setProposal((proposal) => [...proposal, data])
            : null;
        });
      });
    } else if (es3) {
      getRegulasiNonTerimaEs3(
        user.kantorLegacyKode,
        user.unitLegacyKode,
        "Eselon 3"
      ).then(({ data }) => {
        data.map((data) => {
          return data.status === "Eselon 3"
            ? setProposal((proposal) => [...proposal, data])
            : null;
        });
      });
    }
  }, []);
  const defaultSorted = [{ dataField: "id_peraturan", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: proposal.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_peraturan"
                  data={proposal}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          {/* <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right",
                            }}
                            onClick={addProposal}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button> */}
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      <Route path="/knowledge/regulasi/penelitian/:id/showhistory">
        {({ history, match }) => (
          <RegulasiHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/regulasi/penelitian");
            }}
            onRef={() => {
              history.push("/knowledge/regulasi/penelitian");
            }}
          />
        )}
      </Route>

      <Route path="/knowledge/regulasi/penelitian/:id/showhistoryreject">
        {({ history, match }) => (
          <RegulasiHistoryReject
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/regulasi/penelitian");
            }}
            onRef={() => {
              history.push("/knowledge/regulasi/penelitian");
            }}
          />
        )}
      </Route>

      <Route path="/knowledge/regulasi/penelitian/:id/open">
        {({ history, match }) => (
          <RegulasiOpen
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/regulasi/penelitian");
            }}
            onRef={() => {
              history.push("/knowledge/regulasi/penelitian");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default RegulasiTable;
