import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
    PaginationProvider
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
    sortCaret,
    headerSortingClasses, toAbsoluteUrl
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "../../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";

import { deletePeraturanTerkait, addPerTerkaitById } from "../../Api";

import { Pagination } from "../../pagination/Pagination";
import swal from "sweetalert";
import SVG from "react-inlinesvg";
import RegulasiTerkaitModal from "./RegulasiTerkaitModal";
import DokumenLainModal from "./DokumenLainModal";


function DokumenLainTableDetail({ dokLain, idPeraturan }) {

    const history = useHistory();
    const initialFilter = {
        sortOrder: "asc", // asc||desc
        sortField: "no_regulasi",
        pageNumber: 1,
        pageSize: 5
    };
    const columns = [
        {
            dataField: "any",
            text: "No",
            sort: true,
            formatter: columnFormatters.IdColumnFormatter,
            sortCaret: sortCaret,
            headerSortingClasses
        },
        {
            dataField: "download",
            text: "File Upload",
            sort: true,
            sortCaret: sortCaret,
            formatter: columnFormatters.FileDokumenFormatter,
            headerSortingClasses
        },
        {
            dataField: "keterangan",
            text: "Keterangan",
            sort: true,
            sortCaret: sortCaret,
            headerSortingClasses
        }
    ];
    //const { SearchBar } = Search;
    const defaultSorted = [{ dataField: "id_km_dok_lainnya", order: "asc" }];
    const sizePerPageList = [
        { text: "10", value: 10 },
        { text: "25", value: 25 },
        { text: "50", value: 50 }
    ];
    const pagiOptions = {
        custom: true,
        totalSize: dokLain.length,
        sizePerPageList: sizePerPageList,
        sizePerPage: initialFilter.pageSize, //default 10
        page: initialFilter.pageNumber //curent page (default 1),
    };
    const emptyDataMessage = () => { return 'No Data to Display'; }

    return (
        <>
            <>
                <div>
                    <br />
                    <br />
                    <h3>Dokumen Lainnya</h3>

                </div>

                <PaginationProvider pagination={paginationFactory(pagiOptions)}>
                    {({ paginationProps, paginationTableProps }) => {
                        return (
                            <>
                                <ToolkitProvider
                                    keyField="id_km_dok_lainnya"
                                    data={dokLain}
                                    columns={columns}
                                    search
                                >
                                    {props => (
                                        <div>
                                            <BootstrapTable
                                                {...props.baseProps}
                                                wrapperClasses="table-responsive"
                                                bordered={false}
                                                headerWrapperClasses="thead-light"
                                                classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                                                defaultSorted={defaultSorted}
                                                bootstrap4
                                                noDataIndication={emptyDataMessage}
                                                {...paginationTableProps}
                                            ></BootstrapTable>
                                            <Pagination paginationProps={paginationProps} />
                                        </div>
                                    )}
                                </ToolkitProvider>
                            </>
                        );
                    }}
                </PaginationProvider>


            </>
       </>
    );
}

export default DokumenLainTableDetail;
