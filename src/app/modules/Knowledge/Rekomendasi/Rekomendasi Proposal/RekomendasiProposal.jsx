import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../../_metronic/_partials/controls";
import RekomendasiProposalTable from "./RekomendasiProposalTable";

function RekomendasiProposal() {
  return (
    <>
      <Card>
        <CardHeader
          title="Usulan Surat Rekomendasi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <RekomendasiProposalTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default RekomendasiProposal;
