/* Library */
import React, { useEffect, useState } from "react";
import { useHistory, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import * as columnFormatters from "../../column-formatters";
import { deleteRekomendasiById, getRekomendasiNonTerimaByNip, updateStatusRekomendasi } from "../../Api";
import RekomendasiHistory from "../RekomendasiHistory";
import RekomendasiReject from "../RekomendasiReject";

function RekomendasiProposalTable() {
  const { user } = useSelector((state) => state.auth);
  const history = useHistory();
  const [rekomendasi, setRekomendasi] = useState([]);

  const addRekomendasi = () => history.push("/knowledge/rekomendasi/usulan/add");
  
  const showHistoryRekomendasi = (id) =>
    history.push(`/knowledge/rekomendasi/usulan/${id}/history`);
  
    const rejectRekomendasi = (id) =>
    history.push(`/knowledge/rekomendasi/usulan/${id}/reject`);
  
    const editRekomendasi = (id) =>
    history.push(`/knowledge/rekomendasi/usulan/${id}/edit`);
  
    const detailRekomendasi = (id) =>
    history.push(`/knowledge/rekomendasi/usulan/${id}/detail`);

    const addBody = id => history.push(`/knowledge/rekomendasi/usulan/${id}/body`);

    const applyRekomendasi = (id) => {
      swal({
        title: "Apakah Anda Ingin Mengajukan Usulan Ini?",
        text: "Klik OK untuk melanjutkan",
        icon: "warning",
        buttons: true,
        dangerMode: false,
      }).then ((willApply) => {
        if(willApply){
          updateStatusRekomendasi(id, 1, 2, user.nip9).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Usulan berhasil diajukan", "success").then(() => {
                history.push("/dashboard");
                history.replace("/knowledge/rekomendasi/usulan");
              });
            } else {
              swal("Gagal", "Usulan gagal diajukan", "error").then(() => {
                history.push("/dashboard");
                history.replace("/knowledge/rekomendasi/usulan");
              });
            }
          });
        }
      })
  };

  const deleteAction = (id) => {
    swal({
      title: "Apakah Anda Ingin Menghapus Usulan Ini?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        deleteRekomendasiById(id).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Usulan berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/rekomendasi/usulan");
            });
          } else {
            swal("Gagal", "Usulan gagal dihapus", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/rekomendasi/usulan");
            });
          }
        });
      }
    });
  };

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "km_rekomendasi.no_hsl_pemeriksaan",
      text: "No Pemeriksaan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "km_rekomendasi.tgl_pemeriksaan",
      text: "Tgl Pemeriksaan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "km_rekomendasi.perihal",
      text: "perihal Pemeriksaan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "jns_pemeriksaan.nama",
      text: "Jenis Pemeriksaan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "km_rekomendasi.status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.RekomendasiStatusColumnFormatter,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.RekomendasiProposalActionColumnFormatter,
      formatExtraData: {
        openEditDialog: editRekomendasi,
        openDeleteDialog: deleteAction,
        showHistory: showHistoryRekomendasi,
        detailRekomendasi: detailRekomendasi,
        rejectRekomendasi: rejectRekomendasi,
        applyRekomendasi: applyRekomendasi,
        addBody: addBody
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_surat",
    pageNumber: 1,
    pageSize: 5,
  };

  const defaultSorted = [{ dataField: "no_hsl_pemeriksaan", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: rekomendasi.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  useEffect(() => {
    getRekomendasiNonTerimaByNip(user.nip9).then(({ data }) => {
      // setPlan(data);
      data.map((data) => {
        return data.status !== "Terima"
          ? setRekomendasi((rekomendasi) => [...rekomendasi, data])
          : // : data.status === "Draft"
            // ? setProposal((proposal) => [...proposal, data])
            null;
      });
    });
  }, [user.kantorLegacyKode]);
  console.log(user);

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="km_rekomendasi.id_rekomendasi"
                  data={rekomendasi}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right",
                            }}
                            onClick={addRekomendasi}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/rekomendasi/usulan/:id/history">
        {({ history, match }) => (
          <RekomendasiHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/rekomendasi/usulan");
            }}
            onRef={() => {
              history.push("/knowledge/rekomendasi/usulan");
            }}
          />
        )}
      </Route>
      <Route path="/knowledge/rekomendasi/usulan/:id/reject">
        {({ history, match }) => (
          <RekomendasiReject
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/rekomendasi/usulan");
            }}
            onRef={() => {
              history.push("/knowledge/rekomendasi/usulan");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default RekomendasiProposalTable;
