import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";

import { Pagination } from "../pagination/Pagination";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import swal from "sweetalert";
import SVG from "react-inlinesvg";
import {
  getRekomendasiNonTerimaByNip,
  getRekomendasiSearchEselon3,
  getRekomendasiNonTerimaEs4
} from "../Api";
import RekomendasiHistory from "./RekomendasiHistory";
import { useSelector } from "react-redux";
import RekomendasiReject from "./RekomendasiReject";

function RekomendasiTable() {
  const { role, user } = useSelector(state => state.auth);
  const konseptor = role.includes("ROLE_PERATURAN_KONSEPTOR");
  const es4 = role.includes("ROLE_PERATURAN_PENELITI_LVL1");
  const es3 = role.includes("ROLE_PERATURAN_PENELITI_LVL2");
  const admin = role.includes("ROLE_ADMIN_PERATURAN");
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_hsl_pemeriksaan",
    pageNumber: 1,
    pageSize: 5,
  };
  const history = useHistory();
  
  const showHistoryPengajuan = (id) =>
    history.push(`/knowledge/rekomendasi/penelitian/${id}/history`);

  const prosesRekomendasi = (id) =>
    history.push(`/knowledge/rekomendasi/penelitian/${id}/proses`);

  const detailRekomendasi = (id) =>
    history.push(`/knowledge/rekomendasi/penelitian/${id}/detail`);

  const rejectRekomendasi = (id) =>
    history.push(`/knowledge/rekomendasi/penelitian/${id}/reject`);

  const [rekomendasi, setRekomendasi] = useState([]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "km_rekomendasi.no_hsl_pemeriksaan",
      text: "No Pemeriksaan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "km_rekomendasi.tgl_pemeriksaan",
      text: "Tgl Pemeriksaan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "km_rekomendasi.perihal",
      text: "perihal Pemeriksaan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "jns_pemeriksaan.nama",
      text: "Jenis Pemeriksaan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "km_rekomendasi.status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.RekomendasiStatusColumnFormatter,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        user.jabatan === "Pelaksana"
          ? columnFormatters.RekomendasiActionColumnFormatterPelaksana
          : user.jabatan === "Kepala Seksi"
          ? columnFormatters.RekomendasiActionColumnFormatterEselon4
          : columnFormatters.RekomendasiActionColumnFormatterEselon3,
      formatExtraData: {
        rejectRekomendasi: rejectRekomendasi,
        showHistory: showHistoryPengajuan,
        detailRekomendasi: detailRekomendasi,
        prosesRekomendasi: prosesRekomendasi,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  const { SearchBar } = Search;

  useEffect(() => {
    if(es4){
      getRekomendasiNonTerimaByNip(user.nip9).then(({ data }) => {
        data.map((data) => {
          return data.km_rekomendasi.status === "Draft"
          ? setRekomendasi(rekomendasi => [...rekomendasi, data])
          : data.km_rekomendasi.status === "Tolak"
          ? setRekomendasi(rekomendasi => [...rekomendasi, data])
          : null
        });
      });
      getRekomendasiNonTerimaEs4(user.kantorLegacyKode, user.unitLegacyKode).then(({ data }) => {
        data.map((data) => {
          return data.km_rekomendasi.status === "Eselon 4"
          ? setRekomendasi(rekomendasi => [...rekomendasi, data])
          : data.km_rekomendasi.status === "Eselon 3"
          ? setRekomendasi(rekomendasi => [...rekomendasi, data])
          : data.km_rekomendasi.status === "Tolak"
          ? setRekomendasi(rekomendasi => [...rekomendasi, data])
          : null
        });
    });
    }
  }, [es4, user])

  useEffect(() => {
    if(es3){
      getRekomendasiSearchEselon3(user.kantorLegacyKode, user.unitLegacyKode, "Eselon%203").then(({ data }) => {
        data.map(dt => {
    setRekomendasi(rekomendasi => [...rekomendasi, dt]);
  });
  });
    }
  }, [es3, user])

  const defaultSorted = [{ dataField: "no_hsl_pemeriksaan", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: rekomendasi.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="km_rekomendasi.id_rekomendasi"
                  data={rekomendasi}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/rekomendasi/penelitian/:id/history">
        {({ history, match }) => (
          <RekomendasiHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/rekomendasi/penelitian");
            }}
            onRef={() => {
              history.push("/knowledge/rekomendasi/penelitian");
            }}
          />
        )}
      </Route>
      <Route path="/knowledge/rekomendasi/penelitian/:id/reject">
        {({ history, match }) => (
          <RekomendasiReject
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/rekomendasi/penelitian");
            }}
            onRef={() => {
              history.push("/knowledge/rekomendasi/penelitian");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default RekomendasiTable;
