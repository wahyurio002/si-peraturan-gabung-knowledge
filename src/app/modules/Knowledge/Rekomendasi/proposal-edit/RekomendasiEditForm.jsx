import React, { useState, useEffect } from "react";
import {useSelector } from "react-redux";
import Select from "react-select";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
  Checkbox
} from "./helpers";
import CustomFileInput from "./helpers/CustomFileInput";
import { getJenisPemeriksaan } from "../../Api";

function RekomendasiEditForm({ proposal, btnRef, saveProposal }) {
  const [jenis, setJenis] = useState([]);
  const { user } = useSelector((state) => state.auth);

  // Validation schema
  const RekomendasiEditSchema = Yup.object().shape({
    no_hsl_pemeriksaan: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("No Surat is required"),
    tgl_pemeriksaan: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat is required"),
    id_detiljns:  Yup.string()
    .min(1, "Jenis Pemeriksaan is required")
    .required("Jenis Pemeriksaan is required"),
    perihal: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Perihal is required"),
    thn_periksa:  Yup.string().min(1, "Tahun Pemeriksaan is required")
    .required("Tahun Pemeriksaan is required"),
    // file: Yup.mixed()
    //   .required("A file is required")
    //   .test(
    //     "fileSize",
    //     "File too large",
    //     value => value && value.size <= FILE_SIZE
    //   )
    //   .test(
    //     "fileFormat",
    //     "Unsupported Format",
    //     value => value && SUPPORTED_FORMATS.includes(value.type)
    //   )
  });

  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    getJenisPemeriksaan().then(({ data }) => {
      setJenis(data);
    });
  }, []);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        validationSchema={RekomendasiEditSchema}
        onSubmit={values => {
          saveProposal(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue
        }) => {

          const handleChangeNip = () => {
            setFieldValue("nip_pengusul", user.nip9);
          };
          const handleChangeKdKantor = () => {
            setFieldValue("kd_kantor", user.kantorLegacyKode);
          };
          const handleChangeKdUnit = () => {
            setFieldValue("kd_unit_org", user.unitLegacyKode);
          };

          return (
            <>
              <Form className="form form-label-right">

                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    name="no_hsl_pemeriksaan"
                    component={Input}
                    placeholder="Nomor Hasil Pemeriksaan"
                    label="Nomor Hasil Pemeriksaan"
                    onClick={()=>handleChangeNip()}
                  />
                </div>

                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField name="tgl_pemeriksaan" label="Tanggal Hasil Pemeriksaan" />
                </div>

                {/* FIELD jenis */}
                <div className="form-group row">
                  <Sel name="id_detiljns" label="Jenis Pemeriksaan">
                    <option value="">Jenis Pemeriksaan</option>
                    {jenis.map((data, index) => (
                      <option key={index} value={data.id}>{data.nama}</option>
                    ))}
                  </Sel>
                </div>

                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                    onClick={()=>handleChangeKdKantor()}
                  />
                </div>

                <div className="form-group row">
                  <Sel name="thn_periksa" label="Tahun Pemeriksaan">
                  <option>Tahun Pemeriksaan</option>
                  <option values="2020">2020</option>
                      <option values="2021">2021</option>
                  </Sel>
                </div>

                {/* FIELD UPLOAD FILE */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Upload File
                  </label>
                  <div className="col-lg-8 col-xl-6">
                    <Field
                      name="file"
                      component={CustomFileInput}
                      title="Select a file"
                      label="File"
                      style={{ display: "flex" }}
                    />
                  </div>
                </div>
                
                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                  onClick={()=>handleChangeKdUnit()}
                ></button>
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default RekomendasiEditForm;