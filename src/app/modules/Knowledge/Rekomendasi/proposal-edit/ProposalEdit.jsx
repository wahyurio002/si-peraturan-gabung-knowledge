import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../_metronic/layout";
import RekomendasiEditForm from "./RekomendasiEditForm";
import RekomendasiEditFooter from "./RekomendasiEditFooter";
import { uploadFile, getRekomendasiById, saveRekomendasi, updateRekomendasi, updateStatusRekomendasi } from "../../Api";
import swal from "sweetalert";

const initValues = {
  alasan_tolak: "" ,
  body: "" ,
  file_upload: "" ,
  id_detiljns: "" ,
  kd_kantor: "" ,
  kd_unit_org: "" ,
  nip_pengusul: "" ,
  nip_pjbt_es3: "" ,
  nip_pjbt_es4: "" ,
  no_hsl_pemeriksaan: "" ,
  perihal: "" ,
  status: "" ,
  tgl_pemeriksaan: "",
  thn_periksa: "" 
};

function RekomendasiEdit({
  history,
  match: {
    params: { id }
  }
}) {
  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [rekomendasi, setRekomendasi] = useState();

  useEffect(() => {
    let _title = id ? "Edit Surat Rekomendasi" : "Tambah Surat Rekomendasi";

    setTitle(_title);
    suhbeader.setTitle(_title);

    if (id) {
      getRekomendasiById(id).then(({ data }) => {

        setRekomendasi({
          id_rekomendasi: data.id_rekomendasi,
          no_hsl_pemeriksaan: data.no_hsl_pemeriksaan,
          tgl_pemeriksaan: data.tgl_pemeriksaan,
          id_detiljns: data.id_detiljns,
          thn_periksa: data.thn_periksa,
          perihal: data.perihal,
          file_upload: data.file_upload,
          alasan_tolak: data.alasan_tolak,
          body: data.body,
          nip_pengusul: data.nip_pengusul,
          nip_pjbt_es4: data.nip_pjbt_es4,
          nip_pjbt_es3: data.nip_pjbt_es3,
          wkt_usul: data.wkt_usul,
          wkt_teliti_es4: data.wkt_teliti_es4,
          wkt_teliti_es3: data.wkt_teliti_es3,
          status: data.status,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update,
          kd_kantor: data.kd_kantor,
          kd_unit_org: data.kd_unit_org
        });
      });
    }
  }, [id, suhbeader]);
  const btnRef = useRef();

  console.log(rekomendasi);

  const saveSurat = values => {
    if (!id) {
      const formData = new FormData();
      formData.append("file", values.file);
      uploadFile(formData)
        .then(({ data }) =>
          saveRekomendasi(
            "" ,
            "" ,
            data.message ,
            values.id_detiljns ,
            values.kd_kantor ,
            values.kd_unit_org,
            values.nip_pengusul ,
            "" ,
            "" ,
            values.no_hsl_pemeriksaan ,
            values.perihal ,
            "" ,
            values.tgl_pemeriksaan,
            values.thn_periksa 
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(
                () => {
                  history.push("/knowledge/rekomendasi/usulan");
                }
              );
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push("/knowledge/rekomendasi/usulan/new");
              });
            }
          })
        )
        .catch(() => window.alert("Oops Something went wrong !"));

    } else {

      if (values.file) {
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFile(formData)
          .then(({ data }) =>
          updateRekomendasi(
            values.id_rekomendasi,
            "" ,
            "" ,
            data.message ,
            values.id_detiljns ,
            values.kd_kantor ,
            values.kd_unit_org,
            values.nip_pengusul ,
            "" ,
            "" ,
            values.no_hsl_pemeriksaan ,
            values.perihal ,
            "" ,
            values.tgl_pemeriksaan,
            values.thn_periksa
          ).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Data berhasil disimpan", "success").then(
                  () => {
                    history.push("/knowledge/rekomendasi/usulan");
                  }
                );
              } else {
                swal("Gagal", "Data gagal disimpan", "error").then(() => {
                  history.push("/knowledge/rekomendasi/usulan/new");
                });
              }
            })
          )
          .catch(() => window.alert("Oops Something went wrong !"));
      } else {
        updateRekomendasi(
            values.id_rekomendasi,
            "" ,
            "" ,
            values.file_upload ,
            values.id_detiljns ,
            values.kd_kantor ,
            values.kd_unit_org,
            values.nip_pengusul ,
            "" ,
            "" ,
            values.no_hsl_pemeriksaan ,
            values.perihal ,
            "" ,
            values.tgl_pemeriksaan,
            values.thn_periksa
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
              history.push("/knowledge/rekomendasi/usulan");
            });
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/knowledge/rekomendasi/usulan/new");
            });
          }
        });
      }

      if (values.status == "Tolak") {
        updateStatusRekomendasi(id, 1).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Rekomendasi Berubah Menjadi Draft", "success").then(
              () => {
                history.push("/dashboard");
                history.replace("/knowledge/rekomendasi/usulan");
              }
            );
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/rekomendasi/usulan");
            });
          }
        });
      }
    }
  }

  const backToRekomendasiList = () => {
    history.push(`/knowledge/rekomendasi/usulan`);
  };

  const setDisabled = val => {
    setIsDisabled(val);
  };
  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <RekomendasiEditForm
            actionsLoading={actionsLoading}
            proposal={rekomendasi || initValues}
            btnRef={btnRef}
            saveProposal={saveSurat}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <RekomendasiEditFooter
          backAction={backToRekomendasiList}
          btnRef={btnRef}
        ></RekomendasiEditFooter>
      </CardFooter>
    </Card>
  );
}

export default RekomendasiEdit;
