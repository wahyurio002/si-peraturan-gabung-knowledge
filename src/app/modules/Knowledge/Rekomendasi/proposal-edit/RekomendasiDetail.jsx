import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../_metronic/layout";
import RekomendasiDetailForm from "./RekomendasiDetailForm";
import RekomendasiDetailFooter from "./RekomendasiDetailFooter";
import {
  getPenegasanById,
  getRekomendasiById,
  updateStatusPenegasan,
  updateStatusRekomendasi,
} from "../../Api";
import swal from "sweetalert";
import { useSelector } from "react-redux";

const initValues = {
  alasan_tolak: "" ,
  body: "" ,
  file_upload: "" ,
  id_detiljns: "" ,
  kd_kantor: "" ,
  kd_unit_org: "" ,
  nip_pengusul: "" ,
  nip_pjbt_es3: "" ,
  nip_pjbt_es4: "" ,
  no_hsl_pemeriksaan: "" ,
  perihal: "" ,
  status: "" ,
  tgl_pemeriksaan: "",
  thn_periksa: "" 
};

function RekomendasiDetail({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const { role, user } = useSelector((state) => state.auth);
  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [rekomendasi, setRekomendasi] = useState();
  const [RekomendasiDetail, setRekomendasiDetail] = useState([]);
  let thePath = document.URL;
  let nextStatus = "";
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);
  const [statNow, setstatNow] = useState("");
  
  const acceptAction = (statusNow) => {
    if (statusNow == "Eselon 4") {
      nextStatus = 3;
    } else if (statusNow == "Eselon 3") {
      nextStatus = 6;
    }

    updateStatusRekomendasi(id, statNow, nextStatus, user.nip9).then(({ status }) => {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Usulan berhasil disetujui", "success").then(() => {
          history.push("/dashboard");
          history.replace("/knowledge/rekomendasi/penelitian");
        });
      } else {
        swal("Gagal", "Usulan gagal disetujui", "error").then(() => {
          history.push("/dashboard");
          history.replace("/knowledge/rekomendasi/penelitian");
        });
      }
    });
  };

  function tolakDialog(idPer, statNow) {
    swal("Masukan Alasan Tolak:", {
      title: "Tolak",
      text: "Apakah Anda yakin menolak usulan ini ?",
      icon: "warning",
      buttons: true,
      content: "input",
      content: {
        element: "input",
        attributes: {
          placeholder: "masukan alasan",
          type: "textarea",
        },
      },
    }).then((value) => {
      if (value != null) {
        updateStatusRekomendasi(idPer, statNow, 5, user.nip9, value)
        .then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Usulan berhasil ditolak", "success").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/rekomendasi/penelitian");
            });
          } else {
            swal("Gagal", "Usulan gagal ditolak", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/rekomendasi/penelitian");
            });
          }
        });
      }
    });
  }

  function tombolProses() {
    if (lastPath == "proses") {
      return (
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            onClick={() => tolakDialog(id, statNow)}
            className="btn btn-danger"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tolak
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            onClick={() => setujuDialog(id)}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
            }}
          >
            <i className="fas fa-check"></i>
            Setuju
          </button>
        </div>
      );
    } else {
      return "";
    }
  }

  const setujuDialog = (id) => {
    swal({
      title: "Setuju",
      text: "Apakah Anda yakin menyetujui usulan ini ?",
      icon: "warning",
      buttons: true,
    }).then((ret) => {
      if (ret == true) {
        acceptAction(rekomendasi.status);
      }
    });
  };

  useEffect(() => {
    if (lastPath == "proses") {
      let _title = "Proses Surat Penegasan";
      setTitle(_title);
      suhbeader.setTitle(_title);
    } else {
      let _title = "Detail Surat Penegasan";
      setTitle(_title);
      suhbeader.setTitle(_title);
    }

    getRekomendasiById(id).then(({ data }) => {
      setRekomendasi({
        id_rekomendasi: data.id_rekomendasi,
        no_hsl_pemeriksaan: data.no_hsl_pemeriksaan,
        tgl_pemeriksaan: data.tgl_pemeriksaan,
        id_detiljns: data.id_detiljns,
        thn_periksa: data.thn_periksa,
        perihal: data.perihal,
        file_upload: data.file_upload,
        alasan_tolak: data.alasan_tolak,
        body: data.body,
        nip_pengusul: data.nip_pengusul,
        nip_pjbt_es4: data.nip_pjbt_es4,
        nip_pjbt_es3: data.nip_pjbt_es3,
        wkt_usul: data.wkt_usul,
        wkt_teliti_es4: data.wkt_teliti_es4,
        wkt_teliti_es3: data.wkt_teliti_es3,
        status: data.status,
        wkt_create: data.wkt_create,
        wkt_update: data.wkt_update,
        kd_kantor: data.kd_kantor,
        kd_unit_org: data.kd_unit_org
      });
      if (data.status == "Eselon 4") {
        setstatNow(2);
      } else if (data.status == "Eselon 3") {
        setstatNow(3);
      }
    });
  }, [id, suhbeader]);

  const btnRef = useRef();

  const saveRekomendasiDetail = (values) => {};

  const backToRekomendasiList = (values) => {
    history.push(`/knowledge/rekomendasi/penelitian`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <RekomendasiDetailForm
            actionsLoading={actionsLoading}
            proposal={rekomendasi || initValues}
            RekomendasiDetail={RekomendasiDetail}
            idRekomendasi={id}
            btnRef={btnRef}
            saveRekomendasiDetail={saveRekomendasiDetail}
            setDisabled={setDisabled}
          />
        </div>
        <br></br>

        {tombolProses()}
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <RekomendasiDetailFooter
          backAction={backToRekomendasiList}
          btnRef={btnRef}
        ></RekomendasiDetailFooter>
      </CardFooter>
    </Card>
  );
}

export default RekomendasiDetail;
