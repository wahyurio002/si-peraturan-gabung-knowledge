import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";
import { useSubheader } from "../../../../../_metronic/layout";
import DaftarRekomendasiDetailForm from "./DaftarRekomendasiDetailForm";
import DaftarRekomendasiDetailFooter from "./DaftarRekomendasiDetailFooter";
import {
  getRekomendasiById,
} from "../../Api";

const initValues = {
  alasan_tolak: "",
  body: "",
  file_upload: "",
  id_detiljns: "",
  nip_pengusul: "",
  nip_pjbt_es3: "",
  nip_pjbt_es4: "",
  no_hsl_pemeriksaan: "",
  perihal: "",
  status: "",
  tgl_pemeriksaan: "",
  thn_periksa: ""
};

function DaftarRekomendasiDetail({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [rekomendasiterima, setRekomendasiTerima] = useState();
  const [RekomendasiTerimaDetail, setRekomendasiTerimaDetail] = useState([]);
  let thePath = document.URL;
  let nextStatus = "";
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);

  useEffect(() => {
      let _title = "Detail Rekomendasi";
      setTitle(_title);
      suhbeader.setTitle(_title);

    getRekomendasiById(id).then(({ data }) => {
      setRekomendasiTerima({
        id_rekomendasi: data.id_rekomendasi,
        no_hsl_pemeriksaan: data.no_hsl_pemeriksaan,
        tgl_pemeriksaan: data.tgl_pemeriksaan,
        id_detiljns: data.id_detiljns,
        thn_periksa: data.thn_periksa,
        perihal: data.perihal,
        file_upload: data.file_upload,
        alasan_tolak: data.alasan_tolak,
        body: data.body,
        nip_pengusul: data.nip_pengusul,
        nip_pjbt_es4: data.nip_pjbt_es4,
        nip_pjbt_es3: data.nip_pjbt_es3,
        wkt_usul: data.wkt_usul,
        wkt_teliti_es4: data.wkt_teliti_es4,
        wkt_teliti_es3: data.wkt_teliti_es3,
        status: data.status,
        wkt_create: data.wkt_create,
        wkt_update: data.wkt_update,
        });
    });
    }, [id, suhbeader]);

  const btnRef = useRef();

  const backToDaftarRekomendasiList = () => {
    history.push(`/knowledge/rekomendasi/monitoring`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <DaftarRekomendasiDetailForm
            actionsLoading={actionsLoading}
            proposal={rekomendasiterima || initValues}
            RekomendasiTerimaDetail={RekomendasiTerimaDetail}
            idRekomendasi={id}
            btnRef={btnRef}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <DaftarRekomendasiDetailFooter
          backAction={backToDaftarRekomendasiList}
          btnRef={btnRef}
        ></DaftarRekomendasiDetailFooter>
      </CardFooter>
    </Card>
  );
}

export default DaftarRekomendasiDetail;
