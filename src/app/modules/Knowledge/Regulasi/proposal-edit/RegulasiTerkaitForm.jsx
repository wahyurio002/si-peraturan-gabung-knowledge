import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Field, Formik, Form } from "formik";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel
} from "../proposal-edit/helpers";
import { getJenisPeraturan, getTopik, getPeraturanTerkait,getDokumenPer } from "../../Api";
import RegulasiTerkaitTable from "./RegulasiTerkaitTable";
import DokumenLainTable from "./DokumenLainTable";


function RegulasiTerkaitForm({ proposal, btnRef,saveRegulasiTerkait, peraturanTerkait, idPeraturan }) {
  const [jenisPeraturan, setJenisPeraturan] = useState([]);
  const [topik, setTopik] = useState([]);
  const [dokLain, setDoklain] = useState([]);

  const { user } = useSelector((state) => state.auth);

  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];


  useEffect(() => {


    getJenisPeraturan().then(({ data }) => {
      setJenisPeraturan(data);
    });




    getTopik().then(({ data }) => {
      setTopik(data);
    });




  }, []);

  useEffect(() => {

    getDokumenPer(idPeraturan).then(({ data }) => {
      setDoklain(data);
    });


  }, []);

  return (
    <>

      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        onSubmit={values => {
          //console.log(values);
          saveRegulasiTerkait(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
        }) => {

          const handleChangeNip = () => {
            setFieldValue("nip_perekam", user.nip9);
          }

          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    disabled
                    name="no_regulasi"
                    component={Input}
                    placeholder="Nomor Regulasi"
                    label="Nomor Regulasi"
                  // onClick={()=>handleChangeNip()}
                  />
                </div>
                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField name="tgl_regulasi" label="Tanggal Regulasi" disabled />
                </div>
                {/* FIELD jenis */}
                <div className="form-group row">
                  <Sel name="jns_regulasi" label="Jenis Regulasi" disabled>
                    {jenisPeraturan.map((data, index) => (

                      <option key={index} value={data.id_jnsperaturan}>{data.nm_jnsperaturan}</option>

                    ))}
                  </Sel>
                </div>

                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                    disabled
                  />
                </div>

                <div className="form-group row">
                  <Sel name="id_topik" label="topik" disabled>
                    {topik.map((data, index) => (

                      <option key={index} value={data.id}>{data.nama}</option>

                    ))}
                  </Sel>
                </div>


                <RegulasiTerkaitTable
                  peraturanTerkait={peraturanTerkait}
                  idPeraturan={idPeraturan} />

                <DokumenLainTable
                  dokLain={dokLain}
                  idPeraturan={idPeraturan} />

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>

              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default RegulasiTerkaitForm;