import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import RegDoneTable from "./RegDoneTable";

function RegDone() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Regulasi Perpajakan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <RegDoneTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default RegDone;
