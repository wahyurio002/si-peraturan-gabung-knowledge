import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";

import {
  getPutusanNonTerimaByEs4,
  getPutusanNonTerimaByNip,
  getPutusanSearchEselon3
} from "../Api";

import { Pagination } from "../pagination/Pagination";

import PutusanHistory from "./PutusanHistory";
import { useSelector } from "react-redux";
import PutusanReject from "./PutusanReject";

function PutusanTable() {
  const { role, user } = useSelector(state => state.auth);
  console.log(role);
  console.log(user);
  const konseptor = role.includes("ROLE_PERATURAN_KONSEPTOR");
  const es4 = role.includes("ROLE_PERATURAN_PENELITI_LVL1");
  const es3 = role.includes("ROLE_PERATURAN_PENELITI_LVL2");
  const admin = role.includes("ROLE_ADMIN_PERATURAN");
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id_putpengadilan",
    pageNumber: 1,
    pageSize: 5
  };
  const history = useHistory();

  const rejectPutusan = (id) =>
  history.push(`/knowledge/putusan/penelitian/${id}/reject`);

  const showHistoryPengajuan = id =>
    history.push(`/knowledge/putusan/penelitian/${id}/history`);

  const prosesPutusan = id => history.push(`/knowledge/putusan/penelitian/${id}/proses`);

  const detailPutusan = id => history.push(`/knowledge/putusan/penelitian/${id}/detail`);

  const [proposal, setProposal] = useState([]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "putusanPengadilan.no_putusan",
      text: "No Putusan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "putusanPengadilan.tgl_putusan",
      text: "Tgl Putusan",
      sort: true,
      //formatter: columnFormatters.DateFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "bentukPengadilan.nama",
      text: "Bentuk Pengadilan",
      sort: true,
      //formatter: columnFormatters.DateFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },

    {
      dataField: "putusanPengadilan.perihal",
      text: "perihal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "putusanPengadilan.status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusPutusanColumnFormatter,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        user.jabatan === "Pelaksana"
          ? columnFormatters.PutusanActionColumnFormatterPelaksana
          : user.jabatan === "Kepala Seksi"
          ? columnFormatters.PutusanActionColumnFormatterEselon4
          : columnFormatters.PutusanActionColumnFormatterEselon3,
      formatExtraData: {
        showHistory: showHistoryPengajuan,
        detailPutusan: detailPutusan,
        prosesPutusan: prosesPutusan,
        rejectPutusan: rejectPutusan
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      }
    }
  ];

  const { SearchBar } = Search;

  useEffect(() => {
    if(es4){
      getPutusanNonTerimaByNip(user.nip9).then(({ data }) => {
        data.map((data) => {
          return data.putusanPengadilan.status === "Draft"
          ? setProposal(proposal => [...proposal, data])
          : data.putusanPengadilan.status === "Tolak"
          ? setProposal(proposal => [...proposal, data])
          : null
        });
      });
      getPutusanNonTerimaByEs4(user.kantorLegacyKode, user.unitLegacyKode).then(({ data }) => {
        data.map((data) => {
          return data.putusanPengadilan.status === "Eselon 4"
          ? setProposal(proposal => [...proposal, data])
          : data.putusanPengadilan.status === "Eselon 3"
          ? setProposal(proposal => [...proposal, data])
          : data.putusanPengadilan.status === "Tolak"
          ? setProposal(proposal => [...proposal, data])
          : null
        });
    });
    }
  }, [es4, user])

  useEffect(() => {
    if(es3){
      getPutusanSearchEselon3(user.kantorLegacyKode, user.unitLegacyKode, "Eselon%203").then(({ data }) => {
        data.map(dt => {
    setProposal(proposal => [...proposal, dt]);
  });
  });
    }
  }, [es3, user])

  const defaultSorted = [{ dataField: "id_putpengadilan", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: proposal.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="putusanPengadilan.id_putpengadilan"
                  data={proposal}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      <Route path="/knowledge/putusan/penelitian/:id/history">
        {({ history, match }) => (
          <PutusanHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/putusan/penelitian");
            }}
            onRef={() => {
              history.push("/knowledge/putusan/penelitian");
            }}
          />
        )}
      </Route>
      <Route path="/knowledge/putusan/penelitian/:id/reject">
        {({ history, match }) => (
          <PutusanReject
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/putusan/penelitian");
            }}
            onRef={() => {
              history.push("/knowledge/putusan/penelitian");
            }}
          />
        )}
      </Route>

      {/* <Route path="/knowledge/regulasi/:id/open">
        {({ history, match }) => (
          <RegulasiOpen
            show={match != null}
            id={match && match.params.id}
            onHide={() => { 
              history.push("/knowledge/regulasi");
            }}
            onRef={() => {
              history.push("/knowledge/regulasi");
            }}
          />
        )}
      </Route> */}
    </>
  );
}

export default PutusanTable;
