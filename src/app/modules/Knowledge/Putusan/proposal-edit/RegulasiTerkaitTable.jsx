import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses, toAbsoluteUrl
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "../../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";

import { deletePeraturanTerkait } from "../../Api";

import { Pagination } from "../../pagination/Pagination";
import swal from "sweetalert";
import SVG from "react-inlinesvg";
import RegulasiTerkaitModal from "./RegulasiTerkaitModal";


function RegulasiTerkaitTable({ peraturanTerkait, idPutusan }) {

  const history = useHistory();


  const addPeraturanTerkait = () =>
    history.push(`/knowledge/putusan/usulan/${idPutusan}/terkait/show`);


  const deleteDialog = idPerTerkait => {

    swal({
      title: "Hapus",
      text: "Apakah Anda yakin menghapus data ini ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((ret) => {
        if (ret == true) {

          deletePeraturanTerkait(idPerTerkait)
            .then(({ data }) => {
              if (data.deleted == true) {
                swal("Berhasil", "Data berhasil dihapus", "success").then(
                  () => {
                    history.push('/dashboard');
                    history.replace(`/knowledge/putusan/usulan/${idPutusan}/terkait`);
                  }
                );

              } else {
                swal("Gagal", "Data gagal dihapus", "error").then(
                  () => {
                    history.push('/dashboard');
                    history.replace(`/knowledge/putusan/usulan/${idPutusan}/terkait`);
                  }
                );


              }

            })

        }



      });




  };


  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_regulasi",
    pageNumber: 1,
    pageSize: 5
  };
  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "no_regulasi",
      text: "No Peraturan Terkait",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "perihal",
      text: "perihal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.RegulasiTerkaitActionColoumnFormatter,
      formatExtraData: {
        deleteDialog: deleteDialog,
        // applyProposal: applyProposal,
        // openAddPerTerkait: openAddPerTerkait
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const { SearchBar } = Search;
  const defaultSorted = [{ dataField: "no_regulasi", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: peraturanTerkait.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => { return 'No Data to Display'; }



  return (
    <>
      <>
        <div>
          <br />
          <br />
          <h3>Peraturan Terkait</h3>

        </div>

        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_peraturan"
                  data={peraturanTerkait}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right"
                            }}
                          onClick={addPeraturanTerkait}

                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>


      </>

      <Route path="/knowledge/putusan/usulan/:id/terkait/show">
        {({ history, match }) => (
          <RegulasiTerkaitModal
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push(`/knowledge/putusan/usulan/${idPutusan}/terkait`);
            }}
            onRef={() => {
              history.push(`/knowledge/putusan/usulan/${idPutusan}/terkait`);
            }}
          />
        )}
      </Route>




    </>
  );
}

export default RegulasiTerkaitTable;
