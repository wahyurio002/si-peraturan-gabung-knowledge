import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";

import { getPutusanSearchKantor, getPutusanTerimaByKantor  } from "../Api";

import { Pagination } from "../pagination/Pagination";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import swal from "sweetalert";
import SVG from "react-inlinesvg";
import PutusanHistory from "./PutusanHistory"
import { useSelector } from "react-redux";



function PutusanDoneTable() {
  const { role, user } = useSelector(state => state.auth);
  console.log(user);
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id_putpengadilan",
    pageNumber: 1,
    pageSize: 50
  };
  const history = useHistory();
  let thePath = document.URL;
  const lastPath = thePath.substring(thePath.lastIndexOf('/') + 1);
  const showHistoryPengajuan = id => history.push(`/knowledge/putusan/monitoring/${id}/history`);

  const detailPutusan = id => history.push(`/knowledge/putusan/monitoring/${id}/detail`);


  const [proposal, setProposal] = useState([]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "putusanPengadilan.no_putusan",
      text: "No Putusan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "putusanPengadilan.tgl_putusan",
      text: "Tgl Putusan",
      sort: true,
      //formatter: columnFormatters.DateFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "bentukPengadilan.nama",
      text: "Bentuk Pengadilan",
      sort: true,
      //formatter: columnFormatters.DateFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },

    {
      dataField: "putusanPengadilan.perihal",
      text: "perihal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "putusanPengadilan.status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusPutusanColumnFormatter,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.DaftarPutusanColumnActionFormatter,
      formatExtraData: {
        showHistory : showHistoryPengajuan,
        detailPutusan : detailPutusan,
        //addBody : addBody
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];


  const { SearchBar } = Search;
 
    useEffect(() => {
      getPutusanSearchKantor(user.kantorLegacyKode).then(({ data }) => {
        data.map((data) => {
          return data.putusanPengadilan.status === "Eselon 4"
          ? setProposal(proposal => [...proposal, data])
          : data.putusanPengadilan.status === "Eselon 3"
          ? setProposal(proposal => [...proposal, data])
          : data.putusanPengadilan.status === "Terima"
          ? setProposal(proposal => [...proposal, data])
          : null
        });
    });
    }, []);

  const defaultSorted = [{ dataField: "no_putusan", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "25", value: 25 },
    { text: "10", value: 10 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: proposal.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => { return 'No Data to Display'; }

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="putusanPengadilan.id_putpengadilan"
                  data={proposal}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      <Route path="/knowledge/putusan/monitoring/:id/history">
        {({ history, match }) => (
          <PutusanHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/putusan/monitoring");
            }}
            onRef={() => {
              history.push("/knowledge/putusan/monitoring");
            }}
          />
        )}
      </Route>





    </>
  );
}

export default PutusanDoneTable;
