import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../../_metronic/_partials/controls";
import PutusanProposalTable from "../Putusan Proposal/PutusanProposalTable";

function PutusanProposal() {
  return (
    <>
      <Card>
        <CardHeader
          title="Usulan Putusan Pengadilan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <PutusanProposalTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default PutusanProposal;
