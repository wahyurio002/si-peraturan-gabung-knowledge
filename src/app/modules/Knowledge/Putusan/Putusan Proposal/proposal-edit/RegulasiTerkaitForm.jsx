import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Field, Formik, Form } from "formik";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel
} from "../../proposal-edit/helpers";
import { getJenisPengadilan } from "../../../Api";
import RegulasiTerkaitTable from "./RegulasiTerkaitTable";

function RegulasiTerkaitForm({ proposal, btnRef,saveRegulasiTerkait, peraturanTerkait, idPutusan }) {
  const [bentukPengadilan, setBentukPengadilan] = useState([]);
  const { user } = useSelector((state) => state.auth);

  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];


  useEffect(() => {
    getJenisPengadilan().then(({ data }) => {
      setBentukPengadilan(data);
    });
  }, []);



  return (
    <>

      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        onSubmit={values => {
          //console.log(values);
          saveRegulasiTerkait(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
        }) => {

          const handleChangeNip = () => {
            setFieldValue("nip_perekam", user.nip9);
          }

          return (
            <>
              <Form className="form form-label-right">
              <div className="form-group row">
                  <Field
                    name="no_putusan"
                    component={Input}
                    placeholder="Nomor Putusan"
                    label="Nomor Putusan"
                    disabled
                  />
                </div>

                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField name="tgl_putusan" label="Tanggal Putusan" disabled />
                </div>
                {/* FIELD jenis */}
                <div className="form-group row">
                  <Sel name="id_detiljns" label="Bentuk Pengadilan" disabled>
                    <option value=""></option>
                    {bentukPengadilan.map((data, index) => (
                      <option key={index} value={data.id}>
                        {data.nama}
                      </option>
                    ))}
                  </Sel>
                </div>

                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                    disabled
                  />
                </div>


                <RegulasiTerkaitTable
                  peraturanTerkait={peraturanTerkait}
                  idPutusan={idPutusan} />

                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>

              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default RegulasiTerkaitForm;