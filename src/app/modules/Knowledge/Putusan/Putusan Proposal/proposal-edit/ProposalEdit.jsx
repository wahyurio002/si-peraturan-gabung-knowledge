import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../../_metronic/layout";
import ProposalEditForm from "./ProposalEditForm";
import ProposalEditFooter from "./ProposalEditFooter";
import {
  updateStatusPutusan,
  getPutusanById,
  uploadFile,
  savePutusan,
  updatePutusan,
} from "../../../Api";
import swal from "sweetalert";
import { useSelector } from "react-redux";

const initValues = {
  alasan_tolak: "",
  body: "",
  file_upload: "",
  id_detiljns: "",
  kd_kantor: "",
  kd_unit_org: "",
  nip_pengusul: "",
  nip_pjbt_es3: "",
  nip_pjbt_es4: "",
  no_putusan: "",
  perihal: "",
  status: "",
  tgl_putusan: ""
};

function PutusanEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const { user } = useSelector(state => state.auth);
  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [proposal, setProposal] = useState();

  useEffect(() => {
    let _title = id ? "Edit Putusan" : "Tambah Putusan";

    setTitle(_title);
    suhbeader.setTitle(_title);

    if (id) {
      getPutusanById(id).then(({ data }) => {
        setProposal({
          id_putpengadilan: data.id_putpengadilan,
          no_putusan: data.no_putusan,
          tgl_putusan: data.tgl_putusan,
          id_detiljns: data.id_detiljns,
          perihal: data.perihal,
          file_upload: data.file_upload,
          alasan_tolak: data.alasan_tolak,
          body: data.body,
          nip_pengusul: data.nip_pengusul,
          nip_pjbt_es4: data.nip_pjbt_es4,
          nip_pjbt_es3: data.nip_pjbt_es3,
          wkt_usul: data.wkt_usul,
          wkt_teliti_es4: data.wkt_teliti_es4,
          wkt_teliti_es3: data.wkt_teliti_es3,
          status: data.status,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update,
          kd_kantor: data.kd_kantor,
          kd_unit_org: data.kd_unit_org,
        });
      });
    }
  }, [id, suhbeader]);
  const btnRef = useRef();

  const saveProposal = values => {
    if (!id) {
      const formData = new FormData();
      formData.append("file", values.file);
      uploadFile(formData)
        .then(({ data }) =>
        savePutusan(
          "",
          values.body,
          data.message,
          values.id_detiljns,
          values.kd_kantor,
          values.kd_unit_org,
          values.nip_pengusul,
          "",
          "",
          values.no_putusan,
          values.perihal,
          "",
          values.tgl_putusan,
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Usulan berhasil dibuat", "success").then(() => {
              history.push("/knowledge/putusan/usulan");
            });
          } else {
            swal("Gagal", "Usulan gagal dibuat", "error").then(() => {
              history.push("/knowledge/putusan/usulan/new");
            });
          }
        })
        )
        .catch(() => window.alert("Oops Something went wrong !"));

    } else {

      if (values.file) {
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFile(formData)
          .then(({ data }) =>
          updatePutusan(
            values.id_putpengadilan,
            "",
            values.body,
            data.message,
            values.id_detiljns,
            values.kd_kantor,
            values.kd_unit_org,
            values.nip_pengusul,
            "",
            "",
            values.no_putusan,
            values.perihal,
            "",
            values.tgl_putusan,
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Usulan berhasil diubah", "success").then(
                () => {
                  history.push("/knowledge/putusan/usulan");
                }
              );
            } else {
              swal("Gagal", "Usulan gagal diubah", "error").then(() => {
                history.push("/knowledge/putusan/usulan/new");
              });
            }
          })
          )
          .catch(() => window.alert("Oops Something went wrong !"));
      } else {
        updatePutusan(
          values.id_putpengadilan,
          "",
          values.body,
          values.file_upload,
          values.id_detiljns,
          values.kd_kantor,
          values.kd_unit_org,
          values.nip_pengusul,
          "",
          "",
          values.no_putusan,
          values.perihal,
          "",
          values.tgl_putusan,
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Usulan berhasil diubah", "success").then(
              () => {
                history.push("/knowledge/putusan/usulan");
              }
            );
          } else {
            swal("Gagal", "Usulan gagal diubah", "error").then(() => {
              history.push("/knowledge/putusan/usulan/new");
            });
          }
        })
      }

      if (values.status == "Tolak") {
        if (values.file) {
          const formData = new FormData();
          formData.append("file", values.file);
          uploadFile(formData)
            .then(({ data }) =>
            updatePutusan(
              values.id_putpengadilan,
              "",
              values.body,
              data.message,
              values.id_detiljns,
              values.kd_kantor,
              values.kd_unit_org,
              values.nip_pengusul,
              "",
              "",
              values.no_putusan,
              values.perihal,
              "",
              values.tgl_putusan,
            ).then(({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Status Usulan Berubah Menjadi Draft", "success").then(
                    () => {
                      history.push("/knowledge/putusan/usulan");
                    }
                  );
                } else {
                  swal("Gagal", "Status Usulan Gagal Diubah", "error").then(() => {
                    history.push("/knowledge/putusan/usulan/new");
                  });
                }
              })
            )
            .catch(() => window.alert("Oops Something went wrong !"));
        } else {
          updatePutusan(
            values.id_putpengadilan,
            "",
            values.body,
            values.file_upload,
            values.id_detiljns,
            values.kd_kantor,
            values.kd_unit_org,
            values.nip_pengusul,
            "",
            "",
            values.no_putusan,
            values.perihal,
            "",
            values.tgl_putusan,
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Status Usulan Berubah Menjadi Draft", "success").then(() => {
                history.push("/knowledge/putusan/usulan");
              });
            } else {
              swal("Gagal", "Status Usulan Gagal Diubah", "error").then(() => {
                history.push("/knowledge/putusan/usulan/new");
              });
            }
          });
        }
        updateStatusPutusan(id,5, 1, user.nip9).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Status Usulan Berubah Menjadi Draft", "success").then(
              () => {
                history.push("/dashboard");
                history.replace("/knowledge/putusan/usulan");
              }
            );
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/putusan/usulan");
            });
          }
        });
      }
    }
  }
  
  const backToProposalList = () => {
    history.push(`/knowledge/putusan/usulan`);
  };

  const setDisabled = (val) => {
    setIsDisabled(val);
  };
  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <ProposalEditForm
            actionsLoading={actionsLoading}
            proposal={proposal || initValues}
            btnRef={btnRef}
            saveProposal={saveProposal}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <ProposalEditFooter
          backAction={backToProposalList}
          btnRef={btnRef}
        ></ProposalEditFooter>
      </CardFooter>
    </Card>
  );
}

export default PutusanEdit;
