import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Field, Formik, Form } from "formik";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
} from "../../proposal-edit/helpers";
import {
  getJenisPengadilan, getPutusanById
} from "../../../Api";
import RegulasiTerkaitTableDetail from "./RegulasiTerkaitTableDetail";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "ckeditor5-custom-build/build/ckeditor";
import "./decoupled.css"
// import BodyEditor from "./helpers/BodyEditor";

function RegulasiDetailForm({
  proposal,
  btnRef,
  saveRegulasiTerkait,
  peraturanTerkait,
  idPutusan,
}) {
  const [bentukPengadilan, setBentukPengadilan] = useState([]);
  const { user } = useSelector((state) => state.auth);
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  const [propbody, setPropbody] = useState("");

  useEffect(() => {
    getPutusanById(idPutusan).then(({ data }) => {
      if (data.body) {
        setPropbody(JSON.parse(data.body));
      }
    });
  }, []);

  useEffect(() => {
    getJenisPengadilan().then(({ data }) => {
      setBentukPengadilan(data);
    });
  }, []);

  //console.log(dokLain);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        onSubmit={(values) => {
          //console.log(values);
          saveRegulasiTerkait(values);
        }}
      >
        {({ handleSubmit, setFieldValue }) => {
          const handleChangeNip = () => {
            setFieldValue("nip_perekam", user.nip9);
          };

          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    name="no_putusan"
                    component={Input}
                    placeholder="Nomor Putusan"
                    label="Nomor Putusan"
                    disabled
                  />
                </div>

                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField name="tgl_putusan" label="Tanggal Putusan" disabled />
                </div>
                {/* FIELD jenis */}
                <div className="form-group row">
                  <Sel name="id_detiljns" label="Bentuk Pengadilan" disabled>
                    <option value=""></option>
                    {bentukPengadilan.map((data, index) => (
                      <option key={index} value={data.id}>
                        {data.nama}
                      </option>
                    ))}
                  </Sel>
                </div>

                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                    disabled
                  />
                </div>

                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    File
                  </label>
                  <div
                    className="col-lg-9 col-xl-6"
                    style={{ marginTop: "10px" }}
                  >
                    <a
                      href={proposal.file_upload}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {proposal.file_upload.slice(28)}
                    </a>
                  </div>
                </div>

                <RegulasiTerkaitTableDetail
                  peraturanTerkait={peraturanTerkait}
                  idPutusan={idPutusan}
                />

                {/* FIELD BODY */}
                <div>
                  <br />
                  <h5>Body</h5>
                </div>
                <div className="document-editor">
                  <div className="document-editor__toolbar"></div>
                  <div className="document-editor__editable-container">
                    <CKEditor
                      disabled
                      onReady={(editor) => {
                        console.log("Editor is ready to use!", editor);
                        window.editor = editor;

                        // Add these two lines to properly position the toolbar
                        const toolbarContainer = document.querySelector(
                          ".document-editor__toolbar"
                        );
                        toolbarContainer.appendChild(
                          editor.ui.view.toolbar.element
                        );
                      }}
                      config={{
                        removePlugins: ["Heading", "Link"],
                        toolbar: [],
                        isReadOnly: true,
                      }}
                      editor={Editor}
                      //ICAAAAAAAAAAAAAAALLLLLLLLLLLLLLLLLLLLLLLL
                      //INI BUAT NAMBAHIN TEMPLATE NYA DI BAWAH INI PAKAI HTML
                      data={propbody}
                    />
                  </div>
                </div>

                {/* <div>
                  <br />
                  <br />
                  <h3>Body</h3>
                </div> */}

                {/* <div className="form-group row m-1">
                  <Field
                    name="COBA"
                    component={BodyEditor}
                    label="Coba Text Editor"
                  />
                </div> */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default RegulasiDetailForm;
