import React from "react";
import {FieldFeedbackLabel} from "./FieldFeedbackLabel";

const getFieldCSSClasses = (touched, errors) => {
  const classes = ["form-control form-control-lg"];
  if (touched && errors) {
    classes.push("is-invalid");
  }

  if (touched && !errors) {
    classes.push("is-valid");
  }

  return classes.join(" ");
};

export function Textarea({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  label,
  form,
  withFeedbackLabel = true,
  customFeedbackLabel,
  type = "text",
  custom,
  ...props
}) {
  return (   
    <>
    {custom === 'custom' ? 
    <>
    {label && <label className="col-xl-3 col-lg-3 col-form-label">{label}</label>}
      <div className="col-lg-12 col-xl-12">
      <textarea
        type={type}
        name={field.name}
        placeholder={props.placeholder}
        className={getFieldCSSClasses(touched[field.name], errors[field.name])}
        {...field}
        {...props}
      />
      {withFeedbackLabel && (
        <FieldFeedbackLabel
          error={errors[field.name]}
          touched={touched[field.name]}
          label={label}
          type={type}
          customFeedbackLabel={customFeedbackLabel}
        />
      )}
      </div>
      </>
    : 
    <>
    {label && <label className="col-xl-3 col-lg-3 col-form-label">{label}</label>}
      <div className="col-lg-9 col-xl-6">
      <textarea
        type={type}
        name={field.name}
        placeholder={props.placeholder}
        className={getFieldCSSClasses(touched[field.name], errors[field.name])}
        {...field}
        {...props}
      />
      {withFeedbackLabel && (
        <FieldFeedbackLabel
          error={errors[field.name]}
          touched={touched[field.name]}
          label={label}
          type={type}
          customFeedbackLabel={customFeedbackLabel}
        />
      )}
      </div>
      </>}
      {/* {label && <label className="col-xl-3 col-lg-3 col-form-label">{label}</label>}
      <div className="col-lg-9 col-xl-6">
      <textarea
        type={type}
        name={field.name}
        placeholder={props.placeholder}
        className={getFieldCSSClasses(touched[field.name], errors[field.name])}
        {...field}
        {...props}
      />
      {withFeedbackLabel && (
        <FieldFeedbackLabel
          error={errors[field.name]}
          touched={touched[field.name]}
          label={label}
          type={type}
          customFeedbackLabel={customFeedbackLabel}
        />
      )}
      </div> */}
    
    </>
  );
}
