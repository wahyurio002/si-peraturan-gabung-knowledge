import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../../_metronic/_partials/controls";
import PenegasanProposalTable from "../Penegasan Proposal/PenegasanProposalTable";

function PenegasanProposal() {
  return (
    <>
      <Card>
        <CardHeader
          title="Usulan Surat Penegasan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <PenegasanProposalTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default PenegasanProposal;
