import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../../_metronic/_partials/controls";

import { useSubheader } from "../../../../../../_metronic/layout";
import PenegasanEditForm from "./PenegasanEditForm";
import PenegasanEditFooter from "./PenegasanEditFooter";
import { uploadFile, getPenegasanById, savePenegasan, updatePenegasan, updateStatusPenegasan } from "../../../Api";
import swal from "sweetalert";
import { useSelector } from "react-redux";

const initValues = {
  alasan_tolak: "" ,
  body: "" ,
  file_upload: "" ,
  kd_kantor: "" ,
  kd_unit_org: "" ,
  nip_pengusul: "" ,
  nip_pjbt_es3: "" ,
  nip_pjbt_es4: "" ,
  no_suratpenegasan: "" ,
  perihal: "" ,
  status: "" ,
  tgl_suratpenegasan: ""
};

function PenegasanEdit({
  history,
  match: {
    params: { id }
  }
}) {
  // Subheader
  const suhbeader = useSubheader();
  const { user } = useSelector(state => state.auth);
  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [penegasan, setPenegasan] = useState();

  useEffect(() => {
    let _title = id ? "Edit Surat Penegasan" : "Tambah Surat Penegasan";

    setTitle(_title);
    suhbeader.setTitle(_title);

    if (id) {
      getPenegasanById(id).then(({ data }) => {

        setPenegasan({
          id_suratpenegasan: data.id_suratpenegasan,
          no_suratpenegasan: data.no_suratpenegasan,
          tgl_suratpenegasan: data.tgl_suratpenegasan,
          perihal: data.perihal,
          file_upload: data.file_upload,
          status: data.status,
          alasan_tolak: data.alasan_tolak,
          body: data.body,
          nip_pengusul: data.nip_pengusul,
          nip_pjbt_es4: data.nip_pjbt_es4,
          nip_pjbt_es3: data.nip_pjbt_es3,
          wkt_usul: data.wkt_usul,
          wkt_teliti_es4: data.wkt_teliti_es4,
          wkt_teliti_es3: data.wkt_teliti_es3,
          wkt_create: data.wkt_create,
          wkt_update: data.wkt_update,
          kd_kantor: data.kd_kantor,
          kd_unit_org: data.kd_unit_org
        });
      });
    }
  }, [id, suhbeader]);
  const btnRef = useRef();

  const saveSurat = values => {
    if (!id) {
      const formData = new FormData();

      formData.append("file", values.file);
      uploadFile(formData)
        .then(({ data }) =>
          savePenegasan(
            "" ,
            "" ,
            data.message ,
            values.kd_kantor ,
            values.kd_unit_org,
            values.nip_pengusul ,
            "" ,
            "" ,
            values.no_suratpenegasan ,
            values.perihal ,
            "" ,
            values.tgl_suratpenegasan
          ).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Usulan berhasil dibuat", "success").then(
                () => {
                  history.push("/knowledge/penegasan/usulan");
                }
              );
            } else {
              swal("Gagal", "Usulan gagal dibuat", "error").then(() => {
                history.push("/knowledge/penegasan/usulan/new");
              });
            }
          })
        )
        .catch(() => window.alert("Oops Something went wrong !"));

    } else {

      if (values.file) {
        //console.log(values.file.name);
        const formData = new FormData();
        formData.append("file", values.file);
        uploadFile(formData)
          .then(({ data }) =>
          updatePenegasan(
            values.id_suratpenegasan,
            "" ,
            "" ,
            data.message ,
            values.kd_kantor ,
            values.kd_unit_org,
            values.nip_pengusul ,
            "" ,
            "" ,
            values.no_suratpenegasan ,
            values.perihal ,
            "" ,
            values.tgl_suratpenegasan
          ).then(({ status }) => {
              if (status === 201 || status === 200) {
                swal("Berhasil", "Usulan berhasil diubah", "success").then(
                  () => {
                    history.push("/knowledge/penegasan/usulan");
                  }
                );
              } else {
                swal("Gagal", "Usulan gagal diubah", "error").then(() => {
                  history.push("/knowledge/penegasan/usulan/new");
                });
              }
            })
          )
          .catch(() => window.alert("Oops Something went wrong !"));
      } else {
        updatePenegasan(
            values.id_suratpenegasan,
            "" ,
            "" ,
            values.file_upload ,
            values.kd_kantor ,
            values.kd_unit_org,
            values.nip_pengusul ,
            "" ,
            "" ,
            values.no_suratpenegasan ,
            values.perihal ,
            "" ,
            values.tgl_suratpenegasan
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Usulan berhasil diubah", "success").then(() => {
              history.push("/knowledge/penegasan/usulan");
            });
          } else {
            swal("Gagal", "Usulan gagal diubah", "error").then(() => {
              history.push("/knowledge/penegasan/usulan/new");
            });
          }
        });
      }

      if (values.status == "Tolak") {
        if (values.file) {
          const formData = new FormData();
          formData.append("file", values.file);
          uploadFile(formData)
            .then(({ data }) =>
            updatePenegasan(
              values.id_suratpenegasan,
              "" ,
              "" ,
              data.message ,
              values.kd_kantor ,
              values.kd_unit_org,
              values.nip_pengusul ,
              "" ,
              "" ,
              values.no_suratpenegasan ,
              values.perihal ,
              "" ,
              values.tgl_suratpenegasan
            ).then(({ status }) => {
                if (status === 201 || status === 200) {
                  swal("Berhasil", "Status Usulan Berubah Menjadi Draft", "success").then(
                    () => {
                      history.push("/knowledge/penegasan/usulan");
                    }
                  );
                } else {
                  swal("Gagal", "Status Usulan gagal diubah", "error").then(() => {
                    history.push("/knowledge/penegasan/usulan/new");
                  });
                }
              })
            )
            .catch(() => window.alert("Oops Something went wrong !"));
        } else {
          updatePenegasan(
            values.id_suratpenegasan,
            "" ,
            "" ,
            values.file_upload ,
            values.kd_kantor ,
            values.kd_unit_org,
            values.nip_pengusul ,
            "" ,
            "" ,
            values.no_suratpenegasan ,
            values.perihal ,
            "" ,
            values.tgl_suratpenegasan
        ).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Status Usulan Berubah Menjadi Draft", "success").then(() => {
              history.push("/knowledge/penegasan/usulan");
            });
          } else {
            swal("Gagal", "Status Usulan gagal diubah", "error").then(() => {
              history.push("/knowledge/penegasan/usulan/new");
            });
          }
        });
        }
        updateStatusPenegasan(id,5, 1, user.nip9).then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Status Usulan Berubah Menjadi Draft", "success").then(
              () => {
                history.push("/dashboard");
                history.replace("/knowledge/penegasan/usulan");
              }
            ).catch(() => window.alert("Oops Something went wrong !"));;
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/penegasan/usulan");
            });
          }
        });
      }
    }
  };

  const backToPenegasanList = () => {
    history.push(`/knowledge/penegasan/usulan`);
  };

  const setDisabled = val => {
    setIsDisabled(val);
  };
  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <PenegasanEditForm
            actionsLoading={actionsLoading}
            proposal={penegasan || initValues}
            btnRef={btnRef}
            saveProposal={saveSurat}
            setDisabled={setDisabled}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <PenegasanEditFooter
          backAction={backToPenegasanList}
          btnRef={btnRef}
        ></PenegasanEditFooter>
      </CardFooter>
    </Card>
  );
}

export default PenegasanEdit;
