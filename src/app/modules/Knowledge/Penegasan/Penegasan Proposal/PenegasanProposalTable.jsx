/* Library */
import React, { useEffect, useState } from "react";
import { useHistory, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import * as columnFormatters from "../../column-formatters";
import { deleteSuratPenegasanById, getPenegasanNonTerimaByNip, updateStatusPenegasan } from "../../Api";
import PenegasanHistory from "../PenegasanHistory";
import PenegasanReject from "../PenegasanReject";

function PenegasanProposalTable() {
  const { user } = useSelector((state) => state.auth);
  const history = useHistory();
  const [penegasan, setPenegasan] = useState([]);

  const addPenegasan = () => history.push("/knowledge/penegasan/usulan/add");
  
  const showHistoryPenegasan = (id) =>
    history.push(`/knowledge/penegasan/usulan/${id}/history`);
  
    const rejectPenegasan = (id) =>
    history.push(`/knowledge/penegasan/usulan/${id}/reject`);
  
    const editPenegasan = (id) =>
    history.push(`/knowledge/penegasan/usulan/${id}/edit`);
  
    const detailPenegasan = (id) =>
    history.push(`/knowledge/penegasan/usulan/${id}/detail`);

    const addBody = id => history.push(`/knowledge/penegasan/usulan/${id}/body`);

    const applyPenegasan = (id) => {
      swal({
      title: "Apakah Anda Ingin Mengajukan Usulan Ini?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: false,
      }).then((willApply) => {
        if(willApply){
          updateStatusPenegasan(id, 1, 2, user.nip9).then(({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Usulan berhasil diajukan", "success").then(() => {
                history.push("/dashboard");
                history.replace("/knowledge/penegasan/usulan");
              });
            } else {
              swal("Gagal", "Usulan gagal diajukan", "error").then(() => {
                history.push("/dashboard");
                history.replace("/knowledge/penegasan/usulan");
              });
            }
          });
        }
      })
  };

  const deleteAction = (id) => {
    swal({
      title: "AApakah Anda Ingin Menghapus Usulan Ini?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        deleteSuratPenegasanById(id).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Usulan berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/penegasan/usulan");
            });
          } else {
            swal("Gagal", "Usulan gagal dihapus", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/penegasan/usulan");
            });
          }
        });
      }
    });
  };

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "no_suratpenegasan",
      text: "No Surat Penegasan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "tgl_suratpenegasan",
      text: "Tgl Surat Penegasan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "perihal",
      text: "Perihal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusColumnFormatter,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.PenegasanProposalActionColumnFormatter,
      formatExtraData: {
        openEditDialog: editPenegasan,
        openDeleteDialog: deleteAction,
        showHistory: showHistoryPenegasan,
        detailPenegasan: detailPenegasan,
        rejectPenegasan: rejectPenegasan,
        applyPenegasan: applyPenegasan,
        addBody: addBody
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_surat",
    pageNumber: 1,
    pageSize: 5,
  };

  const defaultSorted = [{ dataField: "no_perencanaan", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: penegasan.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  useEffect(() => {
    getPenegasanNonTerimaByNip(user.nip9).then(({ data }) => {
      // setPlan(data);
      data.map((data) => {
        return data.status !== "Terima"
          ? setPenegasan((penegasan) => [...penegasan, data])
          : // : data.status === "Draft"
            // ? setProposal((proposal) => [...proposal, data])
            null;
      });
    });
  }, [user.kantorLegacyKode]);
  console.log(user);

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_suratpenegasan"
                  data={penegasan}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right",
                            }}
                            onClick={addPenegasan}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/penegasan/usulan/:id/reject">
        {({ history, match }) => (
          <PenegasanReject
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/penegasan/usulan");
            }}
            onRef={() => {
              history.push("/knowledge/penegasan/usulan");
            }}
          />
        )}
      </Route>
      <Route path="/knowledge/penegasan/usulan/:id/history">
        {({ history, match }) => (
          <PenegasanHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/penegasan/usulan");
            }}
            onRef={() => {
              history.push("/knowledge/penegasan/usulan");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default PenegasanProposalTable;
