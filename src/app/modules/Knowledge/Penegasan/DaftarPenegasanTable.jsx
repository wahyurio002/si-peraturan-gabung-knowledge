import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";

import { Pagination } from "../pagination/Pagination";
import {
  getPenegasanSearchKantor,
} from "../Api";
import PenegasanHistory from "./PenegasanHistory";
import { useSelector } from "react-redux";

function DaftarPenegasanTable() {
  const { role, user } = useSelector(state => state.auth);
  console.log(user);
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_suratpenegasan",
    pageNumber: 1,
    pageSize: 50,
  };
  const history = useHistory();

  const showHistoryPengajuan = (id) =>
    history.push(`/knowledge/penegasan/monitoring/${id}/history`);

  const detailDaftarPenegasan = (id) =>
    history.push(`/knowledge/penegasan/monitoring/${id}/detail`);

  const [penegasanproses, setPenegasanProses] = useState([]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "no_suratpenegasan",
      text: "No Surat Penegasan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "tgl_suratpenegasan",
      text: "Tgl Surat Penegasan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "perihal",
      text: "Perihal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusColumnFormatter,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.DaftarPenegasanActionColumnFormatter,
      formatExtraData: {
        showHistory: showHistoryPengajuan,
        detailDaftarPenegasan: detailDaftarPenegasan,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const { SearchBar } = Search;
  
  useEffect(() => {
    getPenegasanSearchKantor(user.kantorLegacyKode).then(({ data }) => {
      data.map((data) => {
        return data.status === "Eselon 4"
        ? setPenegasanProses(penegasanproses => [...penegasanproses, data])
        : data.status === "Eselon 3"
        ? setPenegasanProses(penegasanproses => [...penegasanproses, data])
        : data.status === "Terima"
        ? setPenegasanProses(penegasanproses => [...penegasanproses, data])
        : null
      });
  });
  }, []);

  const defaultSorted = [{ dataField: "no_suratpenegasan", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50},
    { text: "25", value: 25 },
    { text: "10", value: 10 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: penegasanproses.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_suratpenegasan"
                  data={penegasanproses}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                            <SearchBar
                                {...props.searchProps}
                                style={{ width: "500px" }}
                            />
                            <br />
                        </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/penegasan/monitoring/:id/history">
        {({ history, match }) => (
          <PenegasanHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/penegasan/monitoring");
            }}
            onRef={() => {
              history.push("/knowledge/penegasan/monitoring");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default DaftarPenegasanTable;
