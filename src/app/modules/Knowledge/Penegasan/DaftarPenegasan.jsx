import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import DaftarPenegasanTable from "./DaftarPenegasanTable";

function DaftarPenegasan() {
  return (
    <>
      <Card>
        <CardHeader
          title="Monitoring Surat Penegasan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <DaftarPenegasanTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default DaftarPenegasan;
