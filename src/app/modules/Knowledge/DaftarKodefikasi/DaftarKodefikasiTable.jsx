import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";

import { Pagination } from "../pagination/Pagination";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import swal from "sweetalert";
import SVG from "react-inlinesvg";
import {
  getKodefikasiTerima,
  // getJenisPajak,
} from "../Api";

import DaftarKodefikasiHistory from "../DaftarKodefikasi/DaftarKodefikasiHistory";
// import KodefikasiOpen from "./KodefikasiOpen";

function DaftarKodefikasiTable() {
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_surat",
    pageNumber: 1,
    pageSize: 5,
  };
  const history = useHistory();
  const showHistoryPengajuan = (id) =>
    history.push(`/knowledge/daftarkodefikasi/${id}/showhistory`);
  const detailKodefikasi = (id) =>
    history.push(`/knowledge/daftarkodefikasi/${id}/detail`);
  const [proposal, setProposal] = useState([]);
  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "no_nd",
      text: "No ND Kodefikasi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "tgl_nd",
      text: "Tgl ND Kodefikasi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "perihal",
      text: "Perihal Kodefikasi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "jenis_pajak.nm_jnspajak",
      text: "Jenis Pajak Kodefikasi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusColumnFormatter,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.DaftarKodefikasiActionColumnFormatter,
      formatExtraData: {
        showHistory: showHistoryPengajuan,
        detailKodefikasi: detailKodefikasi,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const { SearchBar } = Search;

  useEffect(() => {
    getKodefikasiTerima().then(({ data }) => {
      data.map((dt) => {
        // if(dt.id_tahapan !== 2 ){
        setProposal((proposal) => [...proposal, dt]);
        // }
      });
    });
  }, []);

  const defaultSorted = [{ dataField: "no_perencanaan", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: proposal.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_kodefikasi"
                  data={proposal}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3"></div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/daftarkodefikasi/:id/showhistory">
        {({ history, match }) => (
          <DaftarKodefikasiHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/daftarkodefikasi");
            }}
            onRef={() => {
              history.push("/knowledge/daftarkodefikasi");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default DaftarKodefikasiTable;
