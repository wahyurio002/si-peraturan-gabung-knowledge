import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import DaftarKodefikasiTable from "./DaftarKodefikasiTable";

function DaftarKodefikasi() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Kodefikasi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <DaftarKodefikasiTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default DaftarKodefikasi;
