import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";

import { Pagination } from "../pagination/Pagination";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import swal from "sweetalert";
import SVG from "react-inlinesvg";
import {
  getSurveiById,
  updateStatusSurvei,
  deleteSurveiById,
  getSurveiNonTerima,
  getSurveiTerima
} from "../Api";
import SurveiHistory from "./SurveiHistory";

function DaftarSurveiTable() {
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_nd",
    pageNumber: 1,
    pageSize: 5,
  };
  const history = useHistory();

  const showHistoryPengajuan = (id) =>
    history.push(`/knowledge/surdone/${id}/showhistory`);

  const detailDaftarSurvei = (id) =>
    history.push(`/knowledge/surdone/${id}/detail`);

  const [surveiterima, setSurveiTerima] = useState([]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "kmSurvei.no_nd",
      text: "No ND Survei",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "kmSurvei.tgl_nd",
      text: "Tgl ND Survei",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "kmSurvei.perihal",
      text: "Perihal Survei",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "jenisSurvei.nama",
      text: "Jenis Audiensi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.DaftarSurveiActionColumnFormatter,
      formatExtraData: {
        showHistory: showHistoryPengajuan,
        detailDaftarSurvei: detailDaftarSurvei,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  const { SearchBar } = Search;

  useEffect(() => {
    getSurveiTerima().then(({ data }) => {
      data.map((dt) => {
        // if(dt.id_tahapan !== 2 ){
        setSurveiTerima((surveiterima) => [...surveiterima, dt]);
        // }
      });
    });
  }, []);

  const defaultSorted = [{ dataField: "no_nd", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: surveiterima.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="kmSurvei.id_lapsurvey"
                  data={surveiterima}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/surdone/:id/showhistory">
        {({ history, match }) => (
          <SurveiHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/surdone");
            }}
            onRef={() => {
              history.push("/knowledge/surdone");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default DaftarSurveiTable;
