import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Field, Formik, Form } from "formik";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
} from "../proposal-edit/helpers";
import { getJenisSurvei, getSurveiNonTerima } from "../../Api";
// import RegulasiTerkaitTableDetail from "./RegulasiTerkaitTableDetail";
// import DokumenLainTableDetail from "./DokumenLainTableDetail";

function SurveiDetail({ proposal, btnRef, saveSurvei, idSurvei }) {
  const [survei, setSurvei] = useState([]);
  const [jenis, setJenis] = useState([]);
  const [dokLain, setDoklain] = useState([]);
  const { user } = useSelector((state) => state.auth);
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    getSurveiNonTerima().then(({ data }) => {
      setSurvei(data);
    });

    getJenisSurvei().then(({ data }) => {
      setJenis(data);
    });
  }, []);

  //console.log(dokLain);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        onSubmit={(values) => {
          //console.log(values);
          saveSurvei(values);
        }}
      >
        {({values}) => {
          
          return (
            <>
              <Form className="form form-label-right">

                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    disabled
                    name="no_nd"
                    component={Input}
                    placeholder="Nomor ND Survei"
                    label="Nomor ND Survei"
                    // onClick={()=>handleChangeNip()}
                  />
                </div>

                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField
                    name="tgl_nd"
                    label="Tanggal ND Survei"
                    disabled
                  />
                </div>

                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                    disabled
                  />
                </div>

                <div className="form-group row">
                  <Sel name="id_detiljns" label="Jenis Survei" disabled>
                    {jenis.map((data, index) => (
                      <option key={index} value={data.id}>{data.nama}</option>
                    ))}
                  </Sel>
                </div>

                {/* Field FILE */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    File Survei
                  </label>
                  <div
                    className="col-lg-9 col-xl-6"
                    style={{ marginTop: "10px" }}
                  >
                    <a
                      href={values.file_upload}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {/* {values.file_survei} */}
                      {values.file_upload.slice(28)}
                    </a>
                  </div>
                </div>
                {/* 

                <RegulasiTerkaitTableDetail
                  peraturanTerkait={peraturanTerkait}
                  idPeraturan={idPeraturan} />

                <DokumenLainTableDetail
                  dokLain={dokLain}
                  idPeraturan={idPeraturan} /> */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default SurveiDetail;
