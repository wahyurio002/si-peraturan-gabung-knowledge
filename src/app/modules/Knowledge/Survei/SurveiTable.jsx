import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";

import { Pagination } from "../pagination/Pagination";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import swal from "sweetalert";
import SVG from "react-inlinesvg";
import {
  getSurveiById,
  updateStatusSurvei,
  deleteSurveiById,
  getSurveiNonTerima
} from "../Api";
import SurveiHistory from "./SurveiHistory";
import { useSelector } from "react-redux";

function SurveiTable() {
  const { role, user } = useSelector(state => state.auth);
  const konseptor = role.includes("ROLE_PERATURAN_KONSEPTOR");
  const es4 = role.includes("ROLE_PERATURAN_PENELITI_LVL1");
  const es3 = role.includes("ROLE_PERATURAN_PENELITI_LVL2");
  const admin = role.includes("ROLE_ADMIN_PERATURAN");
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_nd",
    pageNumber: 1,
    pageSize: 5,
  };
  const history = useHistory();
  const addSurvei = () => history.push("/knowledge/survei/new");
  const editSurvei = (id) =>
    history.push(`/knowledge/survei/${id}/edit`);

  const applySurvei = (id) => {
    getSurveiById(id).then(({ data }) => {
      updateStatusSurvei(data.id_lapsurvey, 2).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/dashboard");
            history.replace("/knowledge/survei");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace("/knowledge/survei");
          });
        }
      });
    });
  };

  const deleteDialog = (id) => {
    swal({
      title: "Hapus",
      text: "Apakah Anda yakin menghapus data ini ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((ret) => {
      if (ret == true) {
        deleteSurveiById(id).then(({ data }) => {
          if (data.deleted == true) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/survei");
            });
          } else {
            swal("Gagal", "Data gagal dihapus", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/survei");
            });
          }
        });
      }
    });
  };

  const showHistoryPengajuan = (id) =>
    history.push(`/knowledge/survei/${id}/showhistory`);

  const prosesSurvei = (id) =>
    history.push(`/knowledge/survei/${id}/proses`);

  const detailSurvei = (id) =>
    history.push(`/knowledge/survei/${id}/detail`);

  const [survei, setSurvei] = useState([]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "kmSurvei.no_nd",
      text: "No ND Survei",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "kmSurvei.tgl_nd",
      text: "Tgl ND Survei",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "kmSurvei.perihal",
      text: "Perihal Survei",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "jenisSurvei.nama",
      text: "Jenis Audiensi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "kmSurvei.status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.SurveiStatusColumnFormatter,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter:
        user.jabatan === "Pelaksana"
          ? columnFormatters.SurveiActionColumnFormatterPelaksana
          : user.jabatan === "Kepala Seksi"
          ? columnFormatters.SurveiActionColumnFormatterEselon4
          : columnFormatters.SurveiActionColumnFormatterEselon3,
      formatExtraData: {
        deleteDialog: deleteDialog,
        applySurvei: applySurvei,
        editSurvei: editSurvei,
        showHistory: showHistoryPengajuan,
        detailSurvei: detailSurvei,
        prosesSurvei: prosesSurvei,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];

  const { SearchBar } = Search;

  useEffect(() => {
    getSurveiNonTerima().then(({ data }) => {
      data.map((dt) => {
        // if(dt.id_tahapan !== 2 ){
        setSurvei((survei) => [...survei, dt]);
        // }
      });
    });
  }, []);

  const defaultSorted = [{ dataField: "no_nd", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: survei.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="kmSurvei.id_lapsurvey"
                  data={survei}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right",
                            }}
                            onClick={addSurvei}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/survei/:id/showhistory">
        {({ history, match }) => (
          <SurveiHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/survei");
            }}
            onRef={() => {
              history.push("/knowledge/survei");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default SurveiTable;
