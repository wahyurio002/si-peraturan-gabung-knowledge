import axios from "axios";

export function getDraft(id_draft) {
  return axios.get(`/draftperaturan/${id_draft}`);
}

export function savePeraturan(
  file_upload,
  id_topik,
  no_regulasi,
  perihal,
  status,
  tgl_regulasi,
  jns_regulasi,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  body,
  alasan_tolak
) {
  return axios.post("/kmregulasiperpajakan/", {
    file_upload,
    id_topik,
    no_regulasi,
    perihal,
    status,
    tgl_regulasi,
    jns_regulasi,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    body,
    alasan_tolak,
  });
}

export function saveDraftComposing(
  body_draft,
  jns_draft,
  id_penyusunan,
  fileupload,
  filelampiranr
) {
  return axios.post("/draftperaturan/", {
    body_draft,
    jns_draft,
    id_penyusunan,
    fileupload,
    filelampiranr
  });
}

export function updateDraftComposing(
  body_draft,
  id_draft,
  fileupload,
  filelampiranr,
  keterangan = 1
) {
  return axios.put(`/draftperaturan/${id_draft}/${keterangan}`, {
    body_draft,
    fileupload,
    filelampiranr
  });
}

export function updateStatusPeraturan(id, status, alasan = null) {
  if (alasan) {
    return axios.put(
      `/kmregulasiperpajakan/updatekmregulasistatus/${id}/${status}`,
      {
        alasan_tolak: alasan,
      }
    );
  } else {
    return axios.put(
      `/kmregulasiperpajakan/updatekmregulasistatus/${id}/${status}`,
      {}
    );
  }
}

//API KMS

export function getRegulasiNonTerimaEs4(kdKantor, kdUnitOrg) {
  return axios.get(
    `/kmregulasiperpajakan/nonterima/es4/${kdKantor}/${kdUnitOrg}`
  );
}

export function getRegulasiNonTerimaEs3(kdKantor, kdUnitOrg) {
  return axios.get(
    `/kmregulasiperpajakan/nonterima/es3/${kdKantor}/${kdUnitOrg}`
  );
}

export function getRegulasiByKantor(kdKantor) {
  return axios.get(`/kmregulasiperpajakan/search/?kdKantor=${kdKantor}`);
}

export function getRegulasiNonTerimaByNip(nip) {
  return axios.get(`/kmregulasiperpajakan/nonterima/nip/${nip}`);
}

export function deleteRegulasi(id) {
  return axios.delete(`/kmregulasiperpajakan/${id}`);
}

export function updateStatusRegulasi(id, statusNow, statusNext, nip, alasan) {
  if (statusNext == 2) {
    return axios.put(
      `/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );

    // return axios.put(`/kmkodefikasi/updatekmkodefikasistatus/${id}/${status}`, {
    //   alasan_tolak: alasan,

    // });
  } else if (statusNext == 3) {
    return axios.put(
      `/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
      { nip_pjbt_es4: nip }
    );
  } else if (statusNext == 5) {
    if (statusNow == 2) {
      return axios.put(
        `/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
        { nip_pjbt_es4: nip, alasan_tolak: alasan }
      );
    } else if (statusNow == 3) {
      return axios.put(
        `/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
        { nip_pjbt_es3: nip, alasan_tolak: alasan }
      );
    }
  } else if (statusNext == 6) {
    return axios.put(
      `/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
      { nip_pjbt_es3: nip }
    );
  } else if (statusNext == 1) {
    return axios.put(
      `/kmregulasiperpajakan/updatekmregulasistatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );
  }
}

export function getRegulasiById(id) {
  return axios.get(`/kmregulasiperpajakan/${id}`);
}

export function getPeraturanTerima() {
  return axios.get("/kmregulasiperpajakan/terima");
}

// export function getPeraturanById(id) {
//   return axios.get(`/kmregulasiperpajakan/${id}`);
// }

export function getJenisPeraturan() {
  return axios.get("/jnsperaturan/");
}

export function getTopik() {
  //return axios.get("/reftopik/");
  return axios.get("/detiljenis/byidjenis/1");
}

export function getRegulasiSearchEselon4(kdKantor, kdUnitOrgEs4, status) {
  return axios.get(
    `/kmregulasiperpajakan/search?kdKantor=${kdKantor}&kdUnitOrgEs4=${kdUnitOrgEs4}&status=${status}`
  );
}

export function getRegulasiSearchEselon3(kdKantor, kdUnitOrgEs3, status) {
  return axios.get(
    `/kmregulasiperpajakan/search?kdKantor=${kdKantor}&kdUnitOrgEs3=${kdUnitOrgEs3}&status=${status}`
  );
}

export function uploadFile(formData) {
  const config = {
    headers: {
      "Content-type": "multipart/form-data",
    },
  };
  return axios.post("/upload", formData, config);
}

export function saveRegulasi(
  alasan_tolak,
  body,
  file_upload,
  id_topik,
  jns_regulasi,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_regulasi,
  perihal,
  status,
  tgl_regulasi
) {
  return axios.post("/kmregulasiperpajakan/", {
    alasan_tolak,
    body,
    file_upload,
    id_topik,
    jns_regulasi,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_regulasi,
    perihal,
    status,
    tgl_regulasi,
  });
}

export function updateRegulasi(
  id,
  alasan_tolak,
  body,
  file_upload,
  id_topik,
  jns_regulasi,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_regulasi,
  perihal,
  status,
  tgl_regulasi
) {
  return axios.put(`/kmregulasiperpajakan/${id}`, {
    file_upload,
    alasan_tolak,
    body,
    file_upload,
    id_topik,
    jns_regulasi,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_regulasi,
    perihal,
    status,
    tgl_regulasi,
  });
}

export function deletePeraturanById(id) {
  return axios.delete(`/kmregulasiperpajakan/${id}`);
}

export function saveBodyRegulasi(id, body) {
  return axios.put(`/kmregulasiperpajakan/updatebody/${id}`, {
    body: body,
  });
}

export function getPeraturanTerkait(id) {
  return axios.get(`/kmperaturanterkait/regulasi/${id}`);
}

export function deletePeraturanTerkait(id) {
  return axios.delete(`/kmperaturanterkait/${id}`);
}

export function getPeraturanByNo() {
  return axios.get(`/kmregulasiperpajakan/terima/by_no?no_regulasi=`);
}

export function addPerTerkaitById(refKm, id, idPer) {

  if(refKm== 1){
    return axios.post(`/kmperaturanterkait/${refKm}`, {
      id_peraturan: id,
      id_peraturan_terkait: idPer,
    });

  }else if(refKm == 3){

    return axios.post(`/kmperaturanterkait/${refKm}`, {
      id_kajian: id,
      id_peraturan_terkait: idPer,
    });

  }else if(refKm == 4){

    return axios.post(`/kmperaturanterkait/${refKm}`, {
      id_putusan: id,
      id_peraturan_terkait: idPer,
    });

  }
  
}

export function getHistoryStatusKM(idRefKm, idKm) {
  return axios.get(`/logstatuskm/${idRefKm}/${idKm}`);
}

export function getTopikById(id) {
  return axios.get(`/reftopik/${id}`);
}

export function getJnsPeraturanById(id) {
  return axios.get(`/jnsperaturan/${id}`);
}

export function getDokumenPer(id) {
  return axios.get(`/kmdoklainnya/per/${id}`);
}

export function saveDokLainPeraturan(file_upload, id_per, keterangan) {
  return axios.post(`/kmdoklainnya/1`, {
    file_upload,
    id_per,
    keterangan,
  });
}

export function deleteDokLain(id) {
  return axios.delete(`/kmdoklainnya/${id}`);
}

export function updateBody(id, body) {
  return axios.put(`/kmregulasiperpajakan/updatebody/${id}`, {
    body: body,
  });
}

export function getKajianNonTerimaEs4(kdKantor, kdUnitOrg) {
  return axios.get(`/kmhslkajian/nonterima/es4/${kdKantor}/${kdUnitOrg}`);
}

export function getKajianNonTerimaEs3(kdKantor, kdUnitOrg) {
  return axios.get(`/kmhslkajian/nonterima/es3/${kdKantor}/${kdUnitOrg}`);
}

export function getKajianSearchEselon4(kdKantor, kdUnitOrgEs4, status) {
  return axios.get(
    `/kmhslkajian/search?kdKantor=${kdKantor}&kdUnitOrgEs4=${kdUnitOrgEs4}&status=${status}`
  );
}

export function getKajianSearchEselon3(kdKantor, kdUnitOrgEs3, status) {
  return axios.get(
    `/kmhslkajian/search?kdKantor=${kdKantor}&kdUnitOrgEs3=${kdUnitOrgEs3}&status=${status}`
  );
}

export function updateStatusKajian(
  id,
  statusNow,
  statusNext,
  nip,
  alasan = null
) {
  if (statusNext == 2) {
    return axios.put(
      `/kmhslkajian/updatekmhasilkajianstatus/${id}/${statusNext}`,
      {
        nip_pengusul: nip,
      }
    );

    // return axios.put(`kmhslkajian/updatekmhasilkajianstatus/${id}/${status}`, {
    //   alasan_tolak: alasan,

    // });
  } else if (statusNext == 3) {
    return axios.put(
      `/kmhslkajian/updatekmhasilkajianstatus/${id}/${statusNext}`,
      {
        nip_pjbt_es4: nip,
      }
    );
  } else if (statusNext == 5) {
    if (statusNow == 2) {
      return axios.put(
        `/kmhslkajian/updatekmhasilkajianstatus/${id}/${statusNext}`,
        {
          nip_pjbt_es4: nip,
          alasan_tolak: alasan,
        }
      );
    } else if (statusNow == 3) {
      return axios.put(
        `/kmhslkajian/updatekmhasilkajianstatus/${id}/${statusNext}`,
        {
          nip_pjbt_es3: nip,
          alasan_tolak: alasan,
        }
      );
    }
  } else if (statusNext == 6) {
    return axios.put(
      `/kmhslkajian/updatekmhasilkajianstatus/${id}/${statusNext}`,
      {
        nip_pjbt_es3: nip,
      }
    );
  } else if (statusNext == 1) {
    return axios.put(
      `/kmhslkajian/updatekmhasilkajianstatus/${id}/${statusNext}`,
      {
        nip_pengusul: nip,
      }
    );
  }
}

export function getKajianNonTerima() {
  return axios.get("/kmhslkajian/nonterima");
}

export function getKajianNonTerimaByNip(nip) {
  return axios.get(`/kmhslkajian/nonterima/nip/${nip}`);
}

export function getKajianById(id) {
  return axios.get(`/kmhslkajian/${id}`);
}

export function deleteKajianById(id) {
  return axios.delete(`/kmhslkajian/${id}`);
}

// export function updateStatusKajian(id, status, alasan = null) {
//   if (alasan) {
//     return axios.put(`/kmhslkajian/updatekmhasilkajianstatus/${id}/${status}`, {
//       alasan_tolak: alasan,
//     });
//   } else {
//     return axios.put(
//       `/kmhslkajian/updatekmhasilkajianstatus/${id}/${status}`,
//       {}
//     );
//   }
// }

export function saveKajian(
  alasan_tolak,
  body,
  file_upload,
  id_detiljns,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  perihal,
  status,
  tgl_kajian,
  unit_kajian
) {
  return axios.post("/kmhslkajian/", {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    perihal,
    status,
    tgl_kajian,
    unit_kajian,
  });
}

export function updateKajian(
  id,
  alasan_tolak,
  body,
  file_upload,
  id_detiljns,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  perihal,
  status,
  tgl_kajian,
  unit_kajian
) {
  return axios.put(`/kmhslkajian/${id}`, {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    perihal,
    status,
    tgl_kajian,
    unit_kajian,
  });
}

export function getKajianByKantor(kdKantor) {
  return axios.get(`/kmhslkajian/search/?kdKantor=${kdKantor}`);
}

export function getJenisKajian() {
  return axios.get("/detiljenis/byidjenis/2");
}

export function getDitPembuat() {
  return axios.get("/detiljenis/byidjenis/8");
}

export function getDokumenKajian(id) {
  return axios.get(`/kmdoklainnya/kajian/${id}`);
}

export function getPeraturanTerkaitKajian(id) {
  return axios.get(`/kmperaturanterkait/kajian/${id}`);
}

export function saveDokLainKajian(file_upload, id_kajian, keterangan) {
  return axios.post(`/kmdoklainnya/3`, {
    file_upload,
    id_kajian,
    keterangan,
  });
}

export function getKajianTerima() {
  return axios.get("/kmhslkajian/terima");
}

export function getKajianTerimaByNip(nip) {
  return axios.get(`/kmhasilkajian/terima/nip/${nip}`);
}

export function getKajianTerimaByKantor(kdKantor){
  return axios.get(`/kmhasilkajian/terima/kantor/${kdKantor}`);
}

export function getKajianTerimaByEs4(kdKantor, kdUnitOrg){
  return axios.get(`/kmhasilkajian/terima/es4/${kdKantor}/${kdUnitOrg}`);
}

export function getKajianTerimaByEs3(kdKantor, kdUnitOrg){
  return axios.get(`/kmhasilkajian/terima/es3/${kdKantor}/${kdUnitOrg}`);
}

export function saveBodyKajian(id, body) {
  return axios.put(`/kmhslkajian/updatebody/${id}`, {
    body: body,
  });
}

// putusan


export function getPutusanNonTerima() {
  return axios.get("/kmputusanpengadilan/nonterima");
}

export function getPutusanNonTerimaByNip(nip) {
  return axios.get(`/kmputusanpengadilan/nonterima/nip/${nip}`);
}

export function getPutusanNonTerimaByKantor(kdKantor){
  return axios.get(`/kmputusanpengadilan/nonterima/kantor/${kdKantor}`);
}

export function getPutusanNonTerimaByEs4(kdKantor, kdUnitOrg){
  return axios.get(`/kmputusanpengadilan/nonterima/es4/${kdKantor}/${kdUnitOrg}`);
}

export function getPutusanNonTerimaByEs3(kdKantor, kdUnitOrg){
  return axios.get(`/kmputusanpengadilan/nonterima/es3/${kdKantor}/${kdUnitOrg}`);
}

export function getPutusanSearchEselon3(kdKantor, kdUnitOrg, status){
  return axios.get(`/kmputusanpengadilan/search?kdKantor=${kdKantor}&kdUnitOrgEs3=${kdUnitOrg}&status=${status}`)
}

export function getPutusanSearchKantor(kdKantor){
  return axios.get(`/kmputusanpengadilan/search?kdKantor=${kdKantor}`)
}

export function getPutusanById(id) {
  return axios.get(`/kmputusanpengadilan/${id}`);
}

export function deletePutusanById(id) {
  return axios.delete(`/kmputusanpengadilan/${id}`);
}

export function updateStatusPutusan(id,
  statusNow,
  statusNext,
  nip,
  alasan = null) {
    if (statusNext == 2) {
      return axios.put(
        `/kmputusanpengadilan/updatekmPutusanPengadilanstatus/${id}/${statusNext}`,
        { nip_pengusul: nip }
      );
  
      // return axios.put(`/kmkodefikasi/updatekmkodefikasistatus/${id}/${status}`, {
      //   alasan_tolak: alasan,
  
      // });
    } else if (statusNext == 3) {
      return axios.put(
        `/kmputusanpengadilan/updatekmPutusanPengadilanstatus/${id}/${statusNext}`,
        { nip_pjbt_es4: nip }
      );
    } else if (statusNext == 5) {
      if (statusNow == 2) {
        return axios.put(
          `/kmputusanpengadilan/updatekmPutusanPengadilanstatus/${id}/${statusNext}`,
          { nip_pjbt_es4: nip, alasan_tolak: alasan }
        );
      } else if (statusNow == 3) {
        return axios.put(
          `/kmputusanpengadilan/updatekmPutusanPengadilanstatus/${id}/${statusNext}`,
          { nip_pjbt_es3: nip, alasan_tolak: alasan }
        );
      }
    } else if (statusNext == 6) {
      return axios.put(
        `/kmputusanpengadilan/updatekmPutusanPengadilanstatus/${id}/${statusNext}`,
        { nip_pjbt_es3: nip }
      );
    } else if (statusNext == 1){
      return axios.put(
        `/kmputusanpengadilan/updatekmPutusanPengadilanstatus/${id}/${statusNext}`,
        { nip_pengusul: nip }
      );
    }
}

export function savePutusan(
  alasan_tolak,
  body,
  file_upload,
  id_detiljns,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_putusan,
  perihal,
  status,
  tgl_putusan
) {
  return axios.post("/kmputusanpengadilan/", {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_putusan,
    perihal,
    status,
    tgl_putusan
  });
}

export function updatePutusan(
    id,
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_putusan,
    perihal,
    status,
    tgl_putusan
) {
  return axios.put(`/kmputusanpengadilan/${id}`, {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_putusan,
    perihal,
    status,
    tgl_putusan
  });
}

export function saveBodyPutusan(id, body) {
  return axios.put(`/kmputusanpengadilan/updatebody/${id}`, {
    body: body,
  });
}

export function getJenisPengadilan() {
  return axios.get("/detiljenis/byidjenis/3");
}


export function getDokumenPutusan(id) {
  return axios.get(`/kmdoklainnya/putusan/${id}`);
}

export function getPeraturanTerkaitPutusan(id) {
  return axios.get(`/kmperaturanterkait/putusan/${id}`);
}

export function getPutusanTerima() {
  return axios.get("/kmputusanpengadilan/terima");
}

export function getPutusanTerimaByNip(nip) {
  return axios.get(`/kmputusanpengadilan/terima/nip/${nip}`);
}

export function getPutusanTerimaByKantor(kdKantor){
  return axios.get(`/kmputusanpengadilan/terima/kantor/${kdKantor}`);
}

export function getPutusanTerimaByEs4(kdKantor, kdUnitOrg){
  return axios.get(`/kmputusanpengadilan/terima/es4/${kdKantor}/${kdUnitOrg}`);
}

export function getPutusanTerimaByEs3(kdKantor, kdUnitOrg){
  return axios.get(`/kmputusanpengadilan/terima/es3/${kdKantor}/${kdUnitOrg}`);
}


/// API WAHYU

export function getPenegasanSearchEselon4(kdKantor, kdUnitOrgEs4, status){
  return axios.get(`/kmsuratpenegasan/search?kdKantor=${kdKantor}&kdUnitOrgEs4=${kdUnitOrgEs4}&status=${status}`);
}

export function getPenegasanSearchEselon3(kdKantor, kdUnitOrgEs3, status){
  return axios.get(`/kmsuratpenegasan/search?kdKantor=${kdKantor}&kdUnitOrgEs3=${kdUnitOrgEs3}&status=${status}`);
}

export function getPenegasanSearchKantor(kdKantor){
  return axios.get(`/kmsuratpenegasan/search?kdKantor=${kdKantor}`)
}

export function getPenegasanSearchTolak(nip){
  return axios.get(`/kmsuratpenegasan/search?nip=${nip}&status=Tolak`)
}

export function getPenegasanByKantor(kdKantor){
  return axios.get(`/kmsuratpenegasan/all/kantor/${kdKantor}`);
}

export function getPenegasanNonTerima() {
  return axios.get("/kmsuratpenegasan/nonterima");
}

export function getPenegasanNonTerimaByNip(nip) {
  return axios.get(`/kmsuratpenegasan/nonterima/nip/${nip}`);
}

export function getPenegasanNonTerimaByKantor(kdKantor){
  return axios.get(`/kmsuratpenegasan/nonterima/kantor/${kdKantor}`);
}

export function getPenegasanNonTerimaByEs4(kdKantor, kdUnitOrg){
  return axios.get(`/kmsuratpenegasan/nonterima/es4/${kdKantor}/${kdUnitOrg}`);
}

export function getPenegasanNonTerimaByEs3(kdKantor, kdUnitOrg){
  return axios.get(`/kmsuratpenegasan/nonterima/es3/${kdKantor}/${kdUnitOrg}`);
}

export function getPenegasanTerima() {
  return axios.get("/kmsuratpenegasan/terima");
}

export function getPenegasanTerimaByNip(nip) {
  return axios.get(`/kmsuratpenegasan/terima/nip/${nip}`);
}

export function getPenegasanTerimaByKantor(kdKantor){
  return axios.get(`/kmsuratpenegasan/terima/kantor/${kdKantor}`);
}

export function getPenegasanTerimaByEs4(kdKantor, kdUnitOrg){
  return axios.get(`/kmsuratpenegasan/terima/es4/${kdKantor}/${kdUnitOrg}`);
}

export function getPenegasanTerimaByEs3(kdKantor, kdUnitOrg){
  return axios.get(`/kmsuratpenegasan/terima/es3/${kdKantor}/${kdUnitOrg}`);
}

export function geturatPenegasanByNoSuratpenegasan(no_suratpenegasan){
  return axios.get(`/kmsuratpenegasan/byno_suratpenegasan/${no_suratpenegasan}`);
}
export function getPenegasanById(id) {
  return axios.get(`/kmsuratpenegasan/${id}`);
}

export function updateStatusPenegasan(
  id,
  statusNow,
  statusNext,
  nip,
  alasan = null) {
    if (statusNext == 2) {
      return axios.put(
        `/kmsuratpenegasan/updatekmsuratpenegasanstatus/${id}/${statusNext}`,
        { nip_pengusul: nip }
      );
  
      // return axios.put(`/kmkodefikasi/updatekmkodefikasistatus/${id}/${status}`, {
      //   alasan_tolak: alasan,
  
      // });
    } else if (statusNext == 3) {
      return axios.put(
        `/kmsuratpenegasan/updatekmsuratpenegasanstatus/${id}/${statusNext}`,
        { nip_pjbt_es4: nip }
      );
    } else if (statusNext == 5) {
      if (statusNow == 2) {
        return axios.put(
          `/kmsuratpenegasan/updatekmsuratpenegasanstatus/${id}/${statusNext}`,
          { nip_pjbt_es4: nip, alasan_tolak: alasan }
        );
      } else if (statusNow == 3) {
        return axios.put(
          `/kmsuratpenegasan/updatekmsuratpenegasanstatus/${id}/${statusNext}`,
          { nip_pjbt_es3: nip, alasan_tolak: alasan }
        );
      }
    } else if (statusNext == 6) {
      return axios.put(
        `/kmsuratpenegasan/updatekmsuratpenegasanstatus/${id}/${statusNext}`,
        { nip_pjbt_es3: nip }
      );
    } else if (statusNext == 1){
      return axios.put(
        `/kmsuratpenegasan/updatekmsuratpenegasanstatus/${id}/${statusNext}`,
        { nip_pengusul: nip }
      );
    }
}

export function savePenegasan(
  
  alasan_tolak ,
  body ,
  file_upload ,
  kd_kantor ,
  kd_unit_org ,
  nip_pengusul ,
  nip_pjbt_es3 ,
  nip_pjbt_es4 ,
  no_suratpenegasan ,
  perihal ,
  status ,
  tgl_suratpenegasan
  
) {

  return axios.post("/kmsuratpenegasan/", {
  alasan_tolak ,
  body ,
  file_upload ,
  kd_kantor ,
  kd_unit_org ,
  nip_pengusul ,
  nip_pjbt_es3 ,
  nip_pjbt_es4 ,
  no_suratpenegasan ,
  perihal ,
  status ,
  tgl_suratpenegasan
  });
}

export function updatePenegasan(
  id,
  alasan_tolak ,
  body ,
  file_upload ,
  kd_kantor ,
  kd_unit_org ,
  nip_pengusul ,
  nip_pjbt_es3 ,
  nip_pjbt_es4 ,
  no_suratpenegasan ,
  perihal ,
  status ,
  tgl_suratpenegasan
) {

  return axios.put(`/kmsuratpenegasan/${id}`, {
  alasan_tolak ,
  body ,
  file_upload ,
  kd_kantor ,
  kd_unit_org ,
  nip_pengusul ,
  nip_pjbt_es3 ,
  nip_pjbt_es4 ,
  no_suratpenegasan ,
  perihal ,
  status ,
  tgl_suratpenegasan
  });
}

export function saveBodyPenegasan(id, body) {
  return axios.put(`/kmsuratpenegasan/updatebody/${id}`, {
    body: body,
  });
}

export function deleteSuratPenegasanById(id) {
  return axios.delete(`/kmsuratpenegasan/${id}`);
}

export function getRekomendasiSearchEselon4(kdKantor, kdUnitOrgEs4, status){
  return axios.get(`/kmrekomendasi/search?kdKantor=${kdKantor}&kdUnitOrgEs4=${kdUnitOrgEs4}&status=${status}`);
}

export function getRekomendasiSearchEselon3(kdKantor, kdUnitOrgEs3, status){
  return axios.get(`/kmrekomendasi/search?kdKantor=${kdKantor}&kdUnitOrgEs3=${kdUnitOrgEs3}&status=${status}`);
}

export function getRekomendasiNonTerima() {
  return axios.get("/kmrekomendasi/nonterima/");
}

export function getRekomendasiNonTerimaByNip(nip) {
  return axios.get(`/kmrekomendasi/nonterima/nip/${nip}`);
}

export function getRekomendasiNonTerimaEs4(kdKantor, kdUnitOrg) {
  return axios.get(`/kmrekomendasi/nonterima/es4/${kdKantor}/${kdUnitOrg}`);
}

export function getRekomendasiTerima() {
  return axios.get("/kmrekomendasi/terima/");
}

export function getRekomendasiSearchKantor(kdKantor){
  return axios.get(`/kmrekomendasi/search?kdKantor=${kdKantor}`)
}

export function getRekomendasiById(id) {
  return axios.get(`/kmrekomendasi/${id}`);
}

export function updateStatusRekomendasi(
  id,
  statusNow,
  statusNext,
  nip,
  alasan = null) {
    if (statusNext == 2) {
      return axios.put(
        `/kmrekomendasi/updatekmrekomendasistatus/${id}/${statusNext}`,
        { nip_pengusul: nip }
      );
  
      // return axios.put(`/kmkodefikasi/updatekmkodefikasistatus/${id}/${status}`, {
      //   alasan_tolak: alasan,
  
      // });
    } else if (statusNext == 3) {
      return axios.put(
        `/kmrekomendasi/updatekmrekomendasistatus/${id}/${statusNext}`,
        { nip_pjbt_es4: nip }
      );
    } else if (statusNext == 5) {
      if (statusNow == 2) {
        return axios.put(
          `/kmrekomendasi/updatekmrekomendasistatus/${id}/${statusNext}`,
          { nip_pjbt_es4: nip, alasan_tolak: alasan }
        );
      } else if (statusNow == 3) {
        return axios.put(
          `/kmrekomendasi/updatekmrekomendasistatus/${id}/${statusNext}`,
          { nip_pjbt_es3: nip, alasan_tolak: alasan }
        );
      }
    } else if (statusNext == 6) {
      return axios.put(
        `/kmrekomendasi/updatekmrekomendasistatus/${id}/${statusNext}`,
        { nip_pjbt_es3: nip }
      );
    } else if (statusNext == 1){
      return axios.put(
        `/kmrekomendasi/updatekmrekomendasistatus/${id}/${statusNext}`,
        { nip_pengusul: nip }
      );
    }
}

export function saveRekomendasi(
  
  alasan_tolak ,
  body ,
  file_upload ,
  id_detiljns ,
  kd_kantor,
  kd_unit_org,
  nip_pengusul ,
  nip_pjbt_es3 ,
  nip_pjbt_es4 ,
  no_hsl_pemeriksaan ,
  perihal ,
  status ,
  tgl_pemeriksaan,
  thn_periksa 
  
) {

  return axios.post("/kmrekomendasi/", {
    alasan_tolak ,
    body ,
    file_upload ,
    id_detiljns ,
    kd_kantor,
    kd_unit_org,
    nip_pengusul ,
    nip_pjbt_es3 ,
    nip_pjbt_es4 ,
    no_hsl_pemeriksaan ,
    perihal ,
    status ,
    tgl_pemeriksaan,
    thn_periksa  
  });
}

export function getJenisPemeriksaan(){
  return axios.get("/detiljenis/byidjenis/4");
}

export function updateRekomendasi(
  id,
  alasan_tolak ,
  body ,
  file_upload ,
  id_detiljns ,
  kd_kantor,
  kd_unit_org,
  nip_pengusul ,
  nip_pjbt_es3 ,
  nip_pjbt_es4 ,
  no_hsl_pemeriksaan ,
  perihal ,
  status ,
  tgl_pemeriksaan,
  thn_periksa
) {

  return axios.put(`/kmrekomendasi/${id}`, {
    alasan_tolak ,
    body ,
    file_upload ,
    id_detiljns ,
    kd_kantor,
    kd_unit_org,
    nip_pengusul ,
    nip_pjbt_es3 ,
    nip_pjbt_es4 ,
    no_hsl_pemeriksaan ,
    perihal ,
    status ,
    tgl_pemeriksaan,
    thn_periksa 
  });
}

export function saveBodyRekomendasi(id, body) {
  return axios.put(`/kmrekomendasi/updatebody/${id}`, {
    body: body,
  });
}

export function deleteRekomendasiById(id) {
  return axios.delete(`/kmrekomendasi/${id}`);
}

// API KMKODEFIKASI

export function applyKodefikasi(id) {
  return axios.put(`/kmkodefikasi/updatekmkodefikasistatus/${id}/2`, {});
}

export function getKodefikasiByKantor(kdKantor) {
  return axios.get(`/kmkodefikasi/search/?kdKantor=${kdKantor}`);
}

export function getKodefikasiProposal(nip, kdKantor, kdUnitOrgEs4, status) {
  return axios.get(
    `/kmkodefikasi/search/?kdKantor=${kdKantor}&kdUnitOrgEs4=${kdUnitOrgEs4}&nip=${nip}&status=${status}`
  );
}

export function getKodefikasiSearchDraft(nip, kdKantor, kdUnitOrgEs4, status) {
  return axios.get(
    `/kmkodefikasi/search/?kdKantor=${kdKantor}&kdUnitOrgEs4=${kdUnitOrgEs4}&nip=${nip}&status=${status}`
  );
}

export function getKodefikasiSearchEselon4(kdKantor, kdUnitOrgEs4, status) {
  return axios.get(
    `/kmkodefikasi/search?kdKantor=${kdKantor}&kdUnitOrgEs4=${kdUnitOrgEs4}&status=${status}`
  );
}

export function getKodefikasiSearchEselon3(kdKantor, kdUnitOrgEs3, status) {
  return axios.get(
    `/kmkodefikasi/search?kdKantor=${kdKantor}&kdUnitOrgEs3=${kdUnitOrgEs3}&status=${status}`
  );
}

export function getKodefikasi() {
  return axios.get("/kmkodefikasi/nonterima");
}

export function getKodefikasiNonTerimaEs4(kdKantor, kdUnitOrg) {
  return axios.get(`/kmkodefikasi/nonterima/es4/${kdKantor}/${kdUnitOrg}`);
}

export function getKodefikasiNonTerimaEs3(kdKantor, kdUnitOrg) {
  return axios.get(`/kmkodefikasi/nonterima/es3/${kdKantor}/${kdUnitOrg}`);
}
export function getKodefikasiNonTerimaByKantor(kdKantor) {
  return axios.get(`/kmkodefikasi/nonterima/kantor/${kdKantor}`);
}

export function getKodefikasiNonTerimaByNip(nip) {
  return axios.get(`/kmkodefikasi/nonterima/nip/${nip}`);
}

export function getKodefikasiTerima() {
  return axios.get("/kmkodefikasi/terima");
}

export function getKodefikasiTerimaEs3(kdKantor, kdUnitOrg) {
  return axios.get(`/kmkodefikasi/terima/es3/${kdKantor}/${kdUnitOrg}`);
}

export function getKodefikasiTerimaEs4(kdKantor, kdUnitOrg) {
  return axios.get(`/kmkodefikasi/terima/es4/${kdKantor}/${kdUnitOrg}`);
}

export function getKodefikasiTerimaByKantor(kdKantor) {
  return axios.get(`/kmkodefikasi/terima/kantor/${kdKantor}`);
}

export function getKodefikasiTerimaByNip(nip) {
  return axios.get(`/kmkodefikasi/terima/nip/${nip}`);
}

export function getKodefikasiKonseptorNip(KdKantor) {
  return axios.get(
    `kmkodefikasi/search?kdKantor={$kdKantor}&kdUnitOrgEs3=3321123&nip=815100735&status=Draft`
  );
}

export function getKodefikasiById(id) {
  return axios.get(`/kmkodefikasi/${id}`);
}

export function deleteKodefikasi(id_kodefikasi) {
  return axios.delete(`/kmkodefikasi/${id_kodefikasi}`);
}
export function updateStatusKodefikasi(
  id,
  statusNow,
  statusNext,
  nip,
  alasan = null
) {
  if (statusNext == 2) {
    return axios.put(
      `/kmkodefikasi/updatekmkodefikasistatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );

    // return axios.put(`/kmkodefikasi/updatekmkodefikasistatus/${id}/${status}`, {
    //   alasan_tolak: alasan,

    // });
  } else if (statusNext == 3) {
    return axios.put(
      `/kmkodefikasi/updatekmkodefikasistatus/${id}/${statusNext}`,
      { nip_pjbt_es4: nip }
    );
  } else if (statusNext == 5) {
    if (statusNow == 2) {
      return axios.put(
        `/kmkodefikasi/updatekmkodefikasistatus/${id}/${statusNext}`,
        { nip_pjbt_es4: nip, alasan_tolak: alasan }
      );
    } else if (statusNow == 3) {
      return axios.put(
        `/kmkodefikasi/updatekmkodefikasistatus/${id}/${statusNext}`,
        { nip_pjbt_es3: nip, alasan_tolak: alasan }
      );
    }
  } else if (statusNext == 6) {
    return axios.put(
      `/kmkodefikasi/updatekmkodefikasistatus/${id}/${statusNext}`,
      { nip_pjbt_es3: nip }
    );
  } else if (statusNext == 1) {
    return axios.put(
      `/kmkodefikasi/updatekmkodefikasistatus/${id}/${statusNext}`,
      { nip_pengusul: nip }
    );
  }
}

export function deleteStatusById(id) {
  return axios.delete(`/kmkodefikasi/${id}`);
}

export function getJenisPajakById(id) {
  return axios.get(`/jnspajak/${id}`);
}

export function getJenisPajak() {
  return axios.get("/jnspajak/");
}
export function saveKodefikasi(
  alasan_tolak,
  body,
  file_upload,
  id_jnspajak,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_nd,
  perihal,
  status,
  tgl_nd
) {
  return axios.post("/kmkodefikasi/", {
    alasan_tolak,
    body,
    file_upload,
    id_jnspajak,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_nd,
    perihal,
    status,
    tgl_nd,
  });
}

export function updateKodefikasi(
  id,
  alasan_tolak,
  body,
  file_upload,
  id_jnspajak,
  kd_kantor,
  kd_unit_org,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_nd,
  perihal,
  status,
  tgl_nd
) {
  return axios.put(`/kmkodefikasi/${id}`, {
    alasan_tolak,
    body,
    file_upload,
    id_jnspajak,
    kd_kantor,
    kd_unit_org,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_nd,
    perihal,
    status,
    tgl_nd,
  });
}

export function saveBodyKodefikasi(id, body) {
  return axios.put(`/kmkodefikasi/updatebody/${id}`, {
    body: body,
  });
}

export function getAudiensiSearchEselon4(kdKantor, kdUnitOrgEs4, status){
  return axios.get(`/kmaudiensi/search?kdKantor=${kdKantor}&kdUnitOrgEs4=${kdUnitOrgEs4}&status=${status}`);
}

export function getAudiensiSearchEselon3(kdKantor, kdUnitOrgEs3, status){
  return axios.get(`/kmaudiensi/search?kdKantor=${kdKantor}&kdUnitOrgEs3=${kdUnitOrgEs3}&status=${status}`);
}

  export function getAudiensiNonTerima(){
    return axios.get(`/kmaudiensi/nonterima`);
  }

  export function getAudiensiNonTerimaByNip(nip) {
    return axios.get(`/kmaudiensi/nonterima/nip/${nip}`);
  }

  export function getAudiensiTerima(){
    return axios.get(`/kmaudiensi/terima`);
  }
  
  export function getJenisAudiensi(){
    return axios.get("/detiljenis/byidjenis/9");
  }

  export function getAudiensiById(id){
    return axios.get(`/kmaudiensi/${id}`);
  }

  export function updateStatusAudiensi(id, status, alasan = null) {
    if (alasan) {
      return axios.put(`/kmaudiensi/updatekmaudiensistatus/${id}/${status}`, {
        alasan_tolak: alasan,
      });
    } else {
      return axios.put(
        `/kmaudiensi/updatekmaudiensistatus/${id}/${status}`,
        {}
      );
    }
  }

  export function deleteAudiensiById(id){
    return axios.delete(`/kmaudiensi/${id}`);
  }

  export function saveAudiensi(
  
    alasan_tolak ,
    body ,
    file_upload ,
    id_detiljns ,
    kd_kantor,
    kd_unit_org,
    nip_pengusul ,
    nip_pjbt_es3 ,
    nip_pjbt_es4 ,
    no_nd ,
    perihal ,
    status ,
    tgl_nd,
    
  ) {
  
    return axios.post("/kmaudiensi/", {
    alasan_tolak ,
    body ,
    file_upload ,
    id_detiljns ,
    kd_kantor,
    kd_unit_org,
    nip_pengusul ,
    nip_pjbt_es3 ,
    nip_pjbt_es4 ,
    no_nd ,
    perihal ,
    status ,
    tgl_nd,
    });
  }

  export function updateAudiensi(
    id,
    alasan_tolak ,
    body ,
    file_upload ,
    id_detiljns ,
    kd_kantor,
    kd_unit_org,
    nip_pengusul ,
    nip_pjbt_es3 ,
    nip_pjbt_es4 ,
    no_nd ,
    perihal ,
    status ,
    tgl_nd,
  ) {
    return axios.put(`/kmaudiensi/${id}`, {
    alasan_tolak ,
    body ,
    file_upload ,
    id_detiljns ,
    kd_kantor,
    kd_unit_org,
    nip_pengusul ,
    nip_pjbt_es3 ,
    nip_pjbt_es4 ,
    no_nd ,
    perihal ,
    status ,
    tgl_nd,
    });
  }

export function getSurveiNonTerima() {
  return axios.get("/kmsurvei/nonterima");
}

export function getSurveiTerima(){
  return axios.get("/kmsurvei/terima");
}

export function getJenisSurvei(){
  return axios.get("/detiljenis/byidjenis/6");
}
export function getSurveiById(id) {
  return axios.get(`/kmsurvei/${id}`);
}

export function deleteSurveiById(id) {
  return axios.delete(`/kmsurvei/${id}`);
}

export function updateStatusSurvei(id, status, alasan = null) {
  if (alasan) {
    return axios.put(`/kmsurvei/updatekmsurveistatus/${id}/${status}`, {
      alasan_tolak: alasan,
    });
  } else {
    return axios.put(`/kmsurvei/updatekmsurveistatus/${id}/${status}`, {});
  }
}

export function saveSurvei(
  alasan_tolak,
  body,
  file_upload,
  id_detiljns,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_nd,
  perihal,
  status,
  tgl_nd
) {
  return axios.post("/kmsurvei/", {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_nd,
    perihal,
    status,
    tgl_nd,
  });
}

export function updateSurvei(
  id,
  alasan_tolak,
  body,
  file_upload,
  id_detiljns,
  nip_pengusul,
  nip_pjbt_es3,
  nip_pjbt_es4,
  no_nd,
  perihal,
  status,
  tgl_nd
) {
  return axios.put(`/kmsurvei/${id}`, {
    alasan_tolak,
    body,
    file_upload,
    id_detiljns,
    nip_pengusul,
    nip_pjbt_es3,
    nip_pjbt_es4,
    no_nd,
    perihal,
    status,
    tgl_nd,
  });
}






