import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";

import { getPeraturan,getPeraturanTerima, getPeraturanById, updateStatusPeraturan, deletePeraturanById } from "../Api";

import { Pagination } from "../pagination/Pagination";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import swal from "sweetalert";
import SVG from "react-inlinesvg";

import RegulasiHistory from "./RegulasiHistory";
import RegulasiOpen from "./RegulasiOpen";



function RegDoneTable() {
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id_peraturan",
    pageNumber: 1,
    pageSize: 5
  };
  const history = useHistory();
  const addProposal = () => history.push("/knowledge/regulasi/new");

  const editProposal = id => history.push(`/knowledge/regulasi/${id}/edit`);

  let thePath = document.URL;
  const lastPath = thePath.substring(thePath.lastIndexOf('/') + 1);
  const applyProposal = id => {
    getPeraturanById(id).then(({ data }) => {
      updateStatusPeraturan(
        data.id_peraturan,
        2,
      )
        .then(({ status }) => {
          if (status === 201 || status === 200) {
            swal("Berhasil", "Data berhasil disimpan", "success").then(
              () => {
                history.push('/dashboard');
                history.replace('/knowledge/regulasi');
              }
            );
          } else {
            swal("Gagal", "Data gagal disimpan", "error").then(
              () => {
                history.push('/dashboard');
                history.replace('/knowledge/regulasi');
              }
            );
          }
        })
    });
  };


  const deleteDialog = id => {

    swal({
      title: "Hapus",
      text: "Apakah Anda yakin menghapus data ini ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((ret) => {

        if (ret == true) {

          deletePeraturanById(id)
            .then(({ data }) => {
              if (data.deleted == true) {
                swal("Berhasil", "Data berhasil dihapus", "success").then(
                  () => {
                    history.push('/dashboard');
                    history.replace('/knowledge/regulasi');
                  }
                );

              } else {
                swal("Gagal", "Data gagal dihapus", "error").then(
                  () => {
                    history.push('/dashboard');
                    history.replace('/knowledge/regulasi');
                  }
                );


              }

            })

        }



      });




  };

  const showHistoryPengajuan = id => history.push(`/knowledge/regdone/${id}/showhistory`);

  const openPeraturan = id => history.push(`/knowledge/regulasi/${id}/open`);

  const prosesPeraturan = id => history.push(`/knowledge/regulasi/${id}/proses`);

  const detailPeraturan = id => history.push(`/knowledge/regdone/${id}/detail`);
  
  const openAddPerTerkait = id => history.push(`/knowledge/regulasi/${id}/terkait`);
  const [proposal, setProposal] = useState([]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "no_regulasi",
      text: "No Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_regulasi",
      text: "Tgl Peraturan",
      sort: true,
      //formatter: columnFormatters.DateFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "perihal",
      text: "perihal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusColumnFormatter,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.RegulasiActionColoumnFormatter,
      formatExtraData: {
        deleteDialog: deleteDialog,
        applyProposal: applyProposal,
        openAddPerTerkait: openAddPerTerkait,
        editProposal: editProposal,
        showHistory : showHistoryPengajuan,
        openPeraturan :openPeraturan,
        detailPeraturan : detailPeraturan,
        prosesPeraturan : prosesPeraturan,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];


  const { SearchBar } = Search;




  

 
    useEffect(() => {
      //console.log(lastPath);
      getPeraturanTerima().then(({ data }) => {
        data.map((dt) => {
  
          setProposal(proposal => [...proposal, dt])
  
        })
      });
  }, []);



  const defaultSorted = [{ dataField: "id_peraturan", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: proposal.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => { return 'No Data to Display'; }

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_peraturan"
                  data={proposal}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      <Route path="/knowledge/regdone/:id/showhistory">
        {({ history, match }) => (
          <RegulasiHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/regdone");
            }}
            onRef={() => {
              history.push("/knowledge/regdone");
            }}
          />
        )}
      </Route>




    </>
  );
}

export default RegDoneTable;
