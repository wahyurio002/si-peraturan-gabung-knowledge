import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Select from "react-select";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
  Checkbox
} from "../proposal-edit/helpers";
import CustomFileInput from "./helpers/CustomFileInput";
import { getJenisPeraturan ,getTopik } from "../../Api";



function ProposalEditForm({ proposal, btnRef, saveProposal }) {
  const [jenisPeraturan, setJenisPeraturan] = useState([]);
  const [topik, setTopik] = useState([]);
  const { user } = useSelector((state) => state.auth);


  //const validProductValues = products.map(({ _id }) => _id);

  // Validation schema
  const ProposalEditSchema = Yup.object().shape({

    no_regulasi: Yup.string()
      .min(2, "Minimum 2 karakter")
      .max(50, "Maximum 50 karakter")
      .required("No Regulasi is required"),
    tgl_regulasi: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat is required"),

    perihal: Yup.string()
      .min(2, "Minimum 2 karakter")
      .max(250, "Maximum 50 karakter")
      .required("Perihal is karakter"),

      // file: Yup.mixed()
      // //.required("A file is required")
      // .test(
      //   "fileSize",
      //   "File too large",
      //   value => value && value.size <= FILE_SIZE
      // )
      // .test(
      //   "fileFormat",
      //   "Unsupported Format",
      //   value => value && SUPPORTED_FORMATS.includes(value.type)
      // ),

  });



  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];


  useEffect(() => {


    getJenisPeraturan().then(( {data} ) => {
      setJenisPeraturan(data);
    });




    getTopik().then(({ data }) => {
      setTopik(data);
    });


  }, []);


  return (
    <>

      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        validationSchema={ProposalEditSchema}
        onSubmit={values => {
          //console.log(values);
          saveProposal(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
        }) => {

          const handleChangeNip = () => {
            setFieldValue("nip_perekam", user.nip9);
          }

          return (
            <>
              <Form className="form form-label-right">
                {/* FIELD NO SURAT */}
                <div className="form-group row">
                  <Field
                    name="no_regulasi"
                    component={Input}
                    placeholder="Nomor Peraturan"
                    label="Nomor Peraturan"
                  // onClick={()=>handleChangeNip()}
                  />
                </div>
                {/* FIELD TANGGAL SURAT */}
                <div className="form-group row">
                  <DatePickerField name="tgl_regulasi" label="Tanggal Peraturan" />
                </div>
                {/* FIELD jenis */}
                <div className="form-group row">
                  <Sel name="jns_regulasi" label="Jenis Peraturan">
                    {jenisPeraturan.map((data, index) => (

                      <option key={index} value={data.id_jnsperaturan}>{data.nm_jnsperaturan}</option>

                    ))}
                  </Sel>
                </div>

                {/* FIELD tentang */}
                <div className="form-group row">
                  <Field
                    name="perihal"
                    component={Textarea}
                    placeholder="Perihal"
                    label="Perihal"
                  />
                </div>

                <div className="form-group row">
                  <Sel name="id_topik" label="topik">
                    {topik.map((data, index) => (

                      <option key={index} value={data.id}>{data.nama}</option>

                      ))}
                  </Sel>
                </div>


                {/* FIELD UPLOAD FILE */}
                <div className="form-group row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Upload File
                  </label>
                  <div className="col-lg-8 col-xl-6">
                    <Field
                      name="file"
                      component={CustomFileInput}
                      title="Select a file"
                      label="File"
                      // setFieldValue={setFieldValue}
                      // errorMessage={errors["file"] ? errors["file"] : undefined}
                      // touched={touched["file"]}
                      style={{ display: "flex" }}
                    // onBlur={handleBlur}
                    />
                  </div>
                </div>






                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                {/* {isValid ? setDisabled(false) : setDisabled(true)} */}
              </Form>
            </>
          );
        }}
      </Formik>
    </>
  );
}

export default ProposalEditForm;