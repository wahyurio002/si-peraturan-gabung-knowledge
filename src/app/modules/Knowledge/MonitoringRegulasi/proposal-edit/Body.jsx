import React, { useEffect, useState, useRef } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
} from "../../../../../_metronic/_partials/controls";

import { Field, Formik, Form } from "formik";

import { useSubheader } from "../../../../../_metronic/layout";
import ProposalEditFooter from "./ProposalEditFooter";
import BodyAddEditor from "./helpers/BodyAddEditor";
//import { updateStatusPeraturan, getPeraturanById, uploadFile, savePeraturan, updatePeraturan } from "../../Api";
//import swal from "sweetalert";

const initValues = {
  no_regulasi: "",
  tgl_regulasi: "",
  perihal: "",
  file_upload: "",
  status: "",
  jns_regulasi: "",
  id_topik: "",
  alasan_tolak: "",
  nip_pengusul: "",
  nip_pjbt_es4: "",
  nip_pjbt_es3: "",
};

function Body({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [isDisabled, setIsDisabled] = useState();
  const [proposal, setProposal] = useState();

  useEffect(() => {
    let _title = "Tambah Body";

    setTitle(_title);
    suhbeader.setTitle(_title);
  }, [id, suhbeader]);
  const btnRef = useRef();

  const backToProposalList = () => {
    history.push(`/knowledge/regulasi`);
  };

  return (
    <Card>
      {/* {actionsLoading && <ModalProgressBar />} */}
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <Formik
          enableReinitialize={true}
          initialValues={{ bodyPeraturan: "" }}
          onSubmit={(values) => {
            console.log(values);
            //saveProposal(values);
          }}
        >
          {({ handleSubmit, setFieldValue }) => {
            return (
              <>
                <Form className="form form-label-right">
                  <div className="mt-5">
                    <div className="form-group row m-1">
                      <Field
                        name="bodyPeraturan"
                        component={BodyAddEditor}
                        label="Coba Text Editor"
                      />
                    </div>
                  </div>
                  <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                </Form>
              </>
            );
          }}
        </Formik>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <ProposalEditFooter
          backAction={backToProposalList}
          btnRef={btnRef}
        ></ProposalEditFooter>
      </CardFooter>
    </Card>
  );
}

export default Body;
