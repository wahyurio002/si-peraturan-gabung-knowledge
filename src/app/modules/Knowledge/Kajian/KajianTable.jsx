import BootstrapTable from "react-bootstrap-table-next";
import { useSelector } from "react-redux";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";

import {
  getKajianNonTerimaByNip,
  getKajianById,
  updateStatusKajian,
  deleteKajianById,
} from "../Api";

import { Pagination } from "../pagination/Pagination";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import swal from "sweetalert";
import SVG from "react-inlinesvg";

import KajianHistory from "./KajianHistory";
import KajianHistoryReject from "./KajianHistoryReject";
// import RegulasiOpen from "./RegulasiOpen";

function KajianTable() {
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id_kajian",
    pageNumber: 1,
    pageSize: 50,
  };
  const history = useHistory();
  const { user } = useSelector((state) => state.auth);
  const addProposal = () => history.push("/knowledge/kajian/usulan/new");

  const editProposal = (id) =>
    history.push(`/knowledge/kajian/usulan/${id}/edit`);

  let thePath = document.URL;
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);
  const applyProposal = (id) => {
    getKajianById(id).then(({ data }) => {
      updateStatusKajian(id, 1, 2, user.nip9).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal(
            "Berhasil",
            "Usulan berhasil diajukan ke atasan",
            "success"
          ).then(() => {
            history.push("/dashboard");
            history.replace("/knowledge/kajian/usulan");
          });
        } else {
          swal("Gagal", "Usulan gagal diajukan ke atasan", "error").then(() => {
            history.push("/dashboard");
            history.replace("/knowledge/kajian/usulan");
          });
        }
      });
    });
  };

  const deleteDialog = (id) => {
    swal({
      title: "Hapus",
      text: "Apakah Anda yakin menghapus data ini ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((ret) => {
      if (ret == true) {
        deleteKajianById(id).then(({ data }) => {
          if (data.deleted == true) {
            swal("Berhasil", "Usulan berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/kajian/usulan");
            });
          } else {
            swal("Gagal", "Usulan gagal dihapus", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/kajian/usulan");
            });
          }
        });
      }
    });
  };

  const showHistoryPengajuan = (id) =>
    history.push(`/knowledge/kajian/usulan/${id}/showhistory`);

  const showHistoryPenolakan = (id) =>
    history.push(`/knowledge/kajian/usulan/${id}/showhistoryreject`);

  const openKajian = (id) =>
    history.push(`/knowledge/kajian/usulan/${id}/open`);

  const prosesKajian = (id) =>
    history.push(`/knowledge/kajian/usulan/${id}/proses`);

  const detailKajian = (id) =>
    history.push(`/knowledge/kajian/usulan/${id}/detail`);

  const openAddPerTerkait = (id) =>
    history.push(`/knowledge/kajian/usulan/${id}/terkait`);

  const addBody = (id) => history.push(`/knowledge/kajian/usulan/${id}/body`);

  const [proposal, setProposal] = useState([]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "jenisKajian.nama",
      text: "Jenis  Kajian",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "hasilKajian.tgl_kajian",
      text: "Tanggal Kajian",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "hasilKajian.perihal",
      text: "Perihal Kajian",
      sort: true,
      //formatter: columnFormatters.DateFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "direktoratPembuat.nama",
      text: "Unit Pembuat  Kajian",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },

    {
      dataField: "hasilKajian.status",
      text: "status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusKajianColumnFormatter,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.KajianActionColoumnFormatter,
      formatExtraData: {
        deleteDialog: deleteDialog,
        applyProposal: applyProposal,
        openAddPerTerkait: openAddPerTerkait,
        editProposal: editProposal,
        showHistory: showHistoryPengajuan,
        showHistoryTolak: showHistoryPenolakan,
        openKajian: openKajian,
        detailKajian: detailKajian,
        prosesKajian: prosesKajian,
        addBody: addBody,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const { SearchBar } = Search;

  useEffect(() => {
    getKajianNonTerimaByNip(user.nip9).then(({ data }) => {
      // setPlan(data);
      data.map((data) => {
        return data.hasilKajian.status !== "Terima"
          ? setProposal((proposal) => [...proposal, data])
          : // : data.status === "Draft"
            // ? setProposal((proposal) => [...proposal, data])
            null;
      });
    });
  }, [user.kantorLegacyKode]);
  console.log(user);

  const defaultSorted = [{ dataField: "id_peraturan", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: proposal.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_peraturan"
                  data={proposal}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right",
                            }}
                            onClick={addProposal}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      <Route path="/knowledge/kajian/usulan/:id/showhistory">
        {({ history, match }) => (
          <KajianHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/kajian/usulan");
            }}
            onRef={() => {
              history.push("/knowledge/kajian/usulan");
            }}
          />
        )}
      </Route>

      <Route path="/knowledge/kajian/usulan/:id/showhistoryreject">
        {({ history, match }) => (
          <KajianHistoryReject
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/kajian/usulan");
            }}
            onRef={() => {
              history.push("/knowledge/kajian/usulan");
            }}
          />
        )}
      </Route>
      {/* <Route path="/knowledge/kajian/usulan/:id/open">
        {({ history, match }) => (
          <RegulasiOpen
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/kajian/usulan");
            }}
            onRef={() => {
              history.push("/knowledge/kajian/usulan");
            }}
          />
        )}
      </Route> */}
    </>
  );
}

export default KajianTable;
