import React, { useEffect, useState } from "react";
import { Modal, Table, FormControl, InputGroup, Button } from "react-bootstrap";
import { saveDokLainKajian, uploadFile } from "../../Api";
import axios from "axios";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import { useHistory } from "react-router-dom";
import { Field, Formik, Form } from "formik";
import swal from "sweetalert";
import {
  DatePickerField,
  Input,
  Textarea,
  Select as Sel,
  Checkbox,
} from "../proposal-edit/helpers";
import CustomFileInput from "./helpers/CustomFileInput";
// import { DateFormat } from "../helpers/DateFormat";

function DokumenLainModal({ id, show, onHide }) {
  const addDokLain = (values) => {
    const formData = new FormData();
    formData.append("file", values.file);
    uploadFile(formData)
      .then(({ data }) =>
        saveDokLainKajian(data.message, id, values.keterangan).then(
          ({ status }) => {
            if (status === 201 || status === 200) {
              swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
                history.push(`/knowledge/kajian/usulan`);
                history.replace(`/knowledge/kajian/usulan/${id}/terkait`);
              });
            } else {
              swal("Gagal", "Data gagal disimpan", "error").then(() => {
                history.push(`/knowledge/kajian/usulan`);
                history.replace(`/knowledge/kajian/usulan/${id}/terkait`);
              });
            }
          }
        )
      )
      .catch(() => window.alert("Oops Something went wrong !"));
  };

  const history = useHistory();
  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Upload Dokumen Lainnya
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <Formik
          enableReinitialize={true}
          initialValues={{ keterangan: "", file: "" }}
          onSubmit={(values) => {
            //console.log(values);
            addDokLain(values);
          }}
        >
          {({ handleSubmit, setFieldValue }) => {
            return (
              <>
                <Form className="form form-label-right">
                  {/* FIELD tentang */}
                  <div className="form-group row">
                    <Field
                      name="keterangan"
                      component={Textarea}
                      placeholder="Keterangan"
                      label="Keterangan"
                    />
                  </div>

                  {/* FIELD UPLOAD FILE */}
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Upload File
                    </label>
                    <div className="col-lg-8 col-xl-6">
                      <Field
                        name="file"
                        component={CustomFileInput}
                        title="Select a file"
                        label="File"
                        style={{ display: "flex" }}
                      />
                    </div>
                  </div>

                  <div className="col-lg-12" style={{ textAlign: "center" }}>
                    <button
                      type="button"
                      className="btn btn-success"
                      style={{
                        textAlign: "center",
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                      onClick={() => handleSubmit()}
                    >
                      Upload
                    </button>
                    {"  "}
                    <button
                      type="button"
                      onClick={onHide}
                      className="btn btn-light"
                      style={{
                        boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                      }}
                    >
                      <i className="flaticon2-cancel icon-nm"></i>
                      Tutup
                    </button>
                  </div>
                </Form>
              </>
            );
          }}
        </Formik>
      </Modal.Body>
      <Modal.Footer style={{ borderTop: "none" }}></Modal.Footer>
    </Modal>
  );
}

export default DokumenLainModal;
