import React, {useState, useEffect} from "react";

function RegulasiTerkaitFooter({ backAction, btnRef}) {



  return (
    <>
      <div className="col-lg-12" style={{ textAlign: "right" }}>
        <button
          type="button"
          onClick={backAction}
          className="btn btn-light"
          style={{
            boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
          }}
        >
          <i className="fa fa-arrow-left"></i>
          Kembali
        </button>
      </div>
    </>
  );
}

export default RegulasiTerkaitFooter;
