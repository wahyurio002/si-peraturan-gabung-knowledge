/* Library */
import React, { useEffect, useState } from "react";
import { useHistory, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import swal from "sweetalert";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import SVG from "react-inlinesvg";

/* Helpers */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
/* Component */
import KodefikasiProposalHistory from "./KodefikasiProposalHistory";
import KodefikasiProposalHistoryReject from "./KodefikasiProposalHistoryReject";

/* Utility */
import {
  updateStatusKodefikasi,
  deleteKodefikasi,
  getKodefikasiNonTerimaByNip,
} from "../Api";
import NdPermintaanForm from "../../Composing/process/nd-permintaan/NdPermintaanForm";

function KodefikasiProposalTable() {
  const { user } = useSelector((state) => state.auth);
  const history = useHistory();
  const [proposal, setProposal] = useState([]);

  const addProposal = () => history.push("/knowledge/kodefikasi/usulan/add");
  const showHistoryKodefikasi = (id) =>
    history.push(`/knowledge/kodefikasi/usulan/${id}/showhistory`);
  const showHistoryPenolakan = (id) =>
    history.push(`/knowledge/kodefikasi/usulan/${id}/showhistoryreject`);
  const detailKodefikasi = (id) =>
    history.push(`/knowledge/kodefikasi/usulan/${id}/detail`);

  const editProposal = (id) =>
    history.push(`/knowledge/kodefikasi/usulan/${id}/edit`);
  const applyProposal = (id) => {
    updateStatusKodefikasi(id, 1, 2, user.nip9).then(({ status }) => {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Usulan berhasil dijaukan ke atasan", "success").then(
          () => {
            history.push("/dashboard");
            history.replace("/knowledge/kodefikasi/usulan");
          }
        );
      } else {
        swal("Gagal", "Usulan gagal diajukan ke atasan", "error").then(() => {
          history.push("/dashboard");
          history.replace("/knowledge/kodefikasi/usulan");
        });
      }
    });
  };

  const deleteAction = (id) => {
    swal({
      title: "Apakah Anda Yakin Ingin Menghapus Data ini?",
      text: "Klik OK untuk melanjutkan",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        deleteKodefikasi(id).then(({ status }) => {
          if (status === 200) {
            swal("Berhasil", "Usulan berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/kodefikasi/usulan");
            });
          } else {
            swal("Gagal", "Usulan gagal dihapus", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/kodefikasi/usulan");
            });
          }
        });
      }
    });
  };

  const addBody = (id) =>
    history.push(`/knowledge/kodefikasi/usulan/${id}/body`);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "no_nd",
      text: "No ND Kodefikasi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "tgl_nd",
      text: "Tgl ND Kodefikasi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "perihal",
      text: "Perihal Kodefikasi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "jenis_pajak.nm_jnspajak",
      text: "Jenis Pajak",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusColumnFormatter,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.KodefikasiProposalActionColumnFormatter,
      formatExtraData: {
        openEditDialog: editProposal,
        openDeleteDialog: deleteAction,
        showHistory: showHistoryKodefikasi,
        showHistoryTolak: showHistoryPenolakan,
        detailKodefikasi: detailKodefikasi,
        applyProposal: applyProposal,
        addBody: addBody,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_surat",
    pageNumber: 1,
    pageSize: 50,
  };

  const defaultSorted = [{ dataField: "no_perencanaan", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: proposal.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };
  const { SearchBar } = Search;

  useEffect(() => {
    getKodefikasiNonTerimaByNip(user.nip9).then(({ data }) => {
      // setPlan(data);
      data.map((data) => {
        return data.status !== "Terima"
          ? setProposal((proposal) => [...proposal, data])
          : // : data.status === "Draft"
            // ? setProposal((proposal) => [...proposal, data])
            null;
      });
    });
  }, [user.kantorLegacyKode]);
  console.log(user);

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_kodefikasi"
                  data={proposal}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right",
                            }}
                            onClick={addProposal}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button>
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/kodefikasi/usulan/:id/showhistoryreject">
        {({ history, match }) => (
          <KodefikasiProposalHistoryReject
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/kodefikasi/usulan");
            }}
            onRef={() => {
              history.push("/knowledge/kodefikasi/usulan");
            }}
          />
        )}
      </Route>
      <Route path="/knowledge/kodefikasi/usulan/:id/showhistory">
        {({ history, match }) => (
          <KodefikasiProposalHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/kodefikasi/usulan");
            }}
            onRef={() => {
              history.push("/knowledge/kodefikasi/usulan");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default KodefikasiProposalTable;
