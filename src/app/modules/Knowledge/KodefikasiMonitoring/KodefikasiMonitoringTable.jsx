import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses,
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";

import { Pagination } from "../pagination/Pagination";
import { toAbsoluteUrl } from "../../../../_metronic/_helpers";
import swal from "sweetalert";
import SVG from "react-inlinesvg";
import {
  // getKodefikasi,
  getKodefikasiByKantor,
  getKodefikasiById,
  updateStatusKodefikasi,
  deleteKodefikasi,
  // getJenisPajak,
} from "../Api";

import KodefikasiMonitoringHistory from "./KodefikasiMonitoringHistory";
import KodefikasiMonitoringHistoryReject from "./KodefikasiMonitoringHistoryReject";

import { useSelector } from "react-redux";
// import KodefikasiOpen from "./KodefikasiOpen";

const localStorageActiveTabKey = "builderActiveTab";
function KodefikasiMonitoringTable() {
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_surat",
    pageNumber: 1,
    pageSize: 50,
  };
  const history = useHistory();
  const { role, user } = useSelector((state) => state.auth);
  const konseptor = role.includes("ROLE_PERATURAN_KONSEPTOR");
  const es4 = role.includes("ROLE_PERATURAN_PENELITI_LVL1");
  const es3 = role.includes("ROLE_PERATURAN_PENELITI_LVL2");
  const admin = role.includes("ROLE_ADMIN_PERATURAN");
  const activeTab = localStorage.getItem(localStorageActiveTabKey);

  const saveCurrentTab = (_tab) => {
    localStorage.setItem(localStorageActiveTabKey, _tab);
  };

  const addProposal = () =>
    history.push("/knowledge/kodefikasi/monitoring/new");
  //const openProposal = id => history.push(`/compose/proposal/${id}/open`);
  const editKodefikasi = (id) =>
    history.push(`/knowledge/kodefikasi/monitoring/${id}/edit`);

  let thePath = document.URL;
  const lastPath = thePath.substring(thePath.lastIndexOf("/") + 1);

  const rejectProposal = (id) =>
    history.push(`/knowledge/kodefikasi/monitoring/${id}/reject`);
  const applyKodefikasi = (id) => {
    getKodefikasiById(id).then(({ data }) => {
      updateStatusKodefikasi(data.id_kodefikasi, 2).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/dashboard");
            history.replace("/knowledge/kodefikasi/monitoring");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace("/knowledge/kodefikasi/monitoring");
          });
        }
      });
    });
  };

  const deleteDialog = (id) => {
    swal({
      title: "Hapus",
      text: "Apakah Anda yakin menghapus data ini ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((ret) => {
      if (ret == true) {
        deleteKodefikasi(id).then(({ data }) => {
          if (data.deleted == true) {
            swal("Berhasil", "Data berhasil dihapus", "success").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/kodefikasi/monitoring");
            });
          } else {
            swal("Gagal", "Data gagal dihapus", "error").then(() => {
              history.push("/dashboard");
              history.replace("/knowledge/kodefikasi/monitoring");
            });
          }
        });
      }
    });
  };

  const showHistoryPengajuan = (id) =>
    history.push(`/knowledge/kodefikasi/monitoring/${id}/showhistory`);

  const showHistoryPenolakan = (id) =>
    history.push(`/knowledge/kodefikasi/monitoring/${id}/showhistoryreject`);

  const openKodefikasi = (id) =>
    history.push(`/knowledge/kodefikasi/monitoring/${id}/open`);

  const prosesKodefikasi = (id) =>
    history.push(`/knowledge/kodefikasi/monitoring/${id}/proses`);

  const detailKodefikasi = (id) =>
    history.push(`/knowledge/kodefikasi/monitoring/${id}/detail`);

  const openAddKodTerkait = (id) =>
    history.push(`/knowledge/kodefikasi/monitoring/${id}/terkait`);
  const [proposal, setProposal] = useState([]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "no_nd",
      text: "No ND Kodefikasi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "tgl_nd",
      text: "Tgl ND Kodefikasi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "perihal",
      text: "Perihal Kodefikasi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "jenis_pajak.nm_jnspajak",
      text: "Jenis Pajak Kodefikasi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusColumnFormatter,
      headerSortingClasses,
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.KodefikasiMonitoringActionColumnFormatter,

      formatExtraData: {
        // deleteDialog: deleteDialog,
        // applyKodefikasi: applyKodefikasi,
        openAddKodTerkait: openAddKodTerkait,
        // editKodefikasi: editKodefikasi,
        showHistory: showHistoryPengajuan,
        showHistoryTolak: showHistoryPenolakan,
        openKodefikasi: openKodefikasi,
        detailKodefikasi: detailKodefikasi,
        showReject: rejectProposal,
        // prosesKodefikasi: prosesKodefikasi,
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  // const proposal = [
  //   {
  //     id: 1,
  //     no: 1,
  //     no_kodefikasi: "No 1/PJ.12/2020",
  //     tgl_kodefikasi: "10/12/2020",
  //     jenis: "PPN",
  //     perihal: "Peraturan I",
  //     status: "Draft",
  //   },
  //   {
  //     id: 2,
  //     no: 2,
  //     no_kodefikasi: "No 2/PJ.12/2020",
  //     tgl_kodefikasi: "10/12/2020",
  //     jenis: "PPh",
  //     perihal: "Peraturan II",
  //     status: "Eselon 4",
  //   },
  // ];

  const { SearchBar } = Search;

  useEffect(() => {
    getKodefikasiByKantor(user.kantorLegacyKode).then(({ data }) => {
      // setPlan(data);
      data.map((data) => {
        // return data.status !== "Draft"
        //   ? setProposal((proposal) => [...proposal, data])
        //   : null;
        return data.status === "Eselon 3"
          ? setProposal((proposal) => [...proposal, data])
          : data.status === "Eselon 4"
          ? setProposal((proposal) => [...proposal, data])
          : data.status === "Terima"
          ? setProposal((proposal) => [...proposal, data])
          : null;
      });
    });
  }, [user.kantorLegacyKode]);
  console.log(user);

  // useEffect(() => {
  //   getKodefikasiByKantor(user.KantorLegacyKode).then(({ data }) => {
  //     data.map((dt) => {
  //       // if(dt.id_tahapan !== 2 ){
  //       setProposal((proposal) => [...proposal, dt]);
  //       // }
  //     });
  //   });
  // }, []);

  // useEffect(() => {

  //     getKodefikasiByKantor(
  //       user.kantorLegacyKode,
  //       user.unitLegacyKode,
  //       "Eselon 4"
  //     ).then(({ data }) => {
  //       setProposal(data);
  //     });

  //   }, []);

  // if (konseptor) {
  //   getKodefikasiNonTerimaByNip(user.nip9).then(({ data }) => {
  //     setProposal(data);
  //   });
  // } else {
  //   if (es4)
  //     getKodefikasiNonTerimaEs4(
  //       user.kantorLegacyKode,
  //       user.unitLegacyKode
  //     ).then(({ data }) => {
  //       setProposal(data);
  //     });
  //   if (es3)
  //     getKodefikasiNonTerimaEs3(
  //       user.kantorLegacyKode,
  //       user.unitLegacyKode
  //     ).then(({ data }) => {
  //       setProposal(data);
  //     });
  // }

  console.log(user);

  // useEffect(() => {
  //   // if (columnFormatters.KodefikasiActionColumnFormatterEs4)
  //   getKodefikasiNonTerimaEs4(user.kantorLegacyKode, user.unitLegacyKode).then(
  //     ({ data }) => {
  //       setProposal(data);
  //     }
  //   );
  // }, []);

  const defaultSorted = [{ dataField: "no_perencanaan", order: "asc" }];
  const sizePerPageList = [
    { text: "50", value: 50 },
    { text: "25", value: 25 },
    { text: "10", value: 10 },
  ];
  const pagiOptions = {
    custom: true,
    totalSize: proposal.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber, //curent page (default 1),
  };
  const emptyDataMessage = () => {
    return "No Data to Display";
  };

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_kodefikasi"
                  data={proposal}
                  columns={columns}
                  search
                >
                  {(props) => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                        <div className="col-lg-6 col-xl-6 mb-3">
                          {/* <button
                            type="button"
                            className="btn btn-primary"
                            style={{
                              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)",
                              float: "right",
                            }}
                            onClick={addProposal}
                          >
                            <span className="svg-icon menu-icon">
                              <SVG
                                src={toAbsoluteUrl(
                                  "/media/svg/icons/Code/Plus.svg"
                                )}
                              />
                            </span>
                            Tambah
                          </button> */}
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>
      <Route path="/knowledge/kodefikasi/monitoring/:id/showhistory">
        {({ history, match }) => (
          <KodefikasiMonitoringHistory
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/kodefikasi/monitoring");
            }}
            onRef={() => {
              history.push("/knowledge/kodefikasi/monitoring");
            }}
          />
        )}
      </Route>

      <Route path="/knowledge/kodefikasi/monitoring/:id/showhistoryreject">
        {({ history, match }) => (
          <KodefikasiMonitoringHistoryReject
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/kodefikasi/usulan");
            }}
            onRef={() => {
              history.push("/knowledge/kodefikasi/usulan");
            }}
          />
        )}
      </Route>
      {/* <Route path="/knowledge/kodefikasi/:id/open">
        {({ history, match }) => (
          <KodefikasiOpen
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/knowledge/regulasi");
            }}
            onRef={() => {
              history.push("/knowledge/regulasi");
            }}
          />
        )}
      </Route> */}
    </>
  );
}

export default KodefikasiMonitoringTable;
