import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import KodefikasiPenelitianTable from "./KodefikasiPenelitianTable";

function KodefikasiPenelitian() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Kodefikasi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
          <KodefikasiPenelitianTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default KodefikasiPenelitian;
