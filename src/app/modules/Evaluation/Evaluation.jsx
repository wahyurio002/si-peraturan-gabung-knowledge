import React from "react";
import { useSelector } from "react-redux";
import { Switch, Redirect } from "react-router-dom";
import { ContentRoute } from "../../../_metronic/layout";
import Proposal from "./Proposal/Proposal";
import ProposalEdit from "./Proposal/proposal-edit/ProposalEdit";
import ProposalUnit from "./Proposal-Unit/ProposalUnit";
import ProposalUnitEdit from "./Proposal-Unit/proposal-unit-edit/ProposalUnitEdit";
import Research from "./Research/Research";
import Validate from "./Validate/Validate"
import ValidateAdd from "./Validate/add/ValidateAdd";
import ValidateResearch from "./Validate-Research/ValidateResearch";
import Monitoring from "./Monitoring/Monitoring";
import EvaluationErrors from "./EvaluationErrors";


export default function Evaluation() {
  const { role } = useSelector(state => state.auth);
  console.log(role)

  const konseptor = role.includes("ROLE_PERATURAN_PEMBUAT");
  const usr = role.includes("ROLE_USER");
  const admin = role.includes("ROLE_ADMIN");

  // console.log(usr)

  return (
    <Switch>
      <Redirect
          exact={true}
          from="/evaluation"
          to="/evaluation/proposal"
        />

      {/* Begin of Usulan */}
      <ContentRoute path="/evaluation/proposal/new" component={usr ? ProposalEdit : EvaluationErrors} />
      <ContentRoute
          path="/evaluation/proposal/:id/edit"
          component={ProposalEdit}
        />
      <ContentRoute path="/evaluation/proposal" component={usr ? Proposal : EvaluationErrors} />
      {/* End of Usulan */}

      {/* Begin of Penelitian */}
      {/* <ContentRoute path="/evaluation/research" component={admin ? Research : EvaluationErrors} /> */}
      <ContentRoute path="/evaluation/research" component={Research} />
      {/* End of Penelitian */}

      {/* Begin of Validasi */}
      <ContentRoute
          path="/evaluation/validate/:id/add"
          component={ValidateAdd}
        />
      <ContentRoute path="/evaluation/validate" component={Validate} />
      {/* End of Validasi */}

      {/* Begin of Penelitian Validasi */}
      <ContentRoute path="/evaluation/revalidate" component={ValidateResearch} />
      {/* End of Penelitian Validasi */}

      {/* Begin of Monitoring */}
      <ContentRoute path="/evaluation/monitoring" component={Monitoring} />
      {/* End of Monitoring */}


      
       

      {/* Layout */}
      {/* <ContentRoute from="/google-material/layout" component={LayoutPage} /> */}
    </Switch>
  );
}
