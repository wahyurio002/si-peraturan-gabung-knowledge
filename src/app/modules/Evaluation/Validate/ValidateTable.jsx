/* Library */
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import swal from "sweetalert";

/* Helper */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";

/* Component */
import ValidateReject from "./ValidateReject";
import ValidateOpen from "./ValidateOpen";


/* Utility */
import { getValidasiUsulan, updateStatusValidasi } from "../Api";

function ValidateTable() {
  const history = useHistory();
  const [proposal, setProposal] = useState([]);
  const { user } = useSelector(state => state.auth);

  const openProposal = id => history.push(`/evaluation/validate/${id}/open`);
  const rejectProposal = id =>
    history.push(`/evaluation/validate/${id}/reject`);
  const addDetil = id => history.push(`/evaluation/validate/${id}/add`);

  useEffect(() => {
    getValidasiUsulan().then(({ data }) => {
      data.map(data => {
        return data.status !== "Terima"
          ? setProposal(proposal => [...proposal, data])
          : null;
      });
    });
  }, []);
  const applyValidation = id => {
    if (user.jabatan === "Pelaksana") {
      updateStatusValidasi(id, 2, user.kantorLegacyKode).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/dashboard");
            history.replace("/evaluation/validate");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/dashboard");
            history.replace("/evaluation/validate");
          });
        }
      });
    }
  };

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "usulan.no_surat",
      text: "No Surat",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "usulan.tgl_surat",
      text: "Tgl Surat",
      sort: true,
      formatter: columnFormatters.SuratDateFormatterNested,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "usulan.wkt_create",
      text: "Tgl Usulan",
      sort: true,
      formatter: columnFormatters.UsulanDateFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "usulan.perihal",
      text: "Perihal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "usulan.file_upload",
      text: "File Surat",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.FileColumnFormatterEvaValidate,
      headerSortingClasses
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.StatusColumnFormatterEvaValidate,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterEvaValidate,
      formatExtraData: {
        openAddDetil: addDetil,
        // openDeleteDialog: deleteAction,
        showProposal: openProposal,
        showReject: rejectProposal,
        applyValidation: applyValidation
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "usulan.no_surat",
    pageNumber: 1,
    pageSize: 5
  };
  const { SearchBar } = Search;
  const defaultSorted = [{ dataField: "usulan.no_surat", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: proposal.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => { return 'No Data to Display';}

  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="id_usulan"
                  data={proposal}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      {/*Route to Open*/}
      <Route path="/evaluation/validate/:id/open">
        {({ history, match }) => (
          <ValidateOpen
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/evaluation/validate");
            }}
            onRef={() => {
              history.push("/evaluation/validate");
            }}
          />
        )}
      </Route>
      {/*Route to Reject*/}
      <Route path="/evaluation/validate/:id/reject">
        {({ history, match }) => (
          <ValidateReject
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/evaluation/validate");
            }}
            onRef={() => {
              history.push("/evaluation/validate");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default ValidateTable;
