/* eslint-disable no-restricted-imports */

import React, {useState, useEffect}from "react";
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepConnector from "@material-ui/core/StepConnector";
import Button from "@material-ui/core/Button";
import { Input, Textarea, Select } from "../../../../helpers";
import { Field } from "formik";
import { getJenisPeraturan } from "../../Api";
import CustomFileInput from "../../../../helpers/form/CustomFileInput";

const useStyles = makeStyles(theme => ({
  root: {
    width: "90%"
  },
  button: {
    marginRight: theme.spacing(1)
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1)
  },
  connectorActive: {
    "& $connectorLine": {
      borderColor: theme.palette.secondary.main
    }
  },
  connectorCompleted: {
    "& $connectorLine": {
      borderColor: theme.palette.primary.main
    }
  },
  connectorDisabled: {
    "& $connectorLine": {
      borderColor: theme.palette.grey[100]
    }
  },
  connectorLine: {
    transition: theme.transitions.create("border-color")
  }
}));

function getSteps() {
  return [
    "Detil Validasi",
    "Peraturan / Penegasan Terkait",
    "Usulan Penyusunan / Perubahan / Penggantian Peraturan / Penegasan"
  ];
}


export default function FormStepper({btnRef, check, handleChange}) {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();

  const [jenisPeraturan, setJenisPeraturan] = useState([])

  useEffect(() => {
   getJenisPeraturan().then(({data})=> {
     setJenisPeraturan(data);
   })
  }, [])

  function handleNext() {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  }

  function handleBack() {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  }

  function handleReset() {
    setActiveStep(0);
  }

  const connector = (
    <StepConnector
      classes={{
        active: classes.connectorActive,
        completed: classes.connectorCompleted,
        disabled: classes.connectorDisabled,
        line: classes.connectorLine
      }}
    />
  );

  function getStepContent(step) {
    switch (step) {
      case 0:
        return (
          <>
            {/* Field Unit Incharge */}
            {/* Diambil dari pegawai yang melakukan perekaman */}
            <div className="form-group row">
              <Field
                name="unit_incharge"
                component={Input}
                placeholder="Unit Incharge"
                label="Unit Incharge"
                disabled
                solid={"true"}
                
              />
            </div>
            {/* Field NIP PIC */}
            {/* Diambil dari pegawai yang melakukan perekaman */}
            <div className="form-group row">
              <Field
                name="nip_perekam"
                component={Input}
                placeholder="NIP PIC"
                label="NIP PIC"
                solid={"true"}
                disabled

              />
            </div>
            {/* Field Nama Pegawai */}
            {/* Diambil dari pegawai yang melakukan perekaman */}
            <div className="form-group row">
              <Field
                name="nm_pegawai"
                component={Input}
                placeholder="Nama Pegawai"
                label="Nama Pegawai"
                solid={"true"}
                disabled
              />
            </div>
            {/* Field Isu Masalah */}
            <div className="form-group row">
              <Field
                name="isu_masalah"
                component={Input}
                placeholder="Isu Masalah"
                label="Isu Masalah"
               
              />
            </div>
          </>
        );
      case 1:
        return (
          <>
          {/* Field No Peraturan */}
          <div className="form-group row">
              <Field
                name="no_peraturan"
                component={Input}
                placeholder="Nomor Peraturan"
                label="Nomor Peraturan"
              />
            </div>
            {/* Field Judul Peraturan */}
            <div className="form-group row">
              <Field
                name="judul_peraturan"
                component={Input}
                placeholder="Judul Peraturan"
                label="Judul Peraturan"
              />
            </div>
  
            {/* Field Tentang */}
            <div className="form-group row">
              <Field
                name="tentang"
                component={Textarea}
                placeholder="Tentang"
                label="Tentang"
              />
            </div>
          </>
        );
      case 2:
        return (
          <>
            {/* Field Jenis Peraturan  */}
            <div className="form-group row">
              <Select name="jns_peraturan" label="Jenis Peraturan" onClick={()=>handleChange()}>
                <option value=''>Pilih Jenis Peraturan</option>
                {jenisPeraturan.map((data)=> (
                  <option key={data.id_jnsperaturan} value={data.nm_jnsperaturan}>{data.nm_jnsperaturan}</option>

                )
                )}
              </Select>
            </div>
            {/* Field Konten Peraturan */}
            <div className="form-group row">
              <Field
                name="konten_peraturan"
                component={Textarea}
                placeholder="Konten Peraturan"
                label="Konten Peraturan"
              />
            </div>
            {/* Field Alasan */}
            <div className="form-group row">
              <Field
                name="alasan"
                component={Textarea}
                placeholder="Alasan"
                label="Alasan"
              />
            </div>
            {/* Field Analisis Dampak */}
            <div className="form-group row">
              <Field
                name="analisa_dampak"
                component={Textarea}
                placeholder="Analisis Dampak"
                label="Analisis Dampak"
              />
            </div>
            {/* Field Unit Kerja Pengusul */}
            <div className="form-group row">
              <Field
                name="unit_pengusul"
                component={Input}
                placeholder="Unit Kerja Pengusul"
                label="Unit Kerja Pengusul"
                solid={"true"}
                disabled
              />
            </div>
            {/* Field Upload File */}
            <div className="form-group row">
                      <label className="col-xl-3 col-lg-3 col-form-label">
                        Upload File
                      </label>
                      <div className="col-lg-9 col-xl-6">
                        <Field
                          name="file"
                          component={CustomFileInput}
                          title="Select a file"
                          label="File"
                          style={{ display: "flex" }}
                        />
                      </div>
                    </div>
          </>
        );
  
      default:
        return "Unknown step";
    }
  }

  const saveForm = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
      // setActiveStep(prevActiveStep => prevActiveStep + 1);

    }
  };
  

  return (
    <div className={classes.root}>
      <Stepper alternativeLabel activeStep={activeStep} connector={connector}>
        {steps.map(label => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <div>
        {/* {activeStep === steps.length ? (
          <div className="text-right">
            <Typography className={classes.instructions}>
              All steps completed - you&apos;re finished
            </Typography>
            <Button onClick={handleReset} className={classes.button}>
              Kembali
            </Button>
            <Button
                variant="contained"
                color="primary"
                onClick={saveForm}
                style={{ display: "none" }}
                className={classes.button}
              >
                Ajukan
              </Button>
          </div>
        ) : ( */}
          <div>
            {getStepContent(activeStep)}
            <div className="text-right">
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
                className={classes.button}
              >
                Kembali
              </Button>


              {activeStep === steps.length - 1 ? 
               <Button
               variant="contained"
               color="secondary"
               onClick={saveForm}
               disabled={check}
               className={classes.button}
             >
               Simpan
             </Button> : 
              <Button
              variant="contained"
              color="secondary"
              onClick={handleNext}
              className={classes.button}
            >
              Selanjutnya
            </Button>
            }
            </div>
          </div>
         {/* )}  */}
      </div>
    </div>
  );
}
