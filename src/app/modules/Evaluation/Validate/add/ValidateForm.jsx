import React,{ useRef} from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import "../../styles.css";
import FormStepper from "./FormStepper";
// import { DateFormat } from "../../helpers/DateFormat";
import {DateFormat} from "../../../../helpers/DateFormat"

function ValidateForm({  usulan, initValues, saveValidation }) {

  const ValidateSchema = Yup.object().shape({
    jns_peraturan: Yup.string().required("Jenis Peraturan is required"),
    judul_peraturan: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Judul Peraturan is required"),
      no_peraturan: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Nomor Peraturan is required"),
    // unit_incharge: Yup.string()
    //   .min(2, "Minimum 2 symbols")
    //   .max(50, "Maximum 50 symbols")
    //   .required("Unit Incharge is required"),
    // nip_perekam: Yup.string()
    //   .min(2, "Minimum 2 symbols")
    //   .max(9, "Maximum 9 symbols")
    //   .required("NIP PIC is required"),
    // nm_pegawai: Yup.string().required("Nama Pegawai is required"),
    // noPeraturan: Yup.string()
    //   .min(2, "Minimum 2 symbols")
    //   .max(9, "Maximum 9 symbols")
    //   .required("Nomor Peraturan is required"),
    tentang: Yup.string()
      .min(2, "Minimum 2 symbols")
      // .max(9, "Maximum 9 symbols")
      .required("Tentang is required"),
    konten_peraturan: Yup.string()
      .min(2, "Minimum 2 symbols")
      // .max(9, "Maximum 9 symbols")
      .required("Konten Peraturan is required"),
    alasan: Yup.string()
      .min(2, "Minimum 2 symbols")
      // .max(9, "Maximum 9 symbols")
      .required("Alasan is required"),
    analisa_dampak: Yup.string()
      .min(2, "Minimum 2 symbols")
      // .max(9, "Maximum 9 symbols")
      .required("Analisis is required"),
    isu_masalah: Yup.string()
      .min(2, "Minimum 2 symbols")
      // .max(9, "Maximum 9 symbols")
      .required("Isu Masalah is required"),
      // id_usulan: Yup.string()
      file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      ),
  });
  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  const btnRef = useRef();

  
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initValues}
        validationSchema={ValidateSchema}
        onSubmit={values => {
          // console.log(values);
          saveValidation(values);
        }}
      >
        {({ handleSubmit, setFieldValue, isValid, values }) => { 
          const handleChange =()=> {
            setFieldValue('id_usulan',usulan.id_usulan)
          }

          return (
          <>
            <div className="row">
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Tanggal Usulan
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                      {/* {DateFormat(usulan.wkt_create)} 
                       */}
                    {`: ${DateFormat(usulan.wkt_create)}`}

                    </p>
                  </div>
                </div>
              </div>
              <div className="col-xl-6 col-lg-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    No Surat
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                    {`: ${usulan.no_surat}`}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6 col-xl-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Tgl Surat
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                    {`: ${DateFormat(usulan.tgl_surat)}`}
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 col-xl-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Perihal
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                    {`: ${usulan.perihal}`}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6 col-xl-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    Instansi / Unit Kerja
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                    {`: ${usulan.instansi_unit}`}
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 col-xl-6 mb-3">
                <div className="row">
                  <label className="col-xl-3 col-lg-3 col-form-label">
                    PIC
                  </label>
                  <div className="col-lg-9 col-xl-6">
                    <p className="text-muted pt-2">
                    {`: ${usulan.nm_pic}`}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <Form className="form form-label-right">
              <FormStepper
              btnRef={btnRef}
              check={!(isValid)}
              handleChange={handleChange}
              />
               <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
            </Form>
          </>
        )}}
      </Formik>
    </>
  );
}

export default ValidateForm;
