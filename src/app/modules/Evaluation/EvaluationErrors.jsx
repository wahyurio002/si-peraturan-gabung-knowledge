import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../_metronic/_partials/controls";
import {toAbsoluteUrl} from "../../../_metronic/_helpers";


function EvaluationErrors() {
  return (
    <>
      <div className="d-flex flex-column flex-root">
      <div
        className="d-flex flex-row-fluid flex-column bgi-size-cover bgi-position-center bgi-no-repeat p-10 p-sm-30"
        style={{
          backgroundImage: `url(${toAbsoluteUrl("/media/bg/error/403.jpg")})`
        }}
      >
      </div>
    </div>
      {/* <Card>
        <CardHeader
        //   title="Daftar Penelitian Penyusunan Peraturan"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
      
        </CardBody>
      </Card> */}
    </>
  );
}

export default EvaluationErrors;
