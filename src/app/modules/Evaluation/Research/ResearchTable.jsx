/* Library */
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";


/* Helper */
import { Pagination } from "../../../helpers/pagination/Pagination";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";

/* Component */
import ResearchOpen from "./ResearchOpen";

/* Utility */
import { getPenelitianUsulan } from "../Api";

function ResearchTable() {
  const history = useHistory();
  const [research, setResearch] = useState([]);

  const openProposal = id => history.push(`/evaluation/research/${id}/open`);
  const editProposal = id => history.push(`/evaluation/research/${id}/edit`);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "no_surat",
      text: "No Surat",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tgl_surat",
      text: "Tgl Surat",
      sort: true,
      formatter: columnFormatters.SuratDateFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "perihal",
      text: "Perihal",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "instansi_unit",
      text: "Instansi Penerbit",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "file_upload",
      text: "File Surat",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.FileColumnFormatterEvaResearch,
      headerSortingClasses
    },
    {
      dataField: "action",
      text: "Aksi",
      formatter: columnFormatters.ActionsColumnFormatterEvaResearch,
      formatExtraData: {
        openEditDialog: editProposal,
        // openDeleteDialog: deleteAction,
        showProposal: openProposal
      },
      classes: "text-center pr-0",
      headerClasses: "text-center pr-3",
      style: {
        minWidth: "100px"
      }
    }
  ];
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "no_surat",
    pageNumber: 1,
    pageSize: 5
  };
  const { SearchBar } = Search;
  const defaultSorted = [{ dataField: "no_surat", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: research.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };

  useEffect(() => {
    getPenelitianUsulan("Eselon 4").then(({ data }) => {
      data.map(dt => {
        //  if (dt.id_tahapan !== 2) {
        //   setResearch(research => [...research, dt]);
        // }
        return dt.id_tahapan !== 2
          ? setResearch(research => [...research, dt])
          : null;
      });
    });
  }, []);

  const emptyDataMessage = () => { return 'No Data to Display';}
  return (
    <>
      <>
        <PaginationProvider pagination={paginationFactory(pagiOptions)}>
          {({ paginationProps, paginationTableProps }) => {
            return (
              <>
                <ToolkitProvider
                  keyField="no_surat"
                  data={research}
                  columns={columns}
                  search
                >
                  {props => (
                    <div>
                      <div className="row">
                        <div className="col-lg-6 col-xl-6 mb-3">
                          <SearchBar
                            {...props.searchProps}
                            style={{ width: "500px" }}
                          />
                          <br />
                        </div>
                      </div>
                      <BootstrapTable
                        {...props.baseProps}
                        wrapperClasses="table-responsive"
                        bordered={false}
                        headerWrapperClasses="thead-light"
                        classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                        defaultSorted={defaultSorted}
                        bootstrap4
                        noDataIndication={emptyDataMessage}
                        {...paginationTableProps}
                      ></BootstrapTable>
                      <Pagination paginationProps={paginationProps} />
                    </div>
                  )}
                </ToolkitProvider>
              </>
            );
          }}
        </PaginationProvider>
      </>

      <Route path="/evaluation/research/:id/open">
        {({ history, match }) => (
          <ResearchOpen
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/evaluation/research");
            }}
            onRef={() => {
              history.push("/evaluation/research");
            }}
          />
        )}
      </Route>
    </>
  );
}

export default ResearchTable;
