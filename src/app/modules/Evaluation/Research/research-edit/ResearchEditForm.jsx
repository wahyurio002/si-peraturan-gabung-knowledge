import React from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import "../../styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select
} from "../../helpers";
import CustomFileInput from "../../helpers/CustomFileInput";

function ResearchEditForm({ proposal, btnRef, saveProposal, setDisabled }) {
  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    noSurat: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("No Surat is required"),
    tglSurat: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat is required"),
    perihal: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Perihal is required"),
    jenisPajak: Yup.string().required("Jenis Pajak is required"),
    jenisKantor: Yup.string().required("Jenis Kantor is required"),
    instansi: Yup.string().required("Instansi is required"),
    alamat: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Alamat is required"),
    noTelp: Yup.number().required("No Telp PIC is required"),
    namaPic: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Nama PIC is required"),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      )
  });

  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  const checkOfficeType = val => {
    switch (val) {
      case "1":
        return (
          <>
            <div className="form-group row">
              <Field
                name="instansi"
                component={Input}
                placeholder="Instansi"
                label="Instansi"
              />
            </div>
            <div className="form-group row">
              <Field
                name="alamat"
                component={Textarea}
                placeholder="Alamat"
                label="Alamat"
              />
            </div>
          </>
        );
        break;
      case "2":
        return (
          <>
            <div className="form-group row">
              <Select name="instansi" label="Unit Kerja">
                {/* {ProductStatusTitles.map((status, index) => (
                    <option key={status} value={index}>
                      {status}
                    </option>
                  ))} */}
                <option>Pilih Unit Kerja</option>
                <option value="1">KPP A</option>
                <option value="2">KPP B</option>
                <option value="3">KPP C</option>
              </Select>
            </div>
          </>
        );
        break;
      default:
        return "";
        break;
    }
  };

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        validationSchema={ProposalEditSchema}
        onSubmit={values => {
          console.log(values);
          // saveApps(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          errors,
          touched,
          values,
          isValid
        }) => (
          <>
            <Form className="form form-label-right">
              {/* FIELD NO SURAT */}
              <div className="form-group row">
                <Field
                  name="noSurat"
                  component={Input}
                  placeholder="No Surat"
                  label="No Surat"
                />
              </div>
              {/* FIELD TANGGAL SURAT */}
              <div className="form-group row">
                <DatePickerField name="tglSurat" label="Tanggal Surat" />
              </div>
              {/* FIELD PERIHAL */}
              <div className="form-group row">
                <Field
                  name="perihal"
                  component={Textarea}
                  placeholder="Perihal"
                  label="Perihal"
                />
              </div>
              {/* FIELD JENIS PAJAK */}
              <div className="form-group row">
                <Select name="jenisPajak" label="Jenis Pajak">
                  {/* {ProductStatusTitles.map((status, index) => (
                    <option key={status} value={index}>
                      {status}
                    </option>
                  ))} */}
                  <option>Pilih Jenis Pajak</option>
                  <option value="1">PPN</option>
                  <option value="2">PPH 21</option>
                  <option value="3">PPH 23</option>
                  <option value="4">PPH 25</option>
                </Select>
              </div>
              {/* FIELD JENIS INSTANSI */}
              <div className="form-group row">
                <Select name="jenisKantor" label="Jenis Instansi">
                  {/* {ProductStatusTitles.map((status, index) => (
                    <option key={status} value={index}>
                      {status}
                    </option>
                  ))} */}
                  <option>Pilih Jenis Instansi</option>
                  <option value="1">Asosiasi / KL</option>
                  <option value="2">Unit DJP</option>
                </Select>
              </div>

              {values.jenisKantor ? (
                <>
                  {" "}
                  {/* FIELD INSTANSI */}
                  {checkOfficeType(values.jenisKantor)}
                  {/* FIELD NO TELPON */}
                  <div className="form-group row">
                    <Field
                      name="noTelp"
                      component={Input}
                      placeholder="No Telp PIC"
                      label="No Telp PIC"
                    />
                  </div>
                  {/* FIELD NAMA PIC */}
                  <div className="form-group row">
                    <Field
                      name="namaPic"
                      component={Input}
                      placeholder="Nama PIC "
                      label="Nama PIC / Penandatangan Surat"
                    />
                  </div>
                  {/* FIELD UPLOAD FILE */}
                  <div className="form-group row">
                    <label className="col-xl-3 col-lg-3 col-form-label">
                      Upload File
                    </label>
                    <div className="col-lg-9 col-xl-6">
                      <Field
                        name="file"
                        component={CustomFileInput}
                        title="Select a file"
                        label="File"
                        // setFieldValue={setFieldValue}
                        // errorMessage={errors["file"] ? errors["file"] : undefined}
                        // touched={touched["file"]}
                        style={{ display: "flex" }}
                        // onBlur={handleBlur}
                      />
                    </div>
                  </div>
                </>
              ) : null}
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
              {isValid ? setDisabled(false) : setDisabled(true)}
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}

export default ResearchEditForm;
