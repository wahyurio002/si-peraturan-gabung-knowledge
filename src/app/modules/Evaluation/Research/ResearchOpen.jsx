import React,{useEffect, useState} from "react";
import { Modal } from "react-bootstrap";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";
import { getUsulanById, updateStatusUsulan } from "../Api";
import ResearchReject from "./ResearchReject";
import { DateFormat } from "../../../helpers/DateFormat";


function ResearchOpen({ id, show, onHide }) {

  const history = useHistory();
  const [data, setData] = useState([]);
  const [isReject, setIsReject] = useState(false);
  const [isShow, setIsShow] = useState(true);

  useEffect(() => {
    if(id){
      getUsulanById(id).then(({data})=> {
        setData(data)
      })
    }
  }, [id])


  const acceptAction = () =>  {
    updateStatusUsulan(id, 6).then(({status})=> {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Data berhasil disimpan", "success").then(
          () => {
            history.push("/dashboard");
            history.replace("/evaluation/research");
          }
        );
      } else {
        swal("Gagal", "Data gagal disimpan", "error").then(() => {
          history.push("/dashboard");
          history.replace("/evaluation/research");
        });
      }
    })
  }

  const handleReject = () => {
    setIsReject(true);
    setIsShow(false);
  }

  const handleCancel = () => {
    setIsReject(false);
    setIsShow(true);
  }

  const reject = (val) => {
    updateStatusUsulan(data.id_usulan, 5 ,val.alasan_penolakan)
    .then(({status})=> {
      if (status === 201 || status === 200) {
        swal("Berhasil", "Data berhasil disimpan", "success").then(
          () => {
            history.push("/dashboard");
            history.replace("/evaluation/research");
          }
        );
      } else {
        swal("Gagal", "Data gagal disimpan", "error").then(() => {
          history.push("/dashboard");
          history.replace("/evaluation/research");
        });
      }
    })
  }


  return (
    <Modal
      size="md"
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header style={{ borderBottom: "none", alignSelf: "center" }}>
        <Modal.Title id="contained-modal-title-vcenter">
          Detil Usulan Evaluasi
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Nomor Surat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
            {`: ${data.no_surat}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Tgl Surat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">
            {`: ${DateFormat(data.tgl_surat)}`}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Perihal</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">{`: ${data.perihal}`}</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Instansi Penerbit/ Unit Kerja</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">{`: ${data.instansi_unit}`}</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Alamat</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">{`: ${data.alamat}`}</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Nama PIC</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">{`: ${data.nm_pic}`}</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="font-weight-bold mr-2">Telp PIC</span>
          </div>
          <div className="col-lg-6 col-xl-6 mb-3" >
            <span className="text-muted text-hover-primary">{`: ${data.no_pic}`}</span>
          </div>
        </div>
        {isReject ?  
       <ResearchReject
       handleCancel={handleCancel}
       reject={reject}
        /> : "" }
       
      </Modal.Body>
      {isShow ? <Modal.Footer style={{ borderTop: "none" }}>
        <div className="col-lg-12" style={{ textAlign: "center" }}>
          <button
            type="button"
            // onClick={backAction}
            // onClick={rejectAction}
            onClick={handleReject}
            className="btn btn-danger"
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="flaticon2-cancel icon-nm"></i>
            Tolak
          </button>
          {`  `}
          <button
            type="submit"
            className="btn btn-success ml-2"
            // onClick={saveForm}
            onClick={acceptAction}
            style={{
              boxShadow: "0px 8px 15px rgb(55 104 165 / 30%)"
            }}
          >
            <i className="fas fa-check"></i>
            Setuju
          </button>
        </div>
      </Modal.Footer> :  ""}
      
    </Modal>
  );
}

export default ResearchOpen;
