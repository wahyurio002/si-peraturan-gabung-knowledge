import React from "react";
import {
  Card,
  CardBody,
  CardHeader
} from "../../../../_metronic/_partials/controls";
import ProposalTable from "./ResearchTable";

function Research() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Penelitian Usulan Evaluasi Regulasi"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
            <ProposalTable />
        </CardBody>
      </Card>
    </>
  );
}

export default Research;
