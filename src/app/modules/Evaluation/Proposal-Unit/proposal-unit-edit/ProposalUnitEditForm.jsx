import React, {useState, useEffect} from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import "./styles.css";
import {
  DatePickerField,
  Input,
  Textarea,
  Select
} from "../proposal-unit-edit/helpers";
import CustomFileInput from "./helpers/CustomFileInput";
import {getJenisPajak} from "../../Api";

function ProposalUnitEditForm({ proposal, btnRef, saveProposal, setDisabled }) {
  const [jenisPajak, setJenisPajak] = useState([]);


  // Validation schema
  const ProposalEditSchema = Yup.object().shape({
    tgl_surat: Yup.mixed()
      .nullable(false)
      .required("Tanggal Surat is required"),
    perihal: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Perihal is required"),
    id_jnspajak: Yup.string().required("Jenis Pajak is required"),
    no_pic: Yup.number().required("No Telp PIC is required"),
    nm_pic: Yup.string()
      .min(2, "Minimum 2 symbols")
      .max(50, "Maximum 50 symbols")
      .required("Nama PIC is required"),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      )
  });

  const FILE_SIZE = 5000000;
  const SUPPORTED_FORMATS = ["application/pdf"];

  useEffect(() => {
    getJenisPajak().then(({ data }) => {
      setJenisPajak(data);
    });
  }, []);

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={proposal}
        validationSchema={ProposalEditSchema}
        onSubmit={values => {
          // console.log(values);
          saveProposal(values);
        }}
      >
        {({
          handleSubmit,
          setFieldValue,
          handleBlur,
          errors,
          touched,
          values,
          isValid
        }) => (
          <>
            <Form className="form form-label-right">
              {/* FIELD TANGGAL SURAT */}
              <div className="form-group row">
                <DatePickerField name="tgl_surat" label="Tanggal Surat" />
              </div>
              {/* FIELD PERIHAL */}
              <div className="form-group row">
                <Field
                  name="perihal"
                  component={Textarea}
                  placeholder="Perihal"
                  label="Perihal"
                />
              </div>
              {/* FIELD JENIS PAJAK */}
              <div className="form-group row">
                <Select name="id_jnspajak" label="Jenis Pajak">
                <option>Pilih Jenis Pajak</option>
                  {jenisPajak.map(data => (
                    <option key={data.nm_jnspajak} value={data.id_jnspajak}>
                      {data.nm_jnspajak}
                    </option>
                  ))}
                </Select>
              </div>
              {/* FIELD NO TELPON */}
              <div className="form-group row">
                <Field
                  name="no_pic"
                  component={Input}
                  placeholder="No Telp PIC"
                  label="No Telp PIC"
                />
              </div>
              {/* FIELD NAMA PIC */}
              <div className="form-group row">
                <Field
                  name="nm_pic"
                  component={Input}
                  placeholder="Nama PIC "
                  label="Nama PIC / Penandatangan Surat"
                />
              </div>
              {/* FIELD UPLOAD FILE */}
              <div className="form-group row">
                <label className="col-xl-3 col-lg-3 col-form-label">
                  Upload File
                </label>
                <div className="col-lg-9 col-xl-6">
                  <Field
                    name="file"
                    component={CustomFileInput}
                    title="Select a file"
                    label="File"
                    // setFieldValue={setFieldValue}
                    // errorMessage={errors["file"] ? errors["file"] : undefined}
                    // touched={touched["file"]}
                    style={{ display: "flex" }}
                    // onBlur={handleBlur}
                  />
                </div>
              </div>
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
              {isValid ? setDisabled(false) : setDisabled(true)}
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}

export default ProposalUnitEditForm;
