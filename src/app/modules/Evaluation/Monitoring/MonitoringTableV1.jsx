// import BootstrapTable from "react-bootstrap-table-next";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import React, { useEffect, useState } from "react";
import axios from "axios";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../column-formatters";
import { useHistory } from "react-router-dom";
import { Route } from "react-router-dom";
import MonitoringOpen from "./MonitoringOpen";

import "../../../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { getValidasiUsulan } from "../Api";

function MonitoringTable() {
  const history = useHistory();
  const [monitoring, setMonitoring] = useState([]);

  const openProposal = id => history.push(`/evaluation/monitoring/${id}/open`);

  useEffect(() => {
    getValidasiUsulan().then(({ data }) => {
      data.map(data => {
        if (data.status === "Terima") {
          setMonitoring(monitoring => [...monitoring, data]);
        }
      });
    });
  }, [setMonitoring]);


  const formatter = (cell, row) => {
    return row.usulan.jns_pajak;
  };

  const idFormatter = (cell, row, enumObject, index) => {
    return <div>{index + 1}</div>;
  };
  return (
    <>
      <>
        <BootstrapTable data={monitoring} bordered version="4">
          <TableHeaderColumn
            row="0"
            thStyle={{ whiteSpace: "normal", textAlign: "center" }}
            tdStyle={{ whiteSpace: "normal", textAlign: "center" }}
            rowSpan="2"
            dataFormat={idFormatter}
            dataField="any"
          >
            No
          </TableHeaderColumn>
          <TableHeaderColumn
            row="0"
            thStyle={{ whiteSpace: "normal", textAlign: "center" }}
            tdStyle={{ whiteSpace: "normal", textAlign: "center" }}
            rowSpan="2"
            isKey
            dataFormat={formatter}
            dataField="usulan"
          >
            Jenis Pajak
          </TableHeaderColumn>
          <TableHeaderColumn
            row="0"
            thStyle={{ whiteSpace: "normal", textAlign: "center" }}
            tdStyle={{ whiteSpace: "normal", textAlign: "center" }}
            rowSpan="2"
            dataField="isu_masalah"
          >
            Isu Masalah
          </TableHeaderColumn>
          <TableHeaderColumn
            row="0"
            thStyle={{ whiteSpace: "normal", textAlign: "center" }}
            tdStyle={{ whiteSpace: "normal", textAlign: "center" }}
            colSpan="2"
          >
            Peraturan/Penegasan
          </TableHeaderColumn>
          <TableHeaderColumn
            row="1"
            thStyle={{ whiteSpace: "normal", textAlign: "center" }}
            tdStyle={{ whiteSpace: "normal", textAlign: "center" }}
            dataField="no_peraturan"
          >
            Nomor Peraturan / Penegasan
          </TableHeaderColumn>
          <TableHeaderColumn
            row="1"
            thStyle={{ whiteSpace: "normal", textAlign: "center" }}
            tdStyle={{ whiteSpace: "normal", textAlign: "center" }}
            dataField="tentang"
          >
            Tentang
          </TableHeaderColumn>
          <TableHeaderColumn
            row="0"
            thStyle={{ whiteSpace: "normal", textAlign: "center" }}
            tdStyle={{ whiteSpace: "normal", textAlign: "center" }}
            colSpan="2"
          >
            Usulan Penyusunan/Perubahan/Penggantian/Peraturan/Penegasan
          </TableHeaderColumn>
          <TableHeaderColumn
            row="1"
            thStyle={{ whiteSpace: "normal", textAlign: "center" }}
            tdStyle={{ whiteSpace: "normal", textAlign: "center" }}
            dataField="jns_peraturan"
          >
            Jenis Peraturan{" "}
          </TableHeaderColumn>
          <TableHeaderColumn
            row="1"
            thStyle={{ whiteSpace: "normal", textAlign: "center" }}
            tdStyle={{ whiteSpace: "normal", textAlign: "center" }}
            dataField="konten_peraturan"
          >
            Konten Pengaturan
          </TableHeaderColumn>
          <TableHeaderColumn
            row="0"
            thStyle={{ whiteSpace: "normal", textAlign: "center" }}
            tdStyle={{ whiteSpace: "normal", textAlign: "center" }}
            rowSpan="2"
            dataField="alasan"
          >
            Alasan
          </TableHeaderColumn>
          <TableHeaderColumn
            row="0"
            thStyle={{ whiteSpace: "normal", textAlign: "center" }}
            tdStyle={{ whiteSpace: "normal", textAlign: "center" }}
            rowSpan="2"
            dataField="analisa_dampak"
          >
            Analisis Dampak Kebijakan(RIA dan CBA)
          </TableHeaderColumn>
          <TableHeaderColumn
            row="0"
            thStyle={{ whiteSpace: "normal", textAlign: "center" }}
            tdStyle={{ whiteSpace: "normal", textAlign: "center" }}
            rowSpan="2"
            dataField="unit_pengusul"
          >
            Unit Kerja DJP/Asosiasi/KL Pengusul
          </TableHeaderColumn>
        </BootstrapTable>
      </>

      <Route path="/evaluation/monitoring/:id/open">
        {({ history, match }) => (
          <MonitoringOpen
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/evaluation/monitoring");
            }}
            onRef={() => {
              history.push("/evaluation/monitoring");
            }}
          />
        )}
      </Route>

      {/*Route to Add Apps*/}
      {/* <Route path="/evaluation/proposal/add">
            {({ history, match }) => (
                <ProposalEdit
                    show={match != null}
                    onHide={() => {
                       history.push("/evaluation/proposal");
                    }}

                    onRef={() => {
                        history.push("/evaluation/proposal");
                    }}
                />
            )}
        </Route> */}

      {/*Route to Edit Apps*/}
      {/* <Route path="/ads/:id/edit">
            {({ history, match }) => (
                <AdsEdit
                    show={match != null}
                    id={match && match.params.id}
                    onHide={() => {
                        history.push("/ads");
                    }}
                    onRef={() => {
                        history.push("/ads");
                    }}
                />
            )}
        </Route> */}

      {/*Route to Delete Apps*/}
      {/* <Route path="/apps/:id/delete">
            {({ history, match }) => (
                <AppsDeleteDialog
                    show={match != null}
                    id={match && match.params.id}
                    onHide={() => {
                        history.push("/apps");
                    }}
                    onRef={() => {
                        history.push("/monitoring");
                        history.replace("/apps");
                    }}
                />
            )} 
        </Route> */}
    </>
  );
}

export default MonitoringTable;
