// import BootstrapTable from "react-bootstrap-table-next";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import paginationFactory, {
  PaginationProvider
} from "react-bootstrap-table2-paginator";
import { Pagination } from "../../../helpers/pagination/Pagination";
import React, { useEffect, useState } from "react";
import {
  sortCaret,
  headerSortingClasses
} from "../../../../_metronic/_helpers";
import * as columnFormatters from "../../../helpers/column-formatters";
import "../../../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { getValidasiUsulan } from "../Api";

function MonitoringTable() {
  const [monitoring, setMonitoring] = useState([]);

  useEffect(() => {
    getValidasiUsulan().then(({ data }) => {
      data.map(data => {
        if (data.status === "Terima") {
          setMonitoring(monitoring => [...monitoring, data]);
        }
      });
    });
  }, [setMonitoring]);

  const columns = [
    {
      dataField: "any",
      text: "No",
      sort: true,
      formatter: columnFormatters.IdColumnFormatter,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "no_evaluasi",
      text: "No Evaluasi",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "usulan.jns_pajak",
      text: "Jenis Pajak",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "isu_masalah",
      text: "Isu Masalah",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "no_peraturan",
      text: "Nomor Peraturan / Penegasan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "tentang",
      text: "Tentang",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "jns_peraturan",
      text: "Jenis Peraturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "konten_peraturan",
      text: "Konten Pengaturan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "alasan",
      text: "Alasan",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "analisa_dampak",
      text: "Analisis Dampak Kebijakan (RIA dan CBA)",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: "usulan.instansi_unit",
      text: "Unit Kerja DJP/Asosiasi/KL Pengusul",
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    }
    // {
    //   dataField: "action",
    //   text: "Aksi",
    //   formatter: columnFormatters.ValidateActionsColumnFormatter,
    //   formatExtraData: {
    //     openAddDetil: addDetil,
    //     // openDeleteDialog: deleteAction,
    //     showProposal: openProposal,
    //     showReject: rejectProposal,
    //     applyValidation: applyValidation
    //   },
    //   classes: "text-center pr-0",
    //   headerClasses: "text-center pr-3",
    //   style: {
    //     minWidth: "100px",
    //   },
    // },
  ];
  const { SearchBar } = Search;
  const initialFilter = {
    sortOrder: "asc", // asc||desc
    sortField: "id_usulan",
    pageNumber: 1,
    pageSize: 5
  };
  const defaultSorted = [{ dataField: "id_usulan", order: "asc" }];
  const sizePerPageList = [
    { text: "10", value: 10 },
    { text: "25", value: 25 },
    { text: "50", value: 50 }
  ];
  const pagiOptions = {
    custom: true,
    totalSize: monitoring.length,
    sizePerPageList: sizePerPageList,
    sizePerPage: initialFilter.pageSize, //default 10
    page: initialFilter.pageNumber //curent page (default 1),
  };
  const emptyDataMessage = () => { return 'No Data to Display';}

  return (
    <>
      <PaginationProvider pagination={paginationFactory(pagiOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <>
              <ToolkitProvider
                keyField="id_validator"
                data={monitoring}
                columns={columns}
                search
              >
                {props => (
                  <div>
                    <div className="row">
                      <div className="col-lg-6 col-xl-6 mb-3">
                        <SearchBar
                          {...props.searchProps}
                          style={{ width: "500px" }}
                        />
                        <br />
                      </div>
                    </div>
                    <BootstrapTable
                      {...props.baseProps}
                      wrapperClasses="table-responsive"
                      bordered={false}
                      headerWrapperClasses="thead-light"
                      classes="table table-bordered table-condensed table-head-custom table-vertical-center overflow-hidden"
                      defaultSorted={defaultSorted}
                      bootstrap4
                      noDataIndication={emptyDataMessage}
                      {...paginationTableProps}
                    ></BootstrapTable>
                    <Pagination paginationProps={paginationProps} />
                  </div>
                )}
              </ToolkitProvider>
            </>
          );
        }}
      </PaginationProvider>
    </>
  );
}

export default MonitoringTable;
