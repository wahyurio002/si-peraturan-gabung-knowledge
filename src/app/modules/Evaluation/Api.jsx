import axios from "axios";

//API USULAN
export function getUsulan() {
  return axios.get("/usulan/");
}

export function getUsulanById(id) {
  return axios.get(`/usulan/${id}`);
}

export function deleteUsulan(id) {
  return axios.delete(`/usulan/${id}`);
}

export function saveUsulan(
  alamat,
  alasan_tolak,
  file_upload,
  instansi,
  jns_pajak,
  jns_pengusul,
  jns_usul,
  nip_perekam,
  nm_pic,
  no_pic,
  no_surat,
  perihal,
  tgl_surat,
  unit_kerja,
  kd_kantor
) {
  // return axios.post('/api/usulan',{values})
  return axios.post("/usulan/", {
    alamat,
    alasan_tolak,
    file_upload,
    instansi,
    jns_pajak,
    jns_pengusul,
    jns_usul,
    nip_perekam,
    nm_pic,
    no_pic,
    no_surat,
    perihal,
    tgl_surat,
    unit_kerja,
    kd_kantor
  });
}

export function updateUsulan(
  id,
  alamat,
  alasan_tolak,
  file_upload,
  instansi,
  jns_pajak,
  jns_pengusul,
  jns_usul,
  nip_perekam,
  nm_pic,
  no_pic,
  no_surat,
  perihal,
  tgl_surat,
  unit_kerja,
  kd_kantor
) {
  return axios.put(`/usulan/${id}`, {
    alamat,
    alasan_tolak,
    file_upload,
    instansi,
    jns_pajak,
    jns_pengusul,
    jns_usul,
    nip_perekam,
    nm_pic,
    no_pic,
    no_surat,
    perihal,
    tgl_surat,
    unit_kerja,
    kd_kantor
  });
}

export function updateStatusUsulan(id, status, alasan = null) {
  if (alasan) {
    return axios.put(`/usulan/updateusulanstatus/${id}/${status}`, {
      alasan_tolak: alasan
    });
  } else {
    return axios.put(`/usulan/updateusulanstatus/${id}/${status}`, {});
  }
}

export function uploadFile(formData) {
  const config = {
    headers: {
      "Content-type": "multipart/form-data"
    }
  };
  return axios.post("/upload", formData, config);
}

export function getJenisPajak() {
  return axios.get("/jnspajak/");
}

export function getJenisPajakById(id_jnspajak){
  return axios.get(`/jnspajak/${id_jnspajak}`)
}

export function saveJenisPajak(kodekantor, kodeunit, nm_jnspajak, status){
  return axios.post("/jnspajak/",{kodekantor, kodeunit, nm_jnspajak, status})
}

export function updateJenisPajak(id_jnspajak, kodekantor, kodeunit, nm_jnspajak, status){
  return axios.put(`/jnspajak/${id_jnspajak}`,{kodekantor, kodeunit, nm_jnspajak, status})
}

export function deleteJenisPajak(id_jnspajak){
  return axios.delete(`/jnspajak/${id_jnspajak}`)
}

export function getMasterJenis(){
  return axios.get("/masterjenis/")
}

export function saveMasterJenis(nama, keterangan){
  return axios.post("/masterjenis/",{nama, keterangan})
}

export function deleteMasterJenis(id){
  return axios.delete(`/masterjenis/${id}`)
}

export function getDetilJenis(){
  return axios.get("/detiljenis/")
}

export function getDetilJenisById(id){
  return axios.get(`/detiljenis/${id}`)
}

export function saveDetilJenis(id_jenis, keterangan, nama){
  return axios.post(`/detiljenis/`,{id_jenis, keterangan, nama})
}

export function deleteDetilJenis(id){
  return axios.delete(`/detiljenis/${id}`)
}

//API PENELITIAN
export function getPenelitianUsulan(status) {
  return axios.get(`/usulan/bystatus/${status}`);
}

// API PERENCANAAN
export function getPerencanaan() {
  return axios.get("/perencanaan/");
}

//API UNIT KERJA

export function getUnitKerja() {
  return axios.get("/unitkerja/");
}

export function getLogUsulan(id) {
  return axios.get(`/logstatus/findByUsulan/${id}`);
}

//API VALIDASI
export function getValidasiUsulan() {
  // return axios.get('/usulan/bystatus/Terima')
  return axios.get("/validator/");
}

export function getJenisPeraturan() {
  return axios.get("/jnsperaturan/");
}

export function getValidasiById(id) {
  return axios.get(`/validator/${id}`);
}

export function saveValidasi(
  id_usulan,
  id_validator,
  alasan,
  analisa_dampak,
  file_upload,
  isu_masalah,
  jns_peraturan,
  judul_peraturan,
  konten_peraturan,
  nip_perekam,
  tentang,
  unit_incharge,
  unit_pengusul,
  nm_pegawai,
  no_peraturan,
  kd_kantor
) {
  return axios.put(`/validator/${id_validator}`, {
    id_usulan,
    alasan,
    analisa_dampak,
    file_upload,
    isu_masalah,
    jns_peraturan,
    judul_peraturan,
    konten_peraturan,
    nip_perekam,
    tentang,
    unit_incharge,
    unit_pengusul,
    nm_pegawai,
    no_peraturan,
    kd_kantor
  });
}

export function updateStatusValidasi(id, status, kd_kantor, alasan = null) {
  if (alasan) {
    return axios.put(
      `/validator/updatevalidatorstatus/${id}/${status}/${kd_kantor}`,
      {
        alasan_tolak: alasan
      }
    );
  } else {
    return axios.put(
      `/validator/updatevalidatorstatus/${id}/${status}/${kd_kantor}`,
      {}
    );
  }
}

export function checkToken(token) {
  return axios.post(
    `/iam/getwhoami`,
    {},
    { headers: { Authorization: token } }
  );
}

export function savePerencanaan(
  alasan,
  analisa_dampak,
  file_kajian,
  id_usulan,
  id_validator,
  isu_masalah,
  jns_pajak,
  jns_peraturan,
  judul_peraturan,
  konten_peraturan,
  nip_perekam,
  no_evaluasi,
  no_peraturan,
  simfoni,
  tentang,
  tgl_perencanaan,
  unit_incharge
) {
  return axios.post("/perencanaan/", {
    alasan,
    analisa_dampak,
    file_kajian,
    id_usulan,
    id_validator,
    isu_masalah,
    jns_pajak,
    jns_peraturan,
    judul_peraturan,
    konten_peraturan,
    nip_perekam,
    no_evaluasi,
    no_peraturan,
    simfoni,
    tentang,
    tgl_perencanaan,
    unit_incharge
  });
}

export function getPerencanaanById(id) {
  return axios.get(`/perencanaan/${id}`);
}

export function deletePerencanaan(id_perencanaan) {
  return axios.delete(`/perencanaan/${id_perencanaan}`);
}

export function updatePerencanaan(
  id,
  alasan,
  analisa_dampak,
  file_kajian,
  id_usulan,
  id_validator,
  isu_masalah,
  jns_pajak,
  jns_peraturan,
  judul_peraturan,
  konten_peraturan,
  nip_perekam,
  no_evaluasi,
  no_peraturan,
  simfoni,
  tentang,
  tgl_perencanaan,
  unit_incharge
) {
  return axios.put(`/perencanaan/${id}`, {
    alasan,
    analisa_dampak,
    file_kajian,
    id_usulan,
    id_validator,
    isu_masalah,
    jns_pajak,
    jns_peraturan,
    judul_peraturan,
    konten_peraturan,
    nip_perekam,
    no_evaluasi,
    no_peraturan,
    simfoni,
    tentang,
    tgl_perencanaan,
    unit_incharge
  });
}

export function applyPerencanaan(id, kd_kantor) {
  return axios.put(
    `/perencanaan/updateperencanaanstatus/${id}/2/${kd_kantor}`,
    {}
  );
}

export function updateStatusPerencanaan(id, status, kd_kantor, alasan = null) {
  if (alasan) {
    return axios.put(
      `/perencanaan/updateperencanaanstatus/${id}/${status}/${kd_kantor}`,
      {
        alasan_tolak: alasan
      }
    );
  } else {
    return axios.put(
      `/perencanaan/updateperencanaanstatus/${id}/${status}/${kd_kantor}`,
      {}
    );
  }
}

export function getLogValidasi(id) {
  return axios.get(`/logstatus/findByValidator/${id}`);
}
export function getLogPerencanaan(id) {
  return axios.get(`/logstatus/findByPerencanaan/${id}`);
}

/* API PENYUSUNAN */

export function getPenyusunan() {
  return axios.get("/penyusunan/");
}

export function getPenyusunanById(id) {
  return axios.get(`/penyusunan/${id}`);
}

export function getPenyusunanByStatus(status) {
  return axios.get(`/penyusunan/bystatus/${status}`);
}

export function deletePenyusunan(id_penyusunan) {
  return axios.put(`/penyusunan/${id_penyusunan}`,{delete: 'yes'});
}

export function savePenyusunan(
  id_perencanaan,
  jns_peraturan,
  judul_peraturan,
  kd_kantor,
  no_penyusunan,
  no_peraturan,
  no_perencanan,
  simfoni,
  tentang,
  tgl_penyusunan,
  unit_incharge,
  update_file_kajian
) {
  // return axios.post('/api/usulan',{values})
  return axios.post("/penyusunan/", {
    id_perencanaan,
    jns_peraturan,
    judul_peraturan,
    kd_kantor,
    no_penyusunan,
    no_peraturan,
    no_perencanan,
    simfoni,
    tentang,
    tgl_penyusunan,
    unit_incharge,
    update_file_kajian
  });
}

export function updatePenyusunan(
  id,
  id_perencanaan,
  jns_peraturan,
  judul_peraturan,
  kd_kantor,
  no_penyusunan,
  no_peraturan,
  no_perencanan,
  simfoni,
  tentang,
  tgl_penyusunan,
  unit_incharge,
  update_file_kajian
) {
  return axios.put(`/penyusunan/${id}`, {
    id_perencanaan,
    jns_peraturan,
    judul_peraturan,
    kd_kantor,
    no_penyusunan,
    no_peraturan,
    no_perencanan,
    simfoni,
    tentang,
    tgl_penyusunan,
    unit_incharge,
    update_file_kajian
  });
}
export function updatePenyusunanKajianUnit(id, file_kajian_unit) {
  return axios.put(`/penyusunan/${id}`, {
    file_kajian_unit
  });
}
export function updatePenyusunanKajianSahli(id, file_kajian_sahli) {
  return axios.put(`/penyusunan/${id}`, {
    file_kajian_sahli
  });
}
export function updatePenyusunanKajianDirjen(id, file_kajian_direktur) {
  return axios.put(`/penyusunan/${id}`, {
    file_kajian_direktur
  });
}

export function updateStatusPenyusunan(id, status, kd_kantor, alasan = null) {
  if (alasan) {
    return axios.put(
      `/penyusunan/updatepenyusunanstatus/${id}/${status}/${kd_kantor}`,
      {
        alasan_tolak: alasan
      }
    );
  } else {
    return axios.put(
      `/penyusunan/updatepenyusunanstatus/${id}/${status}/${kd_kantor}`,
      {alasan_tolak: ''}
    );
  }
}

export function getLogPenyusunan(id) {
  return axios.get(`/logstatus/findByPenyusunan/${id}`);
}

/* API PENYUSUNAN - Surat Undangan */

export function getSurat(id) {
  return axios.get(`/surat/byidpenyusunan/${id}`);
}

export function getJenisSurat(id_jenis) {
  return axios.get(`/detiljenis/byidjenis/${id_jenis}`);
}

export function getJenisSuratById(id) {
  return axios.get(`/detiljenis/${id}`);
}

export function getSuratByIdSurat(id_surat) {
  return axios.get(`/surat/${id_surat}`);
}

export function deleteSuratMenyurat(id_surat){
  return axios.delete(`/surat/${id_surat}`);
}

export function saveSuratUndangan(
  id_penyusunan,
  nomor_surat,
  perihal,
  tgl_surat,
  unit_penerbit,
  unit_penerima,
  id_jenis,
  jenis_instansi,
  status
) {
  return axios.post("/surat/", {
    id_penyusunan,
    nomor_surat,
    perihal,
    tgl_surat,
    unit_penerbit,
    unit_penerima,
    id_jenis,
    jenis_instansi,
    status
  });
}

export function updateSuratUndangan(
  id,
  id_penyusunan,
  nomor_surat,
  perihal,
  tgl_surat,
  unit_penerbit,
  unit_penerima,
  id_jenis,
  jenis_instansi,
  status
) {
  return axios.put(`/surat/${id}`, {
    id_penyusunan,
    nomor_surat,
    perihal,
    tgl_surat,
    unit_penerbit,
    unit_penerima,
    id_jenis,
    jenis_instansi,
    status
  });
}

/* API PENYUSUNAN - LHR Pembahasan */

export function getLhr(id) {
  return axios.get(`/lhr/findByPenyusunan/${id}`);
}

export function getLhrById(id) {
  return axios.get(`/lhr/${id}`);
}

export function getLhrDetilByIdDetil(id_detil) {
  return axios.get(`/lhrdetil/${id_detil}`);
}

export function getLhrDetil(id_lhr) {
  return axios.get(`/lhrdetil/byidLhr/${id_lhr}`);
}

export function deleteLhr(id_lhr){
  return axios.delete(`/lhr/${id_lhr}`);
}

export function deleteLhrDetil(id_lhr_detil){
  return axios.delete(`/lhrdetil/${id_lhr_detil}`)
}

export function saveLhr(
  id_penyusunan,
  id_surat,
  no_surat_lhr,
  tgl_rapat,
  tmpt_bahas,
  unit_penyelenggara,
  file_lhr,
  jenis_instansi,
  status
) {
  return axios.post("/lhr/", {
    id_penyusunan,
    id_surat,
    no_surat_lhr,
    tgl_rapat,
    tmpt_bahas,
    unit_penyelenggara,
    file_lhr,
    jenis_instansi,
    status
  });
}

export function updateLhr(
  id,
  id_penyusunan,
  id_surat,
  no_surat_lhr,
  tgl_rapat,
  tmpt_bahas,
  unit_penyelenggara,
  file_lhr,
  jenis_instansi,
  status
) {
  return axios.put(`/lhr/${id}`, {
    id_penyusunan,
    id_surat,
    no_surat_lhr,
    tgl_rapat,
    tmpt_bahas,
    unit_penyelenggara,
    file_lhr,
    jenis_instansi,
    status
  });
}

export function saveLhrDetil(id_lhr, jenis, pokok_pengaturan) {
  return axios.post(`/lhrdetil/`, {
    id_lhr,
    jenis,
    pokok_pengaturan
  });
}

export function updateLhrDetil(id_detil, id_lhr, jenis, pokok_pengaturan) {
  return axios.put(`/lhrdetil/${id_detil}`, {
    id_lhr,
    jenis,
    pokok_pengaturan
  });
}

/* API PENYUSUNAN - Peraturan Terkait */

export function getPeraturanTerkait(id) {
  return axios.get(`/peraturan/byid_penyusunan/${id}`);
}

export function getPeraturanTerkaitByIdPeraturan(id) {
  return axios.get(`/peraturan/${id}`);
}

export function deletePeraturanTerkait(id_perter){
  return axios.delete(`/peraturan/${id_perter}`)
}

export function savePeraturanTerkait(
  id_penyusunan,
  nomor_peraturan,
  perihal,
  tgl_ditetapkan,
  tgl_diundangkan,
  status
) {
  return axios.post("/peraturan/", {
    id_penyusunan,
    nomor_peraturan,
    perihal,
    tgl_ditetapkan,
    tgl_diundangkan,
    status
  });
}

export function updatePeraturanTerkait(
  id,
  id_penyusunan,
  nomor_peraturan,
  perihal,
  tgl_ditetapkan,
  tgl_diundangkan,
  status
) {
  return axios.put(`/peraturan/${id}`, {
    id_penyusunan,
    nomor_peraturan,
    perihal,
    tgl_ditetapkan,
    tgl_diundangkan,
    status
  });
}

/* API PENYUSUNAN - DRAFTING */

export function getDraftComposingById(id) {
  return axios.get(`/draftperaturan/byidpenyusunan/${id}`);
}
// export function getDraftComposingById(id) {
//    axios.get(`/draftperaturan/byidpenyusunan/${id}`,{transformResponse: (res)=> {return res;}, responseType: 'json'});
// }

export function getDraft(id_draft) {
  return axios.get(`/draftperaturan/${id_draft}`);
}

export function saveDraftComposing(
  body_draft,
  jns_draft,
  id_penyusunan,
  fileupload,
  filelampiranr
) {
  return axios.post("/draftperaturan/", {
    body_draft,
    jns_draft,
    id_penyusunan,
    fileupload,
    filelampiranr
  });
}
export function saveDraftComposingCosign(
  body_cosign,
  jns_draft,
  id_penyusunan,
  fileupload_cosign,
  filelampiran_cosign
) {
  return axios.post("/draftperaturan/", {
    body_cosign,
    jns_draft,
    id_penyusunan,
    fileupload_cosign,
    filelampiran_cosign
  });
}
export function saveDraftComposingSahli(
  body_sahli,
  jns_draft,
  id_penyusunan,
  fieupload_sahli,
  filelampiran_sahli
) {
  return axios.post("/draftperaturan/", {
    body_sahli,
    jns_draft,
    id_penyusunan,
    fieupload_sahli,
    filelampiran_sahli
  });
}

export function saveDraftComposingDirjen(
  body_direktur,
  jns_draft,
  id_penyusunan,
  fileupload_direktur,
  filelampiran_direktur
) {
  return axios.post("/draftperaturan/", {
    body_direktur,
    jns_draft,
    id_penyusunan,
    fileupload_direktur,
    filelampiran_direktur
  });
}

export function updateDraftComposing(
  body_draft,
  id_draft,
  fileupload,
  filelampiranr,
  keterangan = 1
) {
  return axios.put(`/draftperaturan/${id_draft}/${keterangan}`, {
    body_draft,
    fileupload,
    filelampiranr
  });
}
export function updateDraftComposingCosign(
  body_cosign,
  id_draft,
  fileupload_cosign,
  filelampiran_cosign,
  keterangan = 2
) {
  return axios.put(`/draftperaturan/${id_draft}/${keterangan}`, {
    body_cosign,
    fileupload_cosign,
    filelampiran_cosign
  });
}
export function updateDraftComposingSahli(
  body_sahli,
  id_draft,
  fieupload_sahli,
  filelampiran_sahli,
  keterangan = 3
) {
  return axios.put(`/draftperaturan/${id_draft}/${keterangan}`, {
    body_sahli,
    fieupload_sahli,
    filelampiran_sahli
  });
}

export function updateDraftComposingDirjen(
  body_direktur,
  id_draft,
  fileupload_direktur,
  filelampiran_direktur,
  keterangan = 4
) {
  return axios.put(`/draftperaturan/${id_draft}/${keterangan}`, {
    body_direktur,
    fileupload_direktur,
    filelampiran_direktur
  });
}
/* API PENYUSUNAN - PROCESS */

export function getMintaJawab(id_penyusunan) {
  return axios.get(`/mintajawab/byidpenyusunan/${id_penyusunan}`);
}

export function getMintaJawabByIdMintaJawab(id_mintajawab) {
  return axios.get(`/mintajawab/${id_mintajawab}`);
}

export function deleteMintaJawab(id_mintajawab) {
  return axios.delete(`/mintajawab/${id_mintajawab}`);
}

export function saveMintaJawab(
  file_permintaan,
  id_penyusunan,
  jenis,
  nomor_surat,
  perihal,
  tgl_surat,
  tujuan,
  jenis_instansi
) {
  return axios.post(`/mintajawab/`, {
    file_permintaan,
    id_penyusunan,
    jenis,
    nomor_surat,
    perihal,
    tgl_surat,
    tujuan,
    jenis_instansi
  });
}

export function updateMintaJawab(
  id_mintajawab,
  file_permintaan,
  id_penyusunan,
  jenis,
  nomor_surat,
  perihal,
  tgl_surat,
  tujuan,
  jenis_instansi
) {
  return axios.put(`/mintajawab/${id_mintajawab}`, {
    file_permintaan,
    id_penyusunan,
    jenis,
    nomor_surat,
    perihal,
    tgl_surat,
    tujuan,
    jenis_instansi
  });
}

export function getCosignUnit(id_penyusunan) {
  return axios.get(`/cosignunit/byidpenyusunan/${id_penyusunan}`);
}

export function getCosignUnitByIdCosign(id_cosign) {
  return axios.get(`/cosignunit/${id_cosign}`);
}

export function deleteCosignUnit(id_cosign){
  return axios.delete(`/cosignunit/${id_cosign}`);
}

export function saveCosignUnit(
  file_permintaan,
  id_minta_jawab,
  id_penyusunan,
  nama_unit,
  no_surat,
  tgl_surat
) {
  return axios.post(`/cosignunit/`, {
    file_permintaan,
    id_minta_jawab,
    id_penyusunan,
    nama_unit,
    no_surat,
    tgl_surat
  });
}

export function updateCosignUnit(
  id_cosign,
  file_permintaan,
  id_minta_jawab,
  id_penyusunan,
  nama_unit,
  no_surat,
  tgl_surat
) {
  return axios.put(`/cosignunit/${id_cosign}`, {
    file_permintaan,
    id_minta_jawab,
    id_penyusunan,
    nama_unit,
    no_surat,
    tgl_surat
  });
}

export function getCosignKementerian(id_penyusunan) {
  return axios.get(`/cosignkl/byidpenyusunan/${id_penyusunan}`);
}

export function getCosignKementerianByIdCosign(id_cosign) {
  return axios.get(`/cosignkl/${id_cosign}`);
}

export function deleteCosignKementerian(id_cosign){
  return axios.delete(`/cosignkl/${id_cosign}`);
}

export function saveCosignKementerian(
  file_cosign,
  id_minta_jawab,
  id_penyusunan,
  nama_unit,
  no_surat,
  tgl_surat
) {
  return axios.post(`/cosignkl/`, {
    file_cosign,
    id_minta_jawab,
    id_penyusunan,
    nama_unit,
    no_surat,
    tgl_surat
  });
}

export function updateCosignKementerian(
  id_cosign,
  file_cosign,
  id_minta_jawab,
  id_penyusunan,
  nama_unit,
  no_surat,
  tgl_surat
) {
  return axios.put(`/cosignkl/${id_cosign}`, {
    file_cosign,
    id_minta_jawab,
    id_penyusunan,
    nama_unit,
    no_surat,
    tgl_surat
  });
}

export function getAnalisisHarmon(id) {
  return axios.get(`/analisisharmon/byidpenyusunan/${id}`);
}
export function getAnalisisHarmonByIdHarmon(id) {
  return axios.get(`/analisisharmon/${id}`);
}

export function saveAnalisisHarmon(
  id_penyusunan,
  draft,
  jawab1,
  jawab2,
  jawab3,
  jawab4,
  jawab5
) {
  return axios.post("/analisisharmon/", {
    id_penyusunan,
    draft,
    jawab1,
    jawab2,
    jawab3,
    jawab4,
    jawab5
  });
}

export function updateAnalisisHarmon(
  id_analisisharmon,
  id_penyusunan,
  draft,
  jawab1,
  jawab2,
  jawab3,
  jawab4,
  jawab5
) {
  return axios.put(`/analisisharmon/${id_analisisharmon}`, {
    id_penyusunan,
    draft,
    jawab1,
    jawab2,
    jawab3,
    jawab4,
    jawab5
  });
}

export function getCosignSahli(id_penyusunan) {
  return axios.get(`/cosignsahli/byidpenyusunan/${id_penyusunan}`);
}

export function getCosignSahliByIdCosign(id_cosign_sahli) {
  return axios.get(`/cosignsahli/${id_cosign_sahli}`);
}

export function saveCosignSahli(
  id_penyusunan,
  id_minta_jawab,
  no_surat,
  tgl_surat,
  nama_unit,
  file_permintaan
) {
  return axios.post("/cosignsahli/", {
    id_penyusunan,
    id_minta_jawab,
    no_surat,
    tgl_surat,
    nama_unit,
    file_permintaan
  });
}

export function updateCosignSahli(
  id_penyusunan,
  id_cosign_sahli,
  id_minta_jawab,
  no_surat,
  tgl_surat,
  nama_unit,
  file_permintaan
) {
  return axios.put(`/cosignsahli/${id_cosign_sahli}`, {
    id_penyusunan,
    id_minta_jawab,
    no_surat,
    tgl_surat,
    nama_unit,
    file_permintaan
  });
}


export function getRegulasiPerpajakanTerima(){
  return axios.get('/kmregulasiperpajakan/terima')
}

export function saveDraftReal(
  body_draft,
  jns_draft,
  id_penyusunan,
  // fileupload,
  // filelampiranr
) {
  return axios.post("/draftperaturan/", {
    body_draft,
    jns_draft,
    id_penyusunan,
    // fileupload,
    // filelampiranr
  });
}