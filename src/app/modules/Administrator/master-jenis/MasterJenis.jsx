import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import MasterJenisTable from "./MasterJenisTable";

function MasterJenis() {
  return (
    <>
      <Card>
        <CardHeader
          title="Daftar Master Jenis"
          style={{ backgroundColor: "#FFC91B" }}
        ></CardHeader>
        <CardBody>
            <MasterJenisTable />
        </CardBody>
      </Card>
    </>
  );
}

//#a6c8e6
export default MasterJenis;
