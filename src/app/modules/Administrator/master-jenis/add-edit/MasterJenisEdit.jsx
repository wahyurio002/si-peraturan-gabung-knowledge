/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import MasterJenisEditForm from "./MasterJenisEditForm";
import MasterJenisEditFooter from "./MasterJenisEditFooter";
import { saveMasterJenis } from "../../../Evaluation/Api";

function MasterJenisEdit({
  history,
  match: {
    params: { id }
  }
}) {
  const initValues = {
    nama: "",
    keterangan: ""
  };

  // Subheader
  const subheader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);

  useEffect(() => {
    let _title = id ? "Edit Master Jenis" : "Tambah Master Jenis";

    setTitle(_title);
    subheader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id, subheader]);
  const btnRef = useRef();
  const backAction = () => {
    history.push("/admin/master-jenis");
  };
  const saveForm = values => {
    try {
      saveMasterJenis(values.nama, values.keterangan).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/admin/master-jenis");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/admin/master-jenis/add");
          });
        }
      });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <MasterJenisEditForm
            actionsLoading={actionsLoading}
            proposal={initValues}
            btnRef={btnRef}
            saveForm={saveForm}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <MasterJenisEditFooter backAction={backAction} btnRef={btnRef} />
      </CardFooter>
    </Card>
  );
}

export default MasterJenisEdit;
