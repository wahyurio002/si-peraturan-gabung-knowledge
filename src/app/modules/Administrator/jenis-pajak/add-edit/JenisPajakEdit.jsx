/* Library */
import React, { useEffect, useState, useRef } from "react";
import swal from "sweetalert";

/* Helper */
import { useSubheader } from "../../../../../_metronic/layout";
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter
} from "../../../../../_metronic/_partials/controls";
import JenisPajakEditFooter from "./JenisPajakEditFooter";
import JenisPajakEditForm from "./JenisPajakEditForm";

/* Utility */
import {
  getJenisPajakById,
  saveJenisPajak,
  updateJenisPajak
} from "../../../Evaluation/Api";

function JenisPajakEdit({
  history,
  match: {
    params: { id }
  }
}) {
  const initValues = {
    nm_jnspajak: "",
    kodekantor: "",
    kodeunit: "",
    status: ""
  };

  // Subheader
  const subheader = useSubheader();

  const [title, setTitle] = useState("");
  const [actionsLoading] = useState(true);
  const [content, setContent] = useState();

  useEffect(() => {
    let _title = id ? "Edit Jenis Pajak" : "Tambah Jenis Pajak";

    setTitle(_title);
    subheader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps

    if (id) {
      getJenisPajakById(id).then(({ data }) => {
        setContent(data);
      });
    }
  }, [id, subheader]);
  const btnRef = useRef();

  const saveForm = values => {
    if (!id) {
      saveJenisPajak(
        values.kodekantor,
        values.kodeunit,
        values.nm_jnspajak,
        values.status
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/admin/jenis-pajak");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/admin/jenis-pajak/add");
          });
        }
      });
    } else {
      updateJenisPajak(
        id,
        values.kodekantor,
        values.kodeunit,
        values.nm_jnspajak,
        values.status
      ).then(({ status }) => {
        if (status === 201 || status === 200) {
          swal("Berhasil", "Data berhasil disimpan", "success").then(() => {
            history.push("/admin/jenis-pajak");
          });
        } else {
          swal("Gagal", "Data gagal disimpan", "error").then(() => {
            history.push("/admin/jenis-pajak/add");
          });
        }
      });
    }
  };
  const backAction = () => {
    history.push("/admin/jenis-pajak");
  };

  return (
    <Card>
      <CardHeader
        title={title}
        style={{ backgroundColor: "#FFC91B" }}
      ></CardHeader>
      <CardBody>
        <div className="mt-5">
          <JenisPajakEditForm
            actionsLoading={actionsLoading}
            proposal={content || initValues}
            btnRef={btnRef}
            saveForm={saveForm}
          />
        </div>
      </CardBody>
      <CardFooter style={{ borderTop: "none" }}>
        <JenisPajakEditFooter backAction={backAction} btnRef={btnRef} />
      </CardFooter>
    </Card>
  );
}

export default JenisPajakEdit;
