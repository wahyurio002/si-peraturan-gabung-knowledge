import React from "react";
import { Switch } from "react-router-dom";
import { ContentRoute } from "../../../_metronic/layout";
import DetilJenisEdit from "./detil-jenis/add-edit/DetilJenisEdit";
import DetilJenis from "./detil-jenis/DetilJenis";
import JenisPajakEdit from "./jenis-pajak/add-edit/JenisPajakEdit";
import JenisPajak from "./jenis-pajak/JenisPajak";
import MasterJenisEdit from "./master-jenis/add-edit/MasterJenisEdit";
import MasterJenis from "./master-jenis/MasterJenis";

export default function Evaluation() {
  return (
    <Switch>
      {/* <Redirect
          exact={true}
          from="/evaluation"
          to="/evaluation/proposal"
        /> */}
      {/* Usulan */}
      {/* <ContentRoute
          from="/evaluation/proposal"
          component={Proposal}
        /> */}

      <ContentRoute path="/admin/jenis-pajak/:id/edit" component={JenisPajakEdit} />
      <ContentRoute path="/admin/jenis-pajak/add" component={JenisPajakEdit} />
      {/* <ContentRoute
          path="/evaluation/proposal/:id/edit"
          component={ProposalEdit}
        /> */}
      <ContentRoute path="/admin/jenis-pajak" component={JenisPajak} />
      <ContentRoute
        path="/admin/master-jenis/add"
        component={MasterJenisEdit}
      />
      <ContentRoute path="/admin/master-jenis" component={MasterJenis} />


      <ContentRoute path="/admin/detil-jenis/:id/edit" component={DetilJenisEdit} />
      <ContentRoute path="/admin/detil-jenis/add" component={DetilJenisEdit} />
      <ContentRoute path="/admin/detil-jenis" component={DetilJenis} />

      {/* <ContentRoute path="/evaluation/unit/new" component={ProposalUnitEdit} />
      <ContentRoute path="/evaluation/unit" component={ProposalUnit} />
      <ContentRoute path="/evaluation/research" component={Research} />
      <ContentRoute
          path="/evaluation/validate/:id/add"
          component={ValidateAdd}
        />
      <ContentRoute path="/evaluation/validate" component={Validate} />
      <ContentRoute path="/evaluation/revalidate" component={ValidateResearch} />
      <ContentRoute path="/evaluation/monitoring" component={Monitoring} /> */}

      {/* Layout */}
      {/* <ContentRoute from="/google-material/layout" component={LayoutPage} /> */}
    </Switch>
  );
}
