import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import * as auth from "../_redux/authRedux";
import { getRole, getUserByToken, login } from "../_redux/authCrud";
import Card from "react-bootstrap/esm/Card";
import {toAbsoluteUrl} from "../../../../_metronic/_helpers"
/*
  INTL (i18n) docs:
  https://github.com/formatjs/react-intl/blob/master/docs/Components.md#formattedmessage
*/

/*
  Formik+YUP:
  https://jaredpalmer.com/formik/docs/tutorial#getfieldprops
*/

const initialValues = {
  username: "heri.kuswanto",
  password: "Pajak123",
};

function Login(props) {
  const { intl } = props;
  const [loading, setLoading] = useState(false);
  const LoginSchema = Yup.object().shape({
    username: Yup.string()
      .min(3, "Minimum 3 symbols")
      .max(50, "Maximum 50 symbols")
      .required(
        intl.formatMessage({
          id: "AUTH.VALIDATION.REQUIRED_FIELD",
        })
      ),
    password: Yup.string()
      .min(3, "Minimum 3 symbols")
      .max(50, "Maximum 50 symbols")
      .required(
        intl.formatMessage({
          id: "AUTH.VALIDATION.REQUIRED_FIELD",
        })
      ),
  });

  const enableLoading = () => {
    setLoading(true);
  };

  const disableLoading = () => {
    setLoading(false);
  };

  const getInputClasses = (fieldname) => {
    if (formik.touched[fieldname] && formik.errors[fieldname]) {
      return "is-invalid";
    }

    if (formik.touched[fieldname] && !formik.errors[fieldname]) {
      return "is-valid";
    }

    return "";
  };

  const formik = useFormik({
    initialValues,
    validationSchema: LoginSchema,
    onSubmit: (values, { setStatus, setSubmitting }) => {
      enableLoading();
      setTimeout(() => {
        login(values.username, values.password)
          .then(({ data: { token } }) => {
            disableLoading();
            props.login(token);
            getUserByToken(token).then(({data})=> {
              props.fulfillUser(data.data);
            })
            getRole(token).then(({data})=> {
              // console.log(data)
              props.setRole(data)
            })

          })
          .catch(() => {
            disableLoading();
            setSubmitting(false);
            setStatus(
              intl.formatMessage({
                id: "AUTH.VALIDATION.INVALID_LOGIN",
              })
            );
          });
      }, 1000);
    },
  });

  return (
    <div className="login-form login-signin" id="kt_login_signin_form">
    {/* begin::Head */}
    <Card style={{borderRadius:'2.5rem'}}>
    <div className="text-center mb-1 mb-lg-10" style={{marginTop:"2.4rem"}}>
      <img
          src={toAbsoluteUrl("/media/logos/DJP.png")}
          alt="DJP logo"
      />

    </div>
      <div className="text-center mb-2 mb-lg-80">
        <h3 className="font-size-h1">
          PERATURAN
        </h3>

      </div>

      <Card.Body>

    {/* end::Head */}

    {/*begin::Form*/}
    <form
      onSubmit={formik.handleSubmit}
      className="form fv-plugins-bootstrap fv-plugins-framework"
    >
      {formik.status ? (
        <div className="mb-10 alert alert-custom alert-light-danger alert-dismissible">
          <div className="alert-text font-weight-bold">{formik.status}</div>
        </div>
      ) : (
        <div >
        </div>
      )}

      <div className="form-group fv-plugins-icon-container">
        <input
          placeholder="Username"
          type="text"
          className={`form-control form-control-solid h-auto py-5 px-6 ${getInputClasses(
            "username"
          )}`}
          name="username"
          {...formik.getFieldProps("username")}
        />
        {formik.touched.username && formik.errors.username ? (
          <div className="fv-plugins-message-container">
            <div className="fv-help-block">{formik.errors.username}</div>
          </div>
        ) : null}
      </div>
      <div className="form-group fv-plugins-icon-container">
        <input
          placeholder="Password"
          type="password"
          autoComplete="on"
          className={`form-control form-control-solid h-auto py-5 px-6 ${getInputClasses(
            "password"
          )}`}
          name="password"
          {...formik.getFieldProps("password")}
        />
        {formik.touched.password && formik.errors.password ? (
          <div className="fv-plugins-message-container">
            <div className="fv-help-block">{formik.errors.password}</div>
          </div>
        ) : null}
      </div>
      <div className="text-center">

        <button
          id="kt_login_signin_submit"
          type="submit"
          disabled={formik.isSubmitting}
          className={`btn btn-primary font-weight-bold px-9 py-4 my-3`}
          style={{backgroundColor: "#263787", borderColor: "#263787"}}
        >
          <span>LOGIN</span>
          {loading && <span className="ml-3 spinner spinner-white"></span>}
        </button>
      </div>
    </form>
      </Card.Body>

    </Card>
      <div className="text-dark-50 order-2 order-sm-1 my-2" style={{textAlign: 'center' }}>
          Copyright &copy; 2021 Direktorat Jenderal Pajak
      </div>
  </div>

  );
}

export default injectIntl(connect(null, auth.actions)(Login));
