import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { put, takeLatest } from "redux-saga/effects";
import { getUserByToken } from "./authCrud";

export const actionTypes = {
  Login: "[Login] Action",
  Logout: "[Logout] Action",
  Register: "[Register] Action",
  UserRequested: "[Request User] Action",
  UserLoaded: "[Load User] Auth API",
  SetUser: "[Set User] Action",
  SetRole: "[Get Role] Action",
  SetStatus: "[Set Status] Action",
  setDraftUsulan: "[Set Draft Usulan] Action",
  setDraftCosign: "[Set Draft Cosign] Action"
};

const initialAuthState = {
  user: undefined,
  token: undefined,
  role: undefined,
  status: undefined,
  draftUsulan: undefined,
  draftCosign: undefined
  // user: {
  //   "id": 39601,
  //   "pegawaiId": "571284f4-a0aa-49e5-b35d-cb9f41f16494",
  //   "namaPegawai": "MOH FAISAL SHOLEHUDIN",
  //   "nip9": "917330342",
  //   "nip18": "199607022018011003",
  //   "nik": "6371030207960006",
  //   "pangkatId": "375ba71e-c4ab-4750-8c6b-91d86e6e6bf6",
  //   "pangkat": "Pengatur/IIc",
  //   "jabatanId": "ebdab5f1-913e-4a6c-8bf1-d9d06fdda022",
  //   "jabatan": "Pelaksana",
  //   "unitId": "0db574cb-cd30-4221-95ad-7f15907d3b86",
  //   "unit": "Seksi Pengembangan Sistem Pendukung Manajemen",
  //   "unitLegacyKode": "0429030300",
  //   "kantorId": "52a5589e-cfa9-4913-8e2c-4e20f773b5d1",
  //   "kantor": "Direktorat Teknologi Informasi dan Komunikasi",
  //   "kantorLegacyKode": "4020000000",
  //   "agamaId": "e4edfdd5-aeb3-41e8-8e0e-6ae328e7189d",
  //   "agama": "Islam",
  //   "jenisKelaminId": "da1cd184-7b89-4fdf-aaf0-6755a358846a",
  //   "jenisKelamin": "Laki-laki",
  //   "alamatJalan": "JL. JAFRI ZAM-ZAM KOMP. GRAWIRATAMA 4",
  //   "kelurahanId": "028c0a0a-0b7f-47bb-ae2c-a77e58b35d5b",
  //   "kelurahan": "BELITUNG SELATAN",
  //   "kecamatanId": "a91d9cda-6c21-4c43-b21a-433ab14bad82",
  //   "kecamatan": "BANJARMASIN BARAT",
  //   "kabupatenId": "fa58ccd3-9dbc-471c-bd1d-b1b79529b66c",
  //   "kabupaten": "KOTA BANJARMASIN",
  //   "provinsiId": "844891e5-ec6a-4f26-8235-c49030a5ef1e",
  //   "provinsi": "KALIMANTAN SELATAN",
  //   "tanggalLahir": "1996-07-02T00:00:00+07:00",
  //   "tempatLahir": "KEDIRI",
  //   "pensiun": false
  // },
  // token: 'sfasdfasdfsadfsdfs',
};

export const reducer = persistReducer(
  { storage, key: "si-peraturan", whitelist: ["token", "user","role","status"] },
  (state = initialAuthState, action) => {
    switch (action.type) {
      case actionTypes.Login: {
        const { token } = action.payload;

        return { token, user: undefined };
      }

      case actionTypes.Register: {
        const { token } = action.payload;

        return { token, user: undefined };
      }

      case actionTypes.Logout: {
        // TODO: Change this code. Actions in reducer aren't allowed.
        return initialAuthState;
      }

      case actionTypes.UserLoaded: {
        const { user } = action.payload;
        return { ...state, user };
      }

      case actionTypes.SetUser: {
        const { user } = action.payload;
        return { ...state, user };
      }

      case actionTypes.SetRole: {
        const {role} = action.payload;
        return {...state, role};
      }

      case actionTypes.SetStatus: {
        const {status} = action.payload;
        return {...state, status};
      }
      case actionTypes.setDraftUsulan: {
        const {draftUsulan} = action.payload;
        return {...state, draftUsulan};
      }
      case actionTypes.setDraftCosign: {
        const {draftCosign} = action.payload;
        return {...state, draftCosign};
      }

      default:
        return state;
    }
  }
);


export const actions = {
  login: (token) => ({ type: actionTypes.Login, payload: { token } }),
  register: (token) => ({
    type: actionTypes.Register,
    payload: { token },
  }),
  logout: () => ({ type: actionTypes.Logout }),
  requestUser: (user) => ({
    type: actionTypes.UserRequested,
    payload: { user },
  }),
  fulfillUser: (user) => ({ type: actionTypes.UserLoaded, payload: { user } }),
  setUser: (user) => ({ type: actionTypes.SetUser, payload: { user } }),
  setRole: (role) => ({type: actionTypes.SetRole, payload: {role}}),
  setStatus: (status) => ({type: actionTypes.SetStatus, payload: {status}}),
  setDraftUsulan: (draftUsulan) => ({type: actionTypes.setDraftUsulan, payload: {draftUsulan}}),
  setDraftCosign: (draftCosign) => ({type: actionTypes.setDraftCosign, payload: {draftCosign}}),
};

export function* saga() {
  // yield takeLatest(actionTypes.Login, function* loginSaga() {
  //   yield put(actions.requestUser());
  // });

  // yield takeLatest(actionTypes.Register, function* registerSaga() {
  //   yield put(actions.requestUser());
  // });

  // yield takeLatest(actionTypes.UserRequested, function* userRequested() {
  //   const { data: user } = yield getUserByToken(initialAuthState.token);

  //   yield put(actions.fulfillUser(user));
  // });
}
