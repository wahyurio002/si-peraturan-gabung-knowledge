import axios from "axios";

// export const LOGIN_URL = "api/auth/login";
export const LOGIN_URL = "/iam/auth"
export const REGISTER_URL = "api/auth/register";
export const REQUEST_PASSWORD_URL = "api/auth/forgot-password";

export const ME_URL = "/iam/getwhoami";
export const GET_ROLE = "/iam/getrole";

export function login(username, password) {
  return axios.post(LOGIN_URL, { username, password });
}

export function register(email, fullname, username, password) {
  return axios.post(REGISTER_URL, { email, fullname, username, password });
}

export function requestPassword(email) {
  return axios.post(REQUEST_PASSWORD_URL, { email });
}

export function getUserByToken(token) {

  const options ={ headers: { 'Authorization' : token}}
  // Authorization head should be fulfilled in interceptor.
  return axios.post(ME_URL,{},options);
}

export function getRole(token){
  const options = { headers: { 'Authorization' : token}}

  return axios.post(GET_ROLE,{},options);
}
